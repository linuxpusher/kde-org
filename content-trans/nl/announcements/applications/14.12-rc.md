---
aliases:
- ../announce-applications-14.12-rc
custom_spread_install: true
date: '2014-11-27'
description: KDE stelt Applications 14.12 Release Candidate beschikbaar.
layout: application
title: KDE stelt de Release Candidate van KDE Applicaties 14.12 beschikbaar
---
27 november 2014. Vandaag heeft KDE de "release candidate" van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Met verschillende toepassingen gebaseerd op KDE Frameworks 5, heeft de KDE Applications 4.12 uitgave grondig testen nodig om de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritisch in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de "release candidate" te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.

#### Installeren van KDE Applications 14.12 Release Candidate binaire pakketten

<em>Pakketten</em>. Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van KDE Applications 14.12 Release Candidate (intern 14.11.97) voor sommige versies van hun distributie en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan. Extra binaire pakketten, evenals updates voor de pakketten zijn nu beschikbaar of zullen beschikbaar komen in de komende weken.

<em>Pakketlocaties</em>. Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki van de gemeenschap</a>.

#### KDE Applications 14.12 Release Candidate compileren

De complete broncode voor KDE Applications 14.12 Release Candidate kan <a href='http://download.kde.org/unstable/applications/14.11.97/src/'>vrij gedownload</a> worden. Instructies over compileren en installeren zijn beschikbaar vanaf de <a href='/info/applications/applications-14.11.97'>Informatiepagina van KDE Applications Release Candidate</a>.
