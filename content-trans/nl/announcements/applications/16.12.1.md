---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE stelt KDE Applicaties 16.12.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.12.1 beschikbaar
version: 16.12.1
---
12 januari 2017. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../16.12.0'>KDE Applicaties 16.12</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Deze uitgave repareert een bug over GEGEVENSVERLIES in de iCal-hulpbron die bij aanmaken van std.ics mislukt als deze niet bestaat.

Meer dan 40 aangegeven reparaties van bugs, inclusief verbeteringen aan kdepim, gwenview, kajongg, okular, kate, kdenlive, naast andere.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.28.
