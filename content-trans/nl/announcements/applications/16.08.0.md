---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE stelt KDE Applicaties 16.08.0 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.08.0 beschikbaar
version: 16.08.0
---
18 augustus 2016. KDE introduceert vandaag KDE Applications 16.08 met een indrukwekkende hoeveelheid opwaarderingen wanneer het gaat om gemakkelijke toegang, de introductie van zeer nuttige functionaliteiten en reparatie van enige kleinere problemen die KDE Applications nu een stap dichter brengt bij het bieden van de perfecte setup voor uw apparaat.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a>, en KDiskFree zijn nu overgezet naar KDE Frameworks 5 en we zien uit naar uw terugkoppeling en inzicht in de nieuwste mogelijkheden die met deze uitgave zijn geïntroduceerd.

In de voortdurende inspanning om bibliotheken van de Kontact Suite te splitsen om ze gemakkelijker bruikbaar te maken voor derden, is de tarball kdepimlibs gesplitst in akonadi-contacts, akonadi-mime and akonadi-notes.

We gaan niet door met de volgende pakketten: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu en mplayerthumbs. Dit zal ons helpen te focussen op de rest van de code.

### Behouden in Kontact

<a href='https://userbase.kde.org/Kontact'>De Kontact Suite</a> heeft de gebruikelijke ronde van opschonen, reparaties van bugs en optimalisaties in deze uitgave gekregen. Waard te vermelden is het gebruik van QtWebEngine in verschillende componenten, die zorgen voor een meer moderne HTML rendering. We hebben ook de ondersteuning van VCard4 verbeterd evenals nieuwe plug-ins toegevoegd die kunnen waarschuwen als bepaalde condities zich voordoen bij het verzenden van e-mail, bijv. nagaan dat u het verzenden van e-mails met een gegeven identiteit wilt toestaan, of controleren of u e-mail verzendt als platte tekst, etc.

### Nieuwe versie van Marble

<a href='https://marble.kde.org/'>Marble</a> 2.0 is onderdeel van KDE Applications 16.08 en bevat meer dan 450 wijzigingen in de code wat omvat verbeteringen in navigatie, rendering en een experimentele vectorrendering van OpenStreetMap gegevens.

### Meer archivering

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> kan nu AppImage en .xar bestanden uitpakken evanals de integriteit van zip, 7z en rar archieven controleren. Het kan ook commentaar in rar archieven toevoegen/bewerken

### Verbeteringen aan de terminal

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> heeft verbeteringen ontvangen met betrekking tot opties voor rendering van lettertypen en ondersteuning voor toegankelijkheid.

### En meer!

<a href='https://kate-editor.org'>Kate</a> heeft verplaatsbare tabbladen gekregen. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>Meer informatie...</a>

<a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a> heeft provincie- en regiokaarten van Burkina Faso toegevoegd.

### Agressieve bestrijding van problemen

Meer dan 120 bugs zijn opgelost in toepassingen inclusief de Kontact Suite, Ark, Cantor, Dolphin, KCalc, Kdenlive en meer!

### Volledige log met wijzigingen
