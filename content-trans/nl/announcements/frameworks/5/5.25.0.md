---
aliases:
- ../../kde-frameworks-5.25.0
date: 2016-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Algemeen

- Qt &gt;= 5.5 is nu vereist

### Attica

- HTTP-redirects volgen

### Breeze pictogrammen

- mail bijwerken- 16px pictogrammen om de pictogrammen beter te herkennen
- mic en audio statuspictogrammen bijwerken om dezelfde indeling en grootte te hebben
- Nieuw pictogram voor app van systeeminstellingen
- symbolische statuspictogrammen voor gnome toevoegen
- ondersteuning voor symbolische pictogrammen van gnome 3 toegevoegd
- Pictogrammen voor Diaspora en Vector, zie phabricator.kde.org/M59
- Nieuwe pictogrammen voor Dolphin en Gwenview
- weerpictogrammen zijn statuspictogrammen en geen app-pictogrammen
- enige koppelingen naar xliff dankzij gnastyle
- kig-pictogram toevoegen
- MIME-type pictogrammen, krdc pictogram, andere pictogrammen van toepassingen uit gnastyle
- pictogram voor MIME-type van certificaat toevoegen (bug 365094)
- pictogrammen voor gimp dankzij gnastyle (bug 354370)
- globe-actiepictogram is nu geen gekoppeld bestand; nu in digikam te gebruiken
- labplot pictogrammen van labplot volgens mail 13.07. van Alexander Semke
- App-pictogrammen toevoegen uit gnastyle
- kruler pictogram van Yuri Fabirovsky toevoegen
- gebroken svg-bestanden repareren dankzij fuchs (bug 365035)

### Extra CMake-modules

- Invoegen repareren wanneer er geen Qt5 is
- Een terugvalmethode voor query_qmake() wanneer er geen Qt5 installatie is
- Nagaan dat ECMGeneratePriFile.cmake zich gedraagt zoals de rest van ECM
- Appstreamgegevens hebben hun locatie van voorkeur gewijzigd

### KActivities

- [KActivities-CLI] commando's voor starten en stoppen van een activiteit
- [KActivities-CLI] instellen en ophalen van activiteitnaam, pictogram en beschrijving
- Een CLI toepassing voor besturing van activiteiten toegevoegd
- Scripts toegevoegd om te schakelen naar vorige en volgende activiteiten
- Methode voor invoegen in QFlatSet geeft nu een index terug samen met de iterator (bug 365610)
- ZSH-functies toegevoegd voor stoppen en verwijderen van niet-huidige activiteiten
- isCurrent eigenschap toegevoegd aan KActivities::Info
- Constante iterators gebruiken bij zoeken naar activiteit

### KDE Doxygen hulpmiddelen

- Veel verbeteringen aan formattering van uitvoer
- Mainpage.dox heeft nu een hogere prioriteit dan README.md

### KArchive

- Meerdere gzip streams behandelen (bug 232843)
- Neen aan dat een map een map is, zelfs als het rechtenbit fout is ingesteld (bug 364071)

### KBookmarks

- KBookmarkGroup::moveBookmark: teruggavewaarden repareren wanneer het item al op de juiste positie is

### KConfig

- DeleteFile en RenameFile standaard sneltoetsen toegevoegd

### KConfigWidgets

- DeleteFile en RenameFile standaard acties toegevoegd
- De instellingendialoog heeft nu schuifbalken indien nodig (bug 362234)

### KCoreAddons

- Bekende licenties installeren en ze vinden bij uitvoeren (reparatie van regressie) (bug 353939)

### KDeclarative

- valueChanged echt emitteren

### KFileMetaData

- Op xattr controleren in configuratiestap, anders kan het bouwen mislukken (als xattr.h ontbreekt)

### KGlobalAccel

- klauncher dbus gebruiken in plaats van KRun (bug 366415)
- Jumplist-acties starten via KGlobalAccel
- KGlobalAccel: deadlock repareren bij exit onder Windows

### KHTML

- Eenheid percentage ondersteunen in randstraal
- Voorgedefinieerde versie van eigenschappen van achtergrond en randstraal verwijderen
- Reparaties in 4-waardige shorthand-constructie-function
- Tekenreeksobjecten alleen maken als ze zullen worden gebruikt

### KIconThemes

- De prestaties van makeCacheKey enorm verbeteren, omdat het een kritisch pad is in de code bij zoeken naar een pictogram
- KIconLoader: aantal opzoeken verminderen bij uitvoeren van terugval
- KIconLoader: enorme snelheidsverbetering bij laden van niet beschikbare pictogrammen
- Zoekregel niet wissen bij omschakelen van categorie
- KIconEngine: QIcon::hasThemeIcon repareren die altijd true teruggaf (bug 365130)

### KInit

- KInit aanpassen aan Mac OS X

### KIO

- KIO::linkAs() repareren om te werken zoals gedocumenteerd, d.w.z. mislukken als doel al bestaat
- KIO::put("file:///pad") repareren om de umask te respecteren (bug 359581)
- KIO::pasteActionText repareren voor nul-bestemmingsitem en voor lege URL
- Ondersteuning toegevoegd voor ongedaan maken van een symbolische koppeling
- GUI-optie om globale MarkPartial voor KIO-slaves te configureren
- MaxCacheSize beperking tot 99 KiB repareren
- Klembordknoppen aan tabblad voor controlesom toevoegen
- KNewFileMenu: kopiëren van sjabloonbestand uit ingebedde hulpbron repareren (bug 359581)
- KNewFileMenu: maken van koppeling naar toepassing repareren (bug 363673)
- KNewFileMenu: suggestie van nieuwe bestandsnaam wanneer bestand al bestaat in bureaublad repareren
- KNewFileMenu: nagaan dat fileCreated() wordt uitgestuurd ook voor bureaubladbestand van toepassing
- KNewFileMenu: maken van symbolische koppelingen met een relatief doel repareren
- KPropertiesDialog: gebruik van knopvak vereenvoudigen, gedrag bij Esc repareren
- KProtocolInfo: cache opnieuw vullen om nieuw geïnstalleerde protocollen te vinden
- KIO::CopyJob: overzetten naar qCDebug (met zijn eigen gebied, omdat dit nogal spraakzaam kan zijn)
- KPropertiesDialog: tabblad voor controlesom toevoegen
- Pad van URL wissen alvorens KUrlNavigator te initialiseren

### KItemModels

- KRearrangeColumnsProxyModel: toekennen in index(0, 0) bij leeg model repareren
- KDescendantsProxyModel::setSourceModel() repareren van niet wissen van interne caches
- KRecursiveFilterProxyModel: QSFPM corruptie vanwege uitfilteren van rowsRemoved signaal repareren (bug 349789)
- KExtraColumnsProxyModel: hasChildren() implementeren

### KNotification

- Ouder van sublayout niet handmatig instellen, maakt waarschuwing stil

### Pakket Framework

- De ParentApp erven van de plug-in PackageStructure
- kpackagetool5 laten genereren van appstream-informatie voor kpackage-componenten
- Mogelijk maken het bestand metadata.json te laden uit kpackagetool5

### Kross

- Ongebruikte KF5 afhankelijkheden verwijderen

### KService

- applications.menu: referenties verwijderen naar ongebruikte categorieën
- Altijd de Trader-parser bijwerken uit yacc/lex bronnen

### KTextEditor

- Niet tweemaal vragen naar overschrijven van een bestand met eigen dialogen
- FASTQ syntaxis toegevoegd

### KWayland

- [client] een QPointer gebruiken voor de enteredSurface in Pointer
- Geometry in PlasmaWindowModel laten zien
- Een geometrie-event toevoegen aan Plasma Window
- [src/server] ga na dat oppervlak een resource heeft alvorens pointer enter te verzenden
- Toevoegen van ondersteuning voor xdg-shell
- [server] verzend een selectie voor wissen op de juiste manier alvorens focus naar toetsenbord in te gaan
- [server] geen XDG_RUNTIME_DIR situatie netter behandelen

### KWidgetsAddons

- [KCharSelect] crash bij zoeken zonder aanwezig gegevensbestand repareren (bug 300521)
- [KCharSelect] tekens buiten BMP behandelen (bug 142625)
- [KCharSelect] kcharselect-data naar Unicode 9.0.0 bijwerken (bug 336360)
- KCollapsibleGroupBox: animation stoppen in destructor als deze nog actief is
- Breeze palette bijgewerkt (sync vanuit KColorScheme)

### KWindowSystem

- [xcb] Ga na dat het compositingChanged-signaal wordt uitgezonden als NETEventFilter opnieuw wordt gemaakt (bug 362531)
- Een API voor gemak toegevoegd om het venstersysteem/platform op te vragen dat Qt gebruikt

### KXMLGUI

- Tip voor minimumgrootte repareren (afgebroken tekst) (bug 312667)
- [KToggleToolBarAction] beperkingen van actie/options_show_toolbar honoreren

### NetworkManagerQt

- Standaard naar WPA2-PSK en WPA2-EAP bij verkrijgen van type beveiliging uit instellingen voor verbindingen

### Oxygen-pictogrammen

- toepassingen-menu toevoegen aan oxygen (bug 365629)

### Plasma Framework

- Compatiabel slot behouden in createApplet met Frameworks 5.24
- Gl-textuur niet twee keer verwijderen in miniatuur (bug 365946)
- Vertalingendomein toevoegen aan QML-object voor achtergrondafbeelding
- Geen applets handmatig verwijderen
- kapptemplate voor Plasma Wallpaper toevoegen
- Sjablonen: sjablonen registreren in eigen topniveau categorie "Plasma/"
- Sjablonen: koppelingen naar techbase-wiki bijwerken in README's
- Definieer welke Plasma-pakketstructuren plasmashell uitbreiden
- een grootte ondersteunen voor toevoegen van applets
- Definieer Plasma PackageStructure als reguliere KPackage PackageStructure plug-ins
- Reparatie: achtergrondafbeelding van Autumn config.qml bijwerken naar QtQuick.Controls
- KPackage gebruiken om Plasma pakketten te installeren
- Als we een QIcon als een argument doorgeven aan IconItem::Source, gebruik deze
- Ondersteuning voor een overlay toevoegen aan Plasma IconItem
- Qt::Dialog toevoegen aan standaard vlaggen om make QXcbWindow::isTransient() gelukkig te maken (bug 366278)
- [Breeze Plasma Thema] Pictogrammen voor network-flightmode-on/off toevoegen
- contextualActionsAboutToShow uitzenden voor het tonen van het contextualActions-menu van applet (bug 366294)
- [TextField] koppelen aan TextField lengte in plaats van tekst
- [Button Styles] Horizontaal centreren in modus alleen-pictogrammen (bug 365947)
- [Containment] HiddenStatus als lage status behandelen
- kruler systeemvakpictogram van Yuri Fabirovsky toevoegen
- De beroemde bug 'dialogs show up on the Task Manager' nog eens repareren
- pictogram voor draadloos netwerk beschikbaar met een ? embleem repareren (bug 355490)
- IconItem: een betere benadering om animatie uit te schakelen bij gaan van onzichtbaar naar zichtbaar
- Type tekstballonvenster instellen bij ToolTipDialog via KWindowSystem API

### Solid

- De Predicate parser uit yacc/lex bronnen altijd bijwerken

### Sonnet

- hunspell: code opschonen voor zoeken naar woordenboeken, voeg XDG mappen toe (bug 361409)
- Gebruik van taalfiltergebruik van taaldetectie een beetje proberen te repareren

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
