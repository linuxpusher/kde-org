---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: O KDE Lança as Aplicações do KDE 16.12.0
layout: application
title: O KDE Lança as Aplicações do KDE 16.12.0
version: 16.12.0
---
15 de Dezembro de 2016. O KDE introduz hoje as Aplicações do KDE 16.12, com uma lista impressionante de actualizações no que respeita a uma melhor facilidade de acesso. A introdução de funcionalidades realmente úteis, bem como a eliminação de alguns problemas menores, faz com que as Aplicações do KDE fiquem um passo mais próximo de lhe oferecer a configuração perfeita para o seu dispositivo.

O <a href='https://okular.kde.org/'>Okular</a>, o <a href='https://konqueror.org/'>Konqueror</a>, o <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, o <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, o <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a>, entre outros (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Notas de Lançamento</a>) foram agora migrados para as Plataformas do KDE 5 e estamos à espera da sua reacção e opiniões sobre as funcionalidades mais recentes que foram introduzidas com esta versão.

No esforço continuado para facilitar a criação das aplicações de forma autónoma, separámos os pacotes 'kde-baseapps', 'kdepim' e 'kdewebdev'. Poderá encontrar os novos pacotes criados no <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>documento das Notas de Lançamento</a>

Foram descontinuados os seguintes pacotes: 'kdgantt2', 'gpgmepp' e 'kuser'. Isto ajudará a equipa a focar-se no restante código.

### O editor de som Kwave juntou-se às Aplicações do KDE!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

O <a href='http://kwave.sourceforge.net/'>Kwave</a> é um editor de som, que pode gravar, reproduzir, importar e editar diversos tipos de ficheiros de áudio, incluindo os ficheiros multi-canais. O Kwave inclui alguns módulos para transformar os ficheiros de áudio de diversas formas, apresentando uma vista gráfica com capacidades de ampliação e deslocamento.

### O Mundo como papel de parede

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

O Marble agora inclui tanto um Papel de Parede como um Elemento Gráfico que mostra a hora sobre uma vista de satélite da Terra, com a apresentação do dia/noite em tempo-real. Isto costumava estar disponível para o Plasma 4. Agora foi actualizado para funcionar com o Plasma 5.

Poderá encontrar mais informações no <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>'blog' de Friedrich W. H. Kossebau</a>.

### Ícones emotivos aos montes!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

O KCharSelect ganhou a capacidade de apresentar o bloco de Ícones Emotivos (Emojis) do Unicode (assim como outros blocos de símbolos do SMP).

Também ganhou um menu de Favoritos, para que possa colocar como favoritos todos os seus caracteres mais usados.

### A Matemática é melhor com o Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

O Cantor tem uma nova infra-estrutura para o Julia, dando aos seus utilizadores a capacidade de usar os últimos progressos na computação científica.

Poderá descobrir mais informações no <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>'blog' do Ivan Lakhtanov</a>.

### Arquivos avançados

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

O Ark ganhou várias funcionalidades novas:

- Poderá agora mudar o nome aos ficheiros e pastas, bem como copiá-los ou movê-los, dentro do pacote
- Agora é possível seleccionar os algoritmos de compressão e encriptação ao criar os pacotes
- O Ark consegue abrir agora os ficheiros AR (p.ex. as bibliotecas estáticas \*.a do Linux)

Poderá encontrar mais informações no <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>'blog' do Ragnar Thomsen</a>.

### E ainda há mais!

O Kopete ganhou o suporte para a autenticação SASL do X-OAUTH2 no protocolo Jabber, corrigindo também alguns problemas com o 'plugin' de encriptação OTR.

O Kdenlive tem um novo efeito de Rotoscópio, suporte para conteúdo disponível por transferência e um Detector de Movimento actualizado. Também fornece ficheiros <a href='https://kdenlive.org/download/'>Snap e AppImage</a> para uma instalação mais simples.

O KMail e o Akregator podem usar a Navegação Segura do Google para verificar se uma dada ligação a carregar é maliciosa ou não. Ambos adicionaram também de volta o suporte para a impressão (necessita do Qt 5.8).

### Controlo Agressivo de Problemas

Foram resolvidos mais de 130 erros nas aplicações, incluindo o Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark, Kdenlive, entre outros!

### Registo de Alterações Completo
