---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: O KDE Lança as Aplicações do KDE 15.08.1
layout: application
title: O KDE Lança as Aplicações do KDE 15.08.1
version: 15.08.1
---
15 de Setembro de 2015. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../15.08.0'>Aplicações do KDE 15.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 40 correcções de erros registadas incluem as melhorias nos módulos 'kdelibs' e 'kdepim' e nas aplicações 'kdenlive', 'dolphin', 'kompare', 'konsole', 'ark' e 'umbrello'.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.12.
