---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Módulos Extra do CMake

- Novos argumentos para o ecm_add_tests(). (erro 345797)

### Integração da Plataforma

- Usar a 'initialDirectory' (pasta inicial) correcta para o KDirSelectDialog
- Certificação de que o esquema é indicado ao substituir o valor do URL inicial
- Aceitar apenas pastas existentes no modo FileMode::Directory

### KActivities

(sem registo de alterações fornecido)

### KAuth

- Tornar o KAUTH_HELPER_INSTALL_ABSOLUTE_DIR disponível em todos os utilizadores do KAuth

### KCodecs

- KEmailAddress: Adição de uma invocação alternativa para o 'extractEmailAddress' e o 'firstEmailAddress' que devolve uma mensagem de erro.

### KCompletion

- Correcção de uma selecção indesejada ao editar o nome do ficheiro na janela de ficheiros (erro 344525)

### KConfig

- Correcção de um estoiro se o QWindow::screen() for nulo
- Adição do KConfigGui::setSessionConfig() (erro 346768)

### KCoreAddons

- Nova API de conveniência KPluginLoader::findPluginById()

### KDeclarative

- suporte da criação de um ConfigModule a partir do KPluginMetdata
- correcção dos eventos 'pressAndhold'

### Suporte para a KDELibs 4

- Usar o QTemporaryFile em vez de criar um ficheiro temporário com nome fixo.

### KDocTools

- Actualização das traduções
- Actualização do 'customization/ru'
- Correcção de entidades com ligações inválidas

### KEmoticons

- 'Cache' do tema no 'plugin' de integração

### KGlobalAccel

- [execução] Passagem do código específico da plataforma para 'plugins'

### KIconThemes

- Optimização do KIconEngine::availableSizes()

### KIO

- Não tentar completar os utilizadores e estoirar quando o 'prepend' não é vazio. (erro 346920)
- Usar o KPluginLoader::factory() ao carregar o KIO::DndPopupMenuPlugin
- Correcção de um bloqueio quando utilizar 'proxies' de rede (erro 346214)
- Correcção do KIO::suggestName para preservar as extensões dos ficheiros
- Retirar o 'kbuildsycoca4' quando actualizar o 'sycoca5'.
- KFileWidget: Não aceitar ficheiros no modo apenas para pastas
- KIO::AccessManager: Possibilitar o tratamento de QIODevice sequenciais de forma assíncrona

### KNewStuff

- Adição de um novo método 'fillMenuFromGroupingNames'
- KMoreTools: adição de diversos agrupamentos novos
- KMoreToolsMenuFactory: tratamento do "git-clients-and-actions"
- createMenuFromGroupingNames: tornar o parâmetro 'url' opcional

### KNotification

- Correcção de um estoiro no NotifyByExecute quando não tiver definido nenhum elemento gráfico (erro 348510)
- Melhoria do tratamento de notificações ao fechá-las (erro 342752)
- Substituição do uso do QDesktopWidget pelo QScreen
- Garantir que o KNotification pode ser usado a partir de uma tarefa não-gráfica do sistema

### Plataforma de Pacotes

- Guardar o acesso à estrutura 'qpointer' (erro 347231)

### KPeople

- Usar o QTemporaryFile em vez de fixar o /tmp.

### KPty

- Usar o 'tcgetattr' &amp; 'tcsetattr' se estiverem disponíveis

### Kross

- Correcção do carregamento dos módulos "forms" e "kdetranslation" do Kross

### KService

- Ao executar como 'root' preservar o dono dos ficheiros sobre os ficheiros de 'cache' existentes (erro 342438)
- Guarda sobre a impossibilidade de abrir um determinado 'stream' (erro 342438)
- Correcção da verificação da escrita de um ficheiro com permissões inválidas (erro 342438)
- Correcção da consulta do 'ksycoca' com pseudo-tipos-MIME 'x-scheme-handler/*'. (erro 347353)

### KTextEditor

- Permitir, como na altura do KDE 4.x, que as aplicações/'plugins' de terceiros instalem os seus próprios ficheiros XML de realce de sintaxe na pasta 'katepart5/syntax'
- Adição do KTextEditor::Document::searchText()
- Reactivação do uso do KEncodingFileDialog (erro 343255)

### KTextWidgets

- Adição de um método para limpar um decorador
- Permitir a utilização de um decorador personalizado do Sonnet
- Implementação do "procurar o anterior" no KTextEdit.
- Reactivação do suporte de texto-para-fala

### KWidgetsAddons

- KAssistantDialog: Reactivação do botão de Ajuda, que existia na versão para o KDELibs4

### KXMLGUI

- Adição da gestão de sessões para o KMainWindow (erro 346768)

### NetworkManagerQt

- Eliminação do suporte de WiMAX para o NM 1.2.0+

### Plataforma do Plasma

- Os componentes de calendários conseguem agora mostrar os números das semanas (erro 338195)
- Usar o QtRendering nos tipos de letra dos campos de senhas
- Correcção da pesquisa do AssociatedApplicationManager quando um dado tipo MIME tem (erro 340326)
- Correcção da coloração do fundo do painel (erro 347143)
- Eliminação da mensagem "Não foi possível carregar a 'applet'"
- Capacidade de carregamento de KCM's em QML nas janelas de configuração dos plasmóides
- Não usar o DataEngineStructure nas Applets
- Remoção do máximo de dependências do 'sycoca' na libplasma
- [componentes do Plasma] Fazer com que o SectionScroller siga o ListView.section.criteria
- As barras de posicionamento não se escondem mais automaticamente quando estiver a usar um ecrã por toque (erro 347254)

### Sonnet

- Uso de uma 'cache' central para o SpellerPlugins.
- Redução de alocações temporárias.
- Optimização: Não remover a 'cache' de dicionários ao copiar os objectos do verificador ortográfico.
- Optimização de diversas chamadas save(), fazendo apenas uma única no fim, caso seja necessária.

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
