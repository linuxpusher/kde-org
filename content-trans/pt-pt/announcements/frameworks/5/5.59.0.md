---
aliases:
- ../../kde-frameworks-5.59.0
date: 2019-06-08
layout: framework
libCount: 70
---
### Baloo

- Não tentar indexar exportações de bases de dados SQL
- Exclusão dos ficheiros do .gcode e de máquinas virtuais na consideração de indexação

### BluezQt

- Adição da API do Bluez ao processador/gerador de XML do DBus

### Ícones do Brisa

- gcompris-qt também
- Criação do ícone do Falkon como um verdadeiro SVG
- adição dos ícones em falta das aplicações - a refazer https://bugs.kde.org/show_bug.cgi?id=407527
- adição do ícone do KFourinline da aplicação, também precisa de actualização
- adição do ícone do Kigo https://bugs.kde.org/show_bug.cgi?id=407527
- adição do ícone do KWave a partir do KWave, a refazer no estilo Brisa
- Ligação simbólica do arrow-_-double para o go-_-skip, adição do go-_-skip em 24px
- Alteração dos estilos de ícones dos dispositivos input-*, adição de ícones em 16px
- Adição da versão escura do novo ícone do Knights que passou numa modificação anterior
- Criação de um novo ícone para o Knights com base no ícone do Anjuta (erro 407527)
- adição de ícones para as aplicações que os têm em falta no Brisa; estas deverão ser actualizadas para estarem mais compatíveis com o Brisa, mas são também necessárias para as novas aplicações do kde.org
- ícone do kxstitch a partir do kde:kxstitch - a actualizar
- não tentar incluir tudo e mais alguma coisa
- Validação para confirmação da validade das ScaledDirectories

### Módulos Extra do CMake

- Criação de uma pasta específica para o ficheiro de categorias de registos do Qt
- Não activar o QT_STRICT_ITERATORS no Windows

### Integração da Plataforma

- garantia de que pesquisa também na localização antiga
- pesquisa na nova localização pelos ficheiros KNSRC

### KArchive

- Teste da leitura e posicionamento no KCompressionDevice
- KCompressionDevice: Remoção do bIgnoreData
- KAr: correcção de leitura fora-dos-limites (com dados de entrada inválidos) ao migrar para o QByteArray
- KAr: correcção do processamento de nomes de ficheiros compridos com o Qt-5.10
- KAr: as permissões estão em octal, não em hexadecimal
- KAr::openArchive: Verificação também se o ar_longnamesIndex não é &lt; 0
- KAr::openArchive: Correcção de acesso inválido à memória em ficheiros com problemas
- KAr::openArchive: Protecção contra esgotamento da memória de dados em ficheiros com problemas
- KTar::KTarPrivate::readLonglink: Correcção de estoiro com ficheiros com formato inválido

### KAuth

- Não fixar a pasta de instalação da política do DBus

### KConfigWidgets

- Usar a moeda local para o ícone de doação

### KCoreAddons

- Correcção da compilação nas interfaces em Python (erro 407306)
- Adição do GetProcessList para obter a lista de processos activos de momento

### KDeclarative

- correcção dos ficheiros 'qmldir'

### Suporte para a KDELibs 4

- Remoção do QApplication::setColorSpec (método vazio)

### KFileMetaData

- Mostrar 3 algarismos significativos ao mostrar números de precisão dupla (erro 343273)

### KIO

- Manipulação de 'bytes' em vez de caracteres
- Correcção dos executáveis 'kioslave' que nunca saíam quando se definia o KDE_FORK_SLAVES
- Correcção do atalho 'desktop' para um ficheiro ou pasta (erro 357171)
- Teste do filtro actual antes de definir um novo (erro 407642)
- [kioslave/file] Adição de uma codificação para os nomes de ficheiros antigos (erro 165044)
- Uso do QSysInfo para obter os detalhes do sistema
- Adição dos Documentos à lista de Locais predefinida
- kioslave: preservação do argv[0] para corrigir o applicationDirPath() nos sistemas não-Linux
- Possibilidade de largar um ficheiro ou pasta sobre um KDirOperator (erro 45154)
- Truncar os nomes de ficheiros grandes antes de criar um atalho (erro 342247)

### Kirigami

- [ActionTextField] Tornar consistente a dica em QML
- base na altura dos itens que deveriam ter um preenchimento no topo (erro 405614)
- Performance: compressão das mudanças de cores sem um QTimer
- [FormLayout] Uso de um intervalo superior e inferior para o separador (erro 405614)
- ScrollablePage: Validação de que a página deslocada fica em primeiro plano quando for definido (erro 389510)
- Melhoria do uso apenas do teclado na barra de ferramentas (erro 403711)
- configuração da reciclagem como um FocusScope

### KNotification

- Tratamento das aplicações que definem a propriedade 'desktopFileName' com o sufixo do nome do ficheiro

### KService

- Correcção da validação (hash != 0) quando em algumas vezes um ficheiro é apagado por outro processo
- Correcção de outra validação quando o ficheiro desaparece: ASSERT: "ctime != 0"

### KTextEditor

- Não apaga a linha anterior inteira com o Backspace na posição 0 (erro 408016)
- Uso da verificação de sobreposição da janela nativa
- Adição de acção para repor o tamanho do texto
- mostrar o marcador de mudança de linha estática sempre que for pedido
- Verificação do marcador de início/fim do intervalo realçado após a desdobragem
- Correcção: não reiniciar o realce quando gravar alguns ficheiros (erro 407763)
- Indentação automática: Uso do std::vector em vez do QList
- Correcção: Uso do modo de indentação predefinido para os ficheiros novos (erro 375502)
- remoção da atribuição duplicada
- cumprimento da configuração de parêntesis automáticos para a verificação do balanceamento
- melhoria da verificação de caracteres inválidos ao carregar (erro 406571)
- Novo menu de realces de sintaxe na barra de estado
- Impedimento de ciclo infinito na acção "Comutar os Nós Incluídos"

### KWayland

- Possibilidade de os compositores enviarem valores de eixos discretos (erro 404152)
- Implementação do 'set_window_geometry'
- Implementação do wl_surface::damage_buffer

### KWidgetsAddons

- KNewPasswordDialog: adição de pontos aos elementos de mensagens

### NetworkManagerQt

- Não obter estatísticas do dispositivo na construção

### Plataforma do Plasma

- Mudança do Brisa Claro/Escuro para usar mais cores do sistema
- Exportação da coluna de ordenação do SortFilterModel para o QML
- plasmacore: correcção do 'qmldir' e do 'ToolTip.qml' que não fazem mais parte do módulo
- sinal 'availableScreenRectChanged' para todos os elementos
- Uso apenas do 'configure_file' para gerar os ficheiros do 'plasmacomponents3'
- Actualização do *.qmltypes para a API actual dos módulos QML
- FrameSvg: limpeza também da 'cache' de máscaras no clearCache()
- FrameSvg: possibilidade de o hasElementPrefix() também tratar do prefixo com um '-' final
- FrameSvgPrivate::generateBackground: geração do fundo também se o reqp != p
- FrameSvgItem: emissão do 'maskChanged' também a partir do 'geometryChanged()'
- FrameSvg: impedimento de estoiro ao invocar o mask() sem ter uma 'frame' ainda criada
- FrameSvgItem: emissão do 'maskChanged' sempre a partir do doUpdate()
- documentação da API: nota para o FrameSvg::prefix()/actualPrefix() sobre o '-' final
- documentação da API: referência às versões do Plasma5 na 'techbase' se estiverem disponíveis
- FrameSvg: os contornos l &amp; r, ou o t &amp; b não precisam de ter a mesma altura (ou largura, respectivamente)

### Purpose

- [JobDialog] Também cancelar os sinais quando a janela é fechada pelo utilizador
- Comunicação do cancelamento de uma configuração quando é terminada com um erro (erro 407356)

### QQC2StyleBridge

- Remoção do DefaultListItemBackground e da animação do MenuItem
- [Estilo da Barra do QQC2] Correcção do posicionamento errado das pegas quando o valor inicial é 1 (erro 405471)
- ScrollBar: Possibilidade de usá-la também como uma barra de deslocamento horizontal (erro 390351)

### Solid

- Remodelação da forma como as infra-estruturas de dispositivos são criadas e registadas
- [Fstab] Uso do ícone 'folder-decrypted' para as montagens de Fuse encriptadas

### Realce de Sintaxe

- YAML: comentários apenas a seguir a espaços e outras melhorias/correcções (erro 407060)
- Markdown: uso do 'includeAttrib' nos blocos de código
- correcção do realce do "0" no modo em C
- Tcsh: correcção dos operadores e palavras-chave
- Adição da definição de sintaxe a Common Intermediate Language
- SyntaxHighlighter: Correcção da cor de fundo do texto sem um realce especial erro 406816)
- Adição de aplicação de exemplo para imprimir o texto realçado em PDF)
- Markdown: Uso de uma cor com maior contraste nas listas (erro 405824)
- Remoção da extensão .conf do realce de sintaxe dos "Ficheiros INI", para determinar o sistema de realce com base no tipo MIME (erro 400290)
- Perl: correcção do operador // (erro 407327)
- correcção das maiúsculas/minúsculas dos tipos UInt* no realce de Julia (erro 407611)

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
