---
aliases:
- ../../kde-frameworks-5.33.0
date: 2017-04-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Adição de descrição para os comandos (balooctl)
- Pesquisa também em pastas via ligações simbólicas (erro 333678)

### BluezQt

- Fornecimento do tipo de dispositivos nos dispositivos de Baixo Consumo

### Módulos Extra do CMake

- Definição do 'qml-root-path' como pasta partilhada no prefixo
- Correcção da compatibilidade do 'ecm_generate_pkgconfig_file' com o novo 'cmake'
- Só registar as opções APPLE_* com 'if(APPLE)'

### KActivitiesStats

- Adição de predefinições para a aplicação de testes
- Movimento adequado dos itens para a posição desejada
- Reordenação da sincronização para outras instâncias de modelos
- Se a ordem não for definida, ordena os elementos pelo ID

### Ferramentas de Doxygen do KDE

- [Meta] Mudança do responsável de manutenção no setup.py

### KAuth

- Infra-estrutura para o Mac
- Adição do suporte para matar uma KAuth::ExecuteJob

### KConfig

- Limpeza da lista de atalhos na leitura/escrita do 'kdeglobals'
- Evitar novas alocações inúteis, removendo a chamada 'squeeze' no 'buffer' temporário

### KDBusAddons

- KDBusService: Adição de método de acesso ao nome do serviço D-Bus registado por nós

### KDeclarative

- Com o Qt &gt;= 5.8, usa a nova API para definir a infra-estrutura gráfica das cenas
- Não activar o 'acceptHoverEvents' na DragArea, dado que não são usados

### KDocTools

- meinproc5: ligação para os ficheiros, não para a biblioteca (erro 377406)

### KFileMetaData

- Fazer com que o PlainTextExtractor corresponda de novo ao "text/plain"

### KHTML

- Página de erro a carregar correctamente a imagem (com um URL real)

### KIO

- Fazer com que funcione de novo o URL remoto file:// a ser direccionado de novo para o smb://
- Manter a codificação da pesquisa quando é usado um 'proxy' de HTTP
- Actualização dos agentes de utilizadores (Firefox 52 ESR, Chromium 57)
- Tratamento/Corte do texto de apresentação do URL atribuído à descrição da tarefa. Evita que os URL's 'data:' grandes sejam incluídos nas notificações da GUI
- Adição do KFileWidget::setSelectedUrl() (erro 376365)
- Correcção do modo de gravação do KUrlRequester, adicionando o setAcceptMode

### KItemModels

- Menção ao novo QSFPM::setRecursiveFiltering(true), que torna o KRecursiveFilterProxyModel obsoleto

### KNotification

- Não remover as notificações em espera quando é iniciado o serviço 'fd.o'
- Adaptações para a plataforma Mac

### KParts

- Docs da API: correcção de nota em falta na chamada 'setXMLFile' com o KParts::MainWindow

### KService

- Correcção das mensagens de terminal 'Not found: ""' (não encontrado)

### KTextEditor

- Exposição de funcionalidades internas adicionais da View à API pública
- Gravação de diversas alocações do 'setPen'
- Correcção da ConfigInterface do KTextEditor::Document
- Adição de opções do tipo de letra e ortografia no ConfigInterface

### KWayland

- Adição do suporte para o 'wl_shell_surface::set_popup' e 'popup_done'

### KWidgetsAddons

- Suporte para a compilação com um Qt sem acessibilidade activa
- Correcção de sugestão errada de tamanho quando é invocado o 'animatedShow' com um elemento-pai escondido (erro 377676)
- Correcção dos caracteres no KCharSelectTable a ficar com reticências
- Activação de todos os planos na janela de teste do 'kcharselect'

### NetworkManagerQt

- WiredSetting: devolver 'autonegotiate' mesmo se estiver inactivo
- Impedir que os 'signals' na 'glib2' sejam definidos pelo Qt
- WiredSetting: A velocidade e o duplex só podem ser configurados se a negociação automática estiver desligada (erro 376018)
- O valor de auto-negociação na configuração com fios deverá ser falso

### Plataforma do Plasma

- [ModelContextMenu] Uso do Instantiator em vez do truque 'Repetidor-e-mudança-de-pai'
- [Calendário] O Encolhimento e colocação dos nomes das semanas é feito como na delegação do dia (erro 378020)
- [Item de Ícone] Fazer com que a propriedade "smooth" faça algo de facto
- Definição do tamanho implícito no tamanho da origem para as fontes de URL's 'image/svg'
- adição de uma nova propriedade no contentor para um modo de edição
- correcção do 'maskRequestedPrefix' quando não é usado nenhum prefixo (erro 377893)
- [Menu] Harmonização da colocação do 'openRelative'
- A maioria dos menus (de contexto) têm aceleradores (atalhos Alt+letra) agora (erro 361915)
- Controlos do Plasma baseados no QtQuickControls2
- Tratamento do 'applyPrefixes' com um texto vazio (erro 377441)
- limpeza de facto das 'caches' de temas antigos
- [ContainmentInterface] Despoletar os menus de contexto ao carregar na tecla "Menu"
- [Tema Brisa do Plasma] Melhoria nos ícones de sobreposição de acções (erro 376321)

### Realce de Sintaxe

- TOML: Correcção do realce das sequências de escape dos textos
- Actualização do realce de sintaxe do Clojure
- Algumas actualizações na sintaxe do OCaml
- Realce dos ficheiros *.sbt como código em Scala
- uso também do realce de QML para os ficheiros .qmltypes

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
