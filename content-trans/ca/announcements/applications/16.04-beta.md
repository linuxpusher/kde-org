---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: Es distribueixen les aplicacions 16.04 beta del KDE.
layout: application
release: applications-16.03.80
title: KDE distribueix la beta 16.04 de les aplicacions del KDE
---
24 de març de 2016. Avui KDE distribueix la beta de les noves versions de les aplicacions del KDE. S'han congelat les dependències i les funcionalitats, i ara l'equip del KDE se centra a corregir els errors i acabar de polir-la.

Verifiqueu les <a href='https://community.kde.org/Applications/16.04_Release_Notes'>notes de llançament de la comunitat</a> per a informació quant als arxius tar nous, els quals estan basats en els KF5 i quant als problemes coneguts. Hi haurà un anunci més complet disponible per al llançament final.

Amb diverses aplicacions basades en els Frameworks 5 del KDE, la distribució 16.04 de les aplicacions del KDE necessita una prova exhaustiva per tal de mantenir i millorar la qualitat i l'experiència d'usuari. Els usuaris reals són imprescindibles per mantenir l'alta qualitat del KDE, perquè els desenvolupadors no poden provar totes les configuracions possibles. Comptem amb vós per ajudar a trobar errors amb anticipació, a fi que es puguin solucionar abans de la publicació final. Considereu unir-vos a l'equip instal·lant la beta <a href='https://bugs.kde.org/'>i informant de qualsevol error</a>.

#### Instal·lació dels paquets executables 16.04 beta de les aplicacions del KDE

<em>Paquets</em>. Alguns venedors de Linux/UNIX OS han proporcionat gentilment els paquets executables de la 16.04 beta (internament 16.03.80) de les aplicacions del KDE per a algunes versions de la seva distribució, i en altres casos ho han fet voluntaris de la comunitat. En les setmanes vinents poden arribar a estar disponibles paquets binaris addicionals, i també actualitzacions dels paquets actualment disponibles.

<em>Ubicacions dels paquets</em>. Per a una llista actual dels paquets executables disponibles dels quals s'ha informat l'equip de llançament del KDE, si us plau, visiteu el <a href='https://community.kde.org/KDE_SC/Binary_Packages#'>wiki de la comunitat</a>.

#### Compilació de les aplicacions 16.04 beta del KDE

El codi font complet per a la 16.04 beta de les aplicacions del KDE es pot <a href='https://download.kde.org/unstable/applications/16.03.80/src/'>descarregar de franc</a>. Les instruccions per a compilar i instal·lar el programari estan disponibles a la <a href='/info/applications/applications-16.03.80'>pàgina d'informació de la beta de les aplicacions del KDE</a>.
