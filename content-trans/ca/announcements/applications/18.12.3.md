---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: Es distribueixen les aplicacions 18.12.2 del KDE.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE distribueix les aplicacions 18.12.3 del KDE
version: 18.12.3
---
{{% i18n_date %}}

Avui, KDE distribueix la tercera actualització d'estabilització per a les <a href='../18.12.0'>Aplicacions 18.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha més d'una vintena d'esmenes registrades d'errors que inclouen millores al Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize i Umbrello, entre d'altres.

Les millores inclouen:

- S'ha solucionat la càrrega dels arxius «.tar.zstd» a l'Ark
- El Dolphin ja no falla en aturar una activitat del Plasma
- El canvi a una partició diferent ja no pot fer fallar el Filelight
