---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE distribueix les aplicacions 16.12.0 del KDE
layout: application
title: KDE distribueix les aplicacions 16.12.0 del KDE
version: 16.12.0
---
15 de desembre de 2016. Avui KDE presenta les Aplicacions 16.12 del KDE, amb una gamma impressionant d'actualitzacions que milloren la facilitat d'accés, la introducció de funcionalitats molt útils i l'eliminació de petits problemes, portant les Aplicacions del KDE un pas endavant per oferir-vos la configuració perfecta per al vostre dispositiu.

L'<a href='https://okular.kde.org/'>Okular</a>, el <a href='https://konqueror.org/'>Konqueror</a>, el <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, el <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, el <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a> i altres (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>notes del llançament</a>) ara s'han adaptat als Frameworks 5 del KDE. Estem a l'espera dels vostres comentaris i opinions sobre les funcionalitats noves introduïdes en aquest llançament.

En un esforç continuat per fer les aplicacions més senzilles de construir de manera independent, s'han dividit els arxius tar de kde-baseapps, kdepim i kdewebdev. Podreu trobar els arxius tar nous creats al <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>document de les notes del llançament</a>

Hem suspès els paquets següents: kdegantt2, gpgmepp i kuser. Això ens ajudarà a centrar-nos en la resta del codi.

### L'editor de so Kwave s'uneix a les aplicacions del KDE!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

El <a href='http://kwave.sourceforge.net/'>Kwave</a> és un editor de so, que pot enregistrar, reproduir, importar i editar molts tipus de fitxers d'àudio, inclosos fitxers multicanal. El Kwave inclou alguns connectors per a transformar els fitxers d'àudio de diverses formes, i presenta una vista gràfica amb una capacitat de zoom i desplaçament complets.

### El món com a fons de pantalla

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Ara el Marble inclou tant un fons de pantalla com un giny per al Plasma que mostren l'hora sobre una vista de satèl·lit de la Terra, visualitzant el dia i la nit en temps real. Això estava disponible en el Plasma 4, i ara s'ha actualitzat per a funcionar amb el Plasma 5.

Trobareu més informació al <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>blog del Friedrich W. H. Kossebau</a>.

### Emoticones a dojo!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

El KCharSelect ha obtingut la possibilitat de mostrar el bloc d'emoticones Unicode (i altres blocs de símbols SMP).

També ha introduït un menú de punts, així que podreu afegir com a preferits tots els caràcters que aprecieu més.

### Les matemàtiques són millors amb el Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

El Cantor té un dorsal nou per a Julia, oferint als seus usuaris la possibilitat d'usar el programari més actual en computació científica.

Trobareu més informació al <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>blog de l'Ivan Lakhtanov</a>.

### Arxivat avançat

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

L'Ark té diverses característiques noves:

- Els fitxers i carpetes ara es poden reanomenar, copiar o moure dins l'arxiu
- Ara es pot seleccionar l'algorisme de compressió i encriptatge en crear els arxius
- L'Ark ara pot obrir fitxers AR (p. ex. les biblioteques estàtiques \*.a de Linux)

Trobareu més informació al <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>blog d'en Ragnar Thomsen</a>.

### I més!

El Kopete admet l'autenticació SASL X-OAUTH2 en el protocol Jabber i ha esmenat diversos problemes amb el connector d'encriptatge OTR.

El Kdenlive té un efecte de rotoscopi nou, admet contingut descarregable i un seguidor de moviment actualitzat. També proporciona els fitxers <a href='https://kdenlive.org/download/'>Snap i AppImage</a> per a una instal·lació més fàcil.

El KMail i l'Akregator poden usar el «Google Safe Browsing» per comprovar si l'enllaç que s'ha clicat és maliciós. També s'ha afegit la implementació per impressió en ambdós (cal les Qt 5.8).

### Control enèrgic de plagues

S'han esmenat més de 130 errors a les aplicacions, incloses el Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark, Kdenlive i més!

### Registre complet de canvis
