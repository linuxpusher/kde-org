---
aliases:
- ../../kde-frameworks-5.52.0
date: 2018-11-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Soluciona la construcció amb BUILD_QCH=TRUE
- Usa realment «fileNameTerms» i «xAttrTerms»
- [Balooshow] Evita accessos fora de límits en accedir a dades corruptes de la BBDD
- [Extractor] No comprova QFile::exists per a un URL buit
- [Scheduler] Usa un indicador per seguir si un executor queda lliure
- [Extractor] Gestiona correctament els documents quan el tipus MIME no s'ha d'indexar
- [Scheduler] Esmena l'ús incorrecte del segell de temps de QFileInfo::created() (error 397549)
- [Extractor] Fa l'extractor resistent a les fallades (error 375131)
- Passa el FileIndexerConfig com a «const» als indexadors individuals
- [Config] Elimina el suport per la configuració del KDE 4, atura l'escriptura de fitxers de configuració arbitraris
- [Extractor] Millora la depuració de la línia d'ordres, reenvia a la «stderr»
- [Scheduler] Reusa la informació de fitxer del FilteredDirIterator
- [Scheduler] Reusa el tipus MIME de l'UnindexedFileIterator a l'indexador
- [Scheduler] Elimina la variable «m_extractorIdle» supèrflua
- Porta a terme comprovacions de fitxers no indexats i entrades d'índex caducades en iniciar
- [balooctl] Imprimeix l'estat actual i el fitxer que s'està indexant quan s'inicia el monitor (error 364858)
- [balooctl] També controla els canvis d'estat
- [balooctl] Esmena l'ordre «index» amb el fitxer ja indexat però mogut (error 397242)

### BluezQt

- Afegeix la generació de les capçaleres de les API Media i MediaEndpoint

### Icones Brisa

- Canvia les icones del gestor de paquets a emblemes
- Torna a afegir la icona monocroma d'enllaç com a acció
- Millora el contrast dels emblemes, la llegibilitat i la coherència (error 399968)
- Admet el tipus MIME «nou» per als fitxers «.deb» (error 399421)

### Mòduls extres del CMake

- ECMAddQch: ajuda al «doxygen» predefinint més macros «Q*DECL**»
- Bindings: Permet usar els camins del sistema per al directori d'instal·lació del Python
- Bindings: Elimina INSTALL_DIR_SUFFIX de «ecm_generate_python_binding»
- Afegeix la implementació per al sanejador «fuzzer»

### KCMUtils

- Implementació per a múltiples pàgines al KCM

### KConfig

- Afegeix el mecanisme per a notificar sobre D-Bus els altres clients dels canvis a la configuració
- Exposa el mètode «getter» per al «KConfig::addConfigSources»

### KConfigWidgets

- Permet que el KHelpCenter obri les pàgines correctes de l'ajuda del KDE quan el KHelpClient s'invoqui amb una àncora

### KCrash

- KCrash: esmena una falla («ha» irònic) en ser usat en una aplicació sense QCoreApplication

### KDeclarative

- Fa part dels «push/pop» de l'API del ConfigModule

### KDED

- Elimina el missatge inútil «No s'ha trobat X-KDE-DBus-ServiceName»

### KDesignerPlugin

- Fa referència al producte «KF5» a les metadades del giny, en lloc de «KDE»

### KDocTools

- API dox: afegeix documents mínims a l'espai de noms del KDocTools, perquè el «doxygen» ho cobreixi
- Crea un fitxer QCH amb la «dox» de l'API, opcionalment, usant ECMAddQCH
- Espera que es construeixi el «docbookl10nhelper» abans de construir les pàgines «man» pròpies
- Usa l'intèrpret Perl especificat en lloc de confiar en el PATH

### KFileMetaData

- [ExtractorCollection] Només usa el connector extractor que coincideixi millor
- [KFileMetaData] Afegeix un extractor per a XML i SVG
- [KFileMetaData] Afegeix un ajudant per a les metadades XML codificades Dublin Core
- Implementa el funcionament per llegir etiquetes ID3 des de fitxers «aiff» i «wav»
- Implementa més etiquetes per a les metadades dels «asf»
- Extreu les etiquetes «ape» dels fitxers «ape» i «wavpack»
- Proporciona una llista de tipus MIME acceptats per a «embeddedimagedata»
- Compara amb QLatin1String i harmonitza la gestió de tots els tipus
- No falla amb dades «exiv2» no vàlides (error 375131)
- epubextractor: Afegeix la propietat ReleaseYear
- Refactoritza «taglibextractor» amb funcions específiques per a tipus de metadades
- Afegeix fitxers «wma»/etiquetes «asf» com a tipus MIME acceptats
- Usa un extractor propi per provar la «taglibwriter»
- Afegeix una cadena de suffix per provar les dades i usar la prova Unicode de la «taglibwriter»
- Elimina la verificació de l'hora de compilació de la versió de la «taglib»
- Amplia la cobertura de proves a tots els tipus MIME acceptats per «taglibextractor»
- Usa una variable amb el text ja recuperat en lloc de tornar-la a recuperar

### KGlobalAccel

- Esmena les notificacions de canvi de disposició de teclat (error 269403)

### KHolidays

- Afegeix el fitxer de festius de Bahrain
- Fa que KHolidays també funcioni com a biblioteca estàtica

### KIconThemes

- Afegeix un QIconEnginePlugin per permetre la deserialització de QIcon (error 399989)
- [KIconLoader] Reemplaça la QImageReader assignada a monticles amb l'assignada a la pila
- [KIconLoader] Ajusta la vora de l'emblema depenent de la mida de la icona
- Centra adequadament la icona si la mida no encaixa (error 396990)

### KIO

- No intenta usar els protocols SSL «menys segurs» com a contingència
- [KSambaShare] Retalla els / finals del camí de la compartició
- [kdirlistertest] Espera una mica més que finalitzi el «lister»
- Mostra un missatge d'avís si el fitxer no és local
- kio_help: Esmena una fallada a QCoreApplication en accedir a help:// (error 399709)
- Evita esperar les accions d'usuari quan la prevenció de robatori de focus del KWin és alta o extrema
- [KNewFileMenu] No obre un QFile buit
- S'han afegit icones que mancaven al codi del plafó de llocs del KIO
- Es desfà dels apuntadors en brut del KFileItem a KCoreDirListerCache
- Afegeix l'opció «Munta» al menú contextual dels dispositius desmuntats a Llocs
- Afegeix l'entrada «Propietats» al menú contextual del plafó de Llocs
- Esmena l'avís «L'expansió de la macro que produeix 'defined' té un comportament no definit»

### Kirigami

- Esmena els elements que manquen a les construccions estàtiques
- Implementació bàsica per a pàgines ocultes
- Carrega icones del tema d'icones adequat
- (moltes altres esmenes)

### KNewStuff

- Missatges d'error més útils

### KNotification

- Soluciona una fallada causada per una gestió dolenta del temps de vida a les notificacions d'àudio basades en «canberra» (error 398695)

### KParts

- Esmena la Cancel·lació que no es gestionava a BrowserRun::askEmbedOrSave obsolet
- Adaptació a una variant no obsoleta de KRun::runUrl
- Adapta KIO::Job::ui() -&gt; KJob::uiDelegate()

### KWayland

- Afegeix el protocol d'escriptori virtual del KWayland
- Evita que la font de dades se suprimeixi abans de processar l'esdeveniment «dataoffer» rebut (error 400311)
- [server] Respecta la regió d'entrada de les subsuperfícies al focus de la superfície de l'apuntador
- [xdgshell] Afegeix operadors d'indicador d'ajust de restricció de posició

### KWidgetsAddons

- API dox: esmena la nota «Since» de KPageWidgetItem::isHeaderVisible
- Afegeix la propietat «headerVisible» nova

### KWindowSystem

- No compara iteradors retornats des de dues còpies retornades per separat

### KXMLGUI

- Fa 1..n KMainWindows a kRestoreMainWindows

### NetworkManagerQt

- Afegeix les opcions dels paràmetres IPv4 que mancaven
- Afegeix la configuració per VXLAN

### Frameworks del Plasma

- Reverteix l'escalat d'icones al mòbil
- Admet etiquetes mnemotècniques
- Elimina l'opció PLASMA_NO_KIO
- Cerca adequadament els temes de contingència

### Purpose

- Defineix l'indicador Dialog per JobDialog

### Solid

- [solid-hardware5] Llista les icones als detalls dels dispositius
- [UDisks2] Apaga la unitat en treure-la si ho permet (error 270808)

### Sonnet

- Soluciona el trencament de l'estimació del llenguatge

### Sindicació

- Afegeix el fitxer README.md que manca (necessari per a diversos scripts)

### Ressaltat de la sintaxi

- Ressaltat de la sintaxi de fitxers CLIST de z/OS
- S'ha creat un fitxer nou de ressaltat de sintaxi per al Job Control Language (JCL)
- Suprimeix el mode d'obertura des de les versions massa noves de les Qt
- Incrementa la versió + esmena de la versió requerida del Kate a la versió actual del Framework
- Regla de paraules clau: suport per a la inclusió de paraules clau des d'un altre llenguatge/fitxer
- Sense verificació ortogràfica per al Metamath excepte als comentaris
- CMake: Afegeix variables i propietats relacionades amb l'XCode introduïdes al 3.13
- CMake: Introdueix funcionalitats noves de la pròxima versió 3.13

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
