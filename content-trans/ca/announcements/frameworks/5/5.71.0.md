---
aliases:
- ../../kde-frameworks-5.71.0
date: 2020-06-06
layout: framework
libCount: 70
---
### Baloo

- Només emmagatzema una vegada els termes de nom de fitxer

### Icones Brisa

- Millora la precisió de les icones de percentatge de bateria
- Corregeix la icona del tipus MIME KML
- Canvia la icona «mouse» per a millorar el contrast del tema fosc (error 406453)
- Requereix una construcció fora del codi font (error 421637)
- Afegeix icones de lloc de 48px (error 421144)
- Afegeix una drecera d'icona del LibreOffice que manca

### Mòduls extres del CMake

- [android] Usa una versió més nova de les Qt a l'exemple
- [android] Permet especificar la ubicació d'instal·lació dels APK
- ECMGenerateExportHeader: afegeix la generació de *_DEPRECATED_VERSION_BELATED()
- ECMGeneratePriFile: corregeix quan ECM_MKSPECS_INSTALL_DIR és absolut
- ECMGeneratePriFile: fa reubicables els fitxers «pri»
- Suprimeix l'avís de coincidència de nom de paquet «find_package_handle_standard_args»

### Eines de Doxygen del KDE

- Usa el fitxer «logo.png» com a logotip predeterminat, si és present
- Canvia «public_example_dir» a «public_example_dirs», per habilitar directoris múltiples
- Elimina el codi per al funcionament del Python2
- Adapta els enllaços del repositori a invent.kde.org
- Corregeix l'enllaç a l'impressum del kde.org
- Les pàgines de l'historial del KDE4 s'han fusionat en la pàgina de l'historial general
- Soluciona la generació amb els diagrames de dependències amb el Python 3 (trenca Py2)
- Exporta la «metalist» a un fitxer «json»

### KAuth

- Usa ECMGenerateExportHeader per a gestionar millor les API obsoletes

### KBookmarks

- Usa el context de marcador d'IU a més crides «tr()»
- [KBookmarkMenu] Assigna aviat «m_actionCollection» per evitar una fallada (error 420820)

### KCMUtils

- Afegeix «X-KDE-KCM-Args» com a propietat, llegeix la propietat en el carregador del mòdul
- Soluciona una fallada en carregar un KCM d'aplicació externa com el Yast (error 421566)
- KSettings::Dialog: evita duplicar entrades per $XDG_DATA_DIRS en cascada

### KCompletion

- També embolcalla la «dox» de l'API amb KCOMPLETION_ENABLE_DEPRECATED_SINCE; usa per mètode
- Usa el context de marcador d'IU a més crides «tr()»

### KConfig

- No intenta inicialitzar el valor de l'enumeració SaveOptions obsoleta
- Afegeix KStandardShortcut::findByName(const QString&amp;) i fa obsolet «find(const char*)»
- Corregeix KStandardShortcut::find(const char*)
- Ajusta el nom del mètode exportat internament com se suggereix a D29347
- KAuthorized: exporta el mètode per a recarregar les restriccions

### KConfigWidgets

- [KColorScheme] Elimina codi duplicat
- Fa que la reserva dels colors de Header siguin primers els de Window
- Presenta el conjunt de colors Header

### KCoreAddons

- Corregeix l'error 422291 - Vista prèvia dels URI XMPP al KMail (error 422291)

### KCrash

- No invoca cap «qstring» localitzat a la secció crítica

### KDeclarative

- Crea les funcions «kcmshell.openSystemSettings()» i «kcmshell.openInfoCenter()»
- Adapta KKeySequenceItem al QQC2
- El fill de GridViewInternal s'alinea a píxel

### KDED

- Corregeix les icones borroses a l'AppMenu de la barra de títol afegint l'indicador UseHighDpiPixmaps
- Afegeix un fitxer de servei d'usuari del «systemd» per al «kded»

### KFileMetaData

- [TaglibExtractor] Afegeix la implementació per als llibres d'àudio «Audible Enhanced Audio»
- Respecta l'indicador «extractMetaData»

### KGlobalAccel

- Corregeix un error amb els components que contenen caràcters especials (error 407139)

### KHolidays

- Actualitza els festius taiwanesos
- holidayregion.cpp - proporciona cadenes traduïbles per a les regions alemanyes
- holidays_de-foo - estableix el camp de nom per a tots els fitxers de festius alemanys
- holiday-de-&lt;foo&gt; - a on «foo» és una regió d'Alemanya
- Afegeix el fitxer de festius per a DE-BE (Alemanya/Berlín)
- holidays/plan2/holiday_gb-sct_en-gb

### KImageFormats

- Afegeix diverses comprovacions de sanejaments i límits

### KIO

- Presenta KIO::OpenUrlJob, una reescriptura i substitució del KRun
- KRun: fa obsolets tots els mètodes «run*» estàtics, amb instruccions completes d'adaptació
- KDesktopFileActions: fa obsolet «run/runWithStartup», en el seu lloc usa OpenUrlJob
- Cerca «kded» com a dependència en temps d'execució
- [kio_http] Analitza un camí QUrl FullyEncoded amb TolerantMode (error 386406)
- [DelegateAnimationHanlder] Substitueix la QLinkedList obsoleta per QList
- RenameDialog: Avisa quan les mides dels fitxers no siguin la mateixa (error 421557)
- [KSambaShare] Comprova que estiguin disponibles tant «smbd» com «testparm» (error 341263)
- Places: Usa Solid::Device::DisplayName per a DisplayRole (error 415281)
- KDirModel: corregeix una regressió de «hasChildren()» per als arbres que mostren fitxers (error 419434)
- file_unix.cpp: quan «::rename» s'usa com a condició es compara el seu retorn amb -1
- [KNewFileMenu] Permet crear un directori anomenat «~» (error 377978)
- [HostInfo] Estableix QHostInfo::HostNotFound quan un ordinador no es troba a la memòria cau del DNS (error 421878)
- [DeleteJob] Informa els nombres finals després de finalitzar (error 421914)
- KFileItem: localitza «timeString» (error 405282)
- [StatJob] Fa que «mostLocalUrl» ignori els URL remots (ftp, http, etc.) (error 420985)
- [KUrlNavigatorPlacesSelector] Actualitza només una vegada el menú als canvis (error 393977)
- [KProcessRunner] Usa només el nom de l'executable per a l'àmbit
- [knewfilemenu] Mostra un avís en línia en crear elements amb espais inicials o finals (error 421075)
- [KNewFileMenu] Elimina un paràmetre redundant del sòcol
- ApplicationLauncherJob: mostra el diàleg «Obre amb» si no s'ha passat cap servei
- Assegura que el programa existeix i és executable abans d'injectar «kioexec/kdesu/etc»
- Corregeix l'URL que es passa com a argument en llançar un fitxer «.desktop» (error 421364)
- [kio_file] Gestiona en reanomenar un fitxer «A» a «a» als sistemes de fitxers FAT32
- [CopyJob] Comprova si el directori de destinació és un enllaç simbòlic (error 421213)
- Corregeix el fitxer de servei que especifica «Executa al terminal» donant un codi d'error 100 (error 421374)
- kio_trash: afegeix la implementació per reanomenar directoris a la memòria cau
- Avisa si no s'ha definit «info.keepPassword»
- [CopyJob] Comprova l'espai lliure dels URL remots abans de copiar i altres millores (error 418443)
- [kcm trash] Canvia el percentatge de la mida de la paperera a 2 llocs decimals
- [CopyJob] Elimina un TODO antic i usa QFile::rename()
- [LauncherJobs] Emet una descripció

### Kirigami

- Afegeix capçaleres de secció a la barra lateral quan totes les accions es poden expandir
- Correcció: farciment a la capçalera de l'OverlaySheet
- Usa un color predeterminat millor per al calaix global quan s'usa com a barra lateral
- Corregeix un comentari no acurat de com es determina GridUnit
- Escalada més fiable per l'arbre de pares a PageRouter::pushFromObject
- PlaceholderMessage depèn de les Qt 5.13 encara que CMakeLists.txt requereixi les 5.12
- Presenta el grup Header
- No reprodueix l'animació de tancament al «close()» si el full ja està tancat
- Afegeix el component SwipeNavigator
- Presenta el component Avatar
- Corregeix la inicialització del recurs «shaders» a les construccions estàtiques
- AboutPage: Afegeix consells d'eina
- Assegura «closestToWhite» i «closestToBlack»
- AboutPage: Afegeix botons per a correu electrònic i adreces web
- Usa sempre el conjunt de colors de Window per a AbstractApplicationHeader (error 421573)
- Presenta ImageColors
- Recomana un càlcul millor de l'amplada per a PlaceholderMessage
- Millora l'API de PageRouter
- Usa un tipus de lletra petit per al subtítol BasicListItem
- Afegeix la implementació de capes a PagePoolAction
- Presenta el control RouterWindow

### KItemViews

- Usa el context de marcador d'IU a més crides «tr()»

### KNewStuff

- No duplica els missatges d'error a les notificacions passives (error 421425)
- Corregeix els colors incorrectes al quadre de missatge del KNS Quick (error 421270)
- No marca l'entrada com a desinstal·lada si ha fallat l'script de desinstal·lació (error 420312)
- KNS: Fa obsolet el mètode «isRemote» i gestiona adequadament l'error d'anàlisi
- KNS: No marca l'entrada com a instal·lada si ha fallat l'script d'instal·lació
- Corregeix la visualització d'actualitzacions que s'ha seleccionat l'opció (error 416762)
- Corregeix la selecció automàtica d'actualització (error 419959)
- Afegeix la implementació de KPackage a KNewStuffCore (error 418466)

### KNotification

- Implementa el control de visibilitat en pantalla bloquejada a l'Android
- Implementa l'agrupació de notificacions a l'Android
- Mostra els missatges de notificació amb text enriquit a l'Android
- Implementa URL usant consells
- Usa el context de marcador d'IU a més crides «tr()»
- Implementa el funcionament de la urgència de notificacions a l'Android
- Elimina les comprovacions del servei de notificacions i l'alternativa de reserva de KPassivePopup

### KQuickCharts

- Comprovació més senzilla d'arrodoniment de fons
- PieChart: Renderitza un segment de tor com a fons quan no és un cercle complet
- PieChart: Afegeix una funció d'ajuda que té cura d'arrodonir els segments dels tors
- PieChart: Exposa «fromAngle»/«toAngle» al «shader»
- Renderitza sempre el fons del diagrama de sectors
- Defineix sempre el color del fons, inclús quan «toAngle» != 360
- Permet que l'inici/final sigui com a mínim 2pi de distància entre ells
- Tornar a escriure la implementació de PieChart
- Esmena i comenta la funció «sdf» «sdf_torus_segment»
- No especifica explícitament la quantitat de suavitzat en renderitzar «sdfs»
- Solució temporal a la manca de paràmetres de funció de matrius als «shaders» GLES2
- Gestiona les diferències entre els perfils del nucli i els antics als «shaders» de línia/sector
- Corregeix diversos errors de validació
- Actualitza l'script de validació del «shader» amb l'estructura nova
- Afegeix una capçalera separada per als «shaders» GLES3
- Elimina els «shaders» duplicats del perfil del nucli, en el seu lloc usa el preprocessador
- Exposa el nom i el «shortName»
- Admet etiquetes llargues/curtes diferents (error 421578)
- Protegeix el model darrere un QPointer
- Afegeix MapProxySource

### KRunner

- La llista blanca d'executors a no persistir a la configuració
- Corregeix els senyals «prepare»/«teardown» del KRunner (error 420311)
- Detecta fitxers locals i carpetes que comencen per ~

### KService

- Afegeix «X-KDE-DBUS-Restricted-Interfaces» als camps de les entrades «Application desktop»
- Afegeix una etiqueta d'obsolescència de compilador que manca als arguments 5x del constructor del KServiceAction

### KTextEditor

- Afegeix «.diff» al «file-changed-diff» per a activar la detecció MIME al Windows
- Minimapa de la barra de desplaçament: rendiment: retarda l'actualització dels documents inactius
- Fa que el text sempre s'alineï amb la línia base del tipus de lletra
- Reverteix «Emmagatzema i recupera la configuració de la vista completa a i des de la sessió de configuració»
- Corregeix el marcador de línia modificada al minimapa del Kate

### KWidgetsAddons

- Fa soroll quant a «KPageWidgetItem::setHeader(empty-non-null string)» obsolet

### KXMLGUI

- [KMainWindow] Invoca QIcon::setFallbackThemeName (més tard) (error 402172)

### KXmlRpcClient

- Marca KXmlRpcClient com a ajuda d'adaptació

### Frameworks del Plasma

- PC3: Afegeix un separador a la barra d'eines
- Implementa el grup de color Header
- Implementa l'ajust del desplaçament i arrossegament dels valors del control SpinBox
- Usa «font:» en lloc de «font.pointSize:» a on sigui possible
- kirigamiplasmastyle: Afegeix la implementació d'AbstractApplicationHeader
- Evita una desconnexió potencial de tots els senyals a IconItem (error 421170)
- Usa un tipus de lletra petit per al subtítol ExpandableListItem
- Afegeix «smallFont» a l'estil Plasma del Kirigami
- [Plasmoid Heading] Dibuixa la capçalera només quan hi ha un SVG al tema

### QQC2StyleBridge

- Afegeix l'estil ToolSeparator
- Elimina «pressed» de l'estat «on» del CheckIndicator (error 421695)
- Implementa el grup Header
- Implementa «smallFont» al connector del Kirigami

### Solid

- Afegeix un QString Solid::Device::displayName, usat al «Fstab Device» dels muntatges de xarxa (error 415281)

### Sonnet

- Permet substituir la desactivació de la detecció automàtica d'idioma (error 394347)

### Ressaltat de la sintaxi

- Actualitza les ampliacions del Raku als blocs del Markdown
- Raku: corregeix els blocs de codi delimitats al Markdown
- Assigna l'atribut «Identifier» per obrir una cometa doble en lloc de «Comment» (error 421445)
- Bash: corregeix els comentaris després de les escapades (error 418876)
- LaTeX: corregeix el plegat a «end{...}» i als marcadors de regió BEGIN-END (error 419125)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
