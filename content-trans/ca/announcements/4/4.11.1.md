---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE distribueix els espais de treball Plasma, les aplicacions i la plataforma
  4.11.1.
title: KDE distribueix les actualitzacions de setembre als espais de treball Plasma,
  aplicacions i plataforma
---
3 de setembre de 2013. Avui KDE distribueix les actualitzacions als espais de treball del Plasma, les aplicacions i la plataforma de desenvolupament. Aquesta actualització és la primera en una sèrie d'actualitzacions mensuals d'estabilització per les sèries 4.11. Com ja es va anunciar en la publicació, els espais de treball continuaran rebent actualitzacions durant els dos anys vinents. Com que aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 70 esmenes registrades d'errors que inclouen millores al gestor de finestres KWin, el gestor de fitxers Dolphin, i altres. Els usuaris poden esperar que l'escriptori Plasma s'engegui més de pressa, que el desplaçament del Dolphin sigui més suau, i diverses aplicacions i eines utilitzin menys memòria. Les millores inclouen el retorn d'arrossegar i deixar anar des de la barra de tasques al paginador, ressaltat i esmena de colors en el Kate i molts errors petits solucionats en el joc Kmahjongg. Hi ha moltes esmenes d'estabilitat i l'addició normal de traduccions.

Es pot trobar una <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>llista</a> més completa dels canvis en el seguidor d'incidents del KDE. Per a una llista detallada dels canvis que s'apliquen en la 4.11.1, també podeu explorar els registres del Git.

Per baixar el codi font o els paquets a instal·lar aneu a la <a href='/info/4/4.11.1'>pàgina d'informació de la 4.11.1</a>. Si voleu arribar a saber més quant a les versions 4.11 dels espais de treball del KDE, aplicacions i la plataforma de desenvolupament, visiteu les <a href='/announcements/4.11/'>notes del llançament 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`El nou flux de treball «envia més tard» en el Kontact` width="600px">}}

El programari KDE, incloses totes les biblioteques i aplicacions, és disponible de franc d'acord amb les llicències de codi font obert (Open Source). El programari KDE es pot obtenir com a codi font i en diversos formats executables des de <a href='https://download.kde.org/stable/4.11.1/'>download.kde.org</a> o des de qualsevol dels <a href='/distributions'>principals sistemes GNU/Linux i UNIX</a> que es distribueixen avui en dia.
