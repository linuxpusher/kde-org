---
aliases:
- ../../kde-frameworks-5.60.0
date: 2019-07-13
layout: framework
libCount: 70
---
### Загальне

- Для збирання потрібна бібліотека Qt &gt;= 5.11. Актуальною версією бібліотеки є Qt 5.13.

### Baloo

- [QueryTest] Реалізовано перевірку, чи незалежні фрази є справді незалежними
- [TermGenerator] Реалізовано вставлення порожньої позиції між незалежними термінами
- [QueryTest] Реструктуризовано перевірки з метою спрощення розширення
- [TermGenerator] Вилучено фрази з одного терміна з PositionDB
- [TermGenerator] Реалізовано обрізання терміна до перетворення до UTF-8
- [PostingIterator] Метод positions() пересунуто до VectorPositionInfoIterator
- [TermGenerator] Реалізовано використання ByteArray UTF-8 для termList
- [WriteTransactionTest] Спрощено суміш QString і QByteArray
- [experimental/BalooDB] Виправлено тривіальне попередження щодо використання 0 / nullptr
- [PositionDbTest] Виправлено тривіальний виток пам'яті у перевірці
- [PendingFileQueueTest] Реалізовано перевірку того, що create + delete не надсилають зайвих сигналів про події
- [PendingFileQueueTest] Реалізовано перевірку того, що delete + create справді працює
- [PendingFileQueue] Реалізовано запобігання конкуренції між delete + create / create + delete
- [PendingFileQueueTest] Реалізовано використання синтетичних подій таймера для пришвидшення тестування
- [XAttrIndexer] Реалізовано оновлення DocumentTime при оновленні XAttrs
- [PendingFileQueueTest] Скорочено проміжки очікування, реалізовано перевірку часу стеження
- [PendingFileQueue] Реалізовано точніше обчислення часу, який залишився до кінця виконання дії
- [ModifiedFileIndexer] Реалізовано використання належних типів MIME для тек, реалізовано затримку до потрібного моменту
- [NewFileIndexer] З покажчика усунено символічні посилання
- [ModifiedFileIndexer] Реалізовано враховування змін у XAttr, незважаючи на зміни у вмісті
- [NewFileIndexer] Реалізовано використання належного типу MIME для тек, реалізовано перевірку excludeFolders
- [UnindexedFileIndexer] Реалізовано врахування змін у коментарях, мітка та оцінках
- [UnindexedFileIndexer] Вимкнено перевірки часу створення файла для нових файлів
- [DocumentUrlDB] Усунено потребу у обробці усієї ієрархії при тривіальному перейменуванні
- [DocumentUrlDB] Реалізовано раннє перехоплення некоректних адрес
- [DocumentUrlDB] Вилучено невикористаний метод «rename»
- [balooctl] Спрощено команди керування засобом індексування
- [Transaction] Замінено шаблон для функтора на std::function
- [FirstRunIndexer] Використано належний тип MIME для тек
- Інваріантну команду IndexingLevel пересунуто за межі циклу
- [BasicIndexingJob] Вилучено пошук типу документа baloo для каталогів
- [FileIndexScheduler] Забезпечено вимикання засобу індексування у режимі присипляння
- [PowerStateMonitor] Реалізовано консервативну методику визначення стану живлення
- [FileIndexScheduler] Реалізовано зупинення роботи засобу індексування, якщо quit() викликано за допомогою DBus
- Усунено від'єднання контейнера у декількох місцях
- Усунено спробу дописування до QLatin1String
- Вимкнено виявлення valgrind при збиранні за допомогою MSVC
- [FilteredDirIterator] Усі суфікси зібрано у один великий формальний вираз
- [FilteredDirIterator] Усунено можливий вихід формального виразу за межі точної відповідності
- [UnindexedFileIterator] Реалізовано відкладення визначення типу MIME до потрібного моменту
- [UnindexedFileIndexer] Усунено спроби додавання файлів, яких не існує, до покажчика
- Реалізовано виявлення valgrind, усунено вилучення бази даних, якщо використано valgrind
- [UnindexedFileIndexer] Оптимізовано цикл (усунено від'єднання та інваріанти)
- Реалізовано затримку запуску UnindexedFileIndexer та IndexCleaner
- [FileIndexScheduler] Додано новий стан «бездіяльність при роботі на акумуляторі»
- [FileIndexScheduler] Реалізовано відкладення завдань із супроводу бази даних, доки живлення відбувається від акумулятора
- [FileIndexScheduler] Усунено кратне надсилання сигналів щодо зміни стану
- [balooctl] Прояснено і розширено повідомлення щодо стану

### BluezQt

- Додано програмний інтерфейс MediaTransport
- Додано програмні інтерфейси LE Advertising та GATT

### Піктограми Breeze

- Додано id="current-color-scheme" до піктограм collapse-all (виправлено ваду 409546)
- Додано піктограму disk-quota (виправлено ваду 389311)
- Створено символічне посилання install на edit-download
- Піктограму параметрів джойстика змінено на піктограму ігрового контролера (виправлено ваду 406679)
- Додано піктограму edit-select-text, піктограму розміром у 16 пікселів для draw-text зроблено подібною до піктограми розміром у 22 пікселі
- Оновлено піктограму KBruch
- Додано піктограми help-donate-[валюта]
- Піктограму Kolourpaint у темній темі Breeze зроблено такою самою, що у світлій темі
- Додано піктограми сповіщень у розмірі 22 пікселя

### KActivitiesStats

- Усунено аварійне завершення роботи у KactivityTestApp, якщо у Result є рядки, які містять символи, відмінні від символів ASCII

### KArchive

- Усунено аварійне завершення роботи, якщо запакований файл за розміром є більшим за максимальний розмір QByteArray

### KCoreAddons

- KPluginMetaData: використано Q_DECLARE_METATYPE

### KDeclarative

- [GridDelegate] Усунено прогалини у кутах підсвічування thumbnailArea
- Усунено blockSignal-и
- [KCM GridDelegate] Усунено причину попередження
- [KCM GridDelegate] Реалізовано враховування implicitCellHeight для внутрішньої делегованої функції висоти
- Виправлено піктограму GridDelegate
- Усунено ненадійне порівняння із i18n("None") і описано поведінку коду у документації (виправлено ваду 407999)

### KDE WebKit

- Знижено версію KDEWebKit з Tier 3 для допомоги у портуванні

### KDocTools

- Оновлено user.entities для локалі pt-BR

### KFileMetaData

- Виправлено видобування деяких властивостей відповідно до написаного (виправлено ваду 408532)
- Реалізовано використання категорії діагностики у засобі видобування та запису даних taglib
- Реалізовано форматування значення ухилу експонування фотографій (виправлено ваду 343273)
- Виправлено назву властивості
- Вилучено префікс «photo» з усіх назв властивостей exif (виправлено ваду 343273)
- Перейменовано властивості ImageMake і ImageModel (виправлено ваду 343273)
- [UserMetaData] Додано метод для опитування щодо встановлених атрибутів
- Реалізовано форматування фокальної відстані у міліметрах
- Реалізовано форматування запису часу експонування фотографії у вигляді раціонального дробу, якщо це можливо (виправлено ваду 343273)
- Увімкнено usermetadatawritertest для усіх систем UNIX, не лише для Linux
- Реалізовано форматування значень діафрагми у F-числах (виправлено ваду 343273)

### Додатки графічного інтерфейсу KDE

- KModifierKeyInfo: реалізовано спільне використання внутрішньої реалізації
- Вилучено дублювання пошуків
- Рішення щодо використання x11 перенесено на момент виконання програми

### KHolidays

- Оновлено дані щодо банківського вихідного на початку травня у Великій Британії на початку 2020 року (виправлено ваду 409189)
- Виправлено код ISO для Гессена / Німеччина

### KImageFormats

- QImage::byteCount -&gt; QImage::sizeInByes

### KIO

- Виправлено перевірку KFileItemTest::testIconNameForUrl відповідно до іншої назви піктограми
- Виправлено помилку пов'язану із кількістю аргументів i18n у повідомленні-попередженні knewfilemenu
- [ftp] Виправлено помилковий час доступу у Ftp::ftpCopyGet() (виправлено ваду 374420)
- [CopyJob] Реалізовано пакетне звітування щодо оброблених даних
- [CopyJob] Реалізовано звітування щодо результатів після завершення копіювання (виправлено ваду 407656)
- Зайву логіку у KIO::iconNameForUrl() пересунуто до KFileItem::iconName() (виправлено ваду 356045)
- Реалізовано встановлення KFileCustomDialog
- [Панель «Місця»] Типово, прибрано кореневу теку
- Діалогове вікно «Не вдалося змінити права доступу» реалізовано на основі qWarning
- Доступ до O_PATH можливий лише у linux. Для запобігання помилок під час компіляції.
- Реалізовано показ супровідних повідомлень при створенні файлів або тек
- Підтримка уповноважень: реалізовано скидання прав доступу, якщо власником каталогу призначення не є root
- [copyjob] Реалізовано встановлення часу модифікації, лише якщо його надано допоміжним засобом kio (виправлено ваду 374420)
- Скасовано привілейовану дію для призначеного лише для читання каталогу призначення, якщо його власником є поточний користувач
- Додано KProtocolInfo::defaultMimetype
- Реалізовано безумовне збереження параметрів панелі перегляду при перемиканні з одного режиму перегляду до іншого
- Реалізовано відновлення ексклюзивної групи для упорядковування пунктів меню
- Реалізовано режими перегляду у стилі Dolphin для діалогового вікна роботи з файлами (виправлено ваду 86838)
- kio_ftp: поліпшено обробку помилок при помилках копіювання до FTP
- kioexec: замінено невиправдано жорсткі діагностичні повідомлення для відкладеного вилучення

### Kirigami

- [ActionTextField] Реалізовано підсвічування пункту дії при натисканні
- Реалізовано підтримку текстового режиму та позиції
- Реалізовано ефект наведення вказівника миші для послідовної навігації на стільниці
- Примусово встановлено мінімальну висоту у 2 одиниці сітки
- Для implicitHeight у SwipeListItem встановлено значення максимального розміру вмісту та пунктів дій
- Реалізовано приховування панелі підказки, якщо натиснуто PrivateActionToolButton
- Вилучено випадково залишені частини коду CMake для стилю Плазми
- ColumnView::itemAt
- Реалізовано примусове використання breeze-internal, якщо не вказано тему
- Реалізовано належну навігацію на лівій пришпиленій сторінці
- Реалізовано стеження за розмірами пришпилених сторінок
- Реалізовано показ роздільника у режимі лівої бічної панелі
- Враховано, що у режимі одного стовпчика пришпилювання не має значення
- Перший напів працездатний прототип пришпилювання

### KJobWidgets

- [KUiServerJobTracker] Реалізовано обробку зміни власника

### KNewStuff

- [kmoretools] Додано піктограми для дій «отримання даних» та «встановлення»

### KNotification

- Усунено пошук phonon для Android

### KParts

- Додано інтерфейс підтримки профілів для TerminalInterface

### KRunner

- Усунено нескінченну затримку надсилання сигналу matchesChanged

### KService

- Додано X-Flatpak-RenamedFrom як розпізнаване значення ключа

### KTextEditor

- Виправлено центрування рядка переходу (виправлено ваду 408418)
- Виправлено показ піктограми закладки на смужці піктограм при низькій роздільності
- Виправлено перемикання смужки при використанні пункту дії «Показувати смужку піктограм»
- Виправлено порожні сторінки на панелі попереднього перегляду друку і подвійний друк рядків (виправлено ваду 348598)
- Вилучено невикористовуваний файл заголовків
- Виправлено швидкість автоматичного гортання вниз (виправлено ваду 408874)
- Додано типові змінні до інтерфейсу змінних
- Реалізовано роботу автоматичної перевірки правопису після перезавантаження документа (виправлено ваду 408291)
- Збільшено верхню межу довжини рядка до 10000
- Реалізовано вимикання підсвічування після 512 символів у рядку
- KateModeMenuList: перехід на QListView

### KWayland

- Включено опис
- Тестова версія протоколу wayland для уможливлення роботи рушія даних стану клавіш

### KWidgetsAddons

- Нова версія KPasswordLineEdit належним чиному спадковує focusPolicy від QLineEdit (виправлено ваду 398275)
- Кнопку «Подробиці» замінено на KCollapsibleGroupBox

### Бібліотеки Plasma

- [Svg] Виправлено помилку портування з QRegExp::exactMatch
- ContainmentAction: виправлено завантаження з KPlugin
- [TabBar] Вилучено зовнішні поля
- Методи побудови списків для аплету, рушія даних і контейнера у Plasma::PluginLoader більше не фільтрують додатки за допомогою X-KDE-ParentAppprovided, якщо передано порожній рядок
- Відновлено працездатність щипання у календарі
- Додано піктограми disk-quota (виправлено ваду 403506)
- Трохи удосконалено Plasma::Svg::elementRect
- Реалізовано автоматичне встановлення версії пакунків тем стільниці за значенням KF5_VERSION
- Усунено сповіщення щодо зміни до попереднього розміру
- Виправлено вирівнювання мітки на кнопці інструмента
- [PlasmaComponents3] Реалізовано вертикальне вирівнювання тексту на кнопці

### Purpose

- Змінено початковий розмір діалогового вікна налаштовування
- Поліпшено піктограми та текст кнопок діалогового вікна завдань
- Виправлено переклад actiondisplay
- Усунено показ повідомлення щодо помилки, якщо поширення даних було скасовано користувачем
- Усунено причину попередження при читанні метаданих додатка
- Переплановано сторінки налаштовування
- ECMPackageConfigHelpers -&gt; CMakePackageConfigHelpers
- phabricator: виправлено fallthrough у перемиканні

### QQC2StyleBridge

- Реалізовано показ клавіатурного скорочення у меню, якщо таке визначено (виправлено ваду 405541)
- Додано MenuSeparator
- Виправлено помилку, пов'язану із тим, що ToolButton лишалася у натиснутому стані після натискання
- [ToolButton] Реалізовано передавання нетипового розміру піктограми до StyleItem
- Враховано правила видимості (виправлено ваду 407014)

### Solid

- [Fstab] Реалізовано вибір відповідної піктограми для домашнього або кореневого каталогу
- [Fstab] Реалізовано показу змонтованих «накладених» файлових систем
- [Модуль UDev] Звужено перелік опитуваних пристроїв

### Підсвічування синтаксису

- Fortran: умови ліцензування змінено на MIT
- Поліпшено підсвічування синтаксичних конструкцій у фіксованому форматуванні коду Fortran
- Fortran: реалізовано довільний і фіксований формати
- Виправлено підсвічування дужок у вкладеній COMMAND CMake
- Додано більше ключових слів, а також реалізовано підтримку rr у засобі підсвічування коду gdb
- Реалізовано раннє виявлення рядків коментарів у засобі підсвічування GDB
- AppArmor: оновлено синтаксис
- Julia: оновлено синтаксичні конструкції і додано ключові слова для сталих (виправлено ваду 403901)
- CMake: реалізовано підсвічування стандартних змінних середовища CMake
- Додано синтаксичне визначення для збірок ninja
- CMake: реалізовано підтримку можливостей 3.15
- Jam: різноманітні поліпшення та виправлення
- Lua: оновлення до Lua54, кінець функції визначено як Keyword, а не Control
- C++: оновлення до стандарту C++20
- debchangelog: додано Eoan Ermine

### Відомості щодо безпеки

Випущений код підписано за допомогою GPG з використанням такого ключа: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Відбиток основного ключа: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
