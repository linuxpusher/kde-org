---
aliases:
- ../../kde-frameworks-5.55.0
date: 2019-02-09
layout: framework
libCount: 70
---
### Baloo

- [tags_kio] Desactivar o acceso cun URL de barra inclinada dupla, é dicir «tags://» (fallo 400594)
- Crear unha instancia de QApplication antes de KCrash e KCatalog
- Ignorar todos os sinais «deviceAdded» de Solid que non sexan de almacenamento
- Usar un K_PLUGIN_CLASS_WITH_JSON mellor
- Retirar as comprobacións de Qt 5.10 agora que o esiximos como versión mínima

### Iconas de Breeze

- Engadir unha icona para o KCM dos cursores
- Engadir e cambiar de nome algunhas iconas e ligazóns simbólicas de YaST
- Mellorar a icona de campá de notificacións usando o deseño de KAlarm (fallo 400570)
- Engadir yast-upgrade
- Mellorar as iconas weather-storm-* (fallo 403830)
- Engadir as ligazóns simbólicas de font-otf, como as de font-ttf
- Engadir iconas axeitadas de edit-delete-shred (fallo 109241)
- Engadir iconas de reducir as marxes e reducir á selección (fallo 401489)
- Eliminar as ligazóns simbólicas de edit-delete-shred en preparación para substituílas por iconas reais
- Cambiar o nome da icona do KCM das actividades
- Engadir unha icona para o KCM das actividades
- Engadir unha icona para o KCM dos escritorios virtuais
- Engadir iconas para os KCM de área táctil e bordos de pantalla
- Corrixir os nomes das iconas relacionadas coas preferencias de compartición de ficheiros
- Engadir a icona preferences-desktop-effects
- Engadir unha icona de preferencias de tema de Plasma
- Mellorar o contraste de preferences-system-time (fallo 390800)
- Engadir unha nova icona de preferences-desktop-theme-global
- Engadir unha icona de ferramentas
- A icona document-new respecta ColorScheme-Text
- Engadir a icona preferences-system-splash para o KCM da pantalla de carga
- Incluír applets/22
- Engadir iconas do tipo MIME de Kotlin (.kt)
- Mellorar a icona preferences-desktop-cryptography
- Encher o cadeado en preferences-desktop-user-password
- Encher de maneira consistente o cadeado na icona de cifrado
- Usar unha icona de Kile similar á orixinal

### Módulos adicionais de CMake

- FindGperf: definir SKIP_AUTOMOC en ecm_gperf_generate para o ficheiro xerado
- Mover -Wsuggest-override -Wlogical-op á configuración normal do compilador
- Corrixir a xeración de API de Python para clases con construtores de copia eliminados
- Corrixir a xeración de módulos de qmake para Qt 5.12.1
- Usar máis HTTPS nas ligazóns
- Documentación da API: engadir as entradas que faltan de algúns módulos de atopar e módulos
- FindGperf: mellorar a documentación da API: exemplo de uso de etiquetas
- ECMGenerateQmlTypes: corrixir a documentación da API: o título require etiquetas --- adicionais
- ECMQMLModules: corrixir a documentación da API: facer que o título coincida co nome do módulo, engadir un «Desde» que faltaba
- FindInotify: corrixir a etiqueta de .rst da documentación da API, engadir un «Desde» que faltaba

### KActivities

- corrixir para macOS

### KArchive

- Aforrar dúas chamadas a KFilterDev::compressionTypeForMimeType

### KAuth

- Retirar a posibilidade de pasar QVariants de interface gráfica de usuarios a asistentes de KAuth

### KBookmarks

- Construír sen D-Bus en Android
- Aplicar const

### KCMUtils

- [kcmutils] Engadir puntos suspensivos ás etiquetas de busca de KPluginSelector

### KCodecs

- nsSJISProber::HandleData: Non quebrar se aLen é 0

### KConfig

- kconfig_compiler: eliminar o operador de asignación e o construtor de copia

### KConfigWidgets

- Construír sen KAuth nin D-Bus en Android
- Engadir KLanguageName

### KCrash

- Comentar por que se necesita cambiar o ptracer
- [KCrash] Estabelecer un sócket para permitir cambiar de ptracer

### KDeclarative

- [Vista de grade dos controis de KCM] Engadir unha animación de retirar

### Compatibilidade coa versión 4 de KDELibs

- Corrixir algunhas bandeiras de país para que usen o mapa de píxeles completo

### KDESU

- xestionar os contrasinais incorrectos ao usar sudo e preguntar este de novo o contrasinal  (fallo 389049)

### KFileMetaData

- exiv2extractor: engadir compatibilidade con BMP, GIF, WebP e TGA
- Corrixir unha proba de datos GPS de EXIV que fallaba
- Probar datos de GPS baleiros e cero
- Engadir a taglibextractor compatibilidade con máis tipos MIME

### KHolidays

- holidays/plan2/holiday_ua_uk - actualizado para 2019

### KHTML

- Engadir metadatos de JSON ao binario do complemento KHTMLPart

### KIconThemes

- Construír sen D-Bus en Android

### KImageFormats

- xcf: Corrixir que a opacidade superase os límites
- Descomentar as inclusións de qDebug
- tga: Corrixir o uso de valores non inicializados en ficheiros rotos
- a opacidade máxima é 255
- xcf: Corrixir unha aserción en ficheiros con dous PROP_COLORMAP
- ras: Corrixir unha aserción porque ColorMapLength é grande de máis
- pcx: Corrixir unha quebra en ficheiros borrosos
- xcf: codificar robustez para cando PROP_APPLY_MASK non estea no ficheiro
- xcf: loadHierarchy: obedecer o layer.type e non o bpp
- tga: Non permitir máis de 8 bits de alfa
- ras: Devolver falso cando falle a reserva da imaxe
- rgb: Corrixir un derrame de enteiro en ficheiros borrosos
- rgb: Corrixir un derrame de búfer da morea en ficheiros borrosos
- pcx: Corrixir unha quebra en ficheiros borrosos
- xcf: Inicializar x_offset e y_offset
- rgb: Corrixir unha quebra en imaxes borrosas
- pcx: Corrixir unha quebra en imaxes borrosas
- rgb: Corrixir unha quebra en ficheiros borrosos
- xcf: Inicializar o modo da capa
- xcf: Inicializar a opacidade da capa
- xcf: definir o búfer a 0 ao ler menos datos dos esperados
- bzero → memset
- Corrixir varias lecturas e escrituras fóra dos límites en kimg_tga e kimg_xcf
- pic: cambiar de novo o tamaño do identificador da cabeceira se non leu 4 bytes como se esperaba
- xcf: aplicar bzero ao búfer ao ler menos datos dos esperados
- xcf: só chamar a setDotsPerMeterX e setDotsPerMeterY se se atopa PROP_RESOLUTION
- xcf: inicializar num_colors
- xcf: Inicializar a propiedade «visible» da capa
- xcf: non converter int nun enum que non permite o valor do int
- xcf: non desbordar o enteiro nas chamadas a setDotsPerMeterX e setDotsPerMeterY

### KInit

- KLauncher: xestionar que os procesos rematen sen erro (fallo 389678)

### KIO

- Mellorar os controis de teclado do trebello de suma de comprobación
- Engadir unha función de asistencia para desactivar redireccións (útil para kde-open)
- Reverter «Facer cambios internos en SlaveInterface::calcSpeed» (fallo 402665)
- Non configurar a política CMP0028 de CMake co valor vello. Non temos destinos con :: salvo que os importemos.
- [kio] Engadir puntos suspensivos á etiqueta da busca da sección de cookies
- [KNewFileMenu] Non emitir fileCreated ao crear un directorio (fallo 403100)
- Usar (e suxerir usar) K_PLUGIN_CLASS_WITH_JSON, que é mellor
- evitar bloquear kio_http_cache_cleaner e asegurarse de saír coa sesión (fallo 367575)
- Corrixir unha proba de knewfilemenu que fallaba e a causa raíz do fallo

### Kirigami

- elevar só os botóns colorados (fallo 403807)
- corrixir a altura
- a mesma política de tamaño das marxes dos outros elementos de lista
- Operadores ===
- permitir o concepto de elementos expansíbeis
- non baleirar ao substituír todas as páxinas
- a separación horizontal é o dobre que a vertical
- ter en conta o recheo para calcular os consellos de tamaño
- [kirigami] Non usar estilos de fonte claros para as cabeceiras (2/3) (fallo 402730)
- Corrixir a disposición da páxina «Sobre» en dispositivos pequenos
- stopatBounds para o escintilante do ronsel

### KItemViews

- [kitemviews] Cambiar a busca nos comportamentos de escritorio e as actividades para que vaian máis na liña doutras etiquetas de busca

### KJS

- Definir SKIP_AUTOMOC para algúns ficheiros xerados, para solucionar CMP0071

### KNewStuff

- Corrixir a semántica de ghns_exclude (fallo 402888)

### KNotification

- Corrixir unha fuga de memoria ao pasar os datos de iconas a Java
- Retirar a dependencia da biblioteca de apoio AndroidX
- Engadir compatibilidade coa canle de notificacións de Android
- Facer que as notificacións funcionen en Android cun nivel de API &lt; 23
- Mover as comprobacións de nivel da API de Android a tempo de execución
- Retirar unha declaración anticipada sen usar
- Construír AAR de novo cando cambien as fontes de Java
- Construír a parte de Java con Gradle, como AAR en vez de como JAR
- Non confiar na integración dos espazos de traballo de Plasma con Android
- Buscar a configuración de eventos de notificacións nos recursos QRC

### Infraestrutura KPackage

- Facer que as traducións funcionen

### KPty

- Corrixir a falta de coincidencia entre struct e class

### KRunner

- Retirar o uso explícito de ECM_KDE_MODULE_DIR, forma parte de ECM_MODULE_PATH

### KService

- Construír sen D-Bus en Android
- Suxerir que a xente use K_PLUGIN_CLASS_WITH_JSON

### KTextEditor

- Qt 5.12.0 ten problemas co código das expresións regulares de QJSEngine, os sangradores poderían comportarse incorrectamente, Qt 5.12.1 o corrixirá
- KateSpellCheckDialog: Retirar a acción «Selección de corrección ortográfica»
- Actualizar a biblioteca underscore.js de JavaScript á versión 1.9.1
- Corrixir o fallo 403422: Permitir cambiar de novo o tamaño do marcador (fallo 403422)
- Barra de busca: Engadir un botón de «Cancelar» para deter tarefas que duran moito (fallo 244424)
- Retirar o uso explícito de ECM_KDE_MODULE_DIR, forma parte de ECM_MODULE_PATH
- Revisar KateGotoBar
- ViewInternal: Corrixir «Ir ao outro paréntese» no modo de sobreposición (fallo 402594)
- Usar HTTPS nas ligazóns visíbeis para o usuario cando se poida
- Revisar KateStatusBar
- ViewConfig: Engadir unha opción para pegar na posición do cursor co rato (fallo 363492)
- Usar un K_PLUGIN_CLASS_WITH_JSON mellor

### KWayland

- [servidor] Xerar os identificadores de toque correctos
- Facer que XdgTest cumpra coa especificación
- Engadir unha opción para usar wl_display_add_socket_auto
- [servidor] Enviar o org_kde_plasma_virtual_desktop_management.rows inicial
- Engadir información das filas ao protocolo de escritorios virtuais de Plasma
- [cliente] Envolver wl_shell_surface_set_{class,title}
- Protexer da eliminación de recursos en OuptutConfiguration::sendApplied

### KWidgetsAddons

- [KWidgetsAddons] Non usar estilos de fonte claros para as cabeceiras (3/3) (fallo 402730)

### KXMLGUI

- Construír sen D-Bus en Android
- Facer KCheckAccelerators menos invasor para aplicacións que non ligan directamente con KXmlGui

### ModemManagerQt

- Corrixir o código do operador &gt;&gt; de QVariantMapList

### Infraestrutura de Plasma

- [Modelos de fondos de pantalla] Engadir unha entrada Comment= que faltaba no ficheiro de escritorio
- Compartir as instancias de Plasma::Theme entre varios ColorScope
- Facer que as sombras de SVG do reloxo sexan máis lóxicas e axeitadas visualmente (fallo 396612)
- [infraestruturas] Non usar estilos de fonte claros para as cabeceiras (1/3) (fallo 402730)
- [Diálogo] Non alterar a visibilidade de mainItem
- Restabelecer parentItem cando mainItem cambie

### Purpose

- Usar un K_PLUGIN_CLASS_WITH_JSON mellor

### QQC2StyleBridge

- Corrixir o tamaño inicial das listas despregábeis (fallo 403736)
- Facer modais as xanelas emerxentes das listas despregábeis (fallo 403403)
- reverter parcialmente 4f00b0cabc1230fdf
- Usar axuste de palabras nos consellos longos (fallo 396385)
- ComboBox: corrixir o delegado predeterminado
- Falsear a cobertura co rato mentres a lista despregábel estea aberta
- Definir QStyleOptionState == «On» en vez de «Sunken» na lista despregábel para coincidir cos qwidgets (fallo 403153)
- Corrixir a lista despregábel
- Debuxar sempre o consello sobre o resto das cousas
- Permitir un consello nunha zona de rato
- non forzar a visualización do texto de ToolButton

### Solid

- Construír sen D-Bus en Android

### Sonnet

- Non chamar a este código se só temos espazo

### Realce da sintaxe

- Corrixir a fin da rexión de pregado nas regras con lookAhead=true
- AsciiDoc: Corrixir o realce da directiva de inclusión
- Engadir compatibilidade con AsciiDoc
- Corrixir o fallo que causaba un bucle infinito ao realzar ficheiros de KConfig
- comprobar os cambios de contexto infinitos
- Ruby: corrixir a expresión regular tras «: » e corrixir e mellorar a detección de HEREDOC (fallo 358273)
- Haskell: Converter = nun símbolo especial

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
