---
aliases:
- ../../kde-frameworks-5.54.0
date: 2019-01-12
layout: framework
libCount: 70
---
### Attica

- Notificar se non se puido descargar un fornecedor predeterminado

### Baloo

- Mover o asistente de typesForMimeType de BasicIndexingJob a un espazo de nomes anónimo
- Engadir «image/svg» como Type::Image a BasicIndexingJob
- Usar formato compacto de JSON para almacenar os metadatos de documentos

### Iconas de Breeze

- Cambia preferences-system-network a ligazón simbólica
- Usar unha icona de Kolf cun fondo e unhas sombras agradábeis
- Copiar algunhas iconas cambiadas de Breeze a Breeze Escuro
- Engadir a nova icona de YaST e das preferencias
- Engadir unha icona axeitada de código compilado de Python, usan cores consistentes nas iconas de Python (fallo 381051)
- Eliminar as ligazóns simbólicas de código compilado de Python en preparación para que se convertan en iconas de seu
- Usar a icona de tipo MIME de Python como base, e facer que a icona de código compilado de Python sexa unha ligazón a ela
- Engadir iconas de dispositivo para os portos RJ11 e RJ45
- Engadir un separador de directorios que faltaba (fallo 401836)
- Usar a icona correcta para as iconas de scripts de Python 3 (fallo 402367)
- Cambiar as iconas de cor de rede e web por unhas de estilo consistente
- Engadir un novo nome para ficheiros de SQLite de xeito que as iconas se mostren de verdade (fallo 402095)
- Engadir as iconas drive-* para o xestor de particións de YaST
- Engadir a icona view-private (fallo 401646)
- Engadir iconas de acción de lanterna
- Mellorar o simbolismo para as iconas de estado de apagado e silenciado (fallo 398975)

### Módulos adicionais de CMake

- Engadir un módulo de atopar para libphonenumber, de Google

### KActivities

- Corrixir a versión no ficheiro de pkgconfig

### Ferramentas de Doxygen de KDE

- Corrixir a renderización de Markdown de Doxygen

### KConfig

- Escapar os bytes 127 ou superiores nos ficheiros de configuración

### KCoreAddons

- macros de CMake: deixar de usar a variábel obsoleta ECM en kcoreaddons_add_plugin (fallo 401888)
- permitir traducir as unidades e os prefixos de formatValue

### KDeclarative

- non mostrar os separadores en móbiles
- root.contentItem en vez de só contentItem
- Engadir a API que faltaba para que os KCM de varios niveis controlen as columnas

### KFileMetaData

- Facer que falle a proba de escritura se o extractor non é compatíbel co tipo MIME
- Corrixir a extracción de número de disco de APE
- Mellorar a extracción de portadas de ficheiros ASF
- Estender a lista de tipos MIME compatíbeis para o extractor de imaxes incrustadas
- Modificar o extractor de imaxes incrustadas para unha maior facilidade de extensión
- Engadir o tipo MIME de wav que faltaba
- Extraer máis etiquetas dos metadatos de EXIF (fallo 341162)
- corrixir a extracción de altitude de GPS para datos de EXIF

### KGlobalAccel

- Corrixir a construción de KGlobalAccel coa publicación anticipada de Qt 5.13

### KHolidays

- README.md - engadir instrucións básicas para probar ficheiros de vacacións
- varios calendarios - corrixir erros de sintaxe

### KIconThemes

- ksvg2icns: usar a API de QTemporaryDir de Qt ≥ 5.9

### KIdleTime

- [xscreensaverpoller] Descartar tras restablecer o protector de pantalla

### KInit

- Usar un rlimit non ríxido para o número de asas abertas. Isto corrixe o inicio moi lento de Plasma coa última versión de systemd.

### KIO

- Reverter «Agochar a vista previa de ficheiro cando a icona sexa pequena de máis»
- Mostrar o erro en vez de fallar en silencio ao pedirse crear un cartafol que xa existe (fallo 400423)
- Cambiar a ruta de todos os elementos dos subdirectorios ao cambiar o nome dun directorio (fallo 401552)
- Estender o filtro de expresións regulares de getExtensionFromPatternList
- [KRun] Cando se solicita abrir unha ligazón nun navegador externo, usar mimeapps.list como reserva se non hai nada definido en kdeglobals (fallo 100016)
- Facer un pouco máis doado atopar a funcionalidade de abrir un URL nun separador (fallo 402073)
- [kfilewidget] Devolver o navegador de URL editábel ao modo de ronsel se ten o foco e está todo seleccionado e ao premer Ctrl+L
- [KFileItem] Corrixir a comprobación de isLocal en checkDesktopFile (fallo 401947)
- SlaveInterface: Deter speed_timer tras matarse un traballo
- Avisar ao usuario antes dun traballo de copia ou movemento se o tamaño do ficheiro supera o tamaño de ficheiro máximo posíbel no sistema de ficheiros FAT32 (4 GB) (fallo 198772)
- Evitar aumentar constantemente a cola de eventos de Qt nos escravos de KIO
- Compatibilidade con TLS 1.3 (parte de Qt 5.12)
- [KUrlNavigator] Corrixir firstChildUrl ao volver do arquivo

### Kirigami

- Asegurarse de que non sobrepoñemos QIcon::themeName cando non deberíamos
- Introducir un obxecto anexado con DelegateRecycler
- corrixir as marxes da vista de grade tendo en conta as barras de desprazamento
- Facer que AbstractCard.background reaccione a AbstractCard.highlighted
- Simplificar o código de MnemonicAttached
- SwipeListItem: mostrar sempre a icona se !supportsMouseEvents
- Decidir se needToUpdateWidth segundo widthFromItem, non height
- Ter en conta a barra de desprazamento para a marxe de ScrollablePage (fallo 401972)
- Non intentar cambiar de posición a ScrollView cando se recibe unha altura incorrecta (fallo 401960)

### KNewStuff

- Cambiar o criterio de orde predeterminado a «máis descargas» no diálogo de descargas (fallo 399163)
- Notificar de que non se cargase o fornecedor

### KNotification

- [Android] Fallar de maneira máis controlada ao construír cunha API &lt; 23
- Engadir a infraestrutura de notificacións de Android
- Construír sen Phonon nin D-Bus en Android

### KService

- applications.menu: retirar a categoría non usada X-KDE-Edu-Teaching
- applications.menu: retirar &lt;KDELegacyDirs/&gt;

### KTextEditor

- Corrixir o scripting de Qt 5.12
- Corrixir o script de emmet usando números HEX en vez de números OCT nas cadeas (fallo 386151)
- Corrixir Emmet, que estaba roto (fallo 386151)
- ViewConfig: Engadir a opción «Axuste dinámico no marcador estático»
- corrixir o fin da rexión de pregado, engadir o código de fin ao intervalo
- evitar un debuxado excesivo feo con alfa
- Non marcar de novo como incorrectas palabras que se engadiron ao dicionario ou se marcaron para ignorar (fallo 387729)
- KTextEditor: Engadir unha acción para o axuste estático de palabras (fallo 141946)
- Non agochar a acción «Baleirar os intervalos do dicionario»
- Non pedir confirmación ao cargar de novo (fallo 401376)
- clase Message: usar inicialización de membros na clase
- Expoñer KTextEditor::ViewPrivate:setInputMode(InputMode) a KTextEditor::View
- Mellorar o rendemento de pequenas accións de edición, p. ex. corrixir grandes accións de substituílo todo (fallo 333517)
- Só chamar a updateView() en visibleRange() cando endPos() sexa incorrecta

### KWayland

- Engadir unha clarificación sobre usar tanto o ServerDecoration de KDE como XdgDecoration
- Compatibilidade coa decoración de XDG
- Corrixir as instalacións de cabeceira de cliente de XDGForeign
- [servidor] Funcionalidade de arrastrar de maneira táctil
- [servidor] Permitir varias interfaces táctiles por cliente

### KWidgetsAddons

- [KMessageBox] Corrixir o tamaño mínimo de diálogo cando se solicitan os detalles (fallo 401466)

### NetworkManagerQt

- Engadíronse as opcións DCB, macsrc, match, tc, ovs-patch e ovs-port

### Infraestrutura de Plasma

- [Calendario] Expoñer firstDayOfWeek en MonthView (fallo 390330)
- Engadir preferences-system-bluetooth-battery a preferences.svgz

### Purpose

- Engadir o tipo de complementos de compartir enderezos URL

### QQC2StyleBridge

- Corrixir a anchura do elemento de menú cando se sobrepón o delegado (fallo 401792)
- Rotar o indicador de ocupado no sentido horario
- Forzar que as caixas para marcar, mutuamente exclusivas ou non, sexan cadradas

### Solid

- [UDisks2] Usar MediaRemovable para determinar se se pode expulsar o medio
- Compatibilidade coas baterías de Bluetooth

### Sonnet

- Engadir un método a BackgroundChecker para engadir unha palabra á sesión
- DictionaryComboBox: Manter os dicionarios preferidos polo usuario na parte superior (fallo 302689)

### Realce da sintaxe

- Actualizar a compatibilidade coa sintaxe de PHP
- WML: corrixir o código de Lua incrustado e usar novos estilos predeterminados
- Realzar os ficheiros .cu e .cuh de CUDA como C++
- React con TypeScript e TS/JS: mellorar a detección de tipos, corrixir os puntos flotantes e outras melloras e correccións
- Haskell: Realzar os comentarios baleiros tras «import»
- WML: corrixir un bucle infinito en cambios de contexto e só realzar as etiquetas con nomes correctos (fallo 402720)
- BrightScript: engadir unha solución temporal para o realce de «endsub» de Qt Creator 'endsub', engadir función e pregado subordinado
- engadir compatibilidade con máis variantes de números literais de C (fallo 402002)

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
