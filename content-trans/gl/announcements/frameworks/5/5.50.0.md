---
aliases:
- ../../kde-frameworks-5.50.0
date: 2018-09-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Engadir as etiquetas propostas en OCS 1.7

### Baloo

- Corrixiuse un fallo ortográfico na saída de tamaño de índice (fallo 397843)
- Retirar o URL de orixe e non o de destino cando un URL deixa de poder indexarse
- [tags_kio] Simplificar a coincidencia da consulta da ruta do nome de ficheiro usando un grupo de captura
- Reverter «Non poñer na cola ficheiros que xa non se poden indexar e retiralos do índice inmediatamente.»
- [tags_kio] Simplificar a busca de ruta de ficheiro no bucle

### Iconas de Breeze

- Engadir unha icona de ficheiro de proxecto de LabPlot
- ScalableTest, engadir un plasma-browser-integration adaptábel (fallo 393999)

### Módulos adicionais de CMake

- API de Python: comprobar se a API de Python se pode xerar para unha versión concreta de Python
- API de Python: facer o xerador compatíbel con Python 3
- Desactivar a alteración de QT_PLUGIN_PATH por parte de ECM ao executar probas
- API de linguaxe: engadir a posibilidade de usar enumeracións con ámbito (fallo 397154)
- Permitir que ECM detecte ficheiros PO en tempo de configuración

### Integración de infraestruturas

- [KStyle] Usar dialog-question para a icona de pregunta

### KArchive

- xestionar codificacións non ASCII de nomes de ficheiro en arquivos TAR (fallo 266141)
- KCompressionDevice: non chamar a write tras WriteError (fallo 397545)
- Engadir macros Q_OBJECT que faltaban en subclases de QIODevice
- KCompressionDevice: propagar erros de QIODevice::close() (fallo 397545)
- Corrixir a páxina principal de bzip

### KCMUtils

- Usar unha QScrollArea personalizada cun consello de tamaño non limitado polo tamaño da letra (fallo 389585)

### KConfig

- Retirar o aviso sobre unha funcionalidade vella de kiosk que xa non ten sentido
- Definir o atallo predeterminado do sistema Ctrl+0 para a acción «Tamaño real»

### KCoreAddons

- Non retirar o espazo entre dous URL cando a liña comeza con «"» (fallo de KMail)
- KPluginLoader: usar «/» mesmo en Windows, libraryPaths() devolve rutas con «/»
- KPluginMetaData: converter as cadeas baleiras en listas baleiras de cadeas

### KDeclarative

- Reverter «asegurarse de que sempre escribimos no contexto raíz do motor»
- Anexar unha propiedade a «delegate» (fallo 397367)
- [KCM GridDelegate] Usar o efecto de capa só coa infraestrutura de OpenGL (fallo 397220)

### KDocTools

- engadir as siglas ASCII a general.entities
- engadir JSON a general.entities
- Permitir que meinproc5 forneza máis detalles na proba automatizada «install»
- Usar rutas absolutas na proba de kdoctools_install para atopar ficheiros instalados

### KFileMetaData

- Engadir a referencia a enumeración Property::Language para o erro ortográfico Property::Langauge

### KHolidays

- Codificar un algoritmo correcto de cálculo do equinoccio e o solsticio (fallo 396750)
- src/CMakeLists.txt - instalar cabeceiras ao estilo das infraestruturas

### KI18n

- Aseverar se se intenta usar un KCatalog sen un QCoreApplication
- Migrar ki18n de QtScript a QtQml
- Comprobar tamén se po/ existe no directorio de construción

### KIconThemes

- Definir Breeze como tema de iconas de reserva

### KIO

- [KSambaShareData] Aceptar espazos no nome de máquina de ACL
- [KFileItemListProperties] Usar mostLocalUrl para funcionalidades
- [KMountPoint] Tamén comprobar se «smb-share» é ou non unha montaxe de SMB (fallo 344146)
- [KMountPoint] Resolver as montaxes de gvfsd (fallo 344146)
- [KMountPoint] Retirar as trazas de supermount
- [KMountPoint] Retirar a compatibilidade con AIX e Windows CE
- Mostrar os campos de tipo de sistema de ficheiros e orixe da montaxe no diálogo de propiedades (fallo 220976)
- kdirlistertest non falla de maneira aleatoria
- [KUrlComboBox] Corrixir un erro de migración de KIcon
- Migrar o apaño KPATH_SEPARATOR a QDir::listSeparator, engadiuse en Qt 5.6
- Corrixir unha fuga de memoria en KUrlComboBox::setUrl
- [KFileItem] Non ler os comentarios de directorio en montaxes lentas
- Usar QDir::canonicalPath no seu lugar
- Ignorar a marca agochada de NTFS para o volume raíz (fallo 392913)
- Darlle un botón de cancelar ao diálogo de «nome de directorio incorrecto»
- KPropertiesDialog: cambiar a etiqueta en setFileNameReadOnly(true)
- Refinar as palabras coas que se indica que non se puido crear un cartafol cun nome incorrecto
- Usar unha icona axeitada para o botón de cancelar que solicitará un novo nome
- Permitir seleccionar nomes de ficheiros de só lectura
- Usar maiúsculas iniciais nas etiquetas de algúns botóns (en inglés)
- Usar KLineEdit para o nome de cartafol se o cartafol ten acceso de escritura, senón usar QLabel
- KCookieJar: corrixir unha conversión incorrecta de fuso horario

### Kirigami

- permitir fillWidth en elementos
- gardar contra a eliminación externa de páxinas
- mostrar sempre a cabeceira cando estamos en modo pregábel
- corrixir o comportamento de showContentWhenCollapsed
- corrixir buratos en menús no estilo Material
- actionsmenu estándar para o contextmenu da páxina
- Solicitar explicitamente o QtQuick de Qt 5.7 para usar Connections.enabled
- usar a cor de Window en vez de un elemento de fondo
- asegurarse de que o caixón se pecha mesmo tras meter un novo
- exportar separatorvisible a globaltoolbar
- Corrixir a xeración de complementos estáticos de QRC de Kirigami
- Corrixir o modo estático LTO incluído
- Asegurarse de que a propiedade drawerOpen se sincronizou correctamente (fallo 394867)
- drawer: Mostrar o trebello de contido ao arrastrar
- Permitir usar os recursos de qrc en iconas de Actions
- ld en gcc vellos (fallo 395156)

### KItemViews

- Marcar KFilterProxySearchLine como obsoleta

### KNewStuff

- Gardar en caché providerId

### KNotification

- Permitir libcanberra para notificacións de son

### KService

- KBuildSycoca: procesar sempre ficheiros de escritorio de aplicación

### KTextEditor

- Converter a enumeración Kate::ScriptType nunha clase de enumeración
- corrixir a xestión de erros en QFileDevice e KCompressedDevice
- InlineNotes: Non imprimir notas entre liñas
- Retirar QSaveFile en favor da garda de ficheiros tradicional
- InlineNotes: Usar coordenadas globais de pantalla en todas partes
- InlineNote: Inicializar a posición con Cursor::invalid()
- InlineNote: datos de notas entre liñas pimpl sen asignacións
- Engadir unha interface de notas entre liñas
- Mostrar unha vista previa de texto só se a xanela principal está activa (fallo 392396)
- Corrixir unha quebra ao agochar o trebello TextPreview (fallo 397266)
- Fusionar ssh://git.kde.org/ktexteditor
- mellorar a renderización en HiDPI do bordo das iconas
- Mellorar o tema de cores de Vim (fallo 361127)
- Busca: Engadir un apaño para iconas que falten no tema de iconas de Gnome
- corrixir o debuxado sobreposto de _ ou letras como j na última liña (fallo 390665)
- Estender a API de scripting para permitir executar ordes
- Script de sangrado para R
- Fix crash when replacing n around empty lines (bug 381080)
- retirar o diálogo de descarga de salientado
- non hai necesidade de usar new nin delete co hash en cada doHighlight, abonda con baleiralo
- asegurarse de que podemos xestionar índices de atributos incorrectos que poden ocorrer como restos que quedan tras cambiar de realce nun documento
- permitir que o punteiro intelixente xestione a eliminación de obxectos, menos cousas manuais que facer
- retirar o mapa para buscar propiedades de salientado adicionais
- KTextEditor usa a infraestrutura KSyntaxHighlighting para todo
- usar codificacións de caracteres como os fornecen as definicións
- Fusionar a rama «master» con «syntax-highlighting»
- o texto sen letra grosa xa non se debuxa con fonte de grosor fino (fallo 393861)
- usar foldingEnabled
- retirar EncodedCharaterInsertionPolicy
- Impresión: respectar a fonte do pé de páxina, corrixir a posición vertical do pé de páxina, alixeirar visualmente a liña do separador da cabeceira e do pé de páxina
- Fusionar a rama «master» con «syntax-highlighting»
- permitir que a infraestrutura de realce de sintaxe leve a xestión de todas as definicións agora que a definición «Ningunha» está no repositorio
- trebello de completado: corrixir o tamaño de sección de cabeceira mínimo
- Corrección: liñas de vista desprazábel en vez de liñas reais para o desprazamento da rota e da zona táctil (fallo 256561)
- retirar a proba de sintaxe, agora próbase na propia infraestrutura de realce de sintaxe
- A configuración de KTextEditor volve ser local da aplicación, a importación global anterior importarase no primeiro uso
- Usar KSyntaxHighlighting::CommentPosition en vez de KateHighlighting::CSLPos
- Usar isWordWrapDelimiter() de KSyntaxHighlighting
- Cambiar o nome de isDelimiter() a isWordDelimiter()
- crear máis cousas de busca mediante ligazón formato → definición
- agora sempre obtemos formatos válidos
- mellor forma de obter o nome do atributo
- corrixir a proba de sangrado de Python, un accesor máis seguro para as bolsas de propiedades
- engadir de novo os prefixos correctos ás definicións
- Fusionar a rama «syntax-highlighting» de git://anongit.kde.org/ktexteditor con syntax-highlighting
- intentar recuperar as listas necesarias para facer a configuración por esquema
- Usar KSyntaxHighlighting::Definition::isDelimiter()
- facer que poida romper un pouco máis como no código de palabras
- non usar unha lista ligada sen motivo
- limpar a preparación das propiedades
- corrixir a orde dos formatos, lembrar a definición na bolsa de realce
- xestionar formatos incorrectos ou baleiros
- retirar máis parte do código vello, corrixir algúns accesores para usar as cousas de formato
- corrixir o pregamento baseado en sangrado
- retirar a exposición da rima do contexto en doHighlight e corrixir ctxChanged
- comezar a almacenar a cousas de pregamento
- retirar asistentes de salientado que xa non fan falla
- retirar a necesidade de contextNum, engadir o marcador FIXME-SYNTAX ás cousas que hai que apañar de maneira axeitada
- adaptar os cambios de includedDefinitions, retirar contextForLocation, só se necesita ou palabras clave de lugar ou corrección ortográfica de lugar, pode facerse máis tarde
- retirar máis cousas de salientado de sintaxe que xa non se usan
- corrixir m_additionalData e a súa asociación, debería funcionar para atributos, non para o contexto
- crear os atributos iniciais, aínda sen os valores reais dos atributos, só unha lista de algo
- chamar o realce
- derivar do salientador abstracto, estabelecer a definición

### Infraestrutura de KWallet

- Mover un exemplo de TechBase ao repositorio propio

### KWayland

- Sincronizar os métodos set, send e update
- Engadir o número de serie e o identificador de EISA á interface de OutputDevice
- Corrección das curvas de cor do dispositivo de saída
- Corrixir a xestión de memoria en WaylandOutputManagement
- Illar cada proba dentro de WaylandOutputManagement
- Cambio de tamaño fraccionario de OutputManagement

### KWidgetsAddons

- Crear un primeiro exemplo de uso de KMessageBox
- Corrixir dous fallos en KMessageWidget
- [KMessageBox] Chamar ao estilo para a icona
- Engadir un apaño para etiquetas con axuste automático de palabras (fallo 396450)

### KXMLGUI

- Facer que Konqi teña bo aspecto en HiDPI
- Engadir parénteses que faltaban

### NetworkManagerQt

- Requirir NetworkManager 1.4.0 ou posterior
- xestor: engadir a posibilidade de ler ou escribir a propiedade GlobalDnsConfiguration
- Permitir de verdade definir a taxa de actualización para as estatísticas do dispositivo

### Infraestrutura de Plasma

- Aplicar un apaño para o fallo coa renderización e opacidade nativas do texto de TextField (fallo 396813)
- [Elemento de icona] Vixiar o cambio de icona de KIconLoader ao usar QIcon (fallo 397109)
- [Icon Item] Usar ItemEnabledHasChanged
- Retirar o uso obsoleto de QWeakPointer
- Corrixir a folla de estilos de 22-22-system-suspend (fallo 397441)
- Mellorar o texto de retirada e configuración de Widgets

### Solid

- solid/udisks2: Engadir a posibilidade de usar rexistros con categoría
- [Dispositivo de Windows] Só mostrar a etiqueta de dispositivo se a hai
- Forzar unha nova avaliación dos Predicate se se retiran as interfaces (fallo 394348)

### Sonnet

- hunspell: restaurar a construción con hunspell ≤ 1.5.0
- Incluír as cabeceiras de Hunspell como inclusións do sistema

### sindicación

New module

### Realce da sintaxe

- salientar 20 000 liñas por caso de proba
- facer máis reproducíbel a proba de rendemento de realce, de todas formas queremos comparar esta execución co rendemento exterior
- Axustar a busca de KeywordList e evitar as asignacións de grupo de captura implícita
- retirar as capturas de Int, nunca funcionaron
- iteración determinista de probas para unha mellor comparación de resultados
- xestionar atributos incluídos aniñados
- actualizar o salientado de Modula-2 (fallo 397801)
- calcular previamente o formato do atributo para contexto e regras
- evitar a comprobación de delimitador de palabra no inicio das palabras clave (fallo 397719)
- Engadir realce de sintaxe para a linguaxe de políticas de kernel de SELinux
- ocultar bestCandidate, pode ser unha función estática dentro do ficheiro
- Engadir algunhas melloras a kate-syntax-highlighter para o seu uso en scripting
- engadiuse := como unha parte válida do identificador
- usar os nosos propios datos de entrada para facer probas de rendemento
- intentar corrixir o problema de fins de liña na comparación de resultados
- probar a saída de diff trivial para Windows
- engadir defData de novo para a comprobación de validez de estado
- diminuír o espazo de StateData máis dun 50% e a metade do número de chamadas de «malloc» necesarias
- mellorar o rendemento de Rule::isWordDelimiter e KeywordListRule::doMatch
- Mellorar a xestión do desprazamento de salto, permitir saltar unha liña completa cando non haxa coincidencia
- comprobar a lista de caracteres de substitución das extensións
- máis realce de sintaxe de asterisk, probei algunhas configuracións de asterisk, seguen o estilo ini, usar .conf como extensión de ini
- corrixir o salientado de cousas #ifdef _xxx (fallo 397766)
- corrixir os caracteres de substitución nos ficheiros
- Cambiouse a licenza de KSyntaxHighlighting a MIT
- JavaScript: engadir binarios, corrixir os octais, mellorar os escapes e permitir identificadores non ASCII (fallo 393633)
- Permitir apagar as buscas de QStandardPaths
- Permitir instalar ficheiros de sintaxe en vez de telos nun recurso
- xestionar os atributos de cambio de contexto dos propios contextos
- cambiar de biblioteca estática a biblioteca de obxectos coa configuración de imaxe correcta, debería funcionar para combinación de construcións compartidas e estáticas
- evitar calquera asignación no monte de memoria para Format() construído de maneira predeterminada como o que se usa como «incorrecto»
- respectar a variábel de CMake para bibliotecas estáticas ou dinámicas, como p. ex. karchive
- Cambio de licenza a MIT, https://phabricator.kde.org/T9455
- retirar o script vello add_license, xa non se necesita
- Corrixir includedDefinitions, xestionar o cambio de definición ao cambiar de contexto (fallo 397659)
- SQL: varias melloras e corrixir a detección de if, case, loop e end con SQL (Oracle)
- corrixir os ficheiros de referencia
- SCSS: actualizar a sintaxe. CSS: corrixir o realce das etiquetas de operador e selector
- debchangelog: engadir Bookworm
- Cambiar a licenza do Dockerfile a MIT
- retirar a parte de configuración que xa non se permite da corrección ortográfica que sempre tiña un único modo que agora pasou a estar definido manualmente
- Engadir salientado de sintaxe para Stan
- engadir un sangrador cara atrás
- Optimizar moitos ficheiros de realce de sintaxe e corrixir o carácter «/» de SQL
- Liñas de modo: engadir byte-order-mark e pequenas correccións
- Cambiar a licenza de modelines.xml a MIT (fallo 198540)
- Engadir QVector&lt;QPair&lt;QChar, QString&gt;&gt; Definition::characterEncodings() const
- Engadir bool Definition::foldingEnabled() const
- Engadir o salientado «None» ao repositorio de maneira predeterminada
- Actualizar a compatibilidade coa sintaxe da linguaxe Logtalk
- Engadir os formatos de ficheiro sch e brd de EAGLE de Autodesk á categoría XML
- Salientado de C#: preferir o sangrado de C
- AppArmor: actualizar a sintaxe e varias melloras e correccións
- Java: engadir binarios e flotantes hexadecimais e permitir barras baixas nos números (fallo 386391)
- Limpeza: o sangrado moveuse da sección xeral á sección de idioma
- Definición: expoñer os marcadores de orde
- Engadir realce de React con JavaScript
- YAML: corrixir as claves, engadir números ou outras melloras (fallo 389636)
- Engadir bool Definition::isWordWrapDelimiter(QChar)
- Definición: Cambiar o nome de isDelimiter() a isWordDelimiter()
- Apuntar ideas de mellora para a API de KF6 da migración de KTE
- Fornecer un formato correcto tamén para as liñas baleiras
- Facer que Definition::isDelimiter() tamén funcione con definicións incorrectas
- Definición: Expoñer bool isDelimiter() const
- Ordenar os formatos devoltos por Definition::formats() por identificador

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
