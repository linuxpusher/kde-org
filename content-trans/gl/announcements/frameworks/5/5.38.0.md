---
aliases:
- ../../kde-frameworks-5.38.0
date: 2017-09-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Missing in last month's announcement: KF5 includes a new framework, Kirigami, a set of QtQuick components to build user interfaces based on the KDE UX guidelines.

### Baloo

- Corrixir a busca baseada en directorios

### Módulos adicionais de CMake

- Definir CMAKE_*_OUTPUT_5.38 para executar as probas sen instalar
- Incluír un módulo para atopar importacións de QML como dependencias de tempo de execución

### Integración de infraestruturas

- Devolver unha icona de baleirar de liña de edición de alta resolución
- Corrixir a aceptación de diálogos con Ctrl+Intro cando se cambian os nomes dos botóns

### KActivitiesStats

- Reescribir a consulta que combina recursos ligados e usados
- Cargar de novo o modelo cando deixa de ligarse o recurso
- Corrixiuse a consulta ao mesturar recursos ligados e usados

### KConfig

- Corrixir as etiquetas das accións DeleteFile e RenameFile (fallo 382450)
- kconfigini: Retirar espazos ao principio ao ler valores de entradas (fallo 310674)

### KConfigWidgets

- Marcar KStandardAction::Help e KStandardAction::SaveOptions como obsoletas
- Corrixir as etiquetas das accións DeleteFile e RenameFile (fallo 382450)
- Usar «document-close» como icona para KStandardAction::close

### KCoreAddons

- DesktopFileParser: engadir «:/kservicetypes5/*» como busca de reserva
- Engadir a posibilidade de complementos desinstalados en kcoreaddons_add_plugin
- desktopfileparser: corrixir unha análise incorrecta de claves e valores (fallo 310674)

### KDED

- engadir compatibilidade con X-KDE-OnlyShowOnQtPlatforms

### KDocTools

- CMake: corrixir como se acurta o nome de destino cando o directorio de construción ten caracteres especiais (fallo 377573)
- Engadir CC BY-SA 4.0 Internacional e definilo como predeterminado

### KGlobalAccel

- KGlobalAccel: migrar ao novo método symXModXToKeyQt de KKeyServer para corrixir as teclas numéricas (fallo 183458)

### KInit

- klauncher: corrixir a correspondencia de appId para aplicacións de flatpak

### KIO

- Migrar o KCM de atallos web de KServiceTypeTrader a KPluginLoader::findPlugins
- [KFilePropsPlugin] Aplicar a configuración rexional a totalSize durante o cálculo
- KIO: corrixir unha vella fuga de memoria na saída
- Engadir funcionalidades de filtro de tipo MIME a KUrlCompletion
- KIO: migrar os complementos de filtro de URI de KServiceTypeTrader a JSON con KPluginMetaData
- [KUrlNavigator] Emitir tabRequested ao facer clic central nun lugar do menú (fallo 304589)
- [KUrlNavigator] Emitir tabRequested cando se fai clic central no selector de lugares (fallo 304589)
- [KACLEditWidget] Permitir clic duplo para editar entradas
- [kiocore] Corrixir o erro de lóxica da remisión anterior
- [kiocore] Comprobar se klauncher se está a executar
- Limitar de verdade a taxa de mensaxes de INF_PROCESSED_SIZE (fallo 383843)
- Non baleirar o almacenamento de certificados de CA de SSL de Qt
- [KDesktopPropsPlugin] Crear o directorio de destino se non existe
- [Escravo de KIO de ficheiro] Corrixir a aplicación de atributos de ficheiro especiais (fallo 365795)
- Retirar a comprobación de bucle ocupado en TransferJobPrivate::slotDataReqFromDevice
- facer kiod5 un «axente» en Mac
- Corrixir que o KCM de proxy not loading manual proxies correctly

### Kirigami

- ocultar as barras de desprazamento cando non serven
- Engadir un exemplo básico de como axustar a asa arrastrábel de anchura de columna
- colocación de capas ider en asas
- corrixir a colocación das asas ao sobrepor a última páxina
- non mostrar unha asa falsa na última columna
- non almacenar cousas nos delegados (fallo 383741)
- dado que xa definimos keyNavigationEnabled, definir wraps tamén
- mellor aliñamento á esquerda para o botón de atrás (fallo 383751)
- non ter en conta a cabeceira dúas veces ao desprazar (fallo 383725)
- nunha dividir en liñas as etiquetas de cabeceira
- solucionar FIXME: retirar resetTimer (fallo 383772)
- non ocultar a cabeceira ao desprazar fóra de móbiles
- Engadir unha propiedade para agochar o separador de PageRow que coincide con AbstractListItem
- corrixir o desprazamento co fluxo de originY e bottomtotop
- Retirar avisos sobre definir tamaños tanto de píxel como de punto
- non causar o modo alcanzábel en vistas invertidas
- ter en conta o pé da páxina
- engadir un exemplo complexo algo máis complexo dunha aplicación de conversa
- máis protección para atopar o pé correcto
- Comprobar a validez dos elementos antes de usalos
- Respectar a posición da capa para isCurrentPage
- usar unha animación en vez de un animador (fallo 383761)
- deixar o espazo necesario para o pé da páxina, se se pode
- mellor escurecemento para os caixóns de applicationitem
- escurecemento do fondo para applicationitem
- corrixir de verdade as marxes do botón de volver
- marxes axeitadas para o botón de volver
- menos avisos en ApplicationHeader
- non usar o cambio de tamaño de Plasma para os tamaños das iconas
- nova aparencia para as asas

### KItemViews

### KJobWidgets

- Inicializar o estado do botón de «Pausa» no seguidor de trebellos

### KNotification

- Non bloquear o inicio do servizo de notificacións (fallo 382444)

### Infraestrutura KPackage

- retirar as opcións de cadeas de kpackagetool

### KRunner

- Baleirar as accións anteriores ao actualizar
- Engadir executores remotos mediante D-Bus

### KTextEditor

- Migrar a API de scripting de Document e View a unha solución baseada en QJSValue
- Mostrar as iconas no menú contextual do bordo da icona
- Substituír KStandardAction::PasteText por KStandardAction::Paste
- Permitir un cambio de dimensións fraccionario ao xerar a vista previa da barra lateral
- Cambiar de QtScript a QtQml

### KWayland

- Tratar os búferes de RGB de entrada como multiplicados previamente
- Actualizar as saídas de SurfaceInterface cando se destrúe unha global de saída
- KWayland::Client::Surface facer seguimento da destrución da saída
- Evitar enviar ofertas de datos dunha fonte incorrecta (fallo 383054)

### KWidgetsAddons

- simplificar setContents permitindo a Qt facer unha parte maior do traballo
- KSqueezedTextLabel: Engadir isSqueezed() por comodidade
- KSqueezedTextLabel: pequenas melloras na documentación da API
- [KPasswordLineEdit] Definir a edición de liña como proxy de foco (fallo 383653)
- [KPasswordDialog] Restabelecer a propiedade geometry

### KWindowSystem

- KKeyServer: corrixir a xestión de KeypadModifier (fallo 183458)

### KXMLGUI

- Aforrar varias chamadas a stat() calls ao iniciar a aplicación
- Corrixir a posición de KHelpMenu en Wayland (fallo 384193)
- Retirar xestións rotas de clics de botón central (fallo 383162)
- KUndoActions: usar actionCollection para definir o atallo

### Infraestrutura de Plasma

- [ConfigModel] Gardar contra engadir unha ConfigCategory nula
- [ConfigModel] Permitir engadir e retirar ConfigCategory programaticamente (fallo 372090)
- [EventPluginsManager] Expoñer pluginPath no modelo
- [Icon Item] Non retirar a definición de imagePath sen necesidade
- [FrameSvg] Usar QPixmap::mask() en vez de unha maneira complexa e obsoleta mediante alphaChannel()
- [FrameSvgItem] Crear os obxectos margins e fixedMargins baixo demanda
- corrixir o estado de comprobación dos elementos de menú
- Forzar o estilo de Plasma para os miniaplicativos de QQC2
- Instalar o cartafol PlasmaComponents.3/private
- Retirar o que quedaba dos temas «locolor»
- [Theme] Usar SimpleConfig de KConfig 
- Evitar algunhas buscas de contido de tema innecesarias
- ignorar eventos espurios de cambio de tamaño a tamaños baleiros (fallo 382340)

### Realce da sintaxe

- Engadir unha definición de sintaxe para listas de filtros de Adblock Plus
- Reescribir a definición de sintaxe de Sieve
- Engadir salientado para os ficheiros de configuración de QDoc
- Engadir unha definición de salientado para Tiger
- Escapar os guións nas expresións regulares de rest.xml (fallo 383632)
- corrección: o texto simple saliéntase como PowerShell
- Engadir salientado de sintaxe para Metamath
- Redefiniuse o realce de sintaxe de Less en base ao de SCSS (fallo 369277)
- Engadir realce de Pony
- Reescribir a definición de sintaxe de correo electrónico

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
