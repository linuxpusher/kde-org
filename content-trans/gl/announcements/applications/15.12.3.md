---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE publica a versión 15.12.3 das aplicacións de KDE
layout: application
title: KDE publica a versión 15.12.3 das aplicacións de KDE
version: 15.12.3
---
March 15, 2016. Today KDE released the third stability update for <a href='../15.12.0'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 15 correccións de erros inclúen melloras en, entre outros, KDE PIM, Ark, Okteta, Umbrello e KMines.

This release also includes Long Term Support version of KDE Development Platform 4.14.18.
