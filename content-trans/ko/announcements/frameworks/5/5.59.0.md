---
aliases:
- ../../kde-frameworks-5.59.0
date: 2019-06-08
layout: framework
libCount: 70
---
### Baloo

- SQL 데이터베이스 덤프를 색인에 추가하지 않음
- .gcode와 가상 머신 파일을 색인 작업에서 제외

### BluezQt

- D-Bus XML 파서/생성기에 BlueZ API 추가

### Breeze 아이콘

- gcompris-qt도 포함
- Falkon 아이콘을 진짜 SVG로 업데이트
- 앱의 빠진 아이콘 추가, 재작업 필요 https://bugs.kde.org/show_bug.cgi?id=407527
- 앱에서 KFourinline 아이콘 추가, 업데이트 필요
- Kigo 아이콘 추가 https://bugs.kde.org/show_bug.cgi?id=407527
- KWave에서 KWave 아이콘 추가, Breeze 스타일로 재작업 필요
- Symlink arrow-_-double to go-_-skip, add 24px go-*-skip
- input-* 장치 아이콘 스타일 업데이트, 16픽셀 아이콘 추가
- 이전 커밋에서 빠진 새로운 Knights 아이콘의 어두운 버전 추가
- Anjuta 아이콘을 기반으로 Knights 아이콘 제작(버그 407527)
- Breeze에 빠진 앱 아이콘 추가. Breeze에 더 어울리게 업데이트해야 하지만 kde.org/applications 사이트의 새로운 버전에 필요함
- kde:kxstitch에서 kxstitch 아이콘 사용, 업데이트 필요
- 모든 항목과 잡다한 것들을 글로브(glob)에 포함시키지 않음
- ScaledDirectories 조건 가정 설정

### 추가 CMake 모듈

- Qt 로그 분류 파일 특수 디렉터리 생성
- Windows에서 QT_STRICT_ITERATORS 활성화하지 않음

### 프레임워크 통합

- 기존 위치에서도 검색 활성화
- knsrc 파일의 새로운 위치에서 검색

### KArchive

- KCompressionDevice에서 읽기와 검색 시험
- KCompressionDevice: bIgnoreData 삭제
- KAr: QByteArray로 이식하여 경계를 초과하는 곳에서 읽기(및 잘못된 입력) 수정
- KAr: Qt 5.10에서 긴 파일 이름 처리 수정
- KAr: 권한 정보를 10진수 대신 8진수로 표현
- KAr::openArchive: ar_longnamesIndex가 0 미만이 아닌지 확인
- KAr::openArchive: 깨진 파일에서 잘못된 메모리 접근 수정
- KAr::openArchive: 잘못된 파일에서 힙 버퍼 넘침 보호
- KTar::KTarPrivate::readLonglink: 잘못된 파일에서 충돌 문제 수정

### KAuth

- D-Bus 정책 설정 디렉터리를 하드코딩하지 않음

### KConfigWidgets

- 기부하기 아이콘에 로캘별 통화 사용

### KCoreAddons

- Python 바인딩 컴파일 수정(버그 407306)
- 현재 활성화된 프로세스 목록을 가져오는 GetProcessList 추가

### KDeclarative

- qmldir 파일 수정

### KDELibs 4 지원

- QApplication::setColorSpec 삭제(빈 메서드)

### KFileMetaData

- 실수형 값을 표시할 때 유효 숫자 3개 표시(버그 343273)

### KIO

- 글자 단위 대신 바이트 단위로 수정
- KDE_FORK_SLAVES를 설정했을 때 kioslave 실행 파일이 끝나지 않는 오류 수정
- 파일이나 디렉터리를 향한 desktop 링크 수정(버그 357171)
- 새 필터를 적용하기 전에 현재 필터 시험(버그 407642)
- [kioslave/file] 레거시 파일 이름 코덱 추가(버그 165044)
- 시스템 정보를 가져올 때 QSysInfo 사용
- 기본 장소 목록에 문서 추가
- kioslave: 리눅스가 아닌 환경에서 applicationDirPath() 문제를 수정하기 위하여 argv[0] 보존
- KDirOperator에 파일이나 폴더 한 개 드롭 허용(버그 45154)
- 링크를 만들기 전에 긴 파일 이름 자르기(버그 342247)

### Kirigami

- [ActionTextField] QML 풍선 도움말을 일관성 있게 변경
- 위쪽 패딩을 두어야 하는 항목의 높이 사용(버그 405614)
- 성능: QTimer를 사용하지 않고 색 변경 압축
- [FormLayout] 구분자의 위, 아래 간격 동일하게 유지(버그 405614)
- ScrollablePage: 스크롤 가능한 보기에 초점이 설정되었을 때 초점을 갖도록 수정(버그 389510)
- 도구 모음에서 키보드만으로 탐색하는 상황 개선(버그 403711)
- recycler를 FocusScope로 생성

### KNotification

- desktopFileName 속성에 파일 이름 접미사를 설정하는 앱 처리

### KService

- 때때로 다른 프로세스에서 파일을 삭제했을 때 assert (hash != 0) 가정 수정
- 열린 파일이 사라졌을 때의 가정 설정 수정: ASSERT: "ctime != 0"

### KTextEditor

- 0번째 위치에서 Backspace 키를 눌렀을 때 이전 줄 전체를 삭제하지 않음(버그 408016)
- 네이티브 대화 상자에서 덮어쓰기 확인 사용
- 글꼴 크기 초기화 동작 추가
- 요청한 경우 항상 정적 줄 바꿈 표시자 표시
- 접기를 펼쳤을 때에도 강조된 범위 시작/끝 표시기 유지
- 수정: 일부 파일을 저장할 때 구문 강조 초기화하지 않음(버그 407763)
- 자동 들여쓰기: QList 대신 std::vector 사용
- 수정: 새 파일에 기본 들여쓰기 모드 사용(버그 375502)
- 중복된 할당 삭제
- 괄호 균형을 검사할 때 자동 괄호 설정 존중
- 불러올 때 잘못된 문자 검사 개선(버그 406751)
- 상태 표시줄에 새로운 구문 강조 메뉴
- "포함하는 노드 전환"에서 무한 반복 방지

### KWayland

- 컴포지터가 단독 축 값을 보낼 수 있도록 허용(버그 404152)
- set_window_geometry 구현
- wl_surface::damage_buffer 구현

### KWidgetsAddons

- KNewPasswordDialog: 메시지 위젯에 주기 추가

### NetworkManagerQt

- 생성 시 장치 통계를 가져오지 않음

### Plasma 프레임워크

- Breeze 어두움/밝음에서 더 많은 시스템 색을 사용하도록 수정
- QML로 SortFilterModel 정렬 열 내보내기
- plasmacore: qmldir 수정, ToolTip.qml이 더 이상 모듈의 일부가 아님
- 모든 애플릿에서 availableScreenRectChanged 시그널 송신
- plasmacomponents3  파일을 생성할 때 간단히 configure_file 사용
- *.qmltypes 파일을 QML 모듈의 현재 API로 업데이트
- FrameSvg: clearCache() 호출 시 마스크 캐시도 비움
- FrameSvg: hasElementPrefix()에서 접두사 뒤에 - 문자가 붙을 때에도 처리하도록 수정
- FrameSvgPrivate::generateBackground: reqp != p일 때에도 background 생성
- FrameSvgItem: geometryChanged()에서도 maskChanged 시그널 송출
- FrameSvg: 프레임을 만들지 않은 상태에서 mask()를 호출했을 때 충돌 방지
- FrameSvgItem: 항상 doUpdate()에서 maskChanged  시그널 송신
- API dox: FrameSvg::prefix()/actualPrefix()를 호출했을 때 '-'가 붙는 상황 메모
- API dox: TechBase에서 사용할 수 있는 경우 Plasma 5 버전을 가리키도록 변경
- FrameSvg: l, r이나 t, b 경계선의 높이나 너비가 더 이상 동일할 필요가 없음

### Purpose

- [JobDialog] 사용자가 창을 닫았을 때 취소 시그널 전송
- 설정 대화 상자를 취소할 때 오류 발생으로 보고(버그 407356)

### QQC2StyleBridge

- DefaultListItemBackground 및 MenuItem 애니메이션 삭제
- [QQC2 Slider Style] 초기 값이 1일 때 잘못된 핸들 위치 수정(버그 405471)
- ScrollBar: 수평 스크롤 바로도 작동하도록 수정(버그 390351)

### Solid

- 장치 백엔드를 생성 및 등록하는 방법 재작업
- [Fstab] 암호화된 FUSE 마운트를 표시할 때 folder-decrypted 아이콘 사용

### 구문 강조

- YAML: 공백 문자 다음에 주석만 오는 경우 및 기타 개선/버그 수정(버그 407060)
- Markdown: 코드 블록에서 includeAttrib 사용
- fix highlighting of "0" in C mode
- Tcsh: 연산자와 키워드 수정
- 공통 중간 언어(Common Intermediate Language) 구문 강조 정의 추가
- 구문 강조 도구: 특별한 구문 강조가 적용되지 않은 텍스트 글자색 수정(버그 406816)
- 강조된 텍스트를 PDF로 출력하는 예제 추가
- Markdown: 목록에 고대비 색상 사용(버그 405824)
- "INI 파일" 구문 강조에서 .conf 확장자 삭제, MIME 형식으로 구문 강조 형식 설정(버그 400290)
- Perl: // 연산자 버그 수정(버그 407327)
- Julia 구문 강조의 UInt* 형식 지정 수정(버그 407611)

### 보안 정보

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
