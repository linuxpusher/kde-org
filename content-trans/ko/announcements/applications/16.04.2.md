---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE에서 KDE 프로그램 16.04.2 출시
layout: application
title: KDE에서 KDE 프로그램 16.04.2 출시
version: 16.04.2
---
June 14, 2016. Today KDE released the second stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Akonadi, Ark, Artikulate, Dolphin, Kdenlive, kdepim 등에 25개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.21.
