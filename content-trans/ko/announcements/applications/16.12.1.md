---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE에서 KDE 프로그램 16.12.1 출시
layout: application
title: KDE에서 KDE 프로그램 16.12.1 출시
version: 16.12.1
---
January 12, 2017. Today KDE released the first stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

iCal 자원에 std.ics가 없는 경우 생성하지 않아서 데이터가 손실되는 버그를 수정했습니다.

kdepim, Ark, Gwenview, Kajongg, Okular, Kate, Kdenlive 등에 40개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.28.
