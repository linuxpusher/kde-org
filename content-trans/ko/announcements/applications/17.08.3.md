---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE에서 KDE 프로그램 17.08.3 출시
layout: application
title: KDE에서 KDE 프로그램 17.08.3 출시
version: 17.08.3
---
November 9, 2017. Today KDE released the third stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Ark, Gwenview, KGpg, KWave, Okular, Spectacle 등에 10개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes the last version of KDE Development Platform 4.14.38.

개선 사항:

- Samba 4.7에서 암호로 보호된 SMB 공유를 사용할 때 문제점 우회
- Okular의 특정 회전 작업에서의 충돌 문제 해결
- Ark에서 ZIP 압축 파일을 풀 때 파일 수정한 날짜 유지
