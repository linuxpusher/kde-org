---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE에서 KDE 프로그램 16.04.3 출시
layout: application
title: KDE에서 KDE 프로그램 16.04.3 출시
version: 16.04.3
---
July 12, 2016. Today KDE released the third stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Ark, Cantor, Kate, kdepim, Umbrello 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.22.
