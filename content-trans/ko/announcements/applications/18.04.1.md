---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE에서 KDE 프로그램 18.04.1 출시
layout: application
title: KDE에서 KDE 프로그램 18.04.1 출시
version: 18.04.1
---
May 10, 2018. Today KDE released the first stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Cantor, Dolphin, Gwenview, JuK, Okular, Umbrello 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- Dolphin의 장소 패널에 중복된 항목이 있어도 더 이상 충돌하지 않음
- Gwenview에서 SVG 파일을 다시 불러올 때 발생하는 오래된 버그를 수정함
- Umbrello에서 C++를 가져올 때 'explicit' 키워드를 처리함
