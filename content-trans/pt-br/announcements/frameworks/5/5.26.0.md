---
aliases:
- ../../kde-frameworks-5.26.0
date: 2016-09-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Adição do Qt5Network como dependência pública

### BluezQt

- Correcção da pasta de inclusão no ficheiro 'pri'

### Ícones Breeze

- Adição de definições de prefixos de espaços de nomes em falta
- Verificação da boa-formação dos ícones SVG
- Correcção de todos os ícones de editar-limpar-localização-ltr (erro 366519)
- adição do suporte de ícones de efeitos do KWin
- Mudança do nome de 'caps-on' no 'input-caps-on'
- adição de ícones de capitalização na introdução de texto
- adição de alguns ícones específicos do Gnome por Sadi58
- adição de ícones aplicacionais do 'gnastyle'
- Ícones do Dolphin, Konsole e Umbrello optimizados para 16px, 22px, 32px
- Actualização do ícone do VLC para 22px, 32px e 48px
- Adição do ícone da aplicação Subtitle Composer
- Correcção do novo ícone do Kleopatra
- Adição de ícone da aplicação Kleopatra
- Adição de ícones para o Wine e o Wine-Qt
- correcção de erro nos ícones das apresentações, graças ao Sadi58 (erro 358495)
- adição do ícone 'system-log-out' com 32px
- adição de ícones de sistema a 32px, remoção dos ícones coloridos do sistema
- adição do suporte de ícones das aplicações Pidgin e Banshee
- remoção do ícone da aplicação VLC devido à licença, adição de novo ícone do VLC (erro 366490)
- adição do suporte de ícones do 'gthumb'
- uso do HighlightedText (texto realçado) nos ícones das pastas
- os ícones das pastas de locais agora usam uma folha de estilo (cor de realce)

### Módulos extra do CMake

- ecm_process_po_files_as_qm: Ignorar as traduções aproximadas
- O nível de categorias de depuração por omissão deverá ser o 'Info' em vez do 'Warning'
- Documentação da variável ARGS nos alvos 'create-apk-*'
- Criação de um teste que valide a informação do AppStream dos projectos

### Ferramentas Doxygen do KDE

- Adição de uma condição se as plataformas do grupo não estão definidas
- Modelo: Ordenação alfabética das plataformas

### KCodecs

- Migração da 'kdelibs' do ficheiro usado para gerar o 'kentities.c'

### KConfig

- Adição do item de Doação ao KStandardShortcut

### KConfigWidgets

- Adição da acção-padrão de Doação

### KDeclarative

- [kpackagelauncherqml] Assumir que o nome do ficheiro .desktop é o mesmo que o 'PluginId'
- Carregamento da configuração do desenho do QtQuick a partir de um ficheiro de configuração e mudar para predefinido
- icondialog.cpp - correcção adequada de compilação que não esconda o 'm_dialog'
- Correcção do estoiro quando não está disponível nenhuma QApplication
- exposição do domínio da tradução

### KDELibs 4 Support

- Correcção de erro de compilação do Windows no kstyle.h

### KDocTools

- Adição de locais de configuração, cache + dados ao 'general.entities'
- Actualização com a versão em Inglês
- Adição de entidades de teclas 'Space' e 'Meta' ao 'src/customization/en/user.entities'

### KFileMetaData

- Só necessitar do Xattr se o sistema operativo for o Linux
- Reposição da compilação em Windows

### KIdleTime

- [xsync] XFlush no simulateUserActivity

### KIO

- KPropertiesDialog: remoção de nota de aviso da documentação, porque o erro já não existe
- [programa de teste] resolução de locais relativos com o QUrl::fromUserInput
- KUrlRequester: correcção de mensagem de erro ao seleccionar um ficheiro e ao reabrir a janela de ficheiros
- Oferta de uma alternativa se os 'slaves' não apresentarem o item '.' (erro 366795)
- Correcção da criação de ligação simbólica sobre o protocolo "desktop"
- KNewFileMenu: ao criar ligações simbólicas, usar o KIO::linkAs em vez do KIO::link
- KFileWidget: correcção de duplo '/' na localização
- KUrlRequester: usar a sintaxe estática do connect() por ser inconsistente anteriormente
- KUrlRequester: passagem do window() como item-pai do QFileDialog
- evitar a invocação do connect(null, .....) a partir do KUrlComboRequester

### KNewStuff

- descompressão de pacotes em sub-pastas
- Não permitir mais instalar na pasta de dados genérica devido a um potencial erro de segurança

### KNotification

- Obter a propriedade ProtocolVersion do StatusNotifierWatcher de forma assíncrona

### Package Framework

- silenciar os avisos de descontinuação do 'contentHash'

### Kross

- Reversão da "Remoção das dependências do KF5 não usadas"

### KTextEditor

- remoção de conflito de aceleradores (erro 363738)
- correcção do realce de sintaxe dos endereços de e-mail no Doxygen (erro 363186)
- Detecção de mais alguns ficheiros JSON, como os nossos próprios projectos ;)
- melhoria na detecção de tipos MIME (erro 357902)
- Erro 363280 - realce: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (erro 363280)
- Erro 363280 - realce: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif
- Erro 351496 - A dobragem de Python não funciona bem durante a escrita inicial (erro 351496)
- Erro 365171 - Realce da sintaxe de Python: não funciona correctamente com as sequências de escape (erro 365171)
- Erro 344276 - o 'nowdoc' do PHP não é dobrado correctamente (erro 344276)
- Erro 359613 - Algumas propriedades do CSS3 não são suportadas no realce de sintaxe (erro 359613)
- Erro 367821 - sintaxe do wineHQ: A secção num ficheiro 'reg' não é realçada correctamente (erro 367821)
- Melhoria no tratamento de ficheiros de memória temporária se for indicada a pasta temporária
- Correcção de estoiro ao recarregar documentos com linhas mudadas automaticamente pelo limite no tamanho da linha (erro 366493)
- Correcção de estoiros constantes relacionados com a barra de comandos do 'vi' (erro 367786)
- Correcção: Os números de linha nos documentos impressos começam agora em 1 (erro 366579)
- Salvaguarda de Ficheiros Remotos: Tratar os ficheiros montados também como ficheiros remotos
- Limpeza da lógica na criação da barra de pesquisa
- adição do realce de sintaxe do Magma
- Permite apenas um nível de recursividade
- Correcção de ficheiro de memória virtual danificado no Windows
- Modificação: adição do suporte ao Bitbake para o motor de realce de sintaxe
- parêntesis automáticos: pesquisa do atributo de ortografia onde foi introduzido o carácter (erro 367539)
- Realce do QMAKE_CFLAGS
- Não sair do contexto principal
- Adição de alguns nomes de executáveis que são usados normalmente

### KUnitConversion

- Adição da unidade britânica de massa "Pedra"

### KWallet Framework

- Passagem do DocBook do 'kwallet-query' para a sub-pasta correcta docbook to correct subdir
- Correcção da palavras 'an' -&gt; 'one (um)

### KWayland

- Tornar o uso na compilação do 'linux/input' opcional

### KWidgetsAddons

- Correcção do fundo dos caracteres não-BMP
- Adição da pesquisa em UTF-8 no formato octal em C
- Gravar por omissão o KMessageBoxDontAskAgainMemoryStorage no QSettings

### KXMLGUI

- Migração para a acção por omissão de Doação
- Migração da chamada antiga 'authorizeKAction'

### Plasma Framework

- Correcção do ícone de dispositivo a 22px, por não funcionar no ficheiro antigo
- WindowThumbnail: Invocação das chamadas do GL na tarefa correcta (erro 368066)
- Fazer com que o 'plasma_install_package' funcione com o KDE_INSTALL_DIRS_NO_DEPRECATED
- adição de margens e preenchimento ao ícone start.svgz
- correcções na folha de estilo do ícone do computador
- adição de ícones de computador e portátil ao Kicker (erro 367816)
- Correcção do aviso 'não é possível atribuir um valor indefinido a um double' no DayDelegate
- correcção ficheiros .svgz com folhas de estilo com problemas
- mudança de nome dos ícones 22px para '22-22-x' e dos ícones a 32px para 'x' no Kicker
- [Campo de Texto dos Componentes Plasma] Não se incomodar a carregar os ícones dos botões não usados
- Validação extra no Containment::corona no caso especial da bandeja do sistema
- Ao marcar um contentor como removido, marcar da mesma forma todas as suas sub-applets - corrige o facto de as configurações do contentor da bandeja não serem removidas
- Correção do ícone de Notificação de dispositivos
- adição do 'system-search' ao sistema nos tamanhos 32 e 22px
- adição de ícones monocromáticos para o Kicker
- Mudança do esquema de cores no ícone do 'system-search'
- Passagem do 'system-search' para o system.svgz
- Correcção do "X-KDE-ParentApp" errado ou em falta nas definições do ficheiro .desktop
- Correção da API dox do Plasma::PluginLoader: confusão com applets/dataengine/services/..
- adição do ícone 'system-search' no tema do SDDM
- adição de ícone a 32px do Nepomuk
- actualização do ícone do rato por toque na bandeja do sistema
- Remoção de código que nunca será executado
- [Contentor] Mostrar os painéis quando a UI fica pronta
- Não declarar de novo a propriedade 'implicitHeight'
- uso do QQuickViewSharedEngine::setTranslationDomain (erro 361513)
- adição do suporte de ícones Brisa a 22px e 32px ao Plasma
- remoção de ícones coloridos do sistema e adição de ícones monocromáticos a 32px
- Adição de um botão opcional de revelação da senha no TextField
- As dicas-padrão ficam agora invertidas quando usar uma língua da direita-para-a-esquerda
- A performance ao mudar de meses no calendário foi bastante melhorada

### Sonnet

- Não converter para minúsculas os nomes das línguas no processamento de trigramas
- Correcção de estoiro imediato no arranque por um ponteiro para 'plugin' nulo
- Tratamento dos dicionários sem nomes correctos
- Substituição da lista actualizada manualmente de mapeamentos de línguas, utilização de nomes adequados para as línguas
- Adição de ferramenta para gerar trigramas
- Correcção ligeira da detecção de línguas
- Uso da língua seleccionada como sugestão para a detecção
- Usar os verificadores ortográficos em 'cache' na detecção da língua, o que melhora um bocado a performance
- Melhoria na detecção de línguas
- Filtrar a lista de sugestões face aos dicionários disponíveis, removendo os duplicados
- Recordar a adição da última correspondência de trigramas
- Verificação de algum dos trigramas correspondeu de facto
- Lidar com várias línguas com a mesma pontuação na correspondência de trigramas
- Não verificar duas vezes o tamanho mínimo
- Limpeza da lista de línguas face às línguas disponíveis
- Usar o mesmo tamanho mínimo em todo o lado na detecção de línguas
- Verificação de segurança que o modelo carregado tem a mesma quantidade de trigramas para cada língua

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
