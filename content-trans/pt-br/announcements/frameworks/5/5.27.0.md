---
aliases:
- ../../kde-frameworks-5.27.0
date: 2016-10-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Novos ícones de tipos MIME.

{{<figure src="/announcements/frameworks/5/5.27.0/kf-5.27-mimetypes-icons.png" >}}

### Baloo

- Usar a configuração correcta na condição de auto-arranque
- Correcção da inserção ordenada (inserção à 'flat_map') (erro 367991)
- Adição do fecho de ambiente em falta, como referido por Loïc Yhuel (erro 353783)
- Transacção não criada =&gt; não tentar interrompê-la
- Correcção da atribuição em falta 'm_env = nullptr'
- Tornar p.ex. o Baloo::Query seguro em multi-tarefa
- Nos sistemas a 64-bits, o Baloo permite agora um armazenamento no índice &gt; 5 GB (erro 364475)
- Permitir o ctime/mtime == 0 (erro 355238)
- Lidar com a corrupção da base de dados do índice do 'baloo_file', tentando criar de novo a base de dados ou interrompendo se isso falhar

### BluezQt

- Correcção de um estoiro ao tentar adicionar um dispositivo a um adaptador desconhecido (erro 364416)

### Ícones Breeze

- Novos ícones de tipos MIME
- Actualização de alguns ícones do KStars (erro 364981)
- Estil errado do 'actions/24/format-border-set' (erro 368980)
- Adição do ícone das aplicações Wayland
- Adição do ícone das aplicações X11 (erro 368813)
- Anulação do 'distribute-randomize', calendário + nova aplicação da correcção de transformação (erro 367082)
- A mudança dos documentos de uma pasta de um ficheiro para vários numa pasta, faz com que seja incluído mais que um ficheiro (erro 368224)

### Módulos extra do CMake

- Certificação de que não é adicionado o teste do AppStream duas vezes

### KActivities

- Ordenação das actividades na 'cache' alfabética pelo nome (erro 362774)

### Ferramentas Doxygen do KDE

- Muitas alterações à disposição global da documentação gerada da API
- Correcção do local das marcas, dependendo se a biblioteca faz parte de um grupo ou não
- Pesquisa: Correcção do 'href' das bibliotecas que não façam parte de um grupo

### KArchive

- Correcção de fuga de memória com o KCompressionDevice do KTar
- KArchive: correcção de fuga de memória quando já existe um item com o mesmo nome
- Correcção de fuga de memória no KZip ao lidar com pastas vazias
- K7Zip: Correcção de fugas de memória em caso de erro
- Correcção de fuga de memória detectada pelo ASAN quando o open() falha com o dispositivo subjacente
- Remoção de uma má conversão para KFilterDev, detectada pelo ASAN

### KCodecs

- Adição de macros de exportação em falta nas classes Decoder e Encoder

### KConfig

- Correcção de fuga de memória no SignalsTestNoSingletonDpointer, descoberta pelo ASAN

### KCoreAddons

- Registo do QPair&lt;QString,QString&gt; como meta-tipo no KJobTrackerInterface
- Não converter um URL para URL se tiver um carácter de aspas
- Correcção da compilação em Windows
- Correcção de um erro muito antigo onde se removiam o espaço no URL, como por exemplo "xpto &lt;&lt;url&gt; &lt;url&gt;&gt;"

### KCrash

- Opção do CMake KCRASH_CORE_PATTERN_RAISE para encaminhar para o 'kernel'
- Mudança do nível de depuração predefinido de 'Warning' (Aviso) para 'Info' (Informação)

### KDELibs 4 Support

- Limpeza. Não instalar inclusões que apontem para locais inexistentes e também remover esses ficheiros
- Uso mais correcto e, com o C++11 disponível, do std::remove_pointer

### KDocTools

- Correção do bug #369415 'checkXML5 prints generated html to stdout for valid docbooks'
- Correcção de um erro onde não é possível executar ferramentas nnativas no pacote, usando o 'kdoctools' multi-plataforma
- Configuração dos alvos de compilação multi-plataforma que executam o 'kdoctools' de outros pacotes
- Adição do suporte de compilação multi-plataforma do 'docbookl10nhelper'
- Adição do suporte de compilação multi-plataforma do 'meinproc5'
- Conversão do 'checkxml5' para um executável do Qt, para o suporte de compilação multi-plataforma

### KFileMetaData

- Melhoria na extracção do 'epub', gerando menos estoiros (erro 361727)
- Tornar a indexação de ODF mais robusta a erros, verificando se existem os ficheiros dele (e se são de facto ficheiros (meta.xml + content.xml)

### KIO

- Correcção dos 'KIO slaves' que usam apenas o TLS 1.0
- Correção da ABI no kio
- KFileItemActions: adição do addPluginActionsTo(QMenu *)
- Mostrar os botões de cópia apenas depois do cálculo do código de validação
- Adição de reacção em falta após o cálculo de um código de validação (erro 368520)
- Correcção do KFileItem::overlays que devolve valores de texto vazios
- Correcção do lançamento de ficheiros '.desktop' de terminal com o Konsole
- Classificação das montagens de NFS4 como provavelmente lentas, como o NFS/CIFS/..
- KNewFileMenu: mostrar o atalho da acção Nova Pasta (erro 366075)

### KItemViews

- No modo de lista, usar a implementação por omissão do 'moveCursor'

### KNewStuff

- Adição de verificações do KAuthorized para permitir desactivar o 'ghns' no 'kdeglobals' (erro 368240)

### Package Framework

- Não gerar ficheiros do AppStream para os componentes que não estejam no RDN
- Fazer com que o 'kpackage_install_package' funcione com o KDE_INSTALL_DIRS_NO_DEPRECATED
- Remoção da variável não usada KPACKAGE_DATA_INSTALL_DIR

### KParts

- Correcção dos URL's com uma barra no fim, que eram sempre assumidos como pastas

### KPeople

- Correção de compilação ASAN (duplicates.cpp usa KPeople::AbstractContact que se encontra em KF5PeopleBackend)

### KPty

- Uso da localização do ECM para descobrir o binário 'utempter', o que é mais fiável que um prefixo simples do 'cmake'
- Invocação manual do utilitário 'utempter' (erro 364779)

### KTextEditor

- Ficheiros XML: remoção do valor fixo da cor dos valores
- XML: Remoção do valor fixo da cor dos valores
- Definição de Esquema em XML: Transformação do 'version' num xs:integer
- Ficheiros de definição de realce: arredondamento da versão para o número inteiro mais próximo
- Suporte para capturas multi-caracteres apenas no {xxx} para evitar regressões
- Suporte para substituições com expressões regulares e capturas &gt; \9, p.ex. 111 (erro 365124)
- Correcção do desenho dos caracteres que passa para a linha seguinte, p.ex. os sublinhados já não são mais recortados com alguns tipos/tamanhos de letra (erro 335079)
- Correcção de estoiro: Certificação de que o cursor apresentado é válido após a dobragem do texto (erro 367466)
- O KateNormalInputMode precisa de executar de novo os métodos 'enter' do SearchBar
- tentativa de "correcção" do desenho dos sublinhados e de outras coisas do género (erro 335079)
- Mostrar apenas o botão "Ver as diferenças", se o 'diff' estiver instalado
- Usar um item de mensagem não-modal para as notificações de ficheiros modificados no exterior (erro 353712)
- Correcção de regressão: o 'testNormal' só funcionava por causa da execução única do teste
- Divisão do teste de indentação entre execuções separadas
- Suporte de novo à acção "Desdobrar os Nós de Topo" (erro 335590)
- Correcção de um estoiro ao mostrar as mensagens no topo ou no fundo várias vezes
- Correcção da configuração do EOL nas linhas de modo (erro 365705)
- Realce dos ficheiros '.nix' como Bash, dado que não será problemático para já (erro 365006)

### KWallet Framework

- Verificação se o 'kwallet' está activo no Wallet::isOpen(nome) (erro 358260)
- Adição de ficheiro de inclusão do Boost em falta
- Remoção da pesquisa duplicada pelo KF5DocTools

### KWayland

- [servidor] Não enviar a libertação de teclas não carregadas nem o pressionar das mesmas em duplicado (erro 366625)
- [servidor] Ao substituir a selecção da área de transferência, o DataSource anterior tem de ser cancelado (erro 368391)
- Adição do suporte para os eventos de entrada/saída do Surface
- [cliente] Seguir todas as saídas criadas e adição de um método 'get' estático

### KXmlRpcClient

- Conversão das categorias para 'org.kde.pim.*'

### NetworkManagerQt

- É necessário alterar o estado durante a inicialização
- Substituição de todas as chamadas bloqueantes na inicialização por apenas uma chamada bloqueante
- Uso da interface-padrão o.f.DBus.Properties para o sinal PropertiesChanged no NM 1.4.0+ (erro 367938)

### Ícones do Oxygen

- Remoção da pasta inválida no 'index.theme'
- Introdução de teste de duplicações nos ícones do Brisa
- Conversão de todos os ícones duplicados para ligações simbólicas

### Plasma Framework

- Melhoria no resultado do medidor de tempo
- [ToolButtonStyle] Correcção da seta do menu
- i18n: tratamento dos textos nos ficheiros 'kdevtemplate'
- i18n: revisão dos textos nos ficheiros 'kdevtemplate'
- Adição do 'removeMenuItem' ao PlasmaComponents.ContextMenu
- actualização do ícone do KTorrent (erro 369302)
- [WindowThumbnail] Eliminação da imagem nos eventos de mapeamento
- Não incluir o 'kdeglobals' ao lidar com uma configuração da 'cache'
- Correcção do Plasma::knownLanguages
- mudança do tamanho da vista apenas após a definição do contentor
- Evitar criar um KPluginInfo a partir de uma instância de KPluginMetaData
- as tarefas em execução devem ter alguma indicação
- as linhas da barra de tarefas de acordo com a macro RR 128802 devem assinalá-lo
- [AppletQuickItem] Quebra do ciclo quando for encontrada uma disposição

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
