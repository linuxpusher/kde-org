---
aliases:
- ../../kde-frameworks-5.42.0
date: 2018-01-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Mudanças gerais

- Correcções para os avisos do AUTOMOC do cmake 3.10+
- Uso mais alargado do registo por categorias, para desligar o resultado de depuração por omissão (use o 'kdebugsetting' para o reactivar)

### Baloo

- balooctl status: processamento de todos os argumentos
- Correcção de pesquisas por marcas com várias palavras
- Simplificação das condições de mudança de nome
- Correcção do nome visível incorrecto do UDSEntry

### Ícones Breeze

- Correcção do nome do ícone "weather-none" -&gt; "weather-none-available" (erro 379875)
- remoção do ícone do Vivaldi, dado que o ícone original da aplicação se integra bem no Brisa (erro 383517)
- adição de alguns tipos MIME em falta
- Adição do ícone 'document-send' aos ícones do Brisa (erro 388048)
- actualização do ícone do artist do álbum
- adição de suporte para o 'labplot-editlayout'
- remoção de duplicados e actualização do tema escuro
- adição do suporte de ícones do Brisa para o Gnumeric

### Módulos extra do CMake

- Uso do 'readelf' para descobrir as dependências do projecto
- Introdução do INSTALL_PREFIX_SCRIPT para configurar facilmente os prefixos

### KActivities

- Correcção de estoiro no 'kactivities' se não existir nenhuma ligação ao 'dbus'

### KConfig

- Documentação da API: explicar como usar o KWindowConfig no construtor de uma janela
- Descontinuação do KDesktopFile::sortOrder()
- Correcção do resultado do KDesktopFile::sortOrder()

### KCoreAddons

- Extensão do CMAKE_AUTOMOC_MACRO_NAMES também para a própria compilação
- Correspondência das chaves da licença ao 'spdx'

### KDBusAddons

- Correcção da detecção de locais absolutos do cmake 3.5 no Linux
- Adição da função do CMake 'kdbusaddons_generate_dbus_service_file'

### KDeclarative

- Controlos QML para a criação de KCM's

### KDED

- Uso da função do CMake 'kdbusaddons_generate_dbus_service_file' do 'kdbusaddons' para gerar o ficheiro do serviço DBus
- Adição da propriedade 'used' à definição do ficheiro do serviço

### KDELibs 4 Support

- Informar o utilizador se o módulo não puder ser registado com o 'org.kde.kded5' e sair com um erro
- Correcção da compilação em Mingw32

### KDocTools

- adição de entidade do Michael Pyne
- adição de entidades do Martin Koller ao ficheiro 'contributor.entities'
- correcção de item do Debian
- adição da entidade Debian ao 'general.entities'
- adição da entidade 'kbackup', que foi importado
- adição da entidade 'latex', dado já existirem 7 entidades em 'index.docbooks' diferentes no kf5

### KEmoticons

- Adição do esquema (file://). É necessário quando é usado no QML e forem todos adicionados

### KFileMetaData

- remoção de extracção baseada no QtMultimedia
- Pesquisa por Linux em vez de TagLib e evitar a compilação do 'usermetadatawritertest' no Windows
- Reposição do # 6c9111a9 até que seja possível uma compilação com sucesso sem o TagLib
- Remoção da dependência da Taglib, causada por uma inclusão deixada para trás

### KHTML

- Finalmente é possível desactivar o resultado de depuração com o uso de registo por categorias

### KI18n

- Não tratar o 'ts-pmap-compile' como executável
- Correcção de fuga de memória no KuitStaticData
- KI18n: correcção de um número em pesquisas duplas

### KInit

- Remoção de código impossível de atingir

### KIO

- Processar adequadamente as datas nos 'cookies' quando executar numa região não-inglesa (erro 387254)
- [kcoredirlister] Correcção da criação de sub-pasta
- Reflexão do estado do lixo no 'iconNameForUrl'
- Propagação dos sinais do QComboBox em vez dos sinais do campo de texto do QComboBox
- Correcção nos atalhos Web que mostram a localização do seu ficheiro em vez do seu nome legível
- TransferJob: correcção para quando o 'readChannelFinished' já foi emitido antes de o 'start' ter sido invocado (erro 386246)
- Correcção de estoiro que existe possivelmente desde o 5.10? (erro 386364)
- KUriFilter: não devolver um erro em caso de ficheiros inexistentes
- Correcção da criação de caminhos
- Implementação de uma janela de ficheiros onde podem ser adicionados itens gráficos personalizados
- Verificação se o 'qWaitForWindowActive' não falha
- KUriFilter: migração do KServiceTypeTrader
- Documentação da API: uso dos nomes das classes nos títulos das imagens de exemplo
- Documentação da API: lidar também com as macros de exportação KIOWIDGETS na criação do QCH
- correcção do tratamento do KCookieAdvice::AcceptForSession (erro 386325)
- Criação do 'GroupHiddenRole' para o KPlacesModel
- propagação do texto do erro do 'socket' para o KTcpSocket
- Remodelação e remoção de código duplicado no 'kfileplacesview'
- Emissão do sinal 'groupHiddenChanged'
- Remodelação da animação de desaparecimento/aparecimento dentro do KFilePlacesView
- O utilizador consegue agora esconder um grupo inteiro de locais no KFilePlacesView (erro 300247)
- Implementação da ocultação de grupos de locais no KFilePlacesModel (erro 300247)
- [KOpenWithDialog] Remoção da criação redundante do KLineEdit
- Adição de suporte para desfazer uma BatchRenameJob
- Adição do BatchRenameJob ao KIO
- Correcção de blocos de código do Doxygen que não terminam em 'endcode'

### Kirigami

- manter o 'flickable' interactivo
- prefixo adequado também para os ícones
- correcção do dimensionamento do formulário
- ler o 'wheelScrollLines' do 'kdeglobals', caso exista
- adição de um prefixo para os ficheiros do Kirigami para evitar conflitos
- algumas correcções de compilação estática
- passagem dos estilos do Plasma para o 'plasma-framework'
- Uso de plicas para os caracteres correspondentes + QLatin1Char
- FormLayout

### KJobWidgets

- Oferta da API do QWindow para os decoradores do KJobWidgets

### KNewStuff

- Limitar o tamanho da 'cache' do pedido
- Pedir a mesma versão interna com que está a compilar
- Evitar que as variáveis globais sejam usadas após a sua libertação

### KNotification

- [KStatusNotifierItem] Não "repor" a posição do elemento na sua primeira apresentação
- Uso das posições dos ícones legados da bandeja nas acções Minimizar/Repor
- Tratamento das posições para carregar com o botão esquerdo nos ícones da bandeja
- não criar o menu de contexto como uma Window
- Adição de comentário explicativo
- Instanciação e carregamento posterior dos 'plugins' do KNotification

### Plataforma KPackage

- invalidação da 'cache' de execução na instalação
- suporte experimental para o carregamento de ficheiros .rcc no kpackage
- Compilação contra o Qt 5.7
- Correcção da indexação de pacotes e adição da 'cache' em execução
- novo método KPackage::fileUrl()

### KRunner

- [RunnerManager] Não interferir com a contagem de tarefas do ThreadWeaver

### KTextEditor

- Correcção da correspondência com caracteres especiais nas linhas de modo
- Correcção de uma regressão causada pela mudança do comportamento da tecla Backspace
- migração para uma API não-desactualizada como já foi feito noutro local (erro 386823)
- Adição de inclusão em falta para o std::array
- MessageInterface: Adição do CenterInView como posição adicional
- Limpeza da lista de inicializações do QStringList

### KWallet Framework

- Uso da localização correcta do executável do serviço para instalar o serviço de DBus do KWalletd no Win32

### KWayland

- Correcção de inconsistência de nomes
- Criação de uma interface para passar as paletas de decoração do servidor
- Inclusão explícita das funções 'std::bind'
- [servidor] Adição de um método IdleInterface::simulateUserActivity
- Correcção de regressão causada pelo suporte compatível com versões anteriores da fonte de dados
- Adição do suporte para a versão 3 da interface do gestor de dispositivos de dados (erro 386993)
- Mudar o âmbito dos objectos exportados/importados para o teste
- Correcção de erro no WaylandSurface::testDisconnect
- Adição do protocolo explícito do AppMenu
- Correcção da funcionalidade de exclusão do ficheiro gerado do 'automoc'

### KWidgetsAddons

- Correcção de estoiro no setMainWindow no Wayland

### KXMLGUI

- Documentação da API: fazer com que o Doxygen cubra as macros &amp; funções relacionadas com a sessão de novo
- Desligar a chamada do 'shortcutedit' em caso de destruição do elemento (erro 387307)

### NetworkManagerQt

- 802-11-x: suporte para o método de EAP PWD

### Plasma Framework

- [Tema Air] Adição de um gráfico do progresso da barra de tarefas (erro 368215)
- Modelos: remoção de '*' inúteis dos cabeçalhos das licenças
- tornar o 'packageurlinterceptor' o mais reduzido possível
- Reverter a funcionalidade "Don't tear down renderer and other busy work when Svg::setImagePath is invoked with the same arg" (Não eliminar o módulo de desenho e outro trabalho em curso quando o Svg::setImagePath é invocado com o mesmo argumento)
- passagem dos estilos de Plasma do Kirigami para aqui
- barras de deslocamento que desaparecem em dispositivos móveis
- reutilização da instância do KPackage entre o PluginLoader e a Applet
- [AppletQuickItem] Só definir o estilo dos Controlos QtQuick 1 uma vez por cada motor
- Não definir um ícone de janela no Plasma::Dialog
- [RTL] - alinhar adequadamente o texto seleccionado para texto da direita-para-esquerda (erro 387415)
- Inicialização do factor de escala para o último factor definido em qualquer instância
- Reverter o "Initialize scale factor to the last scale factor set on any instance" (Inicializar o factor de escala com o último factor definido em qualquer instância)
- Não actualizar quando o FrameSvg subjacente estiver bloqueado para 'repaint' (actualização)
- Inicialização do factor de escala para o último factor definido em qualquer instância
- Mover se definido dentro do #ifdef
- [FrameSvgItem] Não criar nós desnecessários
- Não eliminar o módulo de desenho e outro trabalho em curso quando o Svg::setImagePath é invocado com o mesmo argumento

### Prison

- Procurar também pelo 'qrencode' com o sufixo 'debug' (depuração)

### QQC2StyleBridge

- simplificar e não tentar bloquear os eventos do rato
- se não estiver definido o 'wheel.pixelDelta', usar as linhas de deslocamento globais da roda
- as barras de páginas do ecrã têm larguras diferentes para cada página
- garantir uma sugestão de tamanho diferente de zero

### Sonnet

- Não exportar os executáveis utilitários internos
- Sonnet: correcção da língua errada para as sugestões no caso de textos com várias línguas
- Remoção de modificação alternativa antiga e problemática
- Não causar compilações circulares no Windows

### Realce de sintaxe

- Indexação do realce: Avisar sobre a mudança de contexto 'fallthroughContext="#stay"'
- Indexação do realce: Avisar sobre atributos vazios
- Indexação do realce: Activação de erros
- Indexação do realce: comunicar os 'itemDatas' não usados e em falta
- Prolog, RelaxNG, RMarkDown: Correcção de problemas no realce
- Haml: Correcção de 'itemDatas' inválidos e não usados
- ObjectiveC++: Remoção de contextos de comentários duplicados
- Diff, ObjectiveC++: Limpezas e correcções dos ficheiros de realce
- XML (Depuração): Correcção de regra DetectChar incorrecta
- Indexação do Realce: Suporte para a verificação de contexto entre realces
- Reversão: Adição do GNUMacros ao 'gcc.xml' de novo, usado pelo 'isocpp.xml'
- email.xml: adição de *.email às extensões
- Indexação do Realce: Pesquisa por ciclos infinitos
- Indexação do Realce: Pesquisa por nomes e expressões regulares em contextos vazios
- Correcção de referência a listas de palavras-chave inexistentes
- Correcção de casos simples de contextos duplicados
- Correcção de 'itemDatas' duplicados
- Correção das regras DetectChar e Detect2Chars
- Indexação do Realce: Verificação das listas de palavras-chave
- Indexação do Realce: Avisar sobre contextos duplicados
- Indexação do Realce: Verificação de 'itemDatas' duplicados
- Indexação do realce: Verificação do DetectChar e Detect2Chars
- Validação se todos os atributos de um 'itemData' existem

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
