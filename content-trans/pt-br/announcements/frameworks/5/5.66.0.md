---
aliases:
- ../../kde-frameworks-5.66.0
date: 2020-01-11
layout: framework
libCount: 70
---
### Todos os frameworks

- Migração do QRegExp para o QRegularExpression
- Migrado do qrand para o QRandomGenerator
- Correção na compilação com Qt 5.15 (p. ex. endl agora é Qt::endl, QHash insertMulti agora requer o uso de QMultiHash...)

### Attica

- Não usar nullptr verificado como fonte de dados
- Suporte a múltiplos elementos filhos em elementos de comentário
- Definida uma sequência específica para o agente em requisições do Attica

### Baloo

- Relatar corretamente se baloo_file está indisponível
- Verificar o valor de retorno do cursor_open
- Inicializar valores de monitoração QML
- Mover métodos de analisador de URL do kioslave para o objeto query

### BluezQt

- Adição de interface Battery1

### Ícones Breeze

- Mudança do ícone de XHTML para ser um ícone de HTML em roxo
- Junção dos auscultadores e do ziguezague no centro
- Adição do ícone 'application/x-audacity-project' (erro 415722)
- Adição de um ícone 'preferences-system' de 32px
- Adição do ícone 'application/vnd.apple.pkpass' (erro 397987)
- ícone do 'ktimetracker' a usar o PNG no repositório de aplicações - a substituir pelo SVG real do Brisa
- adição do ícone do Kipi, precisa de ser refeito como um SVG do tema Brisa [ou simplesmente eliminar o Kipi]

### Módulos extra do CMake

- [android] Correcção do local de instalação do APK
- Suporte para o PyQt5 compilado com o SIP 5

### Integração do Framework

- Remoção do ColorSchemeFilter do KStyle

### Ferramentas Doxygen do KDE

- Apresentação do nome completo da classe/espaço de nomes como cabeçalho da página (erro 406588)

### KCalendarCore

- Melhoria do README.md para ter uma secção Introdução
- Tornar também acessível a coordenada geográfica da incidência como uma propriedade
- Correcção da geração do RRULE para os fusos-horários

### KCMUtils

- Descontinuação do KCModuleContainer

### KCodecs

- Correcção de conversão inválida para um enumerado, mudando o tipo para 'int' em vez de 'enum'

### KCompletion

- Descontinuação do KPixmapProvider
- [KHistoryComboBox] Adição de método para definir um fornecedor de ícones

### KConfig

- limpeza do protocolo de transferência EBN do kconfig
- Exposição de método de leitura para o 'config' do KConfigWatcher
- Correcção do 'writeFlags' com o KConfigCompilerSignallingItem
- Adição de um comentário que aponta para o histórico sobre a partilha de atalhos do Cortar e Apagar

### KConfigWidgets

- Mudança de nome de "Configurar os Atalhos" parar "Configurar os Atalhos de Teclado" (erro 39488)

### KContacts

- Alinhamento da configuração do ECM e do Qt com as convenções das Plataformas
- Indicação da versão da dependência do ECM como nas outras plataformas

### KCoreAddons

- Adição do KPluginMetaData::supportsMimeType
- [KAutoSaveFile] Uso do QUrl::path() em vez do 'toLocalFile()'
- Resolução do problema de compilação com o PROCSTAT: adição de implementação em falta do KProcessList::processInfo
- [KProcessList] Optimização do KProcessList::processInfo (erro 410945)
- [KAutoSaveFile] Melhoria dos comentários no tempFileName()
- Correcção do KAutoSaveFile problemático com localizações extensas (erro 412519)

### KDeclarative

- [KeySequenceHelper] Captura da janela actual quando incorporada
- Adição de legenda opcional para a delegação da grelha
- [QImageItem/QPixmapItem] Não perder a precisão durante os cálculos

### KFileMetaData

- Correcção parcial dos caracteres acentuados nos nomes de ficheiros no Windows
- Remoção das declarações privadas desnecessárias no 'taglibextractor'
- Solução parcial para aceitar caracteres acentuados nos nomes de ficheiros no Windows
- xattr: correcção de estoiro com ligações simbólicas para nada (erro 414227)

### KIconThemes

- Definição do Brisa como tema predefinido ao ler do ficheiro de configuração
- Descontinuação da função de topo IconSize()
- Correcção do alinhamento ao centro dos ícones escalados em imagens de PPP's elevados

### KImageFormats

- pic: Correcção do comportamento indefinido do valor enumerado inválido

### KIO

- [KFilePlacesModel] Correcção da verificação de esquemas suportados dos dispositivos
- Incorporar os dados do protocolo também na versão para Windows do 'ioslave' 'trash'
- Adição de suporte para montar URL's do KIOFuse nas aplicações que não usam o KIO (erro 398446)
- Adição do suporte para truncar no FileJob
- Descontinuação do KUrlPixmapProvider
- Descontinuação do KFileWidget::toolBar
- [KUrlNavigator] Adição do suporte para RPM no 'krarc:' (erro 408082)
- KFilePlaceEditDialog: correcção de estoiro ao editar o local Lixo (erro 415676)
- Adição de botão para abrir a pasta no Filelight para ver mais detalhes
- Mostrar mais detalhes na janela de aviso que aparece antes de iniciar uma operação privilegiada
- KDirOperator: Uso de uma altura de linhas fixa para a velocidade de deslocamento
- Os campos adicionais, como a data de remoção e a localização original, aparecem agora na janela de propriedades do ficheiro
- KFilePlacesModel: correcção do pai do objecto 'tagsLister' para evitar fugas de memória. Introduzido com o D7700
- 'ioslave' HTTP: invocação da classe de base correcta no virtual_hook(). A base do 'ioslave' HTTP é a TCPSlaveBase, não a SlaveBase
- 'ioslave' FTP: correcção de uma hora de 4 caracteres a ser interpretada como um ano
- Nova adição do KDirOperator::keyPressEvent para manter compatibilidade binária
- Uso do QStyle para determinar os tamanhos dos ícones

### Kirigami

- ActionToolBar: Só mostrar o botão de extensão se existirem itens visíveis no menu (erro 415412)
- Não compilar e instalar os modelos de aplicações no Android
- Não fixar a margem do CardsListView
- Adição do suporte para componentes de apresentação personalizada no Action
- Deixar os outros componentes crescer se existirem mais coisas no cabeçalho
- Remoção da criação dinâmica do item no DefaultListItemBackground
- reintrodução do botão de fecho (erro 415074)
- Mostrar o ícone da janela da aplicação no AboutPage

### KItemModels

- Adição do KColumnHeadersModel

### KJS

- Adição de testes para o Math.exp()
- Adição de testes para vários operadores de atribuição
- Teste de casos especiais de operadores de multiplicação (*, / e %)

### KNewStuff

- Garantia de que o título da janela está correcto com um motor não inicializado
- Não mostrar o ícone de informações no método delegado grande de antevisão (erro 413436)
- Suporte para a instalação de pacotes com comandos de adopção (erro 407687)
- Envio do nome da configuração com os pedidos

### KPeople

- Exposição do enumerado no compilador de meta-objectos

### KQuickCharts

- Correcção também dos ficheiros de inclusão do 'shader'
- Correcção dos cabeçalhos de licenças nos 'shaders'

### KService

- Descontinuação do KServiceTypeProfile

### KTextEditor

- Adição da propriedade "line-count" ao objecto ConfigInterface
- Evitar o deslocamento horizontal indesejado (erro 415096)

### KWayland

- [plasmashell] Actualização da documentação do 'panelTakesFocus' para ficar genérico
- [plasmashell] Adição de sinal para a mudança do 'panelTakesFocus'

### KXMLGUI

- KActionCollection: oferta de um sinal changed() como substituição do removed()
- Ajustar o título da janela de configuração dos atalhos de teclado

### NetworkManagerQt

- Manager: adição do suporte para o AddAndActivateConnection2
- cmake: Considerar os ficheiros de inclusão do NM como sendo do sistema
- Sincronização do Utils::securityIsValid com o NetworkManager (erro 415670)

### Plasma Framework

- [ToolTip] Arredondamento da posição
- Activação dos eventos da roda no Slider {}
- Sincronização da opção WindowDoesNotAcceptFocus do QWindow com a interface do 'plasmashell' para o Wayland (erro 401172)
- [calendário] Verificação do acesso fora dos limites a listas na pesquisa do QLocale
- [Janela do Plasma] Uso do QXcbWindowFunctions para definir os tipos de janelas que o WindowFlags do Qt não reconhece
- [PC3] Finalização da animação da barra de progresso do Plasma
- [PC3] Só mostrar o indicador da barra de progresso quando os extremos não se sobrepuserem
- [RFC] Correcção das margens dos ícones da Configuração do Ecrã (erro 400087)
- [ColorScope] Voltar a funcionar com QObject's simples
- [Tema do Ecrã brisa] Adição de ícone 'user-desktop' monocromático
- Remoção da largura predefinida do PlasmaComponents3.Button
- [ToolButton do PC3] Fazer com que a legenda tenha em conta esquemas de cores complementares (erro 414929)
- Adição de cores de fundo para a área de ícones activa e inactiva (erro 370465)

### Purpose

- Uso de ECMQMLModule's padrão

### QQC2StyleBridge

- [ToolTip] Arredondamento da posição
- Actualização da sugestão de tamanho quando muda o tipo de letra

### Solid

- Mostrar a descrição do primeiro / actual acesso ao armazenamento
- Garantia de que os sistemas de ficheiros NFS montados correspondem ao seu elemento respectivo declarado no 'fstab' (erro 390691)

### Sonnet

- O sinal 'done' é obsoleto em detrimento do 'spellCheckDone', sendo agora correctamente emitido

### Realce de sintaxe

- LaTeX: correcção dos parêntesis rectos em alguns comandos (erro 415384)
- TypeScript: adição do tipo primitivo "bigint"
- Python: melhoria dos números, adição de octais e binários e da palavra-chave "breakpoint" (erro 414996)
- SELinux: adição da palavra-chave "glblub" e actualização da lista de permissões
- Diversas melhorias na definição de sintaxe do Gitolite

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
