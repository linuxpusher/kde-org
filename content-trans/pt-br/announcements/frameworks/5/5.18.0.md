---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Correção de diversos problemas na pesquisa relacionada com o <i>mtime</i>
- Iteração da PostingDB: Não afirmar em caso de MDB_NOTFOUND
- Status do Balooctl: Evitar a apresentação de 'Indexando conteúdo' nas pastas
- StatusCommand: Mostrar o status correto das pastas
- SearchStore: Tratamento elegante de valores de termos vazios (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356176'>356176</a>)

### Ícones Breeze

- Alterações de ícones e novidades
- Os ícones de status com 22px de tamanho agora também em 32px, porque podem ser necessários na área de notificação
- Correção do valor <b>Fixo</b> para <b>Escalável</b> nas pastas de 32px com o Breeze Dark

### Módulos extra do CMake

- Transformar o módulo CMake do KAppTemplate para global
- Eliminação dos avisos CMP0063 com o KDECompilerSettings
- ECMQtDeclareLoggingCategory: Inclusão do &lt;QDebug&gt; com o arquivo gerado
- Correção dos avisos CMP0054

### KActivities

- Racionalização do carregamento de QML para o KCM (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356832'>356832</a>)
- Alternativa para o erro do Qt SQL que não elimina as conexões corretamente (erro <a href='https://bugs.kde.org/show_bug.cgi?id=348194'>348194</a>)
- Mesclagem de um plugin que executa aplicativos quando o estado de uma atividade é alterado
- Migração do KService para o KPluginLoader
- Migração dos plugins para usarem o kcoreaddons_desktop_to_json()

### KBookmarks

- Inicialização completa do DynMenuInfo no valor devolvido

### KCMUtils

- KPluginSelector::addPlugins: Correção de validação se o parâmetro 'config' é o padrão (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352471'>352471</a>)

### KCodecs

- Evitar o esgotamento deliberado de um buffer completo

### KConfig

- Garantia de que o grupo é recomposto no <i>kconf_update</i>

### KCoreAddons

- Adição do KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)
- Adição do KPluginMetaData::copyrightText(), extraInformation() e otherContributors()
- Adição do KPluginMetaData::translators() e do KAboutPerson::fromJson()
- Correção de uso-após-liberação no processador de arquivos <i>.desktop</i>
- Permitir a construção de um KPluginMetaData a partir de caminho em JSON
- desktoptojson: Mudança do arquivo de tipo de serviço em falta para um erro do binário
- Torna a chamada do kcoreaddons_add_plugin sem o SOURCES um erro

### KDBusAddons

- Adaptação para o 'dbus-em-tarefa-secundária' do Qt 5.6

### KDeclarative

- [DragArea] Adição da propriedade <i>dragActive</i>
- [KQuickControlsAddons MimeDatabase] Exposição do comentário QMimeType

### KDED

- kded: Adaptação para o dbus em tarefa secundária do Qt 5.6: O <i>messageFilter</i> deverá acionar o carregamento do módulo na tarefa principal

### KDELibs 4 Support

- O <i>kdelibs4support</i> requer o <i>kded</i> (para o <i>kdedmodule.desktop</i>)
- Correção do aviso CMP0064 através da definição da política do CMP0054 como NEW
- Não exportar símbolos que também existem no KWidgetsAddons

### KDESU

- Não perder descritores ao criar um socket

### KHTML

- Windows: Remoção da dependência do <b>kdewin</b>

### KI18n

- Documentação da regra do primeiro argumento para os plurais no QML
- Redução de alterações de tipo indesejadas
- Possibilidade de usar números de precisão dupla como índice nas chamadas i18np*() em QML

### KIO

- Correção do <i>kiod</i> para o <i>dbus</i> em tarefas separadas do Qt 5.6: O <i>messageFilter</i> deve esperar pelo carregamento do módulo antes de devolver o resultado
- Alteração do código de erro ao colar/mover para uma subpasta
- Correção do problema de bloqueio do <i>emptyTrash</i>
- Correção do botão inválido no KUrlNavigator para URLs remotas
- KUrlComboBox: Correção do retorno de um caminho absoluto no urls()
- kiod: desativação do gerenciamento de sessões
- Adição da completação automática para a entrada '.', que oferece todos os arquivos/pastas ocultos (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354981'>354981</a>)
- ktelnetservice: Correção de erro de correspondência 'menos um' no <i>argc</i>, corrigido por Steven Bromley

### KNotification

- [Notificação por Popup] Enviar junto com o ID do evento
- Definição do motivo não-vazio padrão para a inibição do protetor de tela (erro <a href='https://bugs.kde.org/show_bug.cgi?id=334525'>334525</a>)
- Adição de uma dica de desativação do agrupamento de notificações (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356653'>356653</a>)

### KNotifyConfig

- [KNotifyConfigWidget] Permitir a seleção de um evento específico

### Package Framework

- Tornar possível o fornecimento dos metadados em JSON

### KPeople

- Correção de uma possível remoção dupla no DeclarativePersonData

### KTextEditor

- Realce de sintaxe do <i>pli</i>: Adição de funções incorporadas, adição de regiões expandidas

### KWallet Framework

- kwalletd: Correção de vazamento de FILE*

### KWindowSystem

- Adição da variante XCB para os métodos estáticos KStartupInfo::sendFoo

### NetworkManagerQt

- Capacidade de trabalhar com versões antigas do NM

### Plasma Framework

- [ToolButtonStyle] Sempre indicar o <i>activeFocus</i>
- Uso da opção SkipGrouping para a notificação de "widget removido" (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356653'>356653</a>)
- Tratamento adequado dos links simbólicos no caminho dos pacotes
- Adição do HiddenStatus para auto-ocultar os plasmoides
- Parar o redirecionamento de janelas quando o item está desativado ou oculto. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356938'>356938</a>)
- Não emitir o <i>statusChanged</i> se não tiver sido alterado
- Correção dos IDs dos elementos para a orientação a Leste
- Contêiner: Não emitir um <i>appletCreated</i> com um miniaplicativo nulo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356428'>356428</a>)
- [Interface de contêineres] Correção de um deslocamento de alta precisão irregular
- Leitura da propriedade X-Plasma-ComponentTypes do KPluginMetadata como uma lista de strings
- [Miniaturas de janelas] Não falhar se a composição estiver desativada
- Possibilidade de os contêineres substituírem o CompactApplet.qml

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
