---
aliases:
- ../announce-4.11.3
date: 2013-11-05
description: O KDE Lança a Versão 4.11.3 da Área de Trabalho Plasma, as Aplicações
  e a Plataforma.
title: O KDE disponibiliza as atualizações de novembro para o Espaço de Trabalho Plasma,
  os Aplicativos e a Plataforma
---
5 de novembro de 2013. Hoje o KDE disponibilizou as atualizações para o Espaço de Trabalho, Aplicativos e Plataforma de Desenvolvimento. Esta é a terceira de uma sequência de atualizações mensais de estabilização da série 4.11. Como anunciado no lançamento, o Espaço de Trabalho continuará a receber atualizações pelos próximos dois anos. Esta versão contém apenas correções de erros e atualizações de tradução, ela é segura e agradável a todos.

Pelo menos 120 das correções de erros registradas incluem melhorias no gerenciador de janelas KWin, no gerenciador de arquivos Dolphin, no gerenciador de informações pessoais Kontact, na ferramenta de UML Umbrello, entre outras. Há muitas correções de problemas de estabilidade, assim como adições e melhorias nas traduções.

Poderá encontrar uma <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.3&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>lista</a> mais completa das alterações no sistema de registo de erros do KDE. Para uma lista detalhada das alterações que ocorreram no 4.11.3, também poderá navegar pelo histórico do Git.

Para baixar o código-fonte ou os pacotes de instalação, vá para a <a href='/info/4/4.11.3'>Página de Informações do 4.11.3</a>. Se quiser obter mais informações sobre as versões 4.11 do Espaço de Trabalho, Aplicativos e Plataforma de Desenvolvimento do KDE, consulte as <a href='/announcements/4.11/'>notas da versão 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`O novo fluxo de envio posterior no Kontact` width="600px">}}

O 'software' do KDE, incluindo todas as bibliotecas e aplicações, está disponível de graça segundo licenças de Código Aberto. As mesmas poderão ser obtidas nos formatos de código-fonte e em vários formatos binários a partir de <a href='http://download.kde.org/stable/4.11.3'>download.kde.org</a> ou com qualquer um dos <a href='/distributions'>principais sistemas GNU/Linux e UNIX</a> dos dias de hoje.
