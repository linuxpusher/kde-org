---
date: '2014-04-16'
description: O KDE Lança a Versão 4.13 das Aplicações e da Plataforma.
hidden: true
title: Os aplicativos do KDE 4.13 se beneficiam da nova pesquisa semântica e apresentam
  novas funcionalidades
---
A Comunidade KDE orgulha-se em anunciar as últimas grandes atualizações do KDE Applications, que traz consigo novas funcionalidades e correções. O Kontact (gerenciador de informações pessoais) foi um programa com grande atividade, beneficiando-se de melhorias da tecnologia de Pesquisa Semântica do KDE e trazendo novas funcionalidades. O visualizador de documentos Okular e o editor de texto avançado Kate trouxeram também novas funcionalidades e mudanças na interface. Nas áreas de jogos e educação, introduziu-se o novo treinador de pronúncias em idiomas estrangeiros Artikulate; o Marble (o globo virtual) trouxe suporte para o Sol, a Lua, os planetas, para rotas em bicicleta e para milhas náuticas. O Palapeli (o jogo de quebra-cabeça) atingiu novas dimensões e capacidades.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## O Kontact do KDE introduz novas funcionalidades e mais velocidade

O pacote Kontact do KDE introduz uma série de funcionalidades nos seus vários componentes. O KMail introduz o armazenamento em 'nuvem' e suporte melhorado de Sieve para filtragem pelo servidor. O KNotes consegue agora gerar alarmes e introduz funcionalidades de pesquisa e teve também algumas melhorias na camada do cache de dados no Kontact, aumentando a velocidade de quase todas as operações.

### Suporte para armazenamento em 'nuvem'

O KMail introduz suporte do serviço de armazenamento, de modo que os anexos grandes possam ser guardados em serviços na 'nuvem' e incluídos como hiperlinks nas mensagens de e-mail. Os serviços de armazenamento suportados incluem o Dropbox, o Box, o KolabServer, o YouSendIt, o UbuntuOne, o Hubic e uma opção de WebDAV genérico. Uma ferramenta chamada <em>storageservicemanager</em> ajuda-o com o gerenciamento dos arquivos nesses serviços.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Grandes melhorias no suporte para Sieve

Os filtros Sieve, uma tecnologia para deixar que o KMail trabalhe com filtros no servidor, consegue agora lidar com suporte a férias em vários servidores. A ferramenta KSieveEditor permite aos usuários editarem os filtros Sieve sem ter que adicionar o servidor ao Kontact.

### Outras alterações

A barra de filtragem rápida teve uma pequena melhoria na interface e se beneficia muito das capacidades de pesquisa melhoradas que foram introduzidas na versão 4.13 da Plataforma de Desenvolvimento do KDE. A pesquisa tornou-se significativamente mais rápida e mais confiável. O compositor introduziu um encurtador de URLs, aumentando a tradução existente e as ferramentas de trechos de texto.

As marcas e as anotações dos dados PIM são agora armazenadas no Akonadi. Nas versões futuras, serão também armazenadas nos servidores (IMAP ou Kolab), possibilitando o compartilhamento de marcas entre vários computadores. Akonadi: Foi adicionado suporte para a API do Google Drive. Existe o suporte para pesquisa com plugins de terceiros (o que significa que os resultados podem ser obtidos muito rapidamente) e a pesquisa pelo servidor (a pesquisa de itens não indexados por nenhum serviço local).

### KNotes e KAddressbook

Trabalhou-se de forma significativa no KNotes, corrigindo uma série de erros e pequenos incômodos. A capacidade de definir alarmes para as notas é nova, assim como a pesquisa através das notas. Leia mais <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aqui</a>. O KAddressbook ganhou suporte para impressão: Mais detalhes <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aqui</a>.

### Melhorias de desempenho

A performance do Kontact foi melhorada de forma notória nesta versão. Algumas melhorias devem-se à integração com a nova versão da infra-estrutura de <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Pesquisa Semântica</a> do KDE, assim como à camada da 'cache' de dados e ao carregamento dos mesmos no próprio KMail. Ocorreu também algum trabalho notório na melhoria do suporte à base de dados PostgreSQL. Poderá encontrar mais algumas informações e detalhes sobre as alterações de performance nestes endereços:

- Optimizações do Armazenamento: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>relatório do 'sprint'</a>
- optimização e redução do tamanho da base de dados: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>lista de correio</a>
- optimização no acesso às pastas: <a href='https://git.reviewboard.kde.org/r/113918/'>quadro de revisões</a>

### KNotes e KAddressbook

Trabalhou-se de forma significativa no KNotes, corrigindo uma série de erros e pequenos incômodos. A capacidade de definir alarmes para as notas é nova, assim como a pesquisa através das notas. Leia mais <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aqui</a>. O KAddressbook ganhou suporte para impressão: Mais detalhes <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aqui</a>.

## Okular com refinamento na Interface do Usuário

Esta versão do leitor de documentos Okular traz um novo conjunto de melhorias. Pode agora abrir vários ficheiros PDF numa instância do Okular, graças ao suporte de páginas separadas. Existe agora um novo modo de ampliação com o rato e são usadas as dimensões em PPP do monitor actual para a representação do PDF, melhorando a aparência dos documentos. Foi incluído um novo botão Reproduzir no modo de apresentação, e existem melhorias nas acções para Procurar e Desfazer/Refazer.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## O Kate introduz uma barra de status melhorada, a correspondência de parênteses animada e melhoria nos plugins

A última versão do editor de texto avançado Kate introduz a <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>correspondência animada dos parêntesis</a>, as mudanças para que <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>os teclados com o AltGr funcionem no modo do Vim</a> e uma série de melhorias nos 'plugins' do Kate, especialmente na área do suporte para o Python e o <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>'plugin' de compilação</a>. Existe uma nova <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>barra de estado melhorada</a> que activa algumas acções directas, como a alteração da indentação, da codificação e do realce, uma nova barra de páginas em cada janela, o suporte de completação de código para a <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>linguagem de programação D</a> e <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>muito mais</a>. A equipa <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>pediu a reacção e algumas sugestões de melhorias para o Kate</a> e está a dividir parte da sua atenção numa versão para as Plataformas 5.

## Uma diversidade de novas funcionalidades

O Konsole traz alguma flexibilidade adicional ao permitir folhas de estilo personalizadas para controlar as barras de abas. Os perfis podem agora armazenar o tamanho das colunas e linhas desejados. Veja mais <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>aqui</a>.

O Umbrello possibilita a duplicação de diagramas e introduz alguns menus de contexto inteligentes que ajustam o conteúdo aos elementos selecionados. O suporte para desfazer e as propriedades visuais também foram melhorados. O Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introduz suporte a visualização do formato RAW</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

O mixer de som KMix introduziu os comandos à distância pelo protocolo de comunicação entre processos D-Bus (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>detalhes</a>), algumas adições ao menu de som e uma nova janela de configuração (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>detalhes</a>), assim como uma série de correções de erros e pequenas melhorias.

A interface de pesquisa do Dolphin foi modificada para aproveitar a nova infraestrutura de pesquisa e recebeu mais algumas melhorias de desempenho. Para mais detalhes, leia este <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>resumo sobre o trabalho de otimização no último ano</a>.

O KHelpcenter adiciona a ordenação alfabética dos módulos e a reorganização das categorias para se tornar mais fácil de usar.

## Jogos e aplicativos educacionais

Os jogos e aplicativos educativos do KDE receberam muitas atualizações nessa versão. O aplicativo de quebra-cabeças do KDE, o Palapeli, ganhou <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>algumas funcionalidades novas</a> que facilitam a montagem de quebra-cabeças grandes (até 10.000 peças) para os que se sintam aptos para esse desafio. O KNavalBattle mostra a posição dos navios inimigos após acabar o jogo, para você ver onde errou.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

Os aplicativos educativos do KDE ganharam novas funcionalidades. O KStars ganhou uma interface de criação de scripts através de D-Bus e também pode usar a API de serviços Web do astrometry.net para otimizar o uso da memória. O Cantor ganhou realce de sintaxe no seu editor de scripts e suporte às infraestruturas Scilab e Python 2 no editor. A ferramenta de mapas e navegação Marble agora inclui as posições do <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sol, Lua</a> e <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planetas</a>, permitindo também a <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>captura de vídeos das viagens virtuais</a>. Os trajetos em bicicleta também foram melhorados com o suporte ao serviço cyclestreets.net. As milhas náuticas agora também são suportadas e, se clicar em uma <a href='http://en.wikipedia.org/wiki/Geo_URI'>URI geográfica</a>, ela será aberta no Marble.

#### Instalando aplicativos do KDE

O KDE, incluindo todas as suas bibliotecas e aplicações, está disponível gratuitamente segundo licenças de código aberto. As aplicações do KDE correm sobre várias configurações de 'hardware' e arquitecturas de CPU, como a ARM e a x86, bem como em vários sistemas operativos e gestores de janelas ou ambientes de trabalho. Para além do Linux e de outros sistemas operativos baseados em UNIX, poderá descobrir versões para o Microsoft Windows da maioria das aplicações do KDE nas <a href='http://windows.kde.org'>aplicações do KDE em Windows</a>, assim como versões para o Mac OS X da Apple nas <a href='http://mac.kde.org/'>aplicações do KDE no Mac</a>. As versões experimentais das aplicações do KDE para várias plataformas móveis, como o MeeGo, o MS Windows Mobile e o Symbian poderão ser encontradas na Web, mas não são suportadas de momento. O <a href='http://plasma-active.org'>Plasma Active</a> é uma experiência de utilizador para um espectro mais amplo de dispositivos, como tabletes ou outros dispositivos móveis.

As aplicações do KDE podem ser obtidas nos formatos de código-fonte e em vários formatos binários a partir de <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> e também podem ser obtidos via <a href='/download'>CD-ROM</a> ou com qualquer um dos <a href='/distributions'>principais sistemas GNU/Linux e UNIX</a> dos dias de hoje.

##### Pacotes

Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do %1 para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram.

##### Locais dos pacotes

Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Wiki da Comunidade</a>.

Poderá <a href='/info/4/4.13.0'>transferir à vontade</a> o código-fonte completo de 4.13.0. As instruções de compilação e instalação da aplicação do KDE 4.13.0 está disponível na <a href='/info/4/4.13.0#binary'>Página de Informações do 4.13.0</a>.

#### Requisitos do sistema

Para tirar o máximo partido destas versões, recomendamos a utilização de uma versão recente do Qt, como a 4.8.4. Isto é necessário para garantir uma experiência estável e rápida, assim como algumas melhorias feitas no KDE poderão ter sido feitas de facto na plataforma Qt subjacente.

Para tirar um partido completo das capacidades das aplicações do KDE, recomendamos também que use os últimos controladores gráficos para o seu sistema, dado que isso poderá melhorar substancialmente a experiência do utilizador, tanto nas funcionalidades opcionais como numa performance e estabilidade globais.
