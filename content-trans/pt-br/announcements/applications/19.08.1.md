---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: O KDE Lança as Aplicações do KDE 19.08.1.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: O KDE Lança as Aplicações do KDE 19.08.1
version: 19.08.1
---
{{% i18n_date %}}

Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../19.08.0'>Aplicações do KDE 19.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Cerca de vinte correcções de erros registadas incluem melhorias no Kontact, no Dolphin, no Kdenlive, no Konsole, no Step, entre outros.

As melhorias incluem:

- Várias regressões no modo com que o Konsole lida com abas foram arrumadas
- O Dolphin novamente inicia corretamente em modo de exibição dividida
- Deletar um corpo macio no simulador de física Step não causa mais um crash
