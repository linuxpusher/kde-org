---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: O KDE disponibiliza o KDE Applications 15.12.2
layout: application
title: O KDE disponibiliza o KDE Applications 15.12.2
version: 15.12.2
---
16 de Fevereiro de 2016. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../15.12.0'>Aplicações do KDE 15.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 30 correções de erros registradas incluem melhorias no kdelibs, kdepim, Kdenlive, Marble, Kompare, Spectacle, Akonadi, Ark e Umbrello, dentre outras.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.17.
