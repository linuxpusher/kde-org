---
aliases:
- ../announce-applications-15.08-beta
date: 2015-07-28
description: O KDE Lança as Aplicações do KDE 15.08 Beta.
layout: application
release: applications-15.07.80
title: O KDE disponibiliza a versão Beta do KDE Applications 15.08
---
28 de julho de 2015. Hoje o KDE disponibilizou o beta da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Com diversos aplicativos sendo preparados para o KDE Frameworks 5, as versões do KDE Applications 15.08 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Por favor, considere juntar-se à equipe, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
