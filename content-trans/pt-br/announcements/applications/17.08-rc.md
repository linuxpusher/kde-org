---
aliases:
- ../announce-applications-17.08-rc
date: 2017-08-04
description: O KDE Lança as Aplicações do KDE 17.08 Pré-Lançamento.
layout: application
release: applications-17.07.90
title: O KDE disponibiliza a versão Release Candidate do KDE Applications 17.08
---
4 de agosto de 2017. Hoje o KDE disponibilizou o Release Candidate da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Veja mais informações nas <a href='https://community.kde.org/Applications/17.08_Release_Notes'>notas de lançamento da comunidade</a> sobre novos pacotes, pacotes que sejam agora baseados no KF5 e problemas conhecidos. Será disponibilizado um anúncio mais completo para a versão final

O KDE Applications 17.08 precisa de testes aprofundados para manter e melhorar a qualidade e a experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Considere juntar-se à equipe, instalando a versão release candidate e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
