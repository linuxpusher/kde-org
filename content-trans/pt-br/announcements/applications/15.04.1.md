---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: O KDE Lança as Aplicações do KDE 15.04.1.
layout: application
title: O KDE disponibiliza o KDE Applications 15.04.1
version: 15.04.1
---
12 de Maio de 2015. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../15.04.0'>Aplicações do KDE 15.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 50 correções de erros registradas incluem melhorias no kdelibs, kdepim, Kdenlive, Okular, Marble e Umbrello.

Esta versão também inclui as versões de Suporte de Longo Prazo da Área de Trabalho do Plasma 4.11.19, a Plataforma de Desenvolvimento do KDE 4.14.8 e o pacote Kontact 4.14.8.
