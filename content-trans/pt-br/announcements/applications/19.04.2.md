---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: O KDE Lança as Aplicações do KDE 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: O KDE disponibiliza o KDE Applications 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../19.04.0'>Aplicações do KDE 19.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 50 correcções de erros registadas incluem as melhorias no Kontact, no Ark, no Dolphin, no Juk, no Kdenlive, no Kmplot, no Konsole, no Okular, no Spectacle, entre outros.

As melhorias incluem:

- Foi corrigido um estoiro ao ver determinados documentos EPUB no Okular
- As chaves privadas podem agora ser exportadas de novo a partir do gestor de cifras Kleopatra
- A chamada de atenção de eventos do KAlarm já não tem problemas a iniciar com as bibliotecas PIM mais recentes
