---
aliases:
- ../announce-applications-15.08-beta
date: 2015-07-28
description: KDE Uygulamalar 15.08 Beta'yı Gönderdi.
layout: application
release: applications-15.07.80
title: KDE, KDE Uygulamalar 15.08 Beta'yı Gönderdi
---
28 Temmuz 2015. Bugün KDE, KDE Uygulamalarının yeni sürümlerinin beta sürümünü yayınladı. Bağımlılık ve özellik donmalarıyla birlikte, KDE ekibinin odak noktası artık hataları düzeltmek ve daha fazla parlatmaktır.

With the various applications being based on KDE Frameworks 5, the KDE Applications 15.08 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
