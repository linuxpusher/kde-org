---
aliases:
- ../../kde-frameworks-5.25.0
date: 2016-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### General

- Ahora se necesita Qt &gt;= 5.5.

### Attica

- Seguir las redirecciones de HTTP.

### Iconos Brisa

- Se han actualizado los iconos «mail-» de 16 píxeles para que se puedan reconocer mejor.
- Se han actualizado los iconos de estado del micrófono y del sonido para que tengan el mismo diseño y tamaño.
- Nuevo icono para la aplicación de preferencias del sistema.
- Se han añadido iconos simbólicos de estado de gnome.
- permitir el uso de iconos simbólicos de gnome 3
- Se han añadido iconos para «Diaspora» y «Vector»; consulte phabricator.kde.org/M59.
- Nuevos iconos para Dolphin y Gwenview.
- Los iconos meteorológicos son iconos de estado, no de aplicaciones.
- Se han añadido algunos enlaces a «xliff» gracias a «gnastyle».
- Se ha añadido el icono «kig».
- Se han añadido iconos de tipos MIME, el icono «krdc» y otros iconos de aplicaciones desde «gnastyle».
- Se ha añadido el icono del tipo MIME de certificado (error 365094).
- Se han actualizado los iconos de gimp gracias a gnastyle (error 354370).
- El icono de acción «globe» ya no es un enlace a un archivo. Por favor, usar en digikam.
- Se han actualizado los iconos de «labplot» según el mensaje de correo 13.07. de Alexander Semke.
- Se han añadido iconos de aplicaciones desde «gnastyle».
- Se ha añadido el icono de «kruler» de Yuri Fabirovsky.
- Se han corregido archivos SVG dañados, gracias a fuchs (error 365035).

### Módulos CMake adicionales

- Se ha corregido la inclusión cuando no existe Qt5.
- Se ha añadido un método al que recurrir para «query_qmake()» cuando no existe una instalación de Qt5.
- Asegurarse de que «ECMGeneratePriFile.cmake» se comporta como el resto de ECM.
- Los datos de Appstream han cambiado su ubicación preferida.

### KActivities

- [KActivities-CLI] órdenes para iniciar y detener una actividad
- [KActivities-CLI] Definición y obtención del nombre de la actividad, su icono y su descripción.
- Se ha añadido una aplicación de la línea de órdenes para controlar las actividades.
- Se han añadido scripts para cambiar a las actividades anterior y siguiente.
- El método para insertar en «QFlatSet» devuelve ahora un índice junto al iterador (error 365610).
- Se han añadido funciones ZSH para detener y borrar actividades que no sean la actual.
- Se ha añadido la propiedad «isCurrent» a «KActivities::Info».
- Usar iteradores constantes al buscar actividad.

### Herramientas KDE Doxygen

- Numerosas mejoras en el formato de salida.
- «Mainpage.dox» tiene ahora una prioridad superior a la de «README.md».

### KArchive

- Manejar múltiples emisiones «gzip» (error 232843).
- Suponer que un directorio es un directorio, incluso si el bit de permiso está definido incorrectamente (fallo 364071).

### KBookmarks

- KBookmarkGroup::moveBookmark: Se ha corregido el valor devuelto cuando el elemento ya está en la posición correcta.

### KConfig

- Se han añadido los accesos directos estándares para borrar archivo y para cambiar nombre de archivo.

### KConfigWidgets

- Se han añadido las acciones estándares para borrar archivo y para cambiar nombre de archivo.
- La página de configuración tiene ahora barras de desplazamiento cuando es necesario (fallo 362234).

### KCoreAddons

- Instalar licencias conocidas y buscarlas en tiempo de ejecución (corrección de regresión) (error 353939).

### KDeclarative

- Emitir realmente «valueChanged».

### KFileMetaData

- Comprobar la existencia de «xattr» durante el paso de configuración; en caso contrario, la compilación podría fallar (si no se encuentra «xattr.h»).

### KGlobalAccel

- Usar DBus «klauncher» en lugar de «KRun» (fallo 366415).
- Lanzar acciones de la vista de saltos usando «KGlobalAccel».
- KGlobalAccel: Se ha corregido un interbloqueo al salir en Windows.

### KHTML

- Permitir el uso de unidades de porcentaje en el radio del borde.
- Se ha eliminado la versión prefijada de las propiedades del fondo y del radio del borde.
- Correcciones la función del constructor abreviado de 4 valores.
- Crear objetos de cadena de texto solo si se van a usar.

### KIconThemes

- Gran mejora del rendimiento de «makeCacheKey», ya que se trata de una ruta de código crítica en la búsqueda de iconos.
- KIconLoader: Reducir el número de búsquedas al recurrir a alternativas.
- KIconLoader: Impresionante mejora de velocidad para la carga de iconos no disponibles.
- No borrar la línea de búsqueda al cambiar de categoría.
- KIconEngine: Se ha corregido que «QIcon::hasThemeIcon» siempre devuelva «true» (error 365130).

### KInit

- Adaptar «KInit» a Mac OS X.

### KIO

- Corregir «KIO::linkAs()» para que funcione como estaba previsto; es decir, fallar si «dest» ya existe.
- Corregir «KIO::put("file:///ruta")» para respetar el «umask» (fallo 359581).
- Corregir «KIO::pasteActionText» para elementos de destino nulos y para URL vacías.
- Permitir deshacer la creación de enlaces simbólicos
- Opción de la interfaz de usuario para configurar el «MarkPartial» global de los esclavos KIO.
- Corregir MaxCacheSize limitado a 99 KiB
- Añadir botones del portapapeles a la pestaña de sumas de verificación.
- KNewFileMenu: Se ha corregido la copia de la plantilla desde el recurso empotrado (error 359581).
- KNewFileMenu: Se ha corregido la creación del enlace a la aplicación (fallo 363673).
- KNewFileMenu: Corregir la sugerencia de nuevo nombre de archivo cuando ya existe el archivo en el escritorio.
- KNewFileMenu: Asegurar que «fileCreated()» también se emite para los archivos de escritorio de las aplicaciones.
- KNewFileMenu: Corregir la creación de enlaces simbólicos con un destino relativo.
- KPropertiesDialog: Simplificar el uso del cuadro de botones y corregir el comportamiento al pulsar «Escape».
- KProtocolInfo: Volver a rellenar la caché para encontrar los protocolos instalados recientemente.
- KIO::CopyJob: Se ha portado a «qCDebug» (con su propia área, ya que esto puede resultar bastante detallado).
- KPropertiesDialog: añadir la pestaña de sumas de comprobación
- Borrar la ruta del URL antes de inicializar «KUrlNavigator».

### KItemModels

- KRearrangeColumnsProxyModel: Corregir la aserción en «index(0, 0)» cuando el modelo está vacío.
- Se ha corregido que «KDescendantsProxyModel::setSourceModel()» no borre las cachés internas.
- KRecursiveFilterProxyModel: Corregir la corrupción de «QSFPM» debida al filtrado de la señal «rowsRemoved» (fallo 349789).
- KExtraColumnsProxyModel: Se ha implementado «hasChildren()».

### KNotification

- No definir manualmente el padre de «sublayout», silencia las advertencias.

### Framework para paquetes

- Inferir «ParentApp» a partir del complemento «PackageStructure».
- Permitir que «kpackagetool5» genere información de «appstream» para componentes «kpackage».
- Hacer que se pueda cargar el archivo «metadata.json» de «kpackagetool5».

### Kross

- Eliminar dependencias de KF5 que no se usaban.

### KService

- applications.menu: Se han eliminado referencias a las categorías que no se usan.
- Actualizar siempre el analizador de «Trader» de las fuentes «yacc/lex».

### KTextEditor

- No solicitar permiso para sobrescribir un archivo dos veces en los diálogos nativos.
- Se ha añadido la sintaxis de «FASTQ».

### KWayland

- [cliente] Usar un «QPointer» para el «enteredSurface» en «Pointer».
- Exponer la geometría en «PlasmaWindowModel».
- Se ha añadido un evento de geometría a «PlasmaWindow».
- [src/servidor] Verificar que la superficie tiene un recurso antes de entrar en el envío del puntero.
- Permitir el uso de «xdg-shell».
- [servidor] Enviar correctamente una selección clara antes de entrar en el foco del teclado.
- [servidor] Manejar la situación sin «XDG_RUNTIME_DIR» con más acierto.

### KWidgetsAddons

- [KCharSelect] Corregir un fallo cuando se busca sin un archivo de datos presente (error 300521).
- [KCharSelect] Manejar caracteres fuera del BMP (error 142625).
- [KCharSelect] Actualizar «kcharselect-data» a Unicode 9.0.0 (error 336360).
- KCollapsibleGroupBox: Detener la animación en el destructor si todavía se está reproduciendo.
- Actualizar la paleta de Brisa (sincronizarla desde «KColorScheme»).

### KWindowSystem

- [xcb] Asegurar que la señal «compositingChanged» se emite cuando «NETEventFilter» se vuelve a crear (error 362531).
- Añadir una API de conveniencia para consultar el sistema/plataforma de ventanas usado por Qt.

### KXMLGUI

- Corregir la sugerencia de tamaño mínimo (texto recortado) (error 312667).
- [KToggleToolBarAction] Respetar la restricción «action/options_show_toolbar».

### NetworkManagerQt

- Usar por defecto WPA2-PSK y WPA2-EAP al obtener el tipo de seguridad de las preferencias de la conexión.

### Iconos de Oxígeno

- Se ha añadido «application-menu» a Oxígeno (error 365629).

### Framework de Plasma

- Mantener compatible el *slot* «createApplet» con Frameworks 5.24.
- No borrar dos veces la textura «gl» en las miniaturas (error 365946).
- Se ha añadido un dominio de traducciones al objeto QML «wallpaper».
- No borrar manualmente miniaplicaciones.
- Se ha añadido una «kapptemplate» para el fondo de escritorio de Plasma.
- Plantillas: Registrar las plantillas en su propia categoría «Plasma/» de nivel superior.
- Plantillas: actualizar enlaces a la wiki de techbase en archivos README
- Definir que las «packagestructures» de Plasma extienden a «plasmashell».
- Permitir un tamaño para añadir miniaplicaciones.
- Definir «PackageStructure» de Plasma como complemento normal «PackageStructure» de «KPackage».
- Corrección: Se ha actualizado el archivo «config.qml» del fondo de ejemplo «Otoño» para que use «QtQuick.Controls».
- Usar «KPackage» para instalar paquetes de Plasma.
- Si pasamos un «QIcon» como argumento de «IconItem::Source», usarlo.
- Permitir superposición en «IconItem» de Plasma.
- Se ha añadido «Qt::Dialog» a los indicadores por omisión para satisfacer «QXcbWindow::isTransient()» (fallo 366278).
- [Tema Brisa de Plasma] Añadir los iconos «network-flightmode-on/off».
- Emitir «contextualActionsAboutToShow» antes de mostrar el menú «contextualActions» de la miniaplicación (error 366294).
- [TextField] Enlazar a la longitud de «TextField» en lugar de a la del texto.
- [Estilos de botones] Centrar horizontalmente en modo de solo iconos (error 365947).
- [Contención] Tratar «HiddenStatus» como estado bajo.
- Añadir el icono de «kruler» para la bandeja del sistema de Yuri Fabirovsky.
- Corregir el infame error de «diálogos que aparecen en el gestor de tareas» una vez más.
- Corregir el icono de red inalámbrica disponible con un emblema «?» (fallo 355490).
- IconItem: Usar una mejor aproximación para deshabilitar la animación al pasar de invisible a visible.
- Definir el tipo de ventana «Tooltip» en «ToolTipDialog» mediante la API «KWindowSystem».

### Solid

- Actualizar siempre el analizador de «Predicate» de las fuentes «yacc/lex».

### Sonnet

- hunspell: Limpieza del código para buscar diccionarios; se han añadido los directorios XDG (error 361409).
- Tratar de corregir un poco el uso del filtro de idioma durante la detección de idiomas.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
