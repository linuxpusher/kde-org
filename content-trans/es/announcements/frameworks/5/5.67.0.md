---
aliases:
- ../../kde-frameworks-5.67.0
date: 2020-02-02
layout: framework
libCount: 70
---
### General

- Se han adaptado los métodos desaconsejados de Qt 5.15, lo que reduce el número de advertencias durante la compilación.

### Baloo

- Se ha migrado la configuración de KConfig a KConfigXt para que KCM pueda usarla.

### Iconos Brisa

- Se ha creado el icono de Kate para el estilo Brisa basado en el nuevo diseño de Tyson Tan.
- Se ha cambiado el icono de VLC para que se parezca más a los iconos oficiales de VLC.
- Se ha añadido el icono de KTrip desde el repositorio de KTrip.
- Se ha añadido un icono para «application/sql».
- Se han borrado y añadido iconos de 22 píxeles para la repetición multimedia.
- Se ha añadido un icono para «text/vnd.kde.kcrash-report».
- Se ha convertido «application/x-ms-shortcut» en un icono de acceso rápido real.

### Módulos CMake adicionales

- Se ha añadido la variable de entorno de importación que faltaba.
- ECMAddAppIcon: Add sc in regex to extract extension from valid names
- ECMAddQch: support &amp; document K_DOXYGEN macro usage

### Integración con Frameworks

- Descartar la dependencia no usada de QtDBus.

### KActivitiesStats

- Fix broken SQL query in allResourcesQuery

### KActivities

- Remove files that Windows cannot handle
- Ensure to store resource uri without a trailing slash

### Herramientas KDE Doxygen

- Unbreak module imports for Python2
- Hardcode utf-8 as filesystem encoding with Python2 to help api.kde.org

### KCMUtils

- prefer the new kcm plugins to the old
- KCModuleQml: Ensure defaulted is emitted with the current configModule-&gt;representsDefaults on load
- Show button respecting what is declared by KCModule
- Update KPluginSelector to allow KCM to show good state for reset, apply and default button

### KConfig

- Se ha refactorizado «KConfigXT».
- Fix python bindings build after ebd14f29f8052ff5119bf97b42e61f404f223615
- KCONFIG_ADD_KCFG_FILES: regenerate also on new version of kconfig_compiler
- Allow to also pass a target instead of list of sources to KCONFIG_ADD_KCFG_FILES
- Add KSharedConfig::openStateConfig for storing state information
- Fix Python bindings compilation after 7ab8275bdb56882692846d046a5bbeca5795b009

### KConfigWidgets

- KStandardAction: add method for SwitchApplicationLanguage action creation
- [KColorSchemeManager] Don't list duplicates
- [KColorschemeManager] Add option to reenable following global theme

### KCoreAddons

- demote plugin load errors from warning to debug level + reword
- Document how to filter by servicetype the right way
- Add perlSplit() overload taking a QRegularExpression and deprecate the QRegExp one
- Add mime type for backtraces saved from DrKonqi
- Add utility text function KShell::tildeCollapse
- KPluginMetaData: add initialPreference() getter
- desktoptojson: also convert InitialPreference key

### KDeclarative

- Correctly compute bottom margin for grid delegates with subtitles
- [ConfigModule] Say which package is invalid

### KHolidays

- Update holidays and add flagdays and namedays for Sweden

### KI18n

- ki18n_wrap_ui: error when file doesn't exist
- [Kuit] Revert changes in parseUiMarker()

### KIO

- Add missing renamed event when a destination file already existed
- KFilePlacesModel: On new profile in recent show only recentlyused:/ based entries by default
- Añadir el constructor «KFileCustomDialog» con un parámetro «startDir».
- Fix QRegularExpression::wildcardToRegularExpression() usage
- Allow to handle apps with Terminal=True in their desktop file, handle their associated mimetype properly (bug 410506)
- KOpenWithDialog: Allow to return a newly created KService created associated to a mimetype
- Add KIO::DropJobFlag to allow manually showing the menu (bug 415917)
- [KOpenWithDialog] Hide collapsible group box when all options inside are hidden (bug 415510)
- Revertir la eliminación efectiva de «KUrlPixmapProvider» de la API.
- SlaveBase::dispatchLoop: Fix timeout calculation (bug 392768)
- [KDirOperator] Allow renaming files from the context menu (bug 189482)
- Upstream Dolphin's file rename dialog (bug 189482)
- KFilePlaceEditDialog: move logic into isIconEditable()

### Kirigami

- Clip the flickable parent item (bug 416877)
- Remove header top margin from private ScrollView
- proper size hint for the gridlayout (bug 416860)
- use attached property for isCurrentPage
- Deshacerse de varias advertencias.
- try to keep the cursor in window when typing in an OverlaySheet
- properly expand fillWidth items in mobile mode
- Add active, link, visited, negative, neutral and positive background colors
- Expose ActionToolBar's overflow button icon name
- Use QQC2 Page as base for Kirigami Page
- Specify where the code is coming from as the URL
- Don't anchor AbstractApplicationHeader blindly
- emit pooled after the properties have been reassigned
- add reused and pooled signals like TableView

### KJS

- Abort machine run once a timeout signal has been seen
- Support ** exponentiation operator from ECMAScript 2016
- Added shouldExcept() function that works based on a function

### KNewStuff

- Unbreak the KNSQuick::Engine::changedEntries functionality

### KNotification

- Add new signal for default action activation
- Drop dependency to KF5Codecs by using the new stripRichText function
- Strip richtext on Windows
- Adapt to Qt 5.14 Android changes
- Desaconsejar el uso de «raiseWidget».
- Se ha adaptado «KNotification» de «KWindowSystem».

### KPeople

- Adjust metainfo.yaml to new tier
- Se ha eliminado el código heredado de carga de complementos.

### KQuickCharts

- Se ha corregido la comprobación de la versión de Qt.
- Register QAbstractItemModel as anonymous type for property assignments
- Hide the line of a line chart if its width is set to 0

### Kross

- addHelpOption already adds by kaboutdata

### KService

- Support multiple values in XDG_CURRENT_DESKTOP
- Deprecate allowAsDefault
- Make "Default Applications" in mimeapps.list the preferred applications (bug 403499)

### KTextEditor

- Revert "improve word completion to use highlighting to detect word boundaries" (bug 412502)
- import final breeze icon
- Message-related methods: Use more member-function-pointer-based connect
- DocumentPrivate::postMessage: avoid multiple hash lookups
- fix Drag&amp;copy function (by using Ctrl Key) (bug 413848)
- ensure we have a quadratic icon
- set proper Kate icon in about dialog for KatePart
- inline notes: correctly set underMouse() for inline notes
- avoid use of old mascot ATM
- Variable expansion: Add variable PercentEncoded (bug 416509)
- Fix crash in variable expansion (used by external tools)
- KateMessageWidget: remove unused event filter installation

### KTextWidgets

- Descartar la dependencia de «KWindowSystem».

### Framework KWallet

- Revert readEntryList() to use QRegExp::Wildcard
- Fix QRegularExpression::wildcardToRegularExpression() usage

### KWidgetsAddons

- [KMessageWidget] Subtract the correct margin
- [KMessageBox] Only allow selecting text in the dialog box using the mouse (bug 416204)
- [KMessageWidget] Use devicePixelRatioF for animation pixmap (bug 415528)

### KWindowSystem

- [KWindowShadows] Check for X connection
- Introduce shadows API
- Desaconsejar el uso de «KWindowEffects::markAsDashboard()».

### KXMLGUI

- Use KStandardAction convenience method for switchApplicationLanguage
- Allow programLogo property to be a QIcon, too
- Remove ability to report bugs against arbitrary stuff from a static list
- Remove compiler information from bug report dialog
- KMainWindow: fix autoSaveSettings to catch QDockWidgets being shown again
- i18n: Add more semantic context strings
- i18n: Split translations for strings "Translation"

### Framework de Plasma

- Fixed tooltip corners and removed useless color attributes
- Removed hardcoded colors in background SVGs
- Fix the size and pixel alignment of checkboxes and radiobuttons
- Se han actualizado las sombras del tema Brisa.
- [Plasma Quick] Add WaylandIntegration class
- Same behavior for scrollbar as the desktop style
- Make use of KPluginMetaData where we can
- Add edit mode menu item to desktop widget context menu
- Consistency: colored selected buttons
- Port endl to \n Not necessary to flush as QTextStream uses QFile which flush when it's deleted

### Purpose

- Fix QRegularExpression::wildcardToRegularExpression() usage

### QQC2StyleBridge

- Remove scrollbar related workarounds from list delegates
- [TabBar] Eliminar marco.
- Add active, link, visited, negative, neutral and positive background colors
- Usar «hasTransientTouchInput».
- always round x and y
- support mobile mode scrollbar
- ScrollView: Do not overlay scrollbars over contents

### Solid

- Add signals for udev events with actions bind and unbind
- Clarificar la referencia a «DeviceInterface» (error 414200).

### Resaltado de sintaxis

- Updates nasm.xml with the latest instructions
- Perl: Add 'say' to keyword list
- cmake: Fix <code>CMAKE*POLICY**_CMP&amp;lt;N&amp;gt;</code> regex and add special args to <code>get_cmake_property</code>
- Se ha añadido la definición del resaltado de sintaxis para GraphQL.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
