---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Corregir el manejo de «limit/offset» en «SearchStore::exec».
- Volver a crear el índice de baloo
- Configuración de balooctl: añadir opciones para asignar y ver onlyBasicIndexing
- Portar comprobación de balooctl para que funcione con la nueva arquitectura (error 353011)
- FileContentIndexer: corregir la emisión de filePath dos veces
- UnindexedFileIterator: mtime es un quint32, no un quint64
- Transacción: corregir otro fallo tipográfico en Dbi
- Transacción: corregir el uso incorrecto de Dbis en documentMTime() y en documentCTime().
- Transacción: checkPostingDbInTermsDb: Optimizar código
- Corregir advertencias de dbus
- Balooctl: Añadir la orden checkDb
- configuración de balooctl: añadir filtro de exclusión
- KF5Baloo: asegurar que las interfaces de D-Bus se generan antes de usarlas (error 353308)
- Evitar el uso de QByteArray::fromRawData
- Eliminar baloo-monitor de baloo
- TagListJob: emitir un error cuando falla la apertura de la base de datos
- No ignorar subtérminos si no se encuentran
- Código más limpio cuando falla «Baloo::File::load()» al fallar la apertura de la base de datos.
- Hacer que balooctl use IndexerConfig en lugar de manejar baloofilerc directamente
- Mejorar la internacionalización de balooshow
- Hacer que balooshow falle educadamente si no se puede abrir la base de datos.
- Hacer que «Baloo::File::load()» falle si la base de datos no está abierta (error 343049).
- IndexerConfig: añadir el método refresh().
- inotify: no simular un evento closedWrite tras mover sin cookie.
- ExtractorProcess: eliminar el «n» al final de la ruta de archivo.
- baloo_file_extractor: llamar a QProcess::close antes de destruir el QProcess
- baloomonitorplugin/balooctl: internacionalizar el estado del indexador.
- BalooCtl: Añadir una opción «config»
- Hacer que la búsqueda de baloo sea más presentable
- Eliminar archivos EventMonitor vacíos
- BalooShow: mostrar más información cuando los identificadores no coinciden
- BalooShow: cuando se llama con un identificador, comprobar que el identificador es correcto
- Añadir una clase FileInfo
- Añadir comprobaciones de errores en diversos puntos para que Baloo no se cuelgue cuando esté desactivado (error 352454).
- Corregir que Baloo no respeta la opción de configuración «solo indexación básica».
- Monitor: extraer el tiempo restante durante el inicio
- Usar llamadas a métodos reales en MainAdaptor en lugar de QMetaObject::invokeMethod
- Añadir la interfaz org.kde.baloo al objeto raíz para garantizar la compatibilidad con versiones previas
- Corregir la visualización de cadena de fecha en la barra de direcciones debido a haberse portado a QDate
- Añadir una demora tras cada archivo en lugar de tras cada trabajo por lotes
- Eliminar la dependencia de Qt::Widgets en baloo_file
- Eliminar código no usado en baloo_file_extractor
- Añadir monitor de baloo para complemento qml experimental
- Hacer que el hilo de «solicitud de tiempo restante» sea seguro
- kioslaves: añadir redefinión ausente para funciones virtuales
- Extractor: asignar los datos de la aplicación tras construir la aplicación
- Consulta: implementar el uso de «desplazamiento»
- Balooctl: añadir --version y --help (error 351645)
- Se ha eliminado la compatibilidad con KAuth para aumentar el número máximo de supervisiones de inotify si el recuento es demasiado bajo (error 351602).

### BluezQt

- Corregir un cuelgue de fakebluez en obexmanagertest con ASAN
- Declarar anticipadamente todas las clases exportadas en «types.h».
- ObexTransfer: indicar un error cuando se elimina la sesión de transferencia
- Utilidades: mantener los punteros a las instancias de los gestores
- ObexTransfer: indicar un error cuando se cuelga org.bluez.obex

### Módulos CMake adicionales

- Actualizar la caché de iconos GTK cuando se instalan iconos.
- Eliminar el remedio para demorar la ejecución en Android
- ECMEnableSanitizers: El saneador no definido está permitido por gcc 4.9.
- Desactivar la detección de X11, XCB, etc., en OS X
- Buscar archivos en el prefijo instalado en lugar de en la ruta del prefijo
- Usar Qt5 para indicar cuál es el prefijo de instalación de Qt5
- Añadir la definición ANDROID, ya que es necesaria en qsystemdetection.h.

### Integración con Frameworks

- Se ha solucionado el problema del diálogo de archivo que no aparece de forma aleatoria (fallo 350758).

### KActivities

- Uso de una función de coincidencia personalizada en lugar del «glob» de «sqlite» (fallo 352574).
- Se ha corregido un problema al añadir un nuevo recurso al modelo

### KCodecs

- Corregir un cuelgue en UnicodeGroupProber::HandleData con cadenas cortas

### KConfig

- Marcar kconfig-compiler como herramienta sin interfaz gráfica

### KCoreAddons

- KShell::splitArgs: considerar solo el espacio ASCII como separador, no el espacio unicode U+3000 (error 345140)
- KDirWatch: corregir un cuelgue cuando un destructor estático global usa KDirWatch::self() (error 353080)
- Corregir cuelgue cuando KDirWatch se usa en Q_GLOBAL_STATIC.
- KDirWatch: corregir seguridad en hilos
- Aclarar cómo definir los argumentos del constructor de KAboutData.

### KCrash

- KCrash: pasar cwd a kdeinit cuando la aplicación se reinicia automáticamente usando kdeinit. (error 337760)
- Añadir KCrash::initialize() para que las aplicaciones y el complemento de la plataforma puedan activar explícitamente KCrash.
- Desactivar ASAN si está activo

### KDeclarative

- Pequeñas mejoras en ColumnProxyModel
- Hacer posible que las aplicaciones conozcan la ruta al directorio personal
- mover EventForge del contenedor de escritorio
- Proporcionar la propiedad «enabled» para QIconItem.

### KDED

- kded: simplificar la lógica que envuelve a sycoca: solo se llama a ensureCacheValid.

### Soporte de KDELibs 4

- Llamar a «newInstance» desde el hijo en la primera invocación.
- Usar definiciones de kdewin.
- Evitar que se busque X11 en WIN32
- cmake: corregir la comprobación de la versión de taglib en FindTaglib.cmake.

### KDesignerPlugin

- Qt moc no puede manejar macros (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- Implementar metadatos de usuario de las ventanas.

### Complementos KDE GUI

- No buscar X11/XCB también tiene sentido para WIN32

### KHTML

- Sustituir std::auto_ptr por std::unique_ptr
- khtml-filter: Descartar reglas que contienen funcionalidades especiales de bloqueo de publicidad que todavía no se contemplan.
- khtml-filter: organización del código, sin cambios funcionales.
- khtml-filter: ignorar expresiones regulares con opciones, ya que no las usamos.
- khtml-filter: corregir el delimitador de opciones de la detección de bloqueo de publicidad.
- khtml-filter: Limpieza de espacios en blanco sobrantes.
- khtml-filter: No descartar las líneas que empiezan por '&amp;' ya que no se trata de un carácter especial de bloqueo de publicidad.

### KI18n

- Eliminar los iteradores estrictos de «msvc» para que se pueda compilar «ki18n».

### KIO

- KFileWidget: el argumento «parent» debe ser 0 de forma predeterminada, como en el resto de widgets.
- Asegurarse de que el tamaño de la matriz de bytes que se acaba de volcar en la estructura sea lo suficientemente grande antes de calcular «targetInfo»; en caso contrario, estaremos accediendo a memoria que no nos pertenece.
- Corregir el uso de Qurl al llamar a QFileDialog::getExistingDirectory()
- Refrescar la lista de dispositivos de Solid antes de consultar en «kio_trash».
- Permitir «trash:» además de «trash:/» como URL para «listDir» (llama a «listRoot») (error 353181).
- KProtocolManager: se ha corregido una situación que conducía a un punto muerto al usar EnvVarProxy. (bug 350890)
- Evitar que se busque X11 en WIN32
- KBuildSycocaProgressDialog: usar el indicador de ocupado incluido en Qt. (error 158672)
- KBuildSycocaProgressDialog: ejecutar kbuildsycoca5 con QProcess.
- KPropertiesDialog: Corrección para cuando «~/.local» es un enlace simbólico, comparar las rutas canónicas.
- Permitir el uso de recursos de red compartidos en kio_trash (error 177023)
- Conectar a las señales de QDialogButtonBox, no de QDialog (error 352770)
- KCM de cookies: actualizar los nombres de DBus para kded5.
- Usar archivos JSON directamente en lugar de kcoreaddons_desktop_to_json()

### KNotification

- No enviar dos veces la señal de actualización de la notificación
- Volver a analizar la configuración de la notificación solo si cambia
- Evitar que se busque X11 en WIN32

### KNotifyConfig

- Cambiar el método para cargar valores predeterminados.
- Enviar el nombre de la aplicación cuya configuración se ha actualizado junto a la señal DBus.
- Añadir método para revertir kconfigwidget a sus valores predeterminados
- No sincronizar la configuración X veces antes de guardarla.

### KService

- Uso de la marca de tiempo mayor en el subdirectorio como marca de tiempo del directorio de recursos.
- KSycoca: guardar el «mtime» de cada directorio de origen para detectar cambios (error 353036).
- KServiceTypeProfile: Se ha eliminado la creación de factoría innecesaria (error 353360).
- Simplificar y acelerar KServiceTest::initTestCase.
- Hacer que el nombre de instalación del archivo «applications.menu» sea una variable de «cmake» en caché.
- KSycoca: ensureCacheValid() debe crear la base de datos si no existe
- KSycoca: hacer que la base de datos global funcione tras el código de comprobación de la hora de reciente.
- KSycoca: cambiar el nombre de archivo de la base de datos para incluir el idioma y el «sha1» de los directorios a partir de los cuales se construyó.
- KSycoca: hacer que ensureCacheValid() sea parte de la API pública.
- KSycoca: añadir un «QPointer» para eliminar más usos de instancias únicas.
- KSycoca: eliminar todos los métodos «self()» de las factorías y guardarlos en su lugar en KSycoca.
- KBuildSycoca: eliminar la escritura del archivo ksycoca5stamp.
- KBuildSycoca: usar qCWarning en lugar de fprintf(stderr, ...) o de qWarning
- KSycoca: reconstruir ksycoca en el proceso en lugar de ejecutar kbuildsycoca5
- KSycoca: Se ha movido todo el código de «kbuildsycoca» a una biblioteca, excepto la función «main()».
- Optimización de KSycoca: Supervisar el archivo solo si la aplicación se conecta a «databaseChanged()».
- Corregir fugas de memoria en la clase KBuildSycoca
- KSycoca: Sustituir la notificación DBus con una supervisión de archivo usando «KDirWatch».
- kbuildsycoca: desaconsejar el uso de la opción --nosignal.
- KBuildSycoca: Sustituir el bloqueo basado en DBus por un archivo de bloqueo.
- No fallar cuando se encuentre información no válida sobre complementos.
- Cambiar el nombre de los archivos de cabecera a «_p.h» en preparación de la mudanza a la biblioteca «kservice».
- Mover checkGlobalHeader() dentro de KBuildSycoca::recreate().
- Eliminar el código para --checkstamps y para --nocheckfiles.

### KTextEditor

- validar más expresiones regulares
- corregir expresiones regulares en archivos HL (error 352662)
- Sincronizar ocaml HL con el estado de https://code.google.com/p/vincent-hugot-projects/ antes de que el código de Google caiga; algunas pequeñas correcciones de fallos.
- Añadir división de palabras (error 352258).
- Validar la línea antes de llamar a las funciones de plegado (error 339894).
- Se han corregido los problemas de recuento de palabras en Kate al escuchar a «DocumentPrivate» en lugar de a «Document» (error 353258).
- Actualización del resaltado de sintaxis de KConfig: se han añadido nuevos operadores de Linux 4.2.
- Sincronizar con la rama «kate» de KDE/4.14.
- minimap: Corregir que no se dibuje el asa de la barra de desplazamiento cuando no contiene marcas de desplazamiento (error 352641).
- sintaxis: añadir la opción git-user para kdesrc-buildrc

### Framework KWallet

- No cerrar automáticamente tras el último uso.

### KWidgetsAddons

- Corregir la advertencia C4138 (MSVC): «*/» encontrado fuera de un comentario.

### KWindowSystem

- Realizar una copia profunda de QByteArray get_stringlist_reply
- Permitir la interacción con múltiples servidores X en las clases NETWM.
- [xcb] Considerar los «mods» en «KKeyServer» como se inicializan cuando la plataforma es distinta de X11.
- Se ha cambiado «KKeyserver» (x11) para que produzca un registro categorizado.

### KXMLGUI

- Hacer posible que se puedan importar/exportar esquemas de accesos rápidos simétricamente.

### NetworkManagerQt

- Se han corregido introspecciones: «LastSeen» debe estar en «AccessPoint» y no en «ActiveConnection».

### Framework de Plasma

- Hacer que el diálogo de ayuda emergente se oculte cuando el cursor entra en el área inactiva de la ayuda emergente.
- Si el archivo de escritorio contiene «Icon=/foo.svgz», usar dicho archivo del paquete.
- Añadir un tipo de archivo «screenshot» en los paquetes.
- Considerar «devicepixelration» en barras de desplazamiento aisladas.
- No mostrar el efecto de situar el cursor sobre un elemento en pantallas táctiles+móviles.
- Usar los márgenes SVG de «lineedit» en el cálculo de «sizeHint».
- No atenuar el icono animado en las ayudas emergentes de Plasma.
- Se ha corregido la elisión de texto de los botones.
- Los menús de contexto de los componentes de un panel ya no se solapan con el componente
- Se ha simplificado la obtención de la lista de aplicaciones asociadas en «AssociatedApplicationManager».

### Sonnet

- Se ha corregido el ID del complemento «hunspell» para que se cargue correctamente.
- Permitir la compilación estática en Windows, añadir la ruta del diccionario de hunspell de libreoffice para Windows.
- No asumir diccionarios de Hunspell codificados en UTF-8. (error 353133)
- Se ha corregido «Highlighter::setCurrentLanguage()» para los casos en los que el lenguaje anterior no era válido (fallo 349151).
- permitir /usr/share/hunspell como ubicación de diccionarios
- Complemento basado en NSSpellChecker

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
