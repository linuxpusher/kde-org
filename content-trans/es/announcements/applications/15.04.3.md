---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE lanza las Aplicaciones de KDE 15.04.3
layout: application
title: KDE lanza las Aplicaciones de KDE 15.04.3
version: 15.04.3
---
Hoy, 1 de julio de 2015, KDE ha lanzado la tercera actualización de estabilización para las <a href='../15.04.0'>Aplicaciones 15.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en kdenlive, kdepim, kopete, ktp-contact-list, marble, okteta y umbrello.

También se incluyen versiones de los Espacios de trabajo Plasma 4.11.21, de la Plataforma de desarrollo de KDE 4.14.10 y de la suite Kontact 4.14.10 que contarán con asistencia a largo plazo.
