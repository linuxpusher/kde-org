---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE lanza las Aplicaciones de KDE 18.04.2
layout: application
title: KDE lanza las Aplicaciones de KDE 18.04.2
version: 18.04.2
---
Hoy, 7 de junio de 2018, KDE ha lanzado la segunda actualización de estabilización para las <a href='../18.04.0'>Aplicaciones 18.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 25 correcciones de errores registradas, se incluyen mejoras en Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize y Okular, entre otras aplicaciones.

Las mejoras incluyen:

- Las operaciones con imágenes de Gwenview se pueden volver a realizar tras haberlas deshecho.
- KGpg ya no falla al descifrar mensajes sin una cabecera de versión.
- La exportación de hojas de trabajo de Cantor a LaTeX se ha corregido para las matrices de Maxima.
