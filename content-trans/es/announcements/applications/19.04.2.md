---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE lanza las Aplicaciones 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE lanza las Aplicaciones de KDE 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

KDE ha lanzado hoy la segunda actualización de estabilización para las <a href='../19.04.0'>Aplicaciones 19.04</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 50 correcciones de errores registradas, se incluyen mejoras en Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular y Spectacle, entre otras aplicaciones.

Las mejoras incluyen:

- Se ha corregido un fallo en la visualización de ciertos documentos EPUB en Okular.
- Las claves secretas vuelven a poder exportarse desde el gestor de criptografía Kleopatra.
- El recordatorio de eventos de KAlarm ya no falla al iniciarse con las bibliotecas más recientes de PIM.
