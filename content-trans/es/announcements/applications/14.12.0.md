---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE lanza las Aplicaciones 14.12.
layout: application
title: KDE lanza las Aplicaciones de KDE 14.12
version: 14.12.0
---
Hoy, 17 de diciembre de 2014, se han lanzado las aplicaciones de KDE 14.12. Esta versión incluye nuevas funcionalidades y soluciones de errores para más de un centenar de aplicaciones. La mayoría de estas aplicaciones se basan en la plataforma de desarrollo 4. Algunas se han convertido al nuevo <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, un conjunto de bibliotecas modulares que se basan en Qt5, la última versión de esta popular infraestructura multiplataforma de aplicaciones.

<a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> es una biblioteca nueva en esta versión que permite la detección y el reconocimiento facial en fotografías.

La versión incluye las primeras versiones basadas en KDE Frameworks 5 de <a href='http://www.kate-editor.org'>Kate</a> y <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, <a href='https://www.kde.org/applications/system/konsole/s'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/s'>Gwenview</a>, <a href='http://edu.kde.org/kalgebras'>KAlgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>KHangman</a>, <a href='http://edu.kde.org/kig'>Kig</a>, <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> y <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Algunas bibliotecas también están preparadas para usar KDE Frameworks 5: analitza y libkeduvocdocument.

La <a href='http://kontact.kde.org'>Suite Kontact</a> se encuentra ahora en la versión 4.14 de asistencia a largo plazo. Los desarrolladores se están concentrando con nuevas energías en la migración a KDE Frameworks 5.

Las siguientes son algunas de las nuevas funcionalidades de esta versión:

+ <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> tiene una nueva versión gracias a KDE Frameworks 5 y ahora cuenta con la funcionalidad de <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>imprimir gráficos en 3D</a>
+ <a href='http://edu.kde.org/kgeography'>KGeography</a> tiene un nuevo mapa para Bihar.
+ El visor de documentos <a href='http://okular.kde.org'>Okular</a> ahora admite búsquedas inversas para latex-synctex en dvi y algunas pequeñas mejoras en el uso de ePub.
+ <a href='http://umbrello.kde.org'>Umbrello</a>, la aplicación para modelado UML, tiene muchas nuevas funcionalidades, demasiado numerosas para enumerar aquí.

La versión de abril de KDE Applications 15.04 incluirá numerosas nuevas funcionalidades, así como más aplicaciones basadas en el modular KDE Frameworks 5.
