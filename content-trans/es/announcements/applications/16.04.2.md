---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE lanza las Aplicaciones de KDE 16.04.2
layout: application
title: KDE lanza las Aplicaciones de KDE 16.04.2
version: 16.04.2
---
14 de junio de 2016. KDE ha lanzado la segunda actualización de estabilización para las <a href='../16.04.0'>Aplicaciones 16.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones y será una actualización agradable y segura para todo el mundo.

Entre las más de 25 correcciones de errores registradas, se incluyen mejoras en akonadi, ark, artikulate, dolphin, kdenlive y kdepim, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.21 que contará con asistencia a largo plazo.
