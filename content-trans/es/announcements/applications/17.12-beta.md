---
aliases:
- ../announce-applications-17.12-beta
custom_spread_install: true
date: 2017-11-17
description: KDE lanza la Beta de las Aplicaciones 17.12.
layout: application
release: applications-17.11.80
title: KDE lanza la beta para las Aplicaciones 17.12
---
Hoy, 17 de noviembre de 2017, KDE ha lanzado la versión beta de las nuevas versiones de las Aplicaciones de KDE. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Consulte las <a href='https://community.kde.org/Applications/17.12_Release_Notes'>notas de lanzamiento de la comunidad</a> para obtener información sobre los paquetes que ahora se basan en KF5 y sobre los problemas conocidos. Se realizará un anuncio más completo para la versión final.

Los lanzamientos de las Aplicaciones de KDE 17.12 necesitan una prueba exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la beta <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.

#### Instalación de paquetes binarios de las Aplicaciones de KDE 17.12 Beta

<em>Paquetes</em>. Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de Beta de 17.12 (internamente, 17.11.80) para algunas versiones de sus distribuciones y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. En las próximas semanas estarán disponibles paquetes binarios adicionales, así como actualizaciones de los paquetes disponibles en este momento.

<em>Paquetes</em>. Para obtener una lista actualizada de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE tiene conocimiento, visite la <a href='http://community.kde.org/Binary_Packages'>Wiki de la Comunidad</a>.

#### Compilación de las Aplicaciones de KDE 17.12 Beta

La totalidad del código fuente de las Aplicaciones de KDE 17.12 Beta se puede <a href='http://download.kde.org/unstable/applications/17.11.80/src/'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar y realizar la instalación <a href='/info/applications/applications-17.11.80'>página de información de las Aplicaciones de KDE 17.12 Beta</a>.
