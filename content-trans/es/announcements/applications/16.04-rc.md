---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: KDE lanza la candidata a versión final para las Aplicaciones 16.04
layout: application
release: applications-16.03.90
title: KDE lanza la candidata a versión final para las Aplicaciones 16.04
---
Hoy, 7 de marzo de 2016, KDE ha lanzado la candidata a versión final de las nuevas Aplicaciones. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Debido a las numerosas aplicaciones que se basan en KDE Frameworks 5, es necesario probar la versión 16.04 de las Aplicaciones de manera exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario de KDE. Los usuarios reales son de vital importancia para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera precoz de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la versión <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.

#### Instalación de paquetes binarios de las aplicaciones de la candidata a versión final de KDE 16.04.

<em>Paquetes</em>. Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de la candidata a versión final de 16.04 (internamente, 16.03.90) para algunas versiones de sus distribuciones y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. En las próximas semanas estarán disponibles paquetes binarios adicionales, así como actualizaciones de los paquetes disponibles en este momento.

<em>Ubicación de paquetes</em>. Para obtener una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE tiene conocimiento, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki de la Comunidad</a>.

#### Compilación de las aplicaciones de la candidata a versión final de KDE 16.04

La totalidad del código fuente de la candidata a versión final de las Aplicaciones 16.04 se puede <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar dicha versión en la <a href='/info/applications/applications-16.03.90'>página de información sobre la candidata a versión final de las aplicaciones de KDE</a>.
