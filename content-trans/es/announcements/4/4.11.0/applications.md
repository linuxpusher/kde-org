---
date: 2013-08-14
hidden: true
title: Las aplicaciones de KDE 4.11 experimentan un gran avance en la gestión de la
  información personal y mejoras en todas ellas
---
En esta versión, el administrador de archivos Dolphin incorpora la solución de numerosos errores. La carga de carpetas grandes es más rápida y necesita hasta un 30 &#37; menos de memoria. Los altos niveles de actividad del disco y de la CPU se evitan cargando solamente las vistas previas de los elementos visibles. También se incluyen otras mejoras como, por ejemplo, la solución de numerosos errores que afectaban a las carpetas expandidas en la vista de detalles, ya no se muestran iconos «desconocidos» cuando se accede a una carpeta y ahora, al hacer clic en un archivo con el botón central, se abre una nueva pestaña con el contenido del archivo, lo que crea una experiencia general más consistente.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`El nuevo flujo de trabajo «enviar más tarde» de Kontact` width="600px">}}

## Mejoras de la suite Kontact

Una vez más, la suite Kontact se ha centrado en la estabilidad, en el rendimiento y en el uso de memoria. Tareas como Importar archivos, cambiar entre mapas, obtener correo, marcar o mover un gran número de mensajes y el tiempo de instalación se han mejorado en los últimos 6 meses. Consulte <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>este blog</a> para obtener más información. Se han solucionado muchos errores en la <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>funcionalidad de compresión de archivos</a> y también se han producido mejoras en el asistente de importación, que ahora permite importar las preferencias desde el cliente de correo Trojitá y una mejor importación desde otras aplicaciones. Puede encontrar más información <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>aquí</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`El agente de compresión gestiona el almacenamiento de correo de forma comprimida` width="600px">}}

Esta versión también incluye algunas mejoras significativas. Hay un <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>nuevo editor de temas para las cabeceras de los mensajes de correo</a> y se puede modificar el tamaño de las imágenes de los mensajes de correo sobre la marcha. La <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>funcionalidad de «enviar más tarde»</a> permite planificar el envío de mensajes de correo en una fecha y hora determinada con la posibilidad añadida de repetir el envío siguiendo una frecuencia determinada. Se ha mejorado la implementación del filtro KMail Sieve (una funcionalidad IMAP que permite el filtrado en el servidor), lo que permite a los usuarios generar scripts de filtrado <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>con una interfaz fácil de utilizar</a>. En el área de la seguridad, KMail presenta la «detección automática de estafas», la cual <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>muestra una advertencia</a> cuando los mensajes contienen los típicos trucos de fraude electrónico. Ahora se recibe una <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>notificación informativa</a> cuando llega un nuevo mensaje. Y por último, pero no por ello menos importante, la aplicación para creación de blogs, BLogilo, incluye una gran mejora en el editor HTML basado en QtWebKit.

## Ampliación de los lenguajes admitidos en Kate

El editor avanzado de texto, Kate, incluye nuevos complementos: Python (2 y 3), JavaScript y JQuery, Django y XML, los cuales presentan mejoras como el autocompletado estático y dinámico, comprobadores de sintaxis, inserción de fragmentos de código y la capacidad de sangrar XML con un acceso rápido. Pero aún hay más para los entusiastas de Python: una consola de Python que proporciona información detallada sobre el archivo fuente que esté abierto. También se han realizado algunas pequeñas mejoras en la interfaz de usuario, como las <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>nuevas notificaciones pasivas para la funcionalidad de búsqueda</a>, las <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimizaciones para el modo VIM</a> y un <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>nueva funcionalidad de plegado de texto</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars muestra los próximos acontecimientos interesantes que pueden verse desde su ubicación` width="600px">}}

## Otras mejoras de las aplicaciones

En el área de los juegos y de la educación, se han incluido varias funcionalidades y optimizaciones de todos los tamaños. Los futuros mecanógrafos disfrutarán de la implementación de la escritura de derecha a izquierda en KTouch mientras que el compañero de los aficionados a la astronomía ahora cuenta con una herramienta que muestra los próximos acontecimientos interesantes de su zona. Las herramientas matemáticas Rocs, Kig, Cantor y KAlgebra también han experimentado mejoras y ahora admiten más motores y cálculos. Y el juego KJumpingCube ahora incorpora mayores tamaños de tablero, nuevos niveles de dificultad, respuestas más rápidas y una mejor interfaz de usuario.

La sencilla aplicación de dibujo Kolourpaint ahora admite el formato de imagen WebP y el visor universal de documentos Okular ahora incorpora herramientas de revisión configurables e incluye las funcionalidades de deshacer y rehacer para los formularios y las anotaciones. El reproductor de audio JuK admite la reproducción y la edición de los metadatos del nuevo formato de audio Ogg Opus (aunque para ello es necesario que el controlador de audio y TagLib también admitan el formato Ogg Opus).

#### Instalación de las aplicaciones de KDE

El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href='http://windows.kde.org'>KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href='http://mac.kde.org/'>KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no se utilizan. <a href='http://plasma-active.org'>Plasma Active</a> es una experiencia de usuario para una amplia variedad de dispositivos, como tabletas y otro tipo de hardware móvil.

Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a>, y también en <a href='/download'>CD-ROM</a> o con cualquiera de los <a href='/distributions'>principales sistemas GNU/Linux y UNIX</a> de la actualidad.

##### Paquetes

Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 4.11.0 para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. <br />

##### Ubicación de los paquetes

Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha sido notificado, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki de la Comunidad</a>.

La totalidad del código fuente de %[1] se puede <a href='/info/4/4.11.0'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar el software de KDE 4.11.0 en la <a href='/info/4/4.11.0#binary'>Página de información sobre 4.11.0</a>.

#### Requisitos del sistema

Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, como la 4.8.4. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.<br />Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.

## También se han anunciado hoy:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Los espacios de trabajo de Plasma 4.11 continúan refinando la experiencia de usuario</a>

Preparándose para un mantenimiento a largo plazo, los espacios de trabajo de Plasma proporcionan más mejoras a la funcionalidad básica con una barra de tareas más suave, un elemento gráfico más inteligente para la batería y un mezclador de sonido mejorado. La introducción de KScreen aporta a los espacios de trabajo una gestión inteligente de varios monitores y las mejoras de rendimiento a gran escala combinadas con pequeños ajustes de ergonomía hacen que la experiencia general sea más agradable.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 proporciona mejor rendimiento</a>

Esta versión de la plataforma de KDE 4.11 continúa centrándose en la estabilidad. Se implementan nuevas funcionalidades de cara a la futura versión de KDE Frameworks 5.0, pero para la versión estable hemos conseguido incluir optimizaciones para nuestro sistema Nepomuk.
