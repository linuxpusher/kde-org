---
aliases:
- ../announce-4.11.2
date: 2013-10-01
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.2.
title: KDE выпускает октябрьские обновления для среды Plasma, приложений и платформы
  KDE
---
1 октября 2013 года. Сегодня KDE выпустила обновления для среды, приложений и платформы разработки KDE. Это второе обновление из серии ежемесячных стабилизирующих обновлений для версии 4.11. Как было объявлено при выходе этой версии, среда KDE 4.11 будет поддерживаться и обновляться в течение следующих двух лет. Так как этот выпуск содержит только исправления ошибок, установить его будет приятно и безопасно для всех.

Зафиксировано более 70 исправлений ошибок в менеджере окон KWin, файловом менеджере Dolphin, электронном секретаре Kontact и других приложениях. Наконец, улучшена стабильность и как обычно обновлены переводы.

A more complete <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.2&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>list</a> of changes can be found in KDE's issue tracker. For a detailed list of changes that went into 4.11.2, you can also browse the Git logs.

Скачать исходный код или установочные пакеты вы можете на странице <a href='/info/4/4.11.2'>информации о версии 4.11.2</a>. Если вы хотите узнать больше о версии 4.11 среды, приложений и платформы KDE, прочитайте <a href='/announcements/4.11/'>замечания к выпуску 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Новый агент «Отправить позже» в Kontact'е` width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/4.11.2/'>download.kde.org</a> or from any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.
