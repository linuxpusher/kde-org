---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE выпускает KDE Applications 16.12.2
layout: application
title: KDE выпускает KDE Applications 16.12.2
version: 16.12.2
---
February 9, 2017. Today KDE released the second stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to kdepim, dolphin, kate, kdenlive, ktouch, okular, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.29.
