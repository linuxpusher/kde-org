---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: KDE Ships Applications 17.12 Release Candidate.
layout: application
release: applications-17.11.90
title: KDE выпускает первую версию-кандидат к выпуску KDE Applications 17.12
---
1 декабря 2017 года. Сегодня KDE выпустило версию-кандидат к выпуску KDE Applications. Теперь, когда программные зависимости и функциональность «заморожены», команда KDE сконцентрируется на исправлении ошибок и наведении красоты.

Check the <a href='https://community.kde.org/Applications/17.12_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

Выпуски KDE Applications 17.12 требуют тщательного тестирования для обеспечения должного уровня качества и удовлетворения пользователей. Очень важную роль в этом играют те, кто используют нашу систему повседневно, ведь сами разработчики просто не могут протестировать все возможные конфигурации. Мы рассчитываем на вашу помощь в поиске ошибок на этом этапе, чтобы мы могли исправить их до выхода финальной версии. По возможности включайтесь в команду — установите эту версию-кандидат в выпуски и <a href='https://bugs.kde.org/'>сообщайте нам</a> о найденных ошибках.

#### Установка двоичных пакетов KDE Applications 17.12 RC

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 17.12 Release Candidate (internally 17.11.90) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/Binary_Packages'>Community Wiki</a>.

#### Компиляция KDE Applications 17.12 RC

The complete source code for KDE Applications 17.12 Release Candidate may be <a href='http://download.kde.org/unstable/applications/17.11.90/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-17.11.90'>KDE Applications 17.12 Release Candidate Info Page</a>.
