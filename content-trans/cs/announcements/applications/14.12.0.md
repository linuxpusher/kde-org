---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE Ships Applications 14.12.
layout: application
title: KDE vydává Aplikace KDE 14.12
version: 14.12.0
---
December 17, 2014. Today KDE released KDE Applications 14.12. This release brings new features and bug fixes to more than a hundred applications. Most of these applications are based on the KDE Development Platform 4; some have been converted to the new <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, a set of modularized libraries that are based on Qt5, the latest version of this popular cross-platform application framework.

<a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> is new in this release; it is a library to enable face detection and face recognition in photographs.

The release includes the first KDE Frameworks 5-based versions of <a href='http://www.kate-editor.org'>Kate</a> and <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, <a href='https://www.kde.org/applications/system/konsole/s'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/s'>Gwenview</a>, <a href='http://edu.kde.org/kalgebras'>KAlgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>KHangman</a>, <a href='http://edu.kde.org/kig'>Kig</a>, <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> and <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Some libraries are also ready for KDE Frameworks 5 use: analitza and libkeduvocdocument.

The <a href='http://kontact.kde.org'>Kontact Suite</a> is now in Long Term Support in the 4.14 version while developers are using their new energy to port it to KDE Frameworks 5

Some of the new features in this release include:

+ <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> has a new Android version thanks to KDE Frameworks 5 and is now able to <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>print its graphs in 3D</a>
+ <a href='http://edu.kde.org/kgeography'>KGeography</a> has a new map for Bihar.
+ The document viewer <a href='http://okular.kde.org'>Okular</a> now has support for latex-synctex reverse searching in dvi and some small improvements in the ePub support.
+ <a href='http://umbrello.kde.org'>Umbrello</a> --the UML modeller-- has many new features too numerous to list here.

The April release of KDE Applications 15.04 will include many new features as well as more applications based on the modular KDE Frameworks 5.
