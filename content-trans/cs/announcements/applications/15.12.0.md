---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE vydává Aplikace KDE 15.12.
layout: application
release: applications-15.12.0
title: KDE vydává Aplikace KDE 15.12.0
version: 15.12.0
---
16. prosince 2015. KDE vydává Aplikace KDE 15.12.

KDE is excited to announce the release of KDE Applications 15.12, the December 2015 update to KDE Applications. This release brings one new application and feature additions and bug fixes across the board to existing applications. The team strives to always bring the best quality to your desktop and these applications, so we're counting on you to send your feedback.

In this release, we've updated a whole host of applications to use the new <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, including <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> and more of the KDE Games, apart from the KDE Image Plugin Interface and its support libraries. This brings the total number of applications that use KDE Frameworks 5 to 126.

### A Spectacular New Addition

After 14 years of being a part of KDE, KSnapshot has been retired and replaced with a new screenshot application, Spectacle.

With new features and a completely new UI, Spectacle makes taking screenshots as easy and unobtrusive as it can ever be. In addition to what you could do with KSnapshot, with Spectacle you can now take composite screenshots of pop-up menus along with their parent windows, or take screenshots of the entire screen (or the currently active window) without even starting Spectacle, simply by using the keyboard shortcuts Shift+PrintScreen and Meta+PrintScreen respectively.

We've also aggressively optimised application start-up time, to absolutely minimise the time lag between when you start the application and when the image is captured.

### Polish Everywhere

Many of the applications have undergone significant polishing in this cycle, in addition to stability and bug fixes.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, the non-linear video editor, has seen major fixes to its User Interface. You can now copy and paste items on your timeline, and easily toggle transparency in a given track. The icon colours automatically adjust to the main UI theme, making them easier to see.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark can now show ZIP comments`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, can now display comments embedded in ZIP and RAR archives. We've improved support for Unicode characters in file names in ZIP archives, and you can now detect corrupt archives and recover as much data as possible from them. You can also open archived files in their default application, and we've fixed drag-and-drop to the desktop as well previews for XML files.

### All Play and No Work

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`KSudoku new Welcome Screen (on a Mac OS X)`>}}

The KDE Games developers have been working hard for the past few months to optimise our games for a smoother and richer experience, and we've got a lot of new stuff in this area for our users to enjoy.

In <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, we've added two new sets of levels, one allowing digging while falling and one not. We've added solutions for several existing sets of levels. For an added challenge, we now disallow digging while falling in some older sets of levels.

In <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, you can now print Mathdoku and Killer Sudoku puzzles. The new multi-column layout in KSudoku's Welcome Screen makes it easier to see more of the many puzzle types available, and the columns adjust themselves automatically as the window is resized. We've done the same to Palapeli, making it easier to see more of your jigsaw puzzle collection at once.

We've also included stability fixes for games like KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> and <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, and the overall experience is now greatly improved. KTuberling, Klickety and KNavalBattle have also been updated to use the new KDE Frameworks 5.

### Important Fixes

Don't you simply hate it when your favorite application crashes at the most inconvinient time? We do too, and to remedy that we've worked very hard at fixing lots of bugs for you, but probably we've left some sneak, so don't forget to <a href='https://bugs.kde.org'>report them</a>!

In our file manager <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, we've included various fixes for stability and some fixes to make scrolling smoother. In <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, we've fixed a bothersome issue with white text on white backgrounds. In <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, we've attempted to fix a crash that was occurring on shutdown, in addition to cleaning up the UI and adding in a new cache cleaner.

The <a href='https://userbase.kde.org/Kontact'>Kontact Suite</a> has seen tons of added features, big fixes and performance optimisations. In fact, there's been so much development this cycle that we've bumped the version number to 5.1. The team is hard at work, and is looking forward to all your feedback.

### Moving Forward

As part of the effort to modernise our offerings, we've dropped some applications from KDE Applications and are no longer releasing them as of KDE Applications 15.12

We've dropped 4 applications from the release - Amor, KTux, KSnapshot and SuperKaramba. As noted above, KSnapshot has been replaced by Spectacle, and Plasma essentially replaces SuperKaramba as a widget engine. We've dropped standalone screensavers because screen locking is handled very differently in modern desktops.

We've also dropped 3 artwork packages (kde-base-artwork, kde-wallpapers and kdeartwork); their content had not changed in long time.

### Kompletní seznam změn
