---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE vydává Aplikace KDE 18.04.2
layout: application
title: KDE vydává Aplikace KDE 18.04.2
version: 18.04.2
---
June 7, 2018. Today KDE released the second stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 25 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular, among others.

Improvements include:

- Image operations in Gwenview can now be redone after undoing them
- KGpg no longer fails to decrypt messages without a version header
- Exporting of Cantor worksheets to LaTeX has been fixed for Maxima matrices
