---
aliases:
- ../announce-applications-14.12-beta1
custom_spread_install: true
date: '2014-11-06'
description: KDE Ships Applications 14.12 Beta 1.
layout: application
title: KDE vydává první Beta verzi Aplikací KDE 14.12
---
November 6, 2014. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the various applications being based on KDE Frameworks 5, the KDE Applications 14.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### Instalace binárních balíčků Aplikací KDE 14.12 Beta1

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 14.12 Beta1 (internally 14.11.80) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Kompilace Aplikací KDE 14.12 Beta1

Kompletní zdrojový kód KDE 14.12 Beta 1 lze <a href='http://download.kde.org/unstable/applications/14.11.80/src/'>volně stáhnout</a>. Instrukce pro kompilaci a instalaci jsou dostupné ze <a href='/info/applications/applications-14.11.80'>stránek s informacemi o Aplikacích KDE Beta 1</a>.
