---
aliases:
- ../../kde-frameworks-5.66.0
date: 2020-01-11
layout: framework
libCount: 70
---
### Alla ramverk

- Konvertera från QRegExp till QRegularExpression
- Konvertera qrand till QRandomGenerator
- Rätta kompilering med Qt 5.15 (t.ex. är endl nu Qt::endl, QHash insertMulti kräver nu att QMultiHash används ...)

### Attica

- Använd inte en verifierad nullptr som datakälla
- Stöd flera underliggande element i kommentarelement
- Ange en riktig agentsträng för Attica-begäran

### Baloo

- Rapportera korrekt om baloo_file inte är tillgänglig
- Ändra returvärde för cursor_open
- Initiera QML monitorvärden
- Flytta tolkningsmetoder för webbadresser från I/O-slaven till frågeobjekt

### BluezQt

- Lägg till Battery1 gränssnitt

### Breeze-ikoner

- Ändra XHTML-ikon att vara en violett HTML-ikon
- Sammanfoga hörlurar och sicksack i mitten
- Lägg till application/x-audacity-project ikon (fel 415722)
- Lägg till 32-bildpunkters preferences-system
- Lägg till application/vnd.apple.pkpass ikon (fel 397987)
- ikon för ktimetracker som använder PNG-filen i programarkivet, att ersättas med verklig breeze SVG
- lägg till kipi ikon, behöver göras om som en breeze tema svg [eller bara ta död på kipi]

### Extra CMake-moduler

- [android] Rätta apk installationsmål
- Stöd PyQt5 kompilerad med SIP 5

### Integrering med ramverk

- Ta bort ColorSchemeFilter från KStyle

### KDE Doxygen-verktyg

- Visa fullständigt kvalificerat namn på klass/namnrymd som sidhuvud (fel 406588)

### KCalendarCore

- Förbättra README.md så att den har ett inledande avsnitt
- Gör också incidence geografisk koordinat tillgänglig som en egenskap
- Rätta generering av RRULE för tidszoner

### KCMUtils

- Avråd från KCModuleContainer

### KCodecs

- Rätta ogiltig typkonvertering till uppräkningstyp genom att ändra typen till int istället för uppräkningstyp

### KCompletion

- Avråd från KPixmapProvider
- [KHistoryComboBox] Lägg till metod för att ställa in en ikonleverantör

### KConfig

- uppstädning av kconfig EBN-transportprotokoll
- Exponera hämtningsfunktion för inställning av KConfigWatcher
- Rätta writeFlags med KConfigCompilerSignallingItem
- Lägg till en kommentar som pekar på historiken för Klipp ut och Ta bort som delar en genväg

### KConfigWidgets

- Byt namn på "Configure Shortcuts" till "Configure Keyboard Shortcuts" (fel 39488)

### KContacts

- Justera inställning av ECM och Qt med konventioner i Ramverk
- Ange version av ECM-beroende som i alla andra ramverk

### KCoreAddons

- Lägg till KPluginMetaData::supportsMimeType
- [KAutoSaveFile] Använd QUrl::path() istället för toLocalFile()
- Lagga bygge av PROCSTAT: lägg till saknad implementering av KProcessList::processInfo
- [KProcessList] Optimera KProcessList::processInfo (fel 410945)
- [KAutoSaveFile] Förbättra kommentaren i tempFileName()
- Rätta felaktig KAutoSaveFile för lång sökväg (fel 412519)

### KDeclarative

- [KeySequenceHelper] Greppa verkligt fönster vid inbäddning
- Lägg till valfri underrubrik i rutnätsdelegat
- [QImageItem/QPixmapItem] Förlora inte precision under beräkning

### KFileMetaData

- Partiell rättning av tecken med accenter i filnamn på Windows
- Ta bort privata deklarationer för taglibextractor som inte behövs
- Partiell lösning för att acceptera tecken med accenter på Windows
- xattr: rätta krasch för dinglande symboliska länkar (fel 414227)

### KIconThemes

- Ställ in breeze som standardtema vid läsning från inställningsfil
- Avråd från toppnivåfunktionen IconSize()
- Rätta centrering av skalade ikoner för bildpunktsavbildningar med hög upplösning

### KImageFormats

- pic: Rätta odefinierat beteende för Invalid-enum-value

### KIO

- [KFilePlacesModel] Rätta kontroll av stödda scheman för enheter
- Inbädda också protokolldata för Windows-version av I/O-slaven trash
- Lägg till stöd för att montera KIOFuse webbadresser för program som inte använder KIO (fel 398446)
- Lägg till avkortningsstöd i FileJob
- Avråd från KUrlPixmapProvider
- Avråd från KFileWidget::toolBar
- [KUrlNavigator] Lägg till RPM-stöd i krarc: (fel 408082)
- KFilePlaceEditDialog: rätta krasch när platsen för papperskorgen redigeras (fel 415676)
- Lägg till knapp för att öppna katalogen i filelight för att visa mer information
- Visa mer information i varningsdialogrutan som visas innan en privilegierad åtgärd påbörjas
- KDirOperator: Använd en fast radhöjd för rullningshastighet
- Ytterligare fält såsom borttagningstid och originalsökväg visas nu i filegenskapsdialogrutan
- KFilePlacesModel: använd riktigt överliggande objekt för tagsLister för att undvika minnesläcka, Introducerad med D7700
- HTTP I/O-slave: anropa rätt basklass i virtual_hook(). Basen för HTTP I/O-slav är TCPSlaveBase, inte SlaveBase
- Ftp ioslave: rätta att tid med 4 tecken tolkas som år
- Lägg till KDirOperator::keyPressEvent igen för att bevara bakåtkompatibilitet
- Använd QStyle för att bestämma ikonstorlekar

### Kirigami

- ActionToolBar: Visa bara överflödesknappen om det finns synliga objekt i menyn (fel 415412)
- Bygg och installera inte programmallar på android
- Hårdkoda inte marginalerna för CardsListView
- Lägg till stöd för egna visningskomponenter i Action
- Låt andra komponenter växa om det finns flera saker i huvudet
- Ta bort skapa dynamiskt objekt i DefaultListItemBackground
- återintroducera hopdragningsknappen (fel 415074)
- Visa programfönsterikon på AboutPage

### KItemModels

- Lägg till KColumnHeadersModel

### KJS

- Lägg till tester för Math.exp()
- Tillägg av tester för diverse tilldelningsoperatorer
- Testa specialfall för multiplikativa operatorer (*, / och %)

### KNewStuff

- Säkerställ att dialogtiteln är korrekt med ett oinitierat gränssnitt
- Visa inte informationsikon för den stora förhandsgranskningsdelegaten (fel 413436)
- Stöd installation av arkiv med införandekommandon (fel 407687)
- Skicka med inställningsnamnet med begäran

### KPeople

- Exponera uppräkning för metaobjektkompilatorn

### KQuickCharts

- Korrigera också skuggningarnas deklarationsfiler
- Korrigera licenshuvuden för skuggningar

### KService

- Avråd från KServiceTypeProfile

### KTextEditor

- Lägg till egenskapen "line-count" i ConfigInterface
- Undvik oönskad horisontell rullning (fel 415096)

### Kwayland

- [plasmashell] Uppdatera dokument för panelTakesFocus så att det blir generellt
- [plasmashell] Lägg till signal för ändring av panelTakesFocus

### KXMLGUI

- KActionCollection: tillhandahåll signalen changed() som en ersättning av removed()
- Justera titeln för inställningsfönster för snabbtangent

### NetworkManagerQt

- Manager: lägg till stöd för AddAndActivateConnection2
- cmake: Ta hänsyn till NM-deklarationsfiler som systeminkluderingar
- Synkronisera Utils::securityIsValid med NetworkManager (fel 415670)

### Plasma ramverk

- [ToolTip] Avrunda position
- Aktivera hjulhändelser för Slider {}
- Synkronisera QWindow flaggan WindowDoesNotAcceptFocus till wayland plasmashell gränssnitt (fel 401172)
- [calendar] Kontrollera fältåtkomst utanför gränser vid QLocale uppslagning
- [Plasma-dialogruta] Använd QXcbWindowFunctions för att ställa in fönstertyper som Qt WindowFlags inte känner till
- [PC3] Fullständig animering av plasma förloppsrad
- [PC3] Visa bara indikering av förloppsrad när ändarna inte överlappar
- [RFC] Rätta marginaler för skärminställningsikon (fel 400087)
- [ColorScope] Arbeta med enkla QObjects igen
- [Breeze-skrivbordstema] Lägg till svartvit user-desktop ikon
- Ta bort förvald bredd från PlasmaComponents3.Button
- [PC3 ToolButton] Låt etiketten ta hänsyn till komplementära färgscheman (fel 414929)
- Tillägg av bakgrundsfärger för aktiva och inaktiva ikonvyer (fel 370465)

### Syfte

- Använd standard ECMQMLModules

### QQC2StyleBridge

- [ToolTip] Avrunda position
- Uppdatera storlekstips när teckensnitt ändras

### Solid

- Visa första / i beskrivning av åtkomst till monterad lagring
- Säkerställ montering av nfs filsystem som motsvarar fstab deklarerade motsvarigheten (fel 390691)

### Sonnet

- Signalen done avråds från till förmån för spellCheckDone, som nu skickas korrekt

### Syntaxfärgläggning

- LaTeX: rätta hakparenteser i vissa kommandon (fel 415384)
- TypeScript: lägg till primitiv typ "bigint"
- Python: förbättra tal, lägg till oktala tal, binära tal och nyckelordet "breakpoint" (fel 414996)
- SELinux: lägg till nyckelord "glblub" och uppdatera rättighetslistor
- Flera förbättringar av gitolite syntaxdefinition

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
