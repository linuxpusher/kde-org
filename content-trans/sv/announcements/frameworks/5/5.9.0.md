---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Ny modul: ModemManagerQt (Qt omgivning för ModemManager programmeringsgränssnittet) Uppgradera alternativt till Plasma-NM 5.3 Beta vid uppgradering till ModemManagerQt 5.9.0.

### KActivities

- Implementerade att glömma en resurs
- Byggrättningar
- Tillägg av ett insticksprogram för att registrera händelser för underrättelser med KRecentDocument

### KArchive

- Respektera inställningen KZip::extraField också när centrala huvudposter skrivs
- Ta bort två felaktiga assert, som inträffar när disken är full, fel 343214

### KBookmarks

- Rätta byggning med Qt 5.5

### KCMUtils

- Använd nytt json-baserat instickssystem. Inställningsmoduler söks efter i kcms/. För närvarande måste fortfarande en skrivbordsfil vara installerad i kservices5/ för kompatibilitet.
- Ladda och svep in QML-versionen av inställningsmoduler om möjligt

### KConfig

- Rätta assert när KSharedConfig används i en global objektdestruktor.
- kconfig_compiler: Tillägg av stöd för CategoryLoggingName i *.kcfgc-filer, för att skapa anrop till qCDebug(kategori).

### KI18n

- Förladda den globala Qt-katalogen när i18n() används

### KIconThemes

- KIconDialog kan nu visas genom att använda de vanliga metoderna show() och exec() i QDialog
- Rätta KIconEngine::paint för att hantera olika devicePixelRatios

### KIO

- Aktiverade KPropertiesDialog för att visa information om ledigt utrymme också för fjärrfilsystem (t.ex. smb)
- Rättade KUrlNavigator för bildpunktsavbildningar med många punkter/tum
- Låt KFileItemDelegate hantera annat än standardvärden för devicePixelRatio i animeringar

### KItemModels

- KRecursiveFilterProxyModel: Omarbetad för att avge rätt signaler vid rätt tid
- KDescendantsProxyModel: Hantera förflyttningar rapporterade av källmodellen.
- KDescendantsProxyModel: Rättade beteende när en markering görs vid återställning.
- KDescendantsProxyModel: Tillåt konstruktion och användning av KSelectionProxyModel från QML.

### KJobWidgets

- Vidarebefordra felkod till jobView D-Bus gränssnittet

### KNotifications

- Tillägg av en version av event() som inte har någon ikon, och använder en standardikon
- Tillägg av en version av event() som har StandardEvent eventId och QString iconName

### KPeople

- Tillåt att metadata för åtgärder utökas genom att använda fördefinierade typer
- Rätta att modellen inte uppdateras riktigt efter att ha tagit bort en kontakt från Person

### KPty

- Exponera för världen om KPty har byggts med biblioteket utempter

### KTextEditor

- Tillägg av färgläggningsfil för kdesrc-buildrc
- Syntax: Tillägg av binära heltalslitteraler i färgläggningsfilen för PHP

### KWidgetsAddons

- Låt animering av KMessageWidget jämn med högt bildpunktsförhållande för enhet

### KWindowSystem

- Lägg till tom Wayland-implementering för KWindowSystemPrivate
- KWindowSystem::icon med NETWinInfo är nu inte begränsad till X11-plattformen.

### KXmlGui

- Bevara översättningsdomän när .rc-filer sammanfogas
- Rätta varning vid körning för QWidget::setWindowModified: Fönsterrubriken innehåller inte platsmarkören '[*]'

### KXmlRpcClient

- Installera översättningar

### Plasma ramverk

- Rätta kvarblivna verktygstips när tillfällig ägare av verktygstipset försvinner eller blir tom
- Rätta att utläggning av TabBar inte är riktig från början, vilket exempelvis kunde ses i Kickoff
- Övergångar i PageStack använder nu Animators för jämnare animeringar
- Övergångar i TabGroup använder nu Animators för jämnare animeringar
- Låt Svg,FrameSvg fungera med QT_DEVICE_PIXELRATIO

### Solid

- Uppdatera batteriegenskaper vid återupptagning

### Ändringar av byggsystem

- Extra CMake-moduler (ECM) har nu version som KDE-ramverk, och kallas därför nu 5.9, medan de tidigare kallades 1.8.
- Många ramverk har rättats att vara användbara utan att söka efter deras privata beroenden, dvs. program som slår upp ett ramverk behöver bara dess öppna beroenden, inte de privata.
- Tillåt inställning av SHARE_INSTALL_DIR att hantera layouter för flera arkitekturer bättre

### Frameworkintegration

- Rätta möjlig krasch när en QSystemTrayIcon förstörs (exempelvis utlöst av Trojita), fel 343976
- Rätta inbyggda fildialogrutor med fast läge i QML, fel 334963

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
