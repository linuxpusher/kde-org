---
aliases:
- ../../kde-frameworks-5.67.0
date: 2020-02-02
layout: framework
libCount: 70
---
### Allmänt

- Konvertera från många Qt 5.15 metoder som avråds från, det reducerar antal varningar under bygget.

### Baloo

- Överför inställning från KConfig till KConfigXt för att låta systeminställningsmoduler använda den

### Breeze-ikoner

- skapa Breeze-stil Kate-ikon baserad på ny design av Tyson Tan
- Ändra VLC-ikon att vara mer lik VLC-ikoner
- lägg till ktrip ikon från ktrip-arkiv
- Lägg till ikon för application/sql
- Städa och lägg till 27 bildpunkters media repeat ikoner
- Lägg till ikon för text/vnd.kde.kcrash-report
- Omvandla application/x-ms-shortcut till en verklig genvägsikon

### Extra CMake-moduler

- Lägg till saknad import av miljövariabel
- ECMAddAppIcon: Lägg till sc i reguljärt uttryck för att extrahera filändelse från giltiga namn
- ECMAddQch: stöd och dokumentera användning av makrot K_DOXYGEN

### Integrering med ramverk

- Ta bort oanvänt beroende av QtDBus

### KActivitiesStats

- Rätta felaktig SQL-fråga i allResourcesQuery

### KActivities

- Ta bort filer som Windows inte kan hantera
- Säkerställ att lagra resurswebbadress utan avslutande snedstreck

### KDE Doxygen-verktyg

- Laga modulimport för Python2
- Hårdkoda utf-8 som filsystemkodning med Python2 för att hjälpa api.kde.org

### KCMUtils

- föredra insticksprogrammen för nya systeminställningsmoduler framför de gamla
- KCModuleQml: Säkerställ att defaulted skickas med aktuell configModule-&gt;representsDefaults vid laddning
- Visa knapp som respekterar vad som är deklarerat av KCModule
- Uppdatera KPluginSelector för att tillåta systeminställningsmodul att visa gott tillstånd för knapparna återställ, verkställ och förval

### KConfig

- Strukturera om KConfigXT
- Rätta bygge av Python-bindningar efter ebd14f29f8052ff5119bf97b42e61f404f223615
- KCONFIG_ADD_KCFG_FILES: generera också om för ny version av kconfig_compiler
- Tillåt att också skicka ett mål istället för en lista över källor till KCONFIG_ADD_KCFG_FILES
- Lägg till KSharedConfig::openStateConfig för att lagra tillståndsinformation
- Rätta kompilering av Python-bindningar efter 7ab8275bdb56882692846d046a5bbeca5795b009

### KConfigWidgets

- KStandardAction: lägg till metod för att skapa SwitchApplicationLanguage
- [KColorSchemeManager] Lista inte dubbletter
- [KColorschemeManager] Lägg till alternativ för att återaktivera följande globala tema

### KCoreAddons

- nedgradera laddningsfel för insticksprogram från varning till debugnivå + omformulera
- Dokumentera hur man filtrerar enligt tjänsttyp på rätt sätt
- Lägg till överlagrad perlSplit() som tar en QRegularExpression och avråd från den med QRegExp
- Lägg till Mime-typ för bakåtspårningar sparade från DrKonqi
- Lägg till hjälptextfunktionen KShell::tildeCollapse
- KPluginMetaData: lägg till initialPreference() hämtningsmetod
- desktoptojson: konvertera också nyckeln InitialPreference

### KDeclarative

- Beräkna undre marginal riktigt för rutnätsdelegater med undertitlar
- [ConfigModule] Tala om vilket paket som är ogiltigt

### KHolidays

- Uppdatera helger och lägg till flaggdagar och namnsdagar för Sverige

### KI18n

- ki18n_wrap_ui: fel när filen inte finns
- [Kuit] Återställ ändringar i parseUiMarker()

### KIO

- Lägg till saknad renamed händelse när en målfil redan fanns
- KFilePlacesModel: Visa normalt bara poster baserade på recentlyused:/ i ny profil
- Lägg till konstruktorn KFileCustomDialog med parametern startDir
- Rätta användning av QRegularExpression::wildcardToRegularExpression()
- Tillåt att hantera program med Terminal=True i sina skrivbordsfiler, hantera deras tillhörande Mime-typer riktigt (fel 410506)
- KOpenWithDialog: Tillåt att en nyskapad KService skapad med tillhörande Mime-typ returneras
- Lägg till KIO::DropJobFlag för att tillåta att menyn visas manuellt (fel 415917)
- [KOpenWithDialog] Dölj hopfällbar gruppruta när alla alternativ inne i den är dolda (fel 415510)
- Återställ effektiv borttagning av KUrlPixmapProvider från programmeringsgränssnitt
- SlaveBase::dispatchLoop: Rätta beräkning av tidsgräns (fel 392768)
- [KDirOperator] Tillåt att byta namn på filer från den sammanhangsberoende menyn (fel 189482)
- Skicka Dolphins dialogruta för filnamnsbyte uppströms (fel 189482)
- KFilePlaceEditDialog: flytta in logik i isIconEditable()

### Kirigami

- Beskär överliggande objekt till bläddringsbar (fel 416877)
- Ta bort sidhuvudets övre marginal från privat ScrollView
- riktigt storlekstips för gridlayout (fel 416860)
- använd bifogad egenskap för isCurrentPage
- Bli av med några varningar
- försök att hålla markören i fönstret vid inskrivning i ett OverlaySheet
- expandera fillWidth objekt riktigt i mobilläge
- Lägg till aktiv, länk, besökt, negativ, neutral och positiv bakgrundsfärg
- Exponera ikonnamn på överflödesknappen i ActionToolBar
- Använd QQC2 Page som bas för Kirigami Page
- Ange var koden kommer från som webbadressen
- Förankra inte AbstractApplicationHeader blint
- skicka sammanslagna efter egenskaperna har tilldelats om
- lägg till återanvända och sammanslagna signaler som TableView

### KJS

- Avbryt maskinkörning när väl en tidsgränssignal har setts
- Stöd upphöjt till operator ** från ECMAScript 2016
- Tillägg av funktionen shouldExcept() som fungerar baserat på en funktion

### KNewStuff

- Laga KNSQuick::Engine::changedEntries funktionalitet

### KNotification

- Lägg till ny signal för aktivering av förvald åtgärd
- Ta bort beroende av KF5Codecs genom att använda den nya funktionen stripRichText
- Ta bort richtext på Windows
- Anpassa till Qt 5.14 Android-ändringar
- Avråd från raiseWidget
- Konvertera KNotification från KWindowSystem

### KPeople

- Justera metainfo.yaml till nytt lager
- Ta bort föråldrad laddningskod för insticksprogram

### KQuickCharts

- Rätta Qt versionscheck
- Registrera QAbstractItemModel som anonym typ för egenskapstilldelningar
- Dölj linjen i ett linjediagram om dess bredd är inställd till 0

### Kross

- addHelpOption lägger redan till enligt kaboutdata

### KService

- Stöd flera värden i XDG_CURRENT_DESKTOP
- Avråd från allowAsDefault
- Gör "Förvalda program" i mimeapps.list de föredragna programmen (fel 403499)

### KTextEditor

- Återställ "förbättra ordkomplettering att använda färgläggning för att detektera ordgränser" (fel 412502)
- Importera slutlig breeze-ikon
- Meddelanderelaterade metoder: Använd fler medlem-funktion-pekarbaserade anslutningar
- DocumentPrivate::postMessage: undvik multipla uppslagningar av hash
- rätta drag-och-släpp funktion (genom att använda Ctrl-tangent) (fel 413848)
- säkerställ att vi har en kvadratisk ikon
- ställ in riktig Kate ikon i om-dialogruta för KatePart
- inline notes: ställ in underMouse() korrekt för anteckning på plats
- undvik användning av gammal maskot ATM
- Variabelexpansion: Lägg till variabeln PercentEncoded (fel 416509)
- Rätta krasch i variabelexpansion (använd av externa verktyg)
- KateMessageWidget: ta bort installation av oanvänt händelsefilter

### KTextWidgets

- Ta bort beroende av KWindowSystem

### Ramverket KWallet

- Återställ readEntryList() att använda QRegExp::Wildcard
- Rätta användning av QRegularExpression::wildcardToRegularExpression()

### KWidgetsAddons

- [KMessageWidget] Subtrahera korrekt marginal
- [KMessageBox] Tillåt bara att markera text i dialogrutan med användning av musen (fel 416204)
- [KMessageWidget] Använd devicePixelRatioF för animeringars bildpunktsavbildning (fel 415528)

### KWindowSystem

- [KWindowShadows] Kontrollera X-anslutning
- Introducera programmeringsgränssnitt för skuggor
- Avråd från KWindowEffects::markAsDashboard()

### KXMLGUI

- Använd KStandardAction bekvämlighetsmetod för switchApplicationLanguage
- Tillåt också att egenskapen programLogo är en QIcon
- Ta bort möjlighet att rapportera fel för godtyckliga saker från en statisk lista
- Ta bort kompileringsinformation från dialogrutan för felrapport
- KMainWindow: rätta autoSaveSettings för att fånga när QDockWidgets visas igen
- i18n: Lägg till fler semantiska sammanhangsberoende strängar
- i18n: Delat översättningar för strängar "Translation"

### Plasma ramverk

- Rättade hörn på verktygstips och tog bort oanvändbara färgegenskaper
- Ta bort hårdkodade färger för SVG:er i bakgrunden
- Rätta storlekar och bildpunktsjustering av kryssrutor och alternativknappar
- Uppdatera skuggor för breeze-tema
- [Plasma Quick] Lägg till klassen WaylandIntegration
- Samma beteende för rullningslister som skrivbordsstil
- Utnyttja KPluginMetaData när vi kan
- Lägg till menyalternativ för redigeringsläge i sammanhangsberoende meny för skrivbordskomponent
- Konsekvens: färglagda markerade knappar
- Konvertera endl till \n Inte nödvändigt att spola eftersom QTextStream använder QFile som spolar när den tas bort

### Syfte

- Rätta användning av QRegularExpression::wildcardToRegularExpression()

### QQC2StyleBridge

- Ta bort rullningslistrelaterade provisoriska lösningar från listdelegater
- [TabBar] Ta bort ram
- Lägg till aktiv, länk, besökt, negativ, neutral och positiv bakgrundsfärg
- använd hasTransientTouchInput
- avrunda alltid x och y
- stöd rullningslist av mobiltyp
- ScrollView: Överlagra inte rullningslister över innehåll

### Solid

- Lägg till signaler för udev händelser med åtgärderna bind och unbind
- Klargör referenser till DeviceInterface (fel 414200)

### Syntaxfärgläggning

- Uppdaterar nasm.xml med senaste instruktioner
- Perl: Lägg till 'say' i nyckelordslista
- cmake: Rätta reguljärt uttryck <code>CMAKE*POLICY**_CMP&amp;lt;N&amp;gt;</code> och lägg till specialargument i <code>get_cmake_property</code>
- Lägg till färgläggningsdefinition för GraphQL

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
