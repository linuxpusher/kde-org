---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE levererar KDE-program 17.08.1
layout: application
title: KDE levererar KDE-program 17.08.1
version: 17.08.1
---
7:e september, 2017. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../17.08.0'>KDE-program 17.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 20 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Kdenlive, Konsole, Plånbokshantering, Okular, Umbrello och KDE spel.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.36.
