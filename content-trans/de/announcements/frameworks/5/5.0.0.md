---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE veröffentlicht die erste Version von Frameworks 5.
layout: framework
qtversion: 5.2
title: Erste Veröffentlichung der KDE Frameworks 5
---
7. Juli 2014. Die KDE-Gemeinschaft ist freut sich, KDE Frameworks 5.0 anzukündigen. Frameworks 5 ist die nächste Generation von KDE-Bibliotheken, modularisiert und optimiert für die einfache Integration in Qt-Anwendungen. Die Frameworks bieten eine eine Vielzahl von häufig benötigten Funktionen in ausgereiften, von anderen Entwicklern geprüften  und gut getesteten Bibliotheken mit liberalen Lizenzbedingungen. Es gibt über 50 verschiedene Frameworks als Teil dieser Version, die Lösungen wie Hardware-Integration, Dateiformat-Unterstützung, zusätzliche Bedienelemente, Plot-Funktionen, Rechtschreibprüfung und mehr enthalten. Viele der Frameworks sind plattformübergreifend und haben minimale oder gar keine zusätzlichen Abhängigkeiten, so dass sie einfach zu erstellen und zu jeder Qt-Anwendung hinzuzufügen sind.

Die KDE-Frameworks sind das Ergebnis der Aufteilung der leistungsstarken Bibliotheken der KDE Platform 4  in eine Reihe unabhängiger, plattformübergreifender Module, die allen Qt-Entwicklern zur Verfügung stehen, um die Qt-Entwicklung zu vereinfachen, zu beschleunigen und die Kosten der Qt-Entwicklung zu reduzieren. Die einzelnen Frameworks sind plattformübergreifend und gut dokumentiert und getestet, und ihre Verwendung wird den Qt Entwicklern vertraut sein, da sie dem Stil und den Standards folgen, die vom Qt-Projekt festgelegt wurden. Die Frameworks werden unter dem bewährten KDE-Governance-Modell entwickelt mit einem vorhersehbaren Veröffentlichungszeitplan, einem klaren und herstellerneutralen Prozess, offener Verwaltung und flexibler Lizenzierung (LGPL).

Die Frameworks haben eine klare Abhängigkeitsstruktur, unterteilt in Kategorien und Tiers. Die Kategorien beziehen sich auf Laufzeit-Abhängigkeiten:

- <strong>Funktionale</strong> Elemente haben keine Laufzeit-Abhängigkeiten.
- <strong>Integration</strong> bezeichnet Quelltext, der möglicherweise Laufzeit-Abhängigkeiten für die Integration hat, je nachdem, was das Betriebssystem oder die Plattform bietet
- <strong>Lösungen(Solutions)</strong> haben obligatorische Laufzeit-Abhängigkeiten.

Die <strong>Tiers</strong> verweisen auf Abhängigkeiten von anderen Frameworks beim kompilieren. Tier 1 Frameworks haben keine Abhängigkeiten innerhalb von Frameworks und benötigen nur Qt und andere relevante Bibliotheken. Tier 2 Frameworks können nur von von Tier 1 abhängen. Tier 3 Frameworks können sowohl von anderen Tier 3 Frameworks als auch von Tier 2 und Tier 1 abhängen.

Der Übergang von der Plattform zu Frameworks ist seit über 3 Jahren im Gange, durchgeführt von den wichtigsten KDE-Entwicklern. Erfahren Sie mehr über Frameworks 5 <a href='http://dot.kde.org/2013/09/25/frameworks-5'>in diesem Artikel vom letzten Jahr</a>.

## Höhepunkte

Es gibt zurzeit über 50 verschiedene Frameworks. Die vollständige Liste finden Sie in der <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>API-Dokumentation online</a>. Im folgenden einige Beispiele mit Funktionen, die Frameworks für Entwickler von Qt-Anwendungen bereit stellen.

<strong>KArchive</strong> unterstützt viele weit verbreitete Kompressionsalgorithmen in einer in sich geschlossenen, umfangreichen und einfach zu benutzenden Dateiarchivierungs- und -extraktionsbibliothek. Übergeben Sie ihr einfach die Dateien; es ist nicht notwendig, eine eigene Kompressionsfunktion in Ihrer Qt-basierten Anwendungen zu entwickeln.

Mit <strong>ThreadWeaver</strong> bietet eine Programmierschnittstelle auf hoher Ebene an, um Threads über Job- und Queue-basierten Schnittstellen zu verwalten. Es ermöglicht das einfache Scheduling von Thread-Ausführungen, indem Abhängigkeiten zwischen den Threads angegeben werden und diese Abhängigkeiten bei der Ausführung berücksichtigt werden. Dadurch wird die Verwendung mehrerer Threads deutlich vereinfacht.

<strong>KConfig</strong> ist ein Framework für das Speichern und Lesen von Einstellungen. Es verfügt über eine gruppenorientierte API. Es arbeitet mit INI-Dateien und XDG-kompatiblen kaskadierenden Ordnern. Es generiert Quelltext auf Basis von XML-Dateien.

<strong>Solid</strong> bietet Hardware-Erkennung und kann eine Anwendung über Speichergeräte und Datenträger, CPU, Batteriestatus, Energieverwaltung, Netzwerkstatus und -schnittstellen sowie Bluetooth informieren. Für verschlüsselte Partitionen, Stromversorgung und Netzwerke sind laufende Dienste erforderlich.

<strong>KI18n</strong> enthält Gettext-Unterstützung für Anwendungen und macht es einfach, die Übersetzung von Qt-Anwendungen in die allgemeine Übersetzungsinfrastruktur vieler Projekte zu integrieren.
