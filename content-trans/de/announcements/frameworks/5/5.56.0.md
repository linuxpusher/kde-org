---
aliases:
- ../../kde-frameworks-5.56.0
date: 2019-03-09
layout: framework
libCount: 70
---
### Baloo

- Replace several Q_ASSERTs with proper checks
- Check string length to avoid crash for "tags:/" URL
- [tags_kio] Fix local file tagging by checking only tag: urls for double slashes
- Hardcoding the Remaining Time Update Interval
- Fix regression for matching explicitly included folders
- Cleanup idempotent entries from mimetype mapper table
- [baloo/KInotify] Notify if folder was moved from unwatched place (bug 342224)
- Handle folders matching substrings of included/excluded folders correctly
- [balooctl] Normalize include/exclude paths before using it for the config
- Optimize Baloo::File copy assign operator, fix Baloo::File::load(url)
- Use content to determine mime type (bug 403902)
- [Extractor] Exclude GPG encrypted data from being indexed (bug 386791)
- [balooctl] Actually abort a malformed command instead of just saying so
- [balooctl] Add missing help for "config set", normalize string
- Replace recursive isDirHidden with iterative one, allow const argument
- Make sure only directories are added to the inotify watcher

### Breeze Icons

- Add code-oss icon
- [breeze-icons] Add video camera icons
- [breeze-icons] Use new suspend, hibernate and switch user icons in Breeze icon theme
- Add 16 px and 22 px versions of the gamepad icon to devices/
- Make Breeze theme tooltip texts consistent
- Add battery icons
- Rename "visibility" and "hint" icons to "view-visible" and "view-hidden"
- [breeze-icons] Add monochrome/smaller SD card and memory stick icons (bug 404231)
- Add device icons for drones
- Change C/C++ header/source mimetype icons to circle/line style
- Fix missing shadows on C/C++ header mimetype icons (bug 401793)
- Remove monochrome font preferences icon
- Improve font selection icon
- Use new bell-style icon for all users of preferences-desktop-notification (bug 404094)
- [breeze-icons] Add 16px versions of gnumeric-font.svg and link gnumeric-font.svg to font.svg
- Add preferences-system-users symlinks that point to yast-users icon
- Add edit-none icon

### Extra CMake-Module

- Fix releaseme checkout when this is included in a sub-directory
- New find module for Canberra
- Update Android toolchain files to reality
- Add compile check to FindEGL

### KActivities

- Use natural sorting in ActivityModel (bug 404149)

### KArchive

- Guard KCompressionDevice::open being called with no backend available (bug 404240)

### KAuth

- Tell people they should mostly be using KF5::AuthCore
- Compile our own helper against AuthCore and not Auth
- Introduce KF5AuthCore

### KBookmarks

- Replace KIconThemes dependency with equivalent QIcon usage

### KCMUtils

- Use KCM name in KCM header
- Add missing ifndef KCONFIGWIDGETS_NO_KAUTH
- Adapt to changes in kconfigwidgets
- Sync QML module padding to reflect system setting pages

### KCodecs

- Fix for CVE-2013-0779
- QuotedPrintableDecoder::decode: return false on error instead of asserting
- Mark KCodecs::uuencode does nothing
- nsEUCKRProber/nsGB18030Prober::HandleData don't crash if aLen is 0
- nsBig5Prober::HandleData: Don't crash if aLen is 0
- KCodecs::Codec::encode: Don't assert/crash if makeEncoder returns null
- nsEUCJPProbe::HandleData: Don't crash if aLen is 0

### KConfig

- Write valid UTF8 characters without escaping (bug 403557)
- KConfig: handle directory symlinks correctly

### KConfigWidgets

- Skip benchmark if no scheme files can be found
- Add a note for KF6 to use the core version of KF5::Auth
- Cache the default KColorScheme configuration

### KCoreAddons

- Create tel: links for phone numbers

### KDeclarative

- use KPackage::fileUrl to support rcc KCMs packages
- [GridDelegate] Fix long labels blending into each other (bug 404389)
- [GridViewKCM] improve contrast and legibility for delegates' inline hover buttons (bug 395510)
- Correct the accept flag of the event object on DragMove (bug 396011)
- Use different "None" item icon in grid view KCMs

### KDESU

- kdesud: KAboutData::setupCommandLine() already sets help &amp; version

### KDocTools

- Port cross-compilation support to KF5_HOST_TOOLING
- Only report DocBookXML as found if it was actually found
- Update Spanish entities

### KFileMetaData

- [Extractor] Add metadata to extractors (bug 404171)
- Add extractor for AppImage files
- Cleanup ffmpeg extractor
- [ExternalExtractor] Provide more helpful output when extractor fails
- Format EXIF photo flash data (bug 343273)
- Avoid side effects due to stale errno value
- Use Kformat for bit and sample rate
- Add units to framerate and gps data
- Add string formatting function to property info
- Avoid leaking a QObject in ExternalExtractor
- Handle &lt;a&gt; as container element in SVG
- Check Exiv2::ValueType::typeId before converting it to rational

### KImageFormats

- ras: fix crash on broken files
- ras: protect the palette QVector too
- ras: tweak max file check
- xcf: Fix uninitialized memory use on broken documents
- add const, helps understand the function better
- ras: tweak max size that "fits" in a QVector
- ras: don't assert because we try to allocate a huge vector
- ras: Protect against divide by zero
- xcf: Don't divide by 0
- tga: fail gracefully if readRawData errors
- ras: fail gracefully on height*width*bpp &gt; length

### KIO

- kioexec: KAboutData::setupCommandLine() already sets help &amp; version
- Fix crash in Dolphin when dropping trashed file in trash (bug 378051)
- Middle-elide very long filenames in error strings (bug 404232)
- Add support for portals in KRun
- [KPropertiesDialog] Fix group combobox (bug 403074)
- Properly attempt to locate the kioslave bin in $libexec AND $libexec/kf5
- Use AuthCore instead of Auth
- Add icon name to service providers in .desktop file
- Read IKWS search provider icon from desktop file
- [PreviewJob] Also pass along that we're the thumbnailer when stat'ing file (bug 234754)

### Kirigami

- remove the broken messing with contentY in refreshabeScrollView
- add OverlayDrawer to the stuff documentable by doxygen
- map currentItem to the view
- proper color to the arrow down icon
- SwipeListItem: make space for the actions when !supportsMouseEvents (bug 404755)
- ColumnView and partial C++ refactor of PageRow
- we can use at most controls 2.3 as Qt 5.10
- fix height of horizontal drawers
- Improve ToolTip in the ActionTextField component
- Add an ActionTextField component
- fix spacing of buttons (bug 404716)
- fix buttons size (bug 404715)
- GlobalDrawerActionItem: properly reference icon by using group property
- show separator if header toolbar is invisible
- add a default page background
- DelegateRecycler: Fix translation using the wrong domain
- Fix warning when using QQuickAction
- Remove some unnecessary QString constructions
- Don't show the tooltip when the drop-down menu is shown (bug 404371)
- hide shadows when closed
- add the needed properties for the alternate color
- revert most of the icon coloring heuristics change
- properly manage grouped properties
- [PassiveNotification] Don't start timer until window has focus (bug 403809)
- [SwipeListItem] Use a real toolbutton to improve usability (bug 403641)
- support for optional alternating backgrounds (bug 395607)
- only show handles when there are visible actions
- support colored icons in action buttons
- always show the back button on layers
- Update SwipeListItem doc to QQC2
- fix logic of updateVisiblePAges
- expose visible pages in pagerow
- hide breadcrumb when the current page has a toolbar
- support the toolbarstyle page override
- new property in page: titleDelegate to override the title in toolbars

### KItemModels

- KRearrangeColumnsProxyModel: make the two column-mapping methods public

### KNewStuff

- Filter out invalid content in lists
- Fix mem leak found by asan

### KNotification

- port to findcanberra from ECM
- List Android as officially supported

### KPackage-Framework

- remove kpackage_install_package deprecation warning

### KParts

- templates: KAboutData::setupCommandLine() already sets help &amp; version

### Kross

- Install Kross modules to ${KDE_INSTALL_QTPLUGINDIR}

### KService

- kbuildsycoca5: no need to repeat work of KAboutData::setupCommandLine()

### KTextEditor

- try to improve painting height for text lines - bug 403868 avoid to cut _ and other parts still broken: double height things like mixed english/arab, see bug 404713
- Use QTextFormat::TextUnderlineStyle instead of QTextFormat::FontUnderline (bug 399278)
- Make it possible to show all spaces in the document (bug 342811)
- Do not print indent lines
- KateSearchBar: Show also search has wrapped hint in nextMatchForSelection() aka Ctrl-H
- katetextbuffer: refactor TextBuffer::save() to better separate code paths
- Use AuthCore instead of Auth
- Refactor KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e)
- Improvements to completion
- Set the color scheme to Printing for Print Preview (bug 391678)

### KWayland

- Only commit XdgOutput::done if changed (bug 400987)
- FakeInput: add support for pointer move with absolute coordinates
- Add missing XdgShellPopup::ackConfigure
- [server] Add surface data proxy mechanism
- [server] Add selectionChanged signal

### KWidgetsAddons

- Use correct KStandardGuiItem "no" icon

### Plasma Framework

- [Icon Item] Block next animation also based on window visibility
- Show a warning if a plugin requires a newer version
- Bump the theme versions because icons changed, to invalidate old caches
- [breeze-icons] Revamp system.svgz
- Make Breeze theme tooltip texts consistent
- Change glowbar.svgz to smoother style (bug 391343)
- Do background contrast fallback at runtime (bug 401142)
- [breeze desktop theme/dialogs] Add rounded corners to dialogs

### Purpose

- pastebin: don't show progress notifications (bug 404253)
- sharetool: Show shared url on top
- Fix sharing files with spaces or quotes in names via Telegram
- Have ShareFileItemAction provide an output or an error if they are provided (bug 397567)
- Enable sharing URLs via email

### QQC2StyleBridge

- Use PointingHand when hovering links in Label
- Respect the display property of buttons
- clicking on empty areas behaves like pgup/pgdown (bug 402578)
- Support icon on ComboBox
- support text positioning api
- Support icons from local files in buttons
- Use the correct cursor when hovering over the editable part of a spinbox

### Solid

- Bring FindUDev.cmake up to ECM standards

### Sonnet

- Handle the case if createSpeller is passed an unavailable language

### Syntax Highlighting

- Fix repository deletion warning
- MustacheJS: also highlight template files, fix syntax and improve support for Handlebars
- make unused contexts fatal for indexer
- Update example.rmd.fold and test.markdown.fold with new numbers
- Install DefinitionDownloader header
- Update octave.xml to Octave 4.2.0
- Improve highlighting of TypeScript (and React) and add more tests for PHP
- Add more highlighting for nested languages in markdown
- Return sorted definitions for file names and mime types
- add missing ref update
- BrightScript: Unary and hex numbers, @attribute
- Avoid duplicate *-php.xml files in "data/CMakeLists.txt"
- Add functions returning all definitions for a mimetype or file name
- update literate haskell mimetype
- prevent assertion in regex load
- cmake.xml: Updates for version 3.14
- CubeScript: fixes line continuation escape in strings
- add some minimal howto for test adding
- R Markdown: improve folding of blocks
- HTML: highlight JSX, TypeScript &amp; MustacheJS code in the &lt;script&gt; tag (bug 369562)
- AsciiDoc: Add folding for sections
- FlatBuffers schema syntax highlighting
- Add some Maxima constants and function

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
