---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### New frameworks

- KFileMetadata: file metadata and extraction library
- Baloo: file indexing and searching framework

### Changes affecting all frameworks

- The Qt version requirement has been bumped from 5.2 to 5.3
- Debug output has been ported to categorized output, for less noise by default
- Docbook documentation has been reviewed and updated

### Framework-Integration

- Fix crash in directories-only file dialog
- Don't rely on options()-&gt;initialDirectory() for Qt &lt; 5.4

### KDE Doxygen Tools

- Add man pages for kapidox scripts and update maintainer info in setup.py

### KBookmarks

- KBookmarkManager: use KDirWatch instead of QFileSystemWatcher to detect user-places.xbel being created.

### KCompletion

- HiDPI fixes for KLineEdit/KComboBox
- KLineEdit: Don't let the user delete text when the lineedit is readonly

### KConfig

- Don't recommend to use deprecated API
- Don't generate deprecated code

### KCoreAddons

- Add Kdelibs4Migration::kdeHome() for cases not covered by resources
- Fix tr() warning
- Fix KCoreAddons build on Clang+ARM

### KDBusAddons

- KDBusService: document how to raise the active window, in Activate()

### KDeclarative

- Fix deprecated KRun::run call
- Same behavior of MouseArea to map coords of filtered child events
- Detect initial face icon being created
- Don't refresh the entire window when we render the plotter (bug 348385)
- add the userPaths context property
- Don't choke on empty QIconItem

### KDELibs 4 Support

- kconfig_compiler_kf5 moved to libexec, use kreadconfig5 instead for the findExe test
- Document the (suboptimal) replacements for KApplication::disableSessionManagement

### KDocTools

- change sentence about reporting bugs, ack'ed by dfaure
- adapt german user.entities to en/user.entities
- Update general.entities: change markup for frameworks + plasma from application to productname
- Update en/user.entities
- Update book and man page templates
- Use CMAKE_MODULE_PATH in cmake_install.cmake
- BUG: 350799 (bug 350799)
- Update general.entities
- Search for required perl modules.
- Namespace a helper macro in the installed macros file.
- Adapted key name tranlations to standard tranlations provided by Termcat

### KEmoticons

- Install Breeze theme
- Kemoticons: make Breeze emotions standard instead of Glass
- Breeze emoticon pack made by Uri Herrera

### KHTML

- Let KHtml be useable w/o searching for private deps

### KIconThemes

- Remove temporary string allocations.
- Remove Theme tree debug entry

### KIdleTime

- Private headers for platform plugins get installed.

### KIO

- Kill unneeded QUrl wrappers

### KItemModels

- New proxy: KExtraColumnsProxyModel, allows to add columns to an existing model.

### KNotification

- Fix the starting Y position for fallback popups
- Reduce dependencies and move to Tier 2
- catch unknown notification entries (nullptr deref) (bug 348414)
- Remove pretty much useless warning message

### Package Framework

- make the subtitles, subtitles ;)
- kpackagetool: Fix output of non-latin text to stdout

### KPeople

- Add AllPhoneNumbersProperty
- PersonsSortFilterProxyModel now available for usage in QML

### Kross

- krosscore: Install CamelCase header "KrossConfig"
- Fix Python2 tests to run with PyQt5

### KService

- Fix kbuildsycoca --global
- KToolInvocation::invokeMailer: fix attachment when we have multi attachement

### KTextEditor

- guard default log level for Qt &lt; 5.4.0, fix log cat name
- add hl for Xonotic (bug 342265)
- add Groovy HL (bug 329320)
- update J highlighting (bug 346386)
- Make compile with MSVC2015
- less iconloader use, fix more pixelated icons
- enable/disable find all button on pattern changes
- Improved search &amp; replace bar
- remove useless ruler from powermode
- more slim search bar
- vi: Fix misreading of markType01 flag
- Use correct qualification to call base method.
- Remove checks, QMetaObject::invokeMethod guards itself against that already.
- fix HiDPI issues with color pickers
- Cleanup coe: QMetaObject::invokeMethod is nullptr safe.
- more comments
- change the way the interfaces are null safe
- only output warnings and above per default
- remove todos from the past
- Use QVarLengthArray to save the temporary QVector iteration.
- Move the hack to indent group labels to construction time.
- Fixup some serious issues with the KateCompletionModel in tree mode.
- Fix broken model design, which relied on Qt 4 behavior.
- obey umask rules when saving new file (bug 343158)
- add meson HL
- As Varnish 4.x introduces various syntax changes compared to Varnish 3.x, I wrote additional, separate syntax highlighting files for Varnish 4 (varnish4.xml, varnishtest4.xml).
- fix HiDPI issues
- vimode: don't crash if the &lt;c-e&gt; command gets executed in the end of a document. (bug 350299)
- Support QML multi-line strings.
- fix syntax of oors.xml
- add CartoCSS hl by Lukas Sommer (bug 340756)
- fix floating point HL, use the inbuilt Float like in C (bug 348843)
- split directions did got reversed (bug 348845)
- Bug 348317 - [PATCH] Katepart syntax highlighting should recognize u0123 style escapes for JavaScript (bug 348317)
- add *.cljs (bug 349844)
- Update the GLSL highlighting file.
- fixed default colors to be more distinguishable

### KTextWidgets

- Delete old highlighter

### KWallet Framework

- Fix Windows build
- Print a warning with error code when opening the wallet by PAM fails
- Return the backend error code rather than -1 when opening a wallet failed
- Make the backend's "unknown cipher" a negative return code
- Watch for PAM_KWALLET5_LOGIN for KWallet5
- Fix crash when MigrationAgent::isEmptyOldWallet() check fails
- KWallet can now be unlocked by PAM using kwallet-pam module

### KWidgetsAddons

- New API taking QIcon parameters to set the icons in the tab bar
- KCharSelect: Fix unicode category and use boundingRect for width calculation
- KCharSelect: fix cell width to fit contents
- KMultiTabBar margins now are ok on HiDPI screens
- KRuler: deprecate unimplemented KRuler::setFrameStyle(), clean up comments
- KEditListWidget: remove margin, so it aligns better with other widgets

### KWindowSystem

- Harden NETWM data reading (bug 350173)
- guard for older Qt versions like in kio-http
- Private headers for platform plugins are installed.
- Platform specific parts loaded as plugins.

### KXMLGUI

- Fix method behavior KShortcutsEditorPrivate::importConfiguration

### Plasma Framework

- Using a pinch gesture one can now switch between the different zoom levels of the calenda
- comment about code duplication in icondialog
- Slider groove color was hardcoded, modified to use color scheme
- Use QBENCHMARK instead of a hard requirement on the machine's performance
- Calendar navigation has been significantly improved, providing a year and decade overview
- PlasmaCore.Dialog now has an 'opacity' property
- Make some space for the radio button
- Don't show the circular background if there's a menu
- Add X-Plasma-NotificationAreaCategory definition
- Set notifications and osd to show on all desktops
- Print useful warning when we can not get valid KPluginInfo
- Fix potential endless recursion in PlatformStatus::findLookAndFeelPackage()
- Rename software-updates.svgz to software.svgz

### Sonnet

- Add in CMake bits to enable building of Voikko plugin.
- Implement Sonnet::Client factory for Voikko spell chekers.
- Implement Voikko based spell checker (Sonnet::SpellerPlugin)

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
