---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE veröffentlicht die KDE-Anwendungen 18.08.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 18.08.2
version: 18.08.2
---
11. Oktober 2018. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../18.08.0'>KDE-Anwendungen 18.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als ein Dutzend aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Dolphin, KCalc und Umbrello.

Verbesserungen umfassen:

- Dragging a file in Dolphin can no longer accidentally trigger inline renaming
- KCalc again allows both 'dot' and 'comma' keys when entering decimals
- A visual glitch in the Paris card deck for KDE's card games has been fixed
