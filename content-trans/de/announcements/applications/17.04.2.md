---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE veröffentlicht die KDE-Anwendungen 17.08.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.08.2
version: 17.04.2
---
08. Juni 2017. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../17.04.0'>KDE-Anwendungen 17.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für kdepim, Ark, Dolphin, Gwenview und Kdenlive.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
