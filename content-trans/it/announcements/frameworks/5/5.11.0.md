---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Moduli CMake aggiuntivi

- New arguments for ecm_add_tests(). (bug 345797)

### Integrazione della struttura

- Use the correct initialDirectory for the KDirSelectDialog
- Make sure the scheme is specified when overriding the start url value
- Only accept existing directories in FileMode::Directory mode

### KActivities

(nessun elenco dei cambiamenti fornito)

### KAuth

- Make KAUTH_HELPER_INSTALL_ABSOLUTE_DIR available to all KAuth users

### KCodecs

- KEmailAddress: Add overload for extractEmailAddress and firstEmailAddress which returns an error message.

### KCompletion

- Fix unwanted selection when editing the filename in the file dialog (bug 344525)

### KConfig

- Prevent crash if QWindow::screen() is null
- Add KConfigGui::setSessionConfig() (bug 346768)

### KCoreAddons

- New KPluginLoader::findPluginById() convenience API

### KDeclarative

- support creation of ConfigModule from KPluginMetdata
- fix pressAndhold events

### Supporto KDELibs 4

- Use QTemporaryFile instead of hardcoding a temporary file.

### KDocTools

- Update translations
- Update customization/ru
- Fix entities with wrong links

### KEmoticons

- Cache the theme in the integration plugin

### KGlobalAccel

- [runtime] Move platform specific code into plugins

### KIconThemes

- Optimize KIconEngine::availableSizes()

### KIO

- Do not try to complete users and assert when prepend is non-empty. (bug 346920)
- Use KPluginLoader::factory() when loading KIO::DndPopupMenuPlugin
- Fix deadlock when using network proxies (bug 346214)
- Fixed KIO::suggestName to preserve file extensions
- Kick off kbuildsycoca4 when updating sycoca5.
- KFileWidget: Don't accept files in directory only mode
- KIO::AccessManager: Make it possible to treat sequential QIODevice asynchronously

### KNewStuff

- Add new method fillMenuFromGroupingNames
- KMoreTools: add many new groupings
- KMoreToolsMenuFactory: handling for "git-clients-and-actions"
- createMenuFromGroupingNames: make url parameter optional

### KNotification

- Fix crash in NotifyByExecute when no widget has been set (bug 348510)
- Improve handling of notifications being closed (bug 342752)
- Replace QDesktopWidget usage with QScreen
- Ensure KNotification can be used from a non-GUI thread

### Struttura dei pacchetti

- Guard the structure qpointer access (bug 347231)

### KPeople

- Use QTemporaryFile instead of hardcoding /tmp.

### KPty

- Use tcgetattr &amp; tcsetattr if available

### Kross

- Fix loading of Kross modules "forms" and "kdetranslation"

### KService

- When running as root preserve file ownership on existing cache files (bug 342438)
- Guard against being unable to open stream (bug 342438)
- Fix check for invalid permissions writing file (bug 342438)
- Fix querying ksycoca for x-scheme-handler/* pseudo-mimetypes. (bug 347353)

### KTextEditor

- Allow like in KDE 4.x times 3rdparty apps/plugins to install own highlighting XML files into katepart5/syntax
- Add KTextEditor::Document::searchText()
- Bring back use of KEncodingFileDialog (bug 343255)

### KTextWidgets

- Add a method to clear decorator
- Allow to use custom sonnet decorator
- Implement "find previous" in KTextEdit.
- Re-add support for speech-to-text

### KWidgetsAddons

- KAssistantDialog: Re-add the Help button that was present in KDELibs4 version

### KXMLGUI

- Add session management for KMainWindow (bug 346768)

### NetworkManagerQt

- Drop WiMAX support for NM 1.2.0+

### Plasma Framework

- Calendar components can now display week numbers (bug 338195)
- Use QtRendering for fonts in password fields
- Fix AssociatedApplicationManager lookup when a mimetype has (bug 340326)
- Fix panel background coloring (bug 347143)
- Get rid of "Could not load applet" message
- Capability to load QML kcms in plasmoid config windows
- Don't use the DataEngineStructure for Applets
- Port libplasma away from sycoca as much as possible
- [plasmacomponents] Make SectionScroller follow the ListView.section.criteria
- Scroll bars no longer automatically hide when a touch screen is present (bug 347254)

### Sonnet

- Use one central cache for the SpellerPlugins.
- Reduce temporary allocations.
- Optimize: Do not wipe dict cache when copying speller objects.
- Optimise away save() calls by calling it once at the end if needed.

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
