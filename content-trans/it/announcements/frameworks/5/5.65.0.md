---
aliases:
- ../../kde-frameworks-5.65.0
date: 2019-12-14
layout: framework
libCount: 70
qtversion: 5.12
---
Nuovo modulo: KQuickCharts -- un modulo QtQuick che fornisce grafici ad alte prestazioni.

### Icone Brezza

- Pixel align color-picker
- Add new baloo icons
- Add new preferences search icons
- Use an eyedropper for color-picker icons (bug 403924)
- Add "all applications" category icon

### Moduli CMake aggiuntivi

- EBN extra-cmake-modules transport cleanup
- ECMGenerateExportHeader: add NO_BUILD_SET_DEPRECATED_WARNINGS_SINCE flag
- Explicitly use lib for systemd directories
- Add install dir for systemd units
- KDEFrameworkCompilerSettings: enable all Qt &amp; KF deprecation warnings

### Integrazione della struttura

- Conditionally set SH_ScrollBar_LeftClickAbsolutePosition based on kdeglobals setting (bug 379498)
- Set application name and version on the knshandler tool

### KDE Doxygen Tools

- Fix module imports with Python3

### KAuth

- Install .pri file for KAuthCore

### KBookmarks

- Deprecate KonqBookmarkMenu and KonqBookmarkContextMenu
- Move classes only used by KonqBookmarkMenu together with it

### KCalendarCore

- Fallback to system time zone on calendar creation with an invalid one
- Memory Calendar: avoid code duplication
- Use QDate as key in mIncidencesForDate in MemoryCalendar
- Handle incidences in different time zones in MemoryCalendar

### KCMUtils

- [KCMultiDialog] Remove most special margins handling; it's done in KPageDialog now
- KPluginSelector: use new KAboutPluginDialog
- Add guard for missing kirigami (bug 405023)
- Disable the restore defaults button if the KCModule says so
- Have KCModuleProxy take care of the defaulted state
- Make KCModuleQml conform to the defaulted() signal

### KCompletion

- Refactor KHistoryComboBox::insertItems

### KConfig

- Document Notifiers setting
- Only create a session config when actually restoring a session
- kwriteconfig: add delete option
- Add KPropertySkeletonItem
- Prepare KConfigSkeletonItem to allow inheriting its private class

### KConfigWidgets

- [KColorScheme] Make order of decoration colors match DecorationRole enum
- [KColorScheme] Fix mistake in NShadeRoles comment
- [KColorScheme/KStatefulBrush] Switch hardcoded numbers for enum items
- [KColorScheme] Add items to ColorSet and Role enums for the total number of items
- Register KKeySequenceWidget to KConfigDialogManager
- Adjust KCModule to also channel information about defaults

### KCoreAddons

- Deprecate KAboutData::fromPluginMetaData, now there is KAboutPluginDialog
- Add a descriptive warning when inotify_add_watch returned ENOSPC (bug 387663)
- Add test for bug "bug-414360" it's not a ktexttohtml bug (bug 414360)

### KDBusAddons

- Include API to generically implement --replace arguments

### KDeclarative

- EBN kdeclarative transfer protocol cleanup
- Adapt to change in KConfigCompiler
- make header and footer visible when they get content
- support qqmlfileselectors
- Allow to disable autosave behavior in ConfigPropertyMap

### KDED

- Remove kdeinit dependency from kded

### Supporto KDELibs 4

- remove unused kgesturemap from kaction

### KDocTools

- Catalan Works: Add missing entities

### KI18n

- Undeprecate I18N_NOOP2

### KIconThemes

- Deprecate top-level UserIcon method, no longer used

### KIO

- Add new protocol for 7z archives
- [CopyJob] When linking also consider https for text-html icon
- [KFileWidget] Avoid calling slotOk right after the url changed (bug 412737)
- [kfilewidget] Load icons by name
- KRun: don't override user preferred app when opening local *.*html and co. files (bug 399020)
- Repair FTP/HTTP proxy querying for the case of no proxy
- Ftp ioslave: Fix ProxyUrls parameter passing
- [KPropertiesDialog] provide a way of showing the target of a symlink (bug 413002)
- [Remote ioslave] Add Display Name to remote:/ (bug 414345)
- Fix HTTP proxy settings (bug 414346)
- [KDirOperator] Add Backspace shortcut to back action
- When kioslave5 couldn't be found in libexec-ish locations try $PATH
- [Samba] Improve warning message about netbios name
- [DeleteJob] Use a separate worker thread to run actual IO operation (bug 390748)
- [KPropertiesDialog] Make creation date string mouse-selectable too (bug 413902)

Deprecazioni:

- Deprecated KTcpSocket and KSsl* classes
- Remove the last traces of KSslError from TCPSlaveBase
- Port ssl_cert_errors meta data from KSslError to QSslError
- Deprecate KTcpSocket overload of KSslErrorUiData ctor
- [http kio slave] use QSslSocket instead of KTcpSocket (deprecated)
- [TcpSlaveBase] port from KTcpSocket (deprecated) to QSslSocket

### Kirigami

- Fix margins of ToolBarHeader
- Do not crash when icon's source is empty
- MenuIcon: fix warnings when the drawer isn't initialized
- Account for a mnemonic label to go back to ""
- Fix InlineMessage actions always being placed in overflow menu
- Fix default card background (bug 414329)
- Icon: solve threading issue on when the source is http
- keyboard navigation fixes
- i18n: extract messages also from C++ sources
- Fix cmake project command position
- Make QmlComponentsPool one instance per engine (bug 414003)
- Switch ToolBarPageHeader to use the icon collapse behaviour from ActionToolBar
- ActionToolBar: Automatically change to icon-only for actions marked KeepVisible
- Add a displayHint property to Action
- add the dbus interface in the static version
- Revert "take into account dragging speed when a flick ends"
- FormLayout: Fix label height if wide mode is false
- don't show the handle by default when not modal
- Revert "Ensure that GlobalDrawer topContent always stays on top"
- Use a RowLayout for laying out ToolBarPageHeader
- Vertically center left actions in ActionTextField (bug 413769)
- irestore dynamic watch of tablet mode
- replace SwipeListItem
- support actionsVisible property
- start SwipeListItem port to SwipeDelegate

### KItemModels

- Deprecate KRecursiveFilterProxyModel
- KNumberModel: gracefully handle a stepSize of 0
- Expose KNumberModel to QML
- Add new class KNumberModel that is a model of numbers between two values
- Expose KDescendantsProxyModel to QML
- Add qml import for KItemModels

### KNewStuff

- Add some friendly "report bugs here" links
- Fix i18n syntax to avoid runtime errors (bug 414498)
- Turn KNewStuffQuick::CommentsModel into a SortFilterProxy for reviews
- Correctly set i18n arguments in one pass (bug 414060)
- These functions are @since 5.65, not 5.64
- Add OBS to screenrecorders (bug 412320)
- Fix a couple of broken links, update links to https://kde.org/applications/
- Fix translations of $GenericName
- Show a "Loading more..." busy indicator when loading view data
- Give some more pretty feedback in NewStuff::Page while the Engine is loading (bug 413439)
- Add an overlay component for item activity feedback (bug 413441)
- Only show DownloadItemsSheet if there's more than one download item (bug 413437)
- Use the pointing hand cursor for the single-clickable delegates (bug 413435)
- Fix the header layouts for EntryDetails and Page components (bug 413440)

### KNotification

- Make the docs reflect that setIconName should be preferred over setPixmap when possible
- Percorso del file di configurazione del documento su Android

### KParts

- Mark BrowserRun::simpleSave properly as deprecated
- BrowserOpenOrSaveQuestion: move AskEmbedOrSaveFlags enum from BrowserRun

### KPeople

- Allow triggering sort from QML

### kquickcharts

Nuovo modulo. Il modulo Quick Charts fornisce un insieme di grafici che possono essere usati da applicazioni QtQuick. Sono pensati per essere usati sia per la semplice visualizzazione dei dati, sia per la visualizzazione continua di elevati volumi di dati (spesso chiamati anche «plotter»). I grafici usano, per una resa accelerata, un sistema chiamato campo della distanza che fornisce dei modi per usare la GPU per rendere delle forme 2D senza perdita di qualità.

### KTextEditor

- KateModeManager::updateFileType(): validate modes and reload menu of the status bar
- Verify modes of the session config file
- LGPLv2+ after ok by Svyatoslav Kuzmich
- restore files pre-format

### KTextWidgets

- Deprecate kregexpeditorinterface
- [kfinddialog] Remove usage of kregexpeditor plugin system

### KWayland

- [server] Do not own dmabuf implementation
- [server] Make double-buffered properties in xdg-shell double-buffered

### KWidgetsAddons

- [KSqueezedTextLabel] Add icon for "Copy entire text" action
- Unify KPageDialog margin handling into KPageDialog itself (bug 413181)

### KWindowSystem

- Adjust count after _GTK_FRAME_EXTENTS addition
- Add support for _GTK_FRAME_EXTENTS

### KXMLGUI

- Drop unused broken KGesture support
- Add KAboutPluginDialog, to be used with KPluginMetaData
- Also allow invoking session restoration logic when apps are manually launched (bug 413564)
- Add missing property to KKeySequenceWidget

### Icone Oxygen

- Symlink microphone to audio-input-microphone on all sizes (bug 398160)

### Plasma Framework

- move backgroundhints managment in Applet
- use the file selector in the interceptor
- more use of ColorScope
- also monitor window changes
- support for user removing background and automatic shadow
- support file selectors
- support qml file selectors
- remove stray qgraphicsview stuff
- don't delete and recreate wallpaperinterface if not needed
- MobileTextActionsToolBar check if controlRoot is undefined before using it
- Add hideOnWindowDeactivate to PlasmaComponents.Dialog

### Scopo

- include the cmake command we are about to use

### QQC2StyleBridge

- [TabBar] Use window color instead of button color (bug 413311)
- bind enabled properties to the view enabled
- [ToolTip] Base timeout on text length
- [ComboBox] Don't dim Popup
- [ComboBox] Don't indicate focus when popup is open
- [ComboBox] Follow focusPolicy

### Solid

- [udisks2] fix media change detection for external optical drives (bug 394348)

### Sonnet

- Disable ispell backend with mingw
- Implement ISpellChecker backend for Windows &gt;= 8
- Basic cross-compiling support for parsetrigrams
- embed trigrams.map into shared library

### Sottoscrizioni

- Fix Bug 383381 - Getting the feed URL from a youtube channel no longer works (bug 383381)
- Extract code so we can fix parsing code (bug 383381)
- atom has icon support (So we can use specific icon in akregator)
- Convert as a real qtest apps

### Evidenziazione della sintassi

- Updates from CMake 3.16 final release
- reStructuredText: Fix inline literals highlighting preceding characters
- rst: Add support for standalone hyperlinks
- JavaScript: move keywords from TypeScript and other improvements
- JavaScript/TypeScript React: rename syntax definitions
- LaTeX: fix backslash delimiter in some keywords (bug 413493)

### ThreadWeaver

- Use URL with transport encryption

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
