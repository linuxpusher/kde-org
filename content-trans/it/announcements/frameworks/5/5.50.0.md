---
aliases:
- ../../kde-frameworks-5.50.0
date: 2018-09-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Add support for proposed tags addition in OCS 1.7

### Baloo

- Fixed a typo in the index size output (bug 397843)
- Remove src not dest url when a url is newly unindexable
- [tags_kio] Simplify the filename path query matching by using a capture group
- Revert "Skip queueing newly unindexable files and remove them from the index immediately."
- [tags_kio] Simplify file path lookup while loop

### Icone Brezza

- Add LabPlot project file icon
- ScalableTest, add "scalable" plasma-browser-integration (bug 393999)

### Moduli CMake aggiuntivi

- Bindings: Check if bindings can be generated for a specific python version
- Bindings: Make generator forward compatible with Python 3
- Disable alteration of QT_PLUGIN_PATH by ECM when running tests
- Bindings: Add support for scoped enums (bug 397154)
- Make it possible for ECM to detect po files at configure time

### Integrazione della struttura

- [KStyle] Use dialog-question for question icon

### KArchive

- handle non-ASCII encodings of file names in tar archives (bug 266141)
- KCompressionDevice: don't call write after WriteError (bug 397545)
- Add missing Q_OBJECT macros for QIODevice subclasses
- KCompressionDevice: propagate errors from QIODevice::close() (bug 397545)
- Fix bzip main page

### KCMUtils

- Use custom QScrollArea with size hint not limited by font size (bug 389585)

### KConfig

- Remove warning about old kiosk feature that no longer applies
- Set system default shortcut Ctrl+0 for "Actual Size" action

### KCoreAddons

- Don't remove space between two url when line start with " (kmail bug)
- KPluginLoader: use '/' even on Windows, libraryPaths() returns paths with '/'
- KPluginMetaData: convert empty string to empty stringlist

### KDeclarative

- Revert "ensure we are always writing in the engine's root context"
- Attach property to "delegate" (bug 397367)
- [KCM GridDelegate] Use layer effect only on OpenGL backend (bug 397220)

### KDocTools

- add acronym ASCII to general.entities
- add JSON to general.entities
- Let meinproc5 be more verbose in 'install' auto test
- In kdoctools_install test use absolute paths to find installed files

### KFileMetaData

- Add enum alias Property::Language for typo Property::Langauge

### KHolidays

- Implement proper equinox and solstice calculation algorithm (bug 396750)
- src/CMakeLists.txt - install headers the frameworks-way

### KI18n

- Assert if trying to use a KCatalog without a QCoreApplication
- Port ki18n from QtScript to QtQml
- Check the build directory for po/ as well

### KIconThemes

- Set breeze as fallback icon theme

### KIO

- [KSambaShareData] Accept spaces in ACL host name
- [KFileItemListProperties] Use mostLocalUrl for capabilities
- [KMountPoint] Also check "smb-share" for whether it's an SMB mount (bug 344146)
- [KMountPoint] Resolve gvfsd mounts (bug 344146)
- [KMountPoint] Remove traces of supermount
- [KMountPoint] Remove AIX and Windows CE support
- Display mounted file system type and mounted from fields in properties dialog (bug 220976)
- kdirlistertest doesn't fail at random
- [KUrlComboBox] Fix KIcon porting error
- Port KPATH_SEPARATOR "hack" to QDir::listSeparator, added in Qt 5.6
- Fixes memory leak in KUrlComboBox::setUrl
- [KFileItem] Don't read directory comment on slow mounts
- Use QDir::canonicalPath instead
- Ignore NTFS hidden flag for root volume (bug 392913)
- Give the "invalid directory name" dialog a cancel button
- KPropertiesDialog: switch to label in setFileNameReadOnly(true)
- Refine wording when a folder with an invalid name could not be created
- Use appropriate icon for a cancel button that will ask for a new name
- Make read-only filenames selectable
- Use title case for some button labels
- Use KLineEdit for folder name if folder has write access, else use QLabel
- KCookieJar: fix wrong timezone conversion

### Kirigami

- support fillWidth for items
- guard against external deletion of pages
- always show the header when we are in collapsible mode
- fix showContentWhenCollapsed behavior
- fix holes in menus in Material style
- standard actionsmenu for the page contextmenu
- Explicitly request Qt 5.7's QtQuick to make use of Connections.enabled
- use Window color instead of a background item
- make sure the drawer closes even when pushing a new
- export separatorvisible to the globaltoolbar
- Fix the Kirigami QRC static plugin generation
- Fix the build in LTO static mode
- Ensure drawerOpen property is synced correctly (bug 394867)
- drawer: Display the content widget when dragging
- Allow qrc assets to be used in Actions icons
- ld on old gcc (bug 395156)

### KItemViews

- Deprecate KFilterProxySearchLine

### KNewStuff

- Cache providerId

### KNotification

- Support libcanberra for audio notification

### KService

- KBuildSycoca: always process application desktop files

### KTextEditor

- Turn enum Kate::ScriptType into an enum class
- correct error handling for QFileDevice and KCompressedDevice
- InlineNotes: Do not print inline notes
- Remove QSaveFile in favor of plain old file saving
- InlineNotes: Use screen global coordinates everywhere
- InlineNote: Initialize position with Cursor::invalid()
- InlineNote: Pimpl inline note data without allocs
- Add inline note interface
- Show text preview only if main window is active (bug 392396)
- Fix crash when hiding the TextPreview widget (bug 397266)
- Merge ssh://git.kde.org/ktexteditor
- improve hidpi rendering of icon border
- Improve vim color theme (bug 361127)
- Search: Add workaround for missing icons in Gnome icon-theme
- fix overpainting for _ or letters like j in the last line (bug 390665)
- Extend Scripting API to allow executing commands
- Indentation script for R
- Fix crash when replacing n around empty lines (bug 381080)
- remove highlighting download dialog
- no need to new/delete hash on each doHighlight, clearing it is good enough
- ensure we can handle invalid attribute indices that can happen as left overs after HL switch for a document
- let smart pointer handle deletion of objects, less manual stuff to do
- remove map to lookup additional hl properties
- KTextEditor uses the KSyntaxHighlighting framework for all
- use character encodings as provided by the definitions
- Merge branch 'master' into syntax-highlighting
- non-bold text no longer renders with font weight thin but (bug 393861)
- use foldingEnabled
- remove EncodedCharaterInsertionPolicy
- Printing: Respect footer font, fix footer vertical position, make header/footer separator line visually lighter
- Merge branch 'master' into syntax-highlighting
- let syntax-highlighting framework handle all definition management now that there is the None definition around in the repo
- completion widget: fix minimum header section size
- Fix: Scroll view lines instead of real lines for wheel and touchpad scrolling (bug 256561)
- remove syntax test, that is now tested in the syntax-highlighting framework itself
- KTextEditor configuration is now application local again, the old global configuration will be imported on first use
- Use KSyntaxHighlighting::CommentPosition instead of KateHighlighting::CSLPos
- Use isWordWrapDelimiter() from KSyntaxHighlighting
- Rename isDelimiter() to isWordDelimiter()
- implement more lookup stuff via format -&gt; definition link
- we now always get valid formats
- nicer way to get attribute name
- fix python indentation test, safer accessor to property bags
- add right definition prefixes again
- Merge branch 'syntax-highlighting' of git://anongit.kde.org/ktexteditor into syntax-highlighting
- try to bring back lists needed to do the configuration per scheme
- Use KSyntaxHighlighting::Definition::isDelimiter()
- make can break bit more like in word code
- no linked list without any reason
- cleanup properties init
- fix order of formats, remember definition in highlighting bag
- handle invalid formats / zero length formats
- remove more old implementation parts, fixup some accessors to use the format stuff
- fix indentation based folding
- remove exposure of context stack in doHighlight + fix ctxChanged
- start to store folding stuff
- rip out highlighting helpers, no longer needed
- remove need to contextNum, add FIXME-SYNTAX marker to stuff that needs to be fixed up properly
- adapt to includedDefinitions changes, remove contextForLocation, one only needs either keywords for location or spellchecking for location, can be implemented later
- remove more no longer used syntax highlighting things
- fixup the m_additionalData and the mapping for it a bit, should work for attributes, not for context
- create initial attributes, still without real attribute values, just a list of something
- call the highlighting
- derive from abstract highlighter, set definition

### KWallet Framework

- Move example from techbase to own repo

### KWayland

- Sync set/send/update methods
- Add serial number and EISA ID to OutputDevice interface
- Output device color curves correction
- Fix memory management in WaylandOutputManagement
- Isolate every test within WaylandOutputManagement
- OutputManagement fractional scaling

### KWidgetsAddons

- Create a first example of the use of KMessageBox
- Fix two bugs in KMessageWidget
- [KMessageBox] Call style for icon
- Add workaround for labels with word-wrapping (bug 396450)

### KXMLGUI

- Make Konqi look good in HiDPI
- Add missing parentheses

### NetworkManagerQt

- Require NetworkManager 1.4.0 and newer
- manager: add support to R/W the GlobalDnsConfiguration property
- Actually allow to set the refresh rate for device statistics

### Plasma Framework

- Workaround bug with native rendering and opacity in TextField text (bug 396813)
- [Icon Item] Watch KIconLoader icon change when using QIcon (bug 397109)
- [Icon Item] Use ItemEnabledHasChanged
- Get rid of deprecated QWeakPointer usage
- Fix style sheet for 22-22-system-suspend (bug 397441)
- Improve Widgets' removal and configure text

### Solid

- solid/udisks2: Add support for categorized logging
- [Windows Device] Show device label only if there is one
- Force reevaluation of Predicates if interfaces are removed (bug 394348)

### Sonnet

- hunspell: Restore build with hunspell &lt;=v1.5.0
- Include hunspell headers as system includes

### syndication

Nuovo modulo

### Evidenziazione della sintassi

- highlight 20000 lines per testcase
- make highlighting benchmark more reproducible, we anyways want to measure this execution with e.g. perf from the outside
- Tune KeywordList lookup &amp; avoid allocations for implicit capture group
- remove captures for Int, never implemented
- deterministic iteration of tests for better result comparison
- handle nested include attributes
- update Modula-2 highlighting (bug 397801)
- precompute attribute format for context &amp; rules
- avoid word delimiter check at start of keyword (bug 397719)
- Add syntax highlighting for SELinux kernel policy language
- hide bestCandidate, can be static function inside file
- Add some improvements to kate-syntax-highlighter for use in scripting
- added := as a valid part of an identifier
- use our own input data for benchmarking
- try to fix line ending issue in compare of results
- try trivial diff output for Windows
- add defData again for valid state check
- decrease StateData space by more than 50% and half the number of needed mallocs
- improve performance of Rule::isWordDelimiter and KeywordListRule::doMatch
- Improve skip offset handling, allow to skip full line on no match
- check extensions wildcard list
- more asterisk hl, I tried some asterisk configs, they are just ini style, use .conf as ini ending
- fix highlighting for #ifdef _xxx stuff (bug 397766)
- fix wildcards in files
- MIT relicensing of KSyntaxHighlighting done
- JavaScript: add binaries, fix octals, improve escapes &amp; allow Non-ASCII identifiers (bug 393633)
- Allow to turn of the QStandardPaths lookups
- Allow to install syntax files instead of having them in a resource
- handle context switch attributes of the contexts themselves
- change from static lib to object lib with right pic setting, should work for shared + static builds
- avoid any heap allocation for default constructed Format() as used as "invalid"
- honor cmake variable for static vs. dynamic lib, like e.g. karchive
- MIT relicensing, https://phabricator.kde.org/T9455
- remove old add_license script, no longer needed
- Fix includedDefinitions, handle definition change in context switch (bug 397659)
- SQL: various improvements and fix if/case/loop/end detection with SQL (Oracle)
- fix reference files
- SCSS: update syntax. CSS: fix Operator and Selector Tag highlighting
- debchangelog: add Bookworm
- Relicense Dockerfile to MIT license
- remove the no longer supported configuration part of the spellchecking that always had just one mode we now hardcode
- Add syntax highlighting support for Stan
- add back indenter
- Optimize many syntax highlighting files and fix the '/' char of SQL
- Modelines: add byte-order-mark &amp; small fixes
- Relicense modelines.xml to MIT license (bug 198540)
- Add QVector&lt;QPair&lt;QChar, QString&gt;&gt; Definition::characterEncodings() const
- Add bool Definition::foldingEnabled() const
- Add "None" highlighting to repository per default
- Update Logtalk language syntax support
- Add Autodesk EAGLE sch and brd file format to the XML category
- C# highlighting: Prefer C-Style indenter
- AppArmor: update syntax and various improvements/fixes
- Java: add binaries &amp; hex-float, and support underscores in numbers (bug 386391)
- Cleanup: indentation was moved from general to language section
- Definition: Expose command markers
- Add highlighting of JavaScript React
- YAML: fix keys, add numbers and other improvements (bug 389636)
- Add bool Definition::isWordWrapDelimiter(QChar)
- Definition: Rename isDelimiter() to isWordDelimiter()
- Note KF6 API improvement ideas from the KTE porting
- Provide a valid format for empty lines too
- Make Definition::isDelimiter() also work for invalid definitions
- Definition: Expose bool isDelimiter() const
- Sort returned formats in Definition::formats() by id

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
