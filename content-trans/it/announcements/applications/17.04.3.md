---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE rilascia KDE Applications 17.04.3
layout: application
title: KDE rilascia KDE Applications 17.04.3
version: 17.04.3
---
13 luglio 2017. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../17.04.0'>KDE Applications 17.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 25 errori corretti includono, tra gli altri, miglioramenti a kdepim, dolphin, dragonplayer, kdenlive e umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.34.
