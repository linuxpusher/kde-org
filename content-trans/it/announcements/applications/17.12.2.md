---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE rilascia KDE Applications 17.12.2
layout: application
title: KDE rilascia KDE Applications 17.12.2
version: 17.12.2
---
8 febbraio 2018. Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../17.12.0'>KDE Applications 17.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Circa venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Dolphin, Gwenview, KGet e Okular.
