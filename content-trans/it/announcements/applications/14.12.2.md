---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE rilascia Applications 14.12.2.
layout: application
title: KDE rilascia KDE Applications 14.12.2
version: 14.12.2
---
3 febbraio 2015. Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../14.12.0'>KDE Applications 14.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 20 bug corretti includono miglioramenti al gioco di anagrammi Kanagram, allo strumento per UML Umbrello, al visualizzatore di documenti Okular ed al globo virtuale Marble.

Questo rilascio include inoltre le versioni con supporto a lungo termine (Long Term Support) di Plasma Workspaces 4.11.16, KDE Development Platform 4.14.5 e della suite Kontact 4.14.5.
