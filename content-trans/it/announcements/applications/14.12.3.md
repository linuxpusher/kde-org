---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE rilascia Applications 14.12.3.
layout: application
title: KDE rilascia KDE Applications 14.12.3
version: 14.12.3
---
3 marzo 2015. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../14.12.0'>KDE Applications 14.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Con 19 errori corretti include anche miglioramenti al gioco di anagrammi Kanagram, allo strumento per UML Umbrello, al visualizzatore di documenti Okular e all'applicazione di geometria Kig.

Questo rilascio include inoltre le versioni con supporto a lungo termine (Long Term Support) di Plasma Workspaces 4.11.17, KDE Development Platform 4.14.6 e della suite Kontact 4.14.6.
