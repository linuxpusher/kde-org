---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE toob välja KDE rakendused 15.12.3
layout: application
title: KDE toob välja KDE rakendused 15.12.3
version: 15.12.3
---
March 15, 2016. Today KDE released the third stability update for <a href='../15.12.0'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 15 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Akonadi, Arki, Kblocksi, Kcalci, Ktouchi, Umbrello ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.18.
