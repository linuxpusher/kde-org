---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Paljudes raamistikes

- Privaatsete klasside nime muutmine vältimaks nende juhuslikku eksportimist

### Baloo

- Tagasiühilduvuse huvides org.kde.baloo liidese lisamine juurobjektile
- Võltsi org.kde.baloo.file.indexer.xml paigaldamine plasma-desktop 5.4 kompileerimise parandamiseks
- D-Busi liideste ümberkorraldamine
- json metaandmete kasutamine kded pluginas ja plugina nime parandamine
- Ühe andmebaasiisendi loomine protsessi kohta (veateade 350427)
- baloo_file_extractor'i tapmise vältimine sissekandmisel
- XML-i liidese faili genereerimine qt5_generate_dbus_interface abil
- Baloo jälgija parandused
- Faili URL-i ekspordi liigutamine põhilõime
- Kaskaadseadistuste arvestamisega tagamine
- Privaatse teegi nimelinki ei paigaldata
- Tõlgete paigaldamine (täheldas Srvoje Senjan).

### BluezQt

- deviceChanged signaali ei edastata enam pärast seda, kui seade on eemaldatud (veateade 351051)
- -DBUILD_TESTING=OFF arvestamine

### CMake'i lisamoodulid

- Makro lisamine Qt5 logimiskategooriate deklaratsioonide genereerimiseks.
- ecm_generate_headers: valiku COMMON_HEADER ja mitme päise funktsionaalsuse lisamine
- KF5 koodile pedantic lisamine (gcc või c-lang'i kasutamisel)
- KDEFrameworkCompilerSettings: ainult rangete iteraatorite lubamine silumisrežiimis
- C koodi vaikimise nähtavuse määramine peidetuks.

### Raamistike lõimimine

- Aknatiitlite näitamine ka ainult katalooge näitavates failidialoogides.

### KActivities

- Ainult ühe tegevuselaadija (lõime) eraldamine, kui FileItemLinkingPlugin'i toiminguid ei ole initsialiseeritud (veateade 351585)
- Privaatsete klasside nime muutmisega tekkinud ehitusprobleemide parandamine (11030ffc0)
- Boosti puuduvad kaasatute asukoha lisamine ehitamiseks OS X peal
- Tegevuse seadistustesse liigutatud kiirklahvide määramine
- Privaatse tegevusrežiimi töötamise tagamine
- Seadistuste kasutajaliidese ümberkorraldamine
- Tegevuse põhimeetodid toimivad
- Tegevus seadistamise ja kustutamise hüpikute kasutajaliides
- Tegevuste Süsteemi seadistuste moodulis loomise/kustutamise/seadistamise algne kasutajaliides
- Tüki suuruse suurendamine tulemuste laadimisel
- std::set'i puuduva kaasatu lisamine

### KDE Doxygeni tööriistad

- Windowsi parandus; olemasolevate failide eemaldamine enne nende asendamist os.rename'i abil.
- Omaasukohtade kasutamine pythoni väljakutsumisel Windowsis ehitamise parandamiseks

### KCompletion

- Halva käitumise / mälunappuse tekkimise parandamine Windowsis (veateade 345860)

### KConfig

- readEntryGui optimeerimine
- QString::fromLatin1() vältimine genereeritud koodis
- Kuluka QStandardPaths::locateAll() väljakutsumise minimeerimine
- Portimise lõpetamine QCommandLineParser'i peale (sel on nüüd addPositionalArgument)

### KDELibs 4 toetus

- kded solid-networkstatus plugina portimine json'i metaandmete peale
- KPixmapCache: kataloogi loomine, kui seda veel pole

### KDocTools

- Katalaani user.entities sünkroonimine inglise (en) versiooniga.
- Olemite lisamine (sebas ja plasma-pa)

### KEmoticons

- Jõudlus: KEmoticons'i isendi puhverdamine siin, mitte KEmoticonsYheme'is.

### KFileMetaData

- PlainTextExtractor: O_NOATIME haru lubamine GNU libc platvormidel
- PlainTextExtractor: Linuxi haru töötab ka ilma O_NOATIME'ita
- PlainTextExtractor: veakontrolli parandamine open(O_NOATIME) nurjumisel

### KGlobalAccel

- kglobalaccel5 käivitamine ainult vajaduse korral.

### KI18n

- Reavahetuse puudumise pmap-faili lõpus armuline kohtlemine

### KIconThemes

- KIconLoader: reconfigure() ei unusta enam päritud teemasid ja rakenduste katalooge
- Ikooni laadimise spetsifikatsiooni täpsem jälgimine

### KImageFormats

- eps: Qt kategoriseeritud logimisega seotud kaasatute parandamine

### KIO

- Q_OS_WIN kasutamine Q_OS_WINDOWS asemel
- KDE_FORK_SLAVES töötab nüüd Windowsis
- Töölauafaili paigaldamise keelamine KDE ProxyScout'i moodulis
- Deterministlik sortimise järjekord KDirSortFilterProxyModelPrivate::compare'is
- Kohandatud kataloogiikoone näidatakse taas (veateade 350612)
- kpasswdserver'i liigutamine kded-st kiod-sse
- Portimisvigade parandamine kpasswdserver'is
- Kõneluste pidamise pärandkoodi eemaldamine kpasswdserver'i väga vanadest versioonidest.
- KDirListerTest: QTRY_COMPARE kasutamine mõlemas lauses, mis peaks kõrvaldama praeguse trügimise
- KFilePlacesModel: vana ülesabde täitmine kasutada täismahulise KDirLister'i asemel trashrc'd.

### KItemModels

- Uus puhverserverimudel: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: layoutChanged'i käitlemise parandamine.
- Põhjalikum valiku kontroll sortimise järel.
- KExtraColumnsProxyModel: vea parandamine sibling()-is, mis tekitas tõrkeid näiteks valimisel

### Paketiraamistik

- kpackagetool suudab eemaldada paketi paketifailist
- kpackagetool oskab nüüd taibukamalt leida õige teenusetüübi

### KService

- KSycoca: ajatemplite kontrollimine ja kbuildsycoca käivitamine vajaduse korral. Ei sõltu enam kded'st.
- ksycoca't ei suleta enam kohe pärast avamist.
- KPluginInfo käitleb nüüd korralikult FormFactor'i metaandmeid

### KTextEditor

- TextLineData ja ref count ploki omistamise ühendamine
- "Liigu eelmisele redigeeritud reale" vaikimisi kiirklahvi muutmine
- Haskelli kommentaaride süntaksi esiletõstmise parandused
- Koodilõpetuse hüpiku ilmumise kiirendamine 
- minikaart: katse täiustada välimust (veateade 309553)
- pesastatud kommentaarid Haskelli süntaksi esiletõstmises
- Pythoni vale treppimise probleemi lahendamine (veateade 351190)

### KWidgetsAddons

- KPasswordDialog: kasutajal on lubatud muuta parooli nähtavust (veateade 224686)

### KXMLGUI

- Enamikku keeli mittenäidanud KSwitchLanguageDialog'i parandamine

### KXmlRpcClient

- QLatin1String'i vältimine kõikjal, kus see omistab kuhimälu

### ModemManagerQt

- Metatüübi konflikti lahendamine nm-qt uusima muudatusega

### NetworkManagerQt

- Uute omaduste lisamine NM uusimatest väljalasetest

### Plasma raamistik

- võimaluse korral seondamine flickable külge
- paketiloendi parandamine
- plasma: apleti toimingud võivad olla nullptr parandus (veateade 351777)
- PlasmaComponents.ModelContextMenu onClicked signaal töötab nüüd korralikult
- PlasmaComponents'i ModelContextMenu võib nüüd luua menüüsektsioone
- kded platformstatus'i plugina portimine json'i metaandmete peale
- Vigaste metaandmete käitlemine PluginLoader'is
- RowLayout arvutab ise välja pealdise suuruse
- redigeerimismenüü näitamine alati, kui kursor on näha
- ButtonStyle'i korduse parandus
- Bupu lamedust klõpsamisel ei muudeta
- kerimisribad on puuteekraanil ja mobiilis läbipaistvad
- vilkumiskiiruse ja aeglustumise kohandamine DPI-ga
- kohandatud kursori delegaat ainult mobiilis
- puutesõbralik tekstikursor
- päriluse ja hüpiku reeglite parandus
- __editMenu deklareerimine
- puuduva delegaate käitleva kursori lisamine
- EditMenu teostuse ümberkirjutamine
- mobiilimenüü kasutamine ainult tingimuslikult
- menüü eellaseks saab juur (root)

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
