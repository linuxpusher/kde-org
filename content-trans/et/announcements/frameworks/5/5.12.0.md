---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### CMake'i lisamoodulid

- Täiustatud query_qmake makro vigadest teadaandmine 

### BluezQt

- Kõigi seadmete eemaldamine adapterist enne adapteri eemaldamist (veateade 349363)
- README.md linkide uuendamine

### KActivities

- Valiku lisamine mitte jälgida kasutajat konkreetsetes tegevustes (sarnaneb "privaatsele lehitsemisele" veebibrauserites)

### KArchive

- Failide täitmisõiguste säilitamine copyTo() korral
- ~KArchive puhastamine surnud koodi eemaldades.

### KAuth

- kauth-policy-gen kasutamise lubamine eri allikatest

### KBookmarks

- Järjehoidjat ei lisata, kui puudu on nii URL kui ka tekst
- KBookmarki URL-i kodeerimine ühilduvuse tagamiseks KDE4 rakendustega

### KCodecs

- x-euc-tw kontrollija eemaldamine

### KConfig

- kconfig_compiler'i paigaldamine libexec'isse
- Uus koodi genereerimise võti TranslationDomain=, mida kasutatakse koos võtmega TranslationSystem=kde, mida läheb tavaliselt tarvis teekides.
- kconfig_compiler'ikasutamise lubamine eri allikatest

### KCoreAddons

- KDirWatch: ühenduse loomine FAM-iga ainult nõudmisel
- Pluginate ja rakenduste filtreerimise lubamine kujuteguri alusel
- desktoptojson'i kasutamise lubamine eri allikatest

### KDBusAddons

- Unikaalsete isendite väljumisväärtuse puhastamine

### KDeclarative

- QQC klooni lisamine KColorButton'ile
- Võimaluse korral igale kdeclarative'i isendile QmlObject'i omistamine
- QML-koodi Qt.quit() töötab jälle
- Haru 'mart/singleQmlEngineExperiment' ühendamine
- sizeHint implicitWidth/height alusel
- QmlObject'i alamklass staatilises mootoris

### KDELibs 4 toetus

- KMimeType::Ptr::isNull parandamine.
- KDateTime'i striimimise toetuse taaslubamine kDebug'i/qDebug'i
- Õige tõlkekataloogi laadimine kdebugdialog'is
- Iganenud meetodite dokumenteerimist ei jäeta enam vahele, et inimesed saaksid näha vihjeid portimise kohta

### KDESU

- CMakeLists.txt parandamine KDESU_USE_SUDO_DEFAULT edastamiseks kompileerimisele, et seda saaks kasutada suprocess.cpp

### KDocTools

- KF5 dokbook-mallide uuendamine

### KGlobalAccel

- privaatse käitusaja API paigaldamine, et KWin saaks pakkuda plugina Waylandile.
- Varuvariandina componentFriendlyForAction nimelahendus

### KIconThemes

- Ikooni ei püüta näidata, kui suurus on vigane

### KItemModels

- Uus puhverserveri mudel KRearrangeColumnsProxyModel. See toetab lähtemudeli veergude ümberkorraldamist ja peitmist.

### KNotification

- Pikselrastritüüpide parandamine failis org.kde.StatusNotifierItem.xml
- [ksni] Meetodi lisamine hankida toiming nime järgi (veateade 349513)

### KPeople

- PersonsModel'i filtreerimise võimaldamine

### KPlotting

- KPlotWidget: setAutoDeletePlotObjects'i lisamine, mälulekke kõrvaldamine replacePlotObject'is
- Puuduvate jaotismärkide parandamine, kui x0 &gt; 0.
- KPlotWidget: puudub vajadus setMinimumSize või resize järele.

### KTextEditor

- debianchangelog.xml: Debian/Stretch, Debian/Buster, Ubuntu-Wily lisamine
- UTF-16 surrogaadipaaride käitumise parandamine Backspace/kustutamise korral.
- QScrollBar võib käidelda WheelEvents'e (veateade 340936)
- KWrite'i arendaja puhta põhilise esiletõstmise uuendamise paiga kasutamine ("Alexander Clay" &lt;tuireann@EpicBasic.org&gt;)

### KTextWidgets

- OK-nupu lubamise/keelamise parandamine

### KWalleti raamistik

- Käsureatööriista kwallet-query importimine ja täiustamine.
- Seondavate kirjete ülekirjutamise toetus.

### KXMLGUI

- KDE teabedialoogis ei näidata "KDE Frameworksi versiooni"

### Plasma raamistik

- Tumeda teema muutmine täiesti tumedaks
- Loomuliku suuruse puhverdamine eraldi skaleerimistegurist
- ContainmentView: vigased corona metaandmed ei põhjusta enam krahhi
- AppletQuickItem: KPluginInfo't ei kasutata, kui see pole korrektne
- Aeg-ajalt esinenud aplettide tühjade seadistuslehekülgede parandamine (veateade 349250)
- hidpi toetuse parandamine kalendri alusvõrgukomponendis
- Enne plugina kasutamist kontrollimine, kas KService'il on selle kohta korrektne info
- [kalender] tagamine, et alusvõrk joonistatakse teema muutmisel ümber
- [kalender] nädalaid hakatakse alati arvestama esmaspäevast (veateade 349044)
- [kalender] alusvõrgu ümberjoonistamine nädalanumbrite näitamise seadistuse muutmisel
- Läbipaistmatu teema kasutamine, kui saadaval on ainult hägustamise efekt (veateade 348154)
- Eri mootoriga aplettide/versioonide panemine lubatute loendisse
- Uus klass ContainmentView

### Sonnet

- Esiletõstmise õigekirja kontrolli kasutamise lubamine QPlainTextEdit'is

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
