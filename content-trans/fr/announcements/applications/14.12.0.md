---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE publie la version 14.12 des applications.
layout: application
title: KDE publie la version 14.12 des applications.
version: 14.12.0
---
Le 17 décembre 2014 . Aujourd'hui, KDE a publié les applications 14.12 de KDE. Cette version apporte de nouvelles fonctionnalités et des corrections de bugs à plus d'une centaine d'applications. La plupart de ces applications reposent sur la plateforme de développement KDE 4. Certaines ont été converties au nouvel <a href='https://dot.kde.org/2013/09/25/frameworks-5'>environnement de développement 5 de KDE</a>, un ensemble de bibliothèques modulaires reposant sur Qt5, la dernière version de cet environnement de développement populaire et multi plate-forme d'applications.

<a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> est une nouveauté dans cette version. C'est une bibliothèque permettant la détection et la reconnaissance faciales dans les photos.

Cette mise à jour inclut les premières versions des environnements de développement de KDE 5 pour <a href='http://www.kate-editor.org'>Kate</a>, <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, <a href='http://edu.kde.org/kalgebra'>KAlgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>KHangman</a>, <a href='http://edu.kde.org/kig'>Kig</a> et <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> and <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Certaines bibliothèques sont aussi prêtes pour une utilisation sous l'environnement de développement KDE version 5 : « analitza » et « libkeduvocdocument ».

La <a href='http://kontact.kde.org'>Suite Kontact</a> est maintenant en version à support étendu (« Long Term Support ») dans sa version 4.14, pendant que les développeurs utilisent toute leur nouvelle énergie pour la porter vers l'environnement de développement KDE 5.

Certaines de ces nouvelles fonctionnalités de cette version intègrent :

+ <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> dispose d'une nouvelle version « Android » grâce à l'environnement de développement KDE 5. Il est maintenant capable <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>d'imprimer ses graphes en 3D</a>.
+ <a href='http://edu.kde.org/kgeography'>KGeography</a> intègre une nouvelle carte pour « Bihar ».
+ L'afficheur de documents, <a href='http://okular.kde.org'>Okular</a> prend maintenant en charge la recherche inversée « latex-synctex » dans les fichiers « dvi » et certains améliorations mineurs dans la prise en charge de « ePub ».
+ <a href='http://umbrello.kde.org'>Umbrello</a>, e modeleur UML intègre de nombreuses nouvelles fonctionnalités, trop nombreuses pour être listées ici.

La version d'avril des applications KDE 15.04 inclura de nombreuses nouvelles fonctionnalités ainsi que plus d'applications reposant sur le modulaire KDE Framework 5.
