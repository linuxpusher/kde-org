---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE publie les applications de KDE en version 18.04.0
layout: application
title: KDE publie les applications de KDE en version 18.04.0
version: 18.04.0
---
19 Avril 2018. Les applications de KDE 18.04.0 ont été mises à jour.

Nous travaillons de façon continue sur l'amélioration des logiciels dont la série d'applications de KDE. Nous espérons que vous trouverez toutes les nouvelles améliorations et les corrections de bogues utiles ! 

## Quoi de neuf dans les applications de KDE 18.04

### Système

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

La première version majeure de 2018 de <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, le gestionnaire puissant de fichiers, proposer de nombreuses améliorations pour ces panneaux :

- Les sections du panneau « Lieux » peuvent désormais être masquées si vous préférez ne pas les afficher, et une nouvelle section « Réseau » est désormais disponible pour conserver les entrées des sites distants.
- Le panneau 'Terminal' peut être ancré sur n'importe quel côté de la fenêtre, et si vous essayez de l'ouvrir sans Konsole installé, Dolphin affichera un avertissement et vous aidera à l'installer.
- La prise en charge « HiDPI » pour le panneau « Informations » a été améliorée.

L'affichage de dossiers et les menus ont été aussi mis à jour :

- Le dossier « Corbeille » affiche maintenant un bouton « Vider la corbeille ».
- Un élément de menu « Afficher la cible » a été ajouté pour aider à trouver les cibles avec liens symboliques.
- L'intégration de Git a été étendue, puisque que le menu contextuel pour les dossiers git affiche maintenant deux nouvelle actions pour « git log » et « git merge ».

Les améliorations supplémentaires incluses sont :

- Un nouveau raccourci a été défini vous donnant une option pour l'ouverture de la barre de filtrage, simplement par un appui de la touche de barre oblique « / ».
- Vous pouvez maintenant trier et organiser les photos avec la date à laquelle elles ont été prises.
- Le glisser et déposer de nombreux petits fichier avec Dolphin est devenu plus rapide et les utilisateurs peuvent maintenant annuler les tâches de renommage de fichiers par lot.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

Pour rendre le travail avec la ligne de commandes encore plus agréable, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'application d'émulation de terminal de KDE, reçoit une apparence plus agréable :

- Vous pouvez télécharger les thèmes de couleurs grâce à KNewStuff.
- La barre de défilement prend mieux en charge le thème actif de couleurs.
- Par défaut, la barre des onglets est affiché uniquement lorsque nécessaire.

Les contributeurs de Konsole ne se sont pas arrêtés là et ont proposé de nombreuses nouvelles fonctionnalités :

- Un nouveau mode « lecture seule » et une propriété de profil pour basculer une copie de texte en « HTML » ont été ajoutés.
- Sous Wayland, Konsole prend maintenant en charge le menu de glisser / déposer.
- Plusieurs améliorations ont eu lieu en ce qui concerne le protocole ZMODEM : Konsole peut maintenant gérer l'indicateur de téléchargement zmodem B01, il affichera la progression du transfert de données, le bouton Annuler de la boîte de dialogue fonctionne maintenant comme il le devrait, et le transfert de fichiers plus volumineux est mieux pris en charge en les lisant en mémoire par tranches de 1 Mo.

Les améliorations supplémentaires incluses sont :

- Le défilement avec la molette de souris avec « libinput » a été corrigé et le défilement cyclique parmi l'historique du shell lors du défilement avec la molette de souris a été maintenant bloqué.
- Les recherches sont rafraîchies après modification de l'option pour l'expression régulière de correspondance de recherche et lors de l'appui de CTRL + Retour arrière, Konsole se comportera comme xterm.
- Le raccourci « --background-mode » a été corrigé.

### Multimédia

<a href='https://juk.kde.org/'>JuK</a>, le lecteur de musique de KDE, bénéficie désormais du support de Wayland. Les nouvelles fonctionnalités de l'interface utilisateur incluent la possibilité de masquer la barre de menu et d'avoir une indication visuelle de la piste en cours de lecture. Bien que l'ancrage dans la barre d'état système soit désactivé, JuK ne plantera plus lors de la tentative de quitter via l'icône de fermeture de la fenêtre et l'interface utilisateur restera visible. Des bogues concernant la lecture automatique inattendue de JuK lors de la reprise de la veille dans Plasma 5 et la gestion de la colonne de la liste de lecture ont également été corrigés.

Pour la version 18.05, les contributeurs de <a href='https://kdenlive.org/'>Kdenlive</a>, l'éditeur vidéo non linéaire de KDE se sont focalisés sur la maintenance :

- Le redimensionnement d'un clip vidéo ne corrompt plus des atténuations et les trames clé.
- Les utilisateurs du dimensionnement d'affichage pour des écrans à haute densité peuvent se réjouir d'icônes plus nettes.
- Un plantage possible au démarrage avec certaines configuration a été corrigé.
- MLT est maintenant nécessaire en version 6.6.0 ou plus récente et la compatibilité est améliorée.

#### Graphiques

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Durant les derniers mois, les contributeurs de <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, l'afficheur et l'organisateur d'images de KDE ont travaillé sur une pléthore d'améliorations. Les principales en sont :

- La prise en charge des contrôleurs MPRIS a été ajoutée, de sorte que vous pouvez désormais contrôler les diaporamas en plein écran via KDE Connect, les touches médias de votre clavier et le plasmoïde de Media Player.
- Les boutons de survol des vignettes peuvent maintenant être masqués.
- L'outil de recadrage a reçu plusieurs améliorations : ses paramètres sont désormais mémorisés lors du passage à une autre image, la forme de la boîte de sélection peut désormais être verrouillée en maintenant les touches « MAJ » ou « CTRL » enfoncées et elle peut également être verrouillée sur le rapport d'aspect de l'image actuellement affichée.
- En mode plein écran, vous pouvez maintenant quitter en utilisant la touche « Échap » et la palette de couleurs reflétera votre thème de couleurs actif. Si vous quittez Gwenview dans ce mode, ce paramètre sera mémorisé et la nouvelle session démarrera également en plein écran.

L'attention portée aux détails est importante. Ainsi, Gwenview a été amélioré sur les aspects suivants :

- Gwenview affiche désormais des chemins de fichiers plus lisibles dans la liste des " Dossiers récents ", il affiche le menu contextuel approprié pour les éléments de la liste des " Fichiers récents ", et oublie correctement tout ce qui a été fait lors de l'utilisation de la fonction "Désactiver l'historique".
- Cliquer sur un dossier dans la barre latérale permet de basculer entre les modes "Parcourir" et "Afficher" et mémorise le dernier mode utilisé lors du passage d'un dossier à l'autre, ce qui permet une navigation plus rapide dans les grandes collections d'images.
- La navigation au clavier a été plus rationalisée avec une présentation plus claire du focus clavier en mode de navigation.
- La fonctionnalité « Ajuste à la largeur » a été remplacée par une fonction plus globale « Remplir ».

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Même les petites améliorations peuvent souvent rendre les processus de travail des utilisateurs plus agréables :

- Pour améliorer la cohérence, les images « svg » sont maintenant élargies comme les autres images lorsque l'option « Affichage en images / Agrandir les petites images » est activée.
- Après une modification d'image ou une annulation de modifications, l'affichage en images et en vignettes ne seront plus en cohérence.
- Lors du renommage d'images, l'extension de nom de fichier ne sera pas sélectionnée par défaut et la boîte de dialogue « Déplacer / Copier / Lier » affiche par défaut le dossier courant.
- De nombreux problèmes visuels ont été corrigés, par exemple dans la barre d'URL, dans la barre d'outils plein écran et pour les animations de l'infobulle des vignettes. Des icônes manquantes ont également été ajoutées.
- Enfin, le bouton de survol en plein écran permet de visualiser directement l'image au lieu d'afficher le contenu du dossier, et les paramètres avancés permettent désormais de mieux contrôler l'intention de rendu des couleurs ICC.

#### Bureau

Dans <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, le visualiseur universel de documents de KDE, le rendu de PDF et l'extraction de texte peuvent désormais être annulés si vous avez la version 0.63 ou supérieure de poppler, ce qui signifie que si vous avez un fichier PDF complexe et que vous modifiez le zoom pendant le rendu, il s'annulera immédiatement au lieu d'attendre la fin du rendu.

Vous allez pouvoir utiliser une prise en charge améliorée du JavaScript PDF pour AFSimple_Calculate, et si vous disposez de la version 0.64 ou supérieure de poppler, Okular prendra en charge les modifications du JavaScript PDF pour les formulaires en mode lecture seule 

La gestion de confirmation de réservation par courriels de <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, le puissant client de messagerie de KDE, a été considérablement améliorée et sait maintenant prendre en charge les réservations de trains. Elle utilise également une base de données d'aéroport basée sur Wikidata pour afficher les vols incluant les informations correctes de fuseaux horaires. Pour vous faciliter la tâche, un nouvel extracteur a été mis en place pour les courriels ne contenant pas de données de réservation structurées.

Les améliorations supplémentaires incluses sont :

- La structure du message peut de nouveau être affichée avec le nouveau module externe « Expert ».
- Un module externe a été ajouté dans l'éditeur Sieve pour la sélection de courriels à partir de la base de données de Akonadi.
- La recherche de texte dans l'éditeur a été améliorée avec la prise en charge des expressions rationnelles.

#### Utilitaires

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

L'amélioration de l'interface utilisateur de <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, l'outil polyvalent de copie d'écran de KDE, a été un point de grande attention :

- La rangée inférieure de boutons a été remaniée et affiche désormais un bouton permettant d'ouvrir la fenêtre des paramètres et un nouveau bouton « Outils » offrant la possibilité d'ouvrir le dernier dossier de copie d'écran utilisé et de lancer un programme de copie d'écran.
- Le mode d'enregistrement selon le dernier utilisé est maintenant mémorisé par défaut.
- La taille de la fenêtre s'adapte maintenant à l'aspect de ration de la copie d'écran, proposant une vignette de copie d'écran plus agréable et plus synthétique.
- La fenêtre « Configuration » a été largement simplifiée.

De plus, les utilisateurs seront capables de simplifier leurs processus de travail avec ces nouvelles fonctionnalités :

- Quand une fenêtre spécifique a été capturée, son titre peut être automatiquement ajouté au nom pour le fichier de copie d'écran.
- L'utilisateur peut maintenant choisir si Spectacle doit ou non quitter automatiquement après toute opération d'enregistrement ou de copie.

Les corrections importantes de bogues concernent :

- Le glisser / déposer vers les fenêtres de Chromium fonctionne maintenant comme attendu.
- L'utilisation répétée des raccourcis de clavier pour l'enregistrement de copies d'écran ne conduit plus à une boîte de dialogue d'alarme de raccourci.
- Pour les copies d'écran avec zone rectangulaire, le bord du bas de la sélection peut être ajusté avec plus de précision.
- Meilleure fiabilité pour la capture des bords de fenêtres pour les écrans tactiles lors que le compositeur est désactivé.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

Avec <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, le gestionnaire de certificats et l'interface graphique cryptographique universelle de KDE, les clés Curve 25519 EdDSA peuvent être générées lorsqu'elles sont utilisées avec une version récente de GnuPG. Une vue 'Notepad' a été ajoutée pour les actions cryptographiques basées sur le texte et vous pouvez maintenant signer/chiffrer et déchiffrer/vérifier directement dans l'application. Sous la vue "Détails du certificat", vous trouverez maintenant une action d'exportation, que vous pouvez utiliser pour exporter en tant que texte à copier et coller. De plus, vous pouvez importer le résultat via la nouvelle vue "Bloc-notes".

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> est un utilitaire graphique de compression / décompression de fichiers prenant en charge de multiples formats. Ark peut maintenant arrêter les compressions ou les extractions durant l'utilisation du moteur « libzip » pour les archives « ZIP ».

### Applications ayant rejoint le planning de publication des applications de KDE

L'enregistreur de webcam de KDE <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> et le programme de sauvegarde <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> suivront désormais les versions des applications. La messagerie instantanée <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> est également réintroduite après avoir été portée sur KDE Frameworks 5.

### Passage des applications vers leurs propres calendriers de publication

L'éditeur hexadécimal <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> aura son propre planning de publication, suite à la demande de son mainteneur.

### Éradication des bogues

Plus de 170 corrections de bogues ont été apportées dans les applications dont la suite Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello et bien d'autres !

### Journal complet des changements
