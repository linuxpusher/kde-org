---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE publie les applications de KDE 16.04.3
layout: application
title: KDE publie les applications de KDE 16.04.3
version: 16.04.3
---
12 Juillet 2016. Aujourd'hui, KDE a publié la troisième mise à jour de consolidation pour les <a href='../16.04.0'>applications 16.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à Ark, Cantor, Kate, Kdepim, Umbrello et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.22 de KDE.
