---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE publie les applications de KDE en version 17.08.3
layout: application
title: KDE publie les applications de KDE en version 17.08.3
version: 17.08.3
---
09 Novembre 2017. Aujourd'hui, KDE a publié la troisième mise à jour de stabilisation des <a href='../17.08.0'>applications 17.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction. Il s'agit d'une mise à jour sûre et bénéfique pour tout le monde.

Plus d'une douzaine de corrections de bogues apportent des améliorations à Kontact, Ark, Gwenview, KGpg, KWave, Okular, Spectacle et bien d'autres.

Cette version intègre aussi la dernière version de la plate-forme de développement 4.14.38 de KDE.

Les améliorations incluses sont :

- Contournement d'une régression « Samba » 4.7 avec les partages « SMB » protégés par un mot de passe.
- Okular ne se plante plus après certaines tâches de rotation.
- Ark préserve les dates de modifications de fichiers lors de l'extraction d'archives « zip »
