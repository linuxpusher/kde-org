---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE publie les applications de KDE en version 16.12.3
layout: application
title: KDE publie les applications de KDE en version 16.12.3
version: 16.12.3
---
09 Mars 2017. Aujourd'hui, KDE publie la troisième mise à jour de stabilisation de <a href='../16.12.0'>des applications 16.12 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction. Il s'agit d'une mise à jour sûre et bénéfique pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à kdepim, ark, filelight, gwenview, kate, kdenlive, okular et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.30 de KDE.
