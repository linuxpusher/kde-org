---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE publie les applications de KDE en version 16.12.0
layout: application
title: KDE publie les applications de KDE en version 16.12.0
version: 16.12.0
---
Le 15 décembre 2016 . Aujourd'hui, KDE présente KDE Applications 16.12. Cette nouvelle version apporte des mises à niveau améliorant l'accessibilité, introduit de nouvelles fonctionnalités facilitant l'utilisation de KDE et supprime quelques problèmes mineurs.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a> et bien d'autres encore (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) ont maintenant été portés sur KDE Frameworks 5. Nous attendons avec impatience vos commentaires et votre retour sur les nouvelles fonctionnalités introduites dans cette version.

Dans le cadre de notre effort continuel pour rendre les applications plus faciles à construire de manière autonome, nous avons divisé les tarballs kde-baseapps, kdepim et kdewebdev. Vous pouvez trouver les tarballs nouvellement créés à <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>the Release Notes document</a>.

Les paquets suivants ont été déclarés comme obsolètes : kdgantt2, gpgmepp and kuser. Ceci permettra de se focaliser sur le reste du code.

### L'éditeur de son « Kwave » rejoint les applications de KDE !

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> est un éditeur de sons, il peut enregistrer, lire, importer et modifier un grand nombre de fichiers audio, y compris des fichiers multicanaux. Kwave inclut des plugins pour transformer les fichiers audio en utilisant des outils dédiés et présente une vue graphique possédant un zoom complet et la fonction de défilement.

### Le Monde comme votre fond d'écran

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble comprend désormais un fond d'écran et un widget pour Plasma qui affichent l'heure au dessus de la vue satellite de la terre, ceci avec un affichage jour/nuit en temps réel. Précédemment disponibles sur Plasma 4 ; ils ont été mis à jour pour fonctionner sur Plasma 5.

Vous pouvez trouver plus d'informations sur le <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>blog de Friedrich W. H. Kossebau</a>.

### Profusion d'émoticônes !

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

KCharSelect a obtenu la capacité d'afficher le bloc « Unicode » des émoticônes (et d'autres blocs de symboles « SMP »).

Il a aussi hérité d'un menu « Signets ». Ainsi, vous pouvez déclarer en favoris tous vos caractères préférés.

### Math est bien meilleur avec Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor possède un nouveau moteur pour Julia, mettant à disposition de ses utilisateurs, les dernières avancées dans le calcul scientifique.

Vous pouvez trouver plus d'informations sur le <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>blog de Ivan Lakhtanov</a>.

### Archivage avancé

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark se dote de nombreuses nouvelles fonctionnalités :

- Les fichiers et les dossiers peuvent maintenant être renommés, copiés ou déplacés à l'intérieur de l'archive.
- Il est maintenant possible de sélectionner les algorithmes de compression et de déchiffrement lors de la création d'archives.
- Ark peut maintenant ouvrir les fichiers « AR » (Par exemple, Linux \*.a bibliothèques statiques)

Vous pouvez trouver plus d'informations sur le <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>blog de Ragnar Thomsen</a>.

### Et encore plus !

Kopete a reçu la prise en charge de l'authentification « X-OAUTH2 SASL » dans le protocole « Jabber » et a corrigé certains problèmes avec le module externe de chiffrement « OTR ».

Kdenlive dispose d'un nouvel effet Rotoscoping, d'un support pour le contenu téléchargeable et d'un Motion Tracker mis à jour. Il fournit également des fichiers <a href='https://kdenlive.org/download/'>Snap et AppImage</a> pour une installation plus facile.

KMail et Akregator peuvent utiliser la navigation privée de Google pour vérifier si un lien à cliquer est malicieux. Les deux ont reçu l'ajout de la prise en charge de l'impression recto / verso (requière la version Qt 5.8).

### Contrôle agressif de nuisance

Plus de 130 bogues ont été corrigés dans les applications, y compris Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark, Kdenlive et bien plus !

### Journal complet des changements
