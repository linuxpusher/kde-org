---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE publie les applications de KDE en version 18.08.1
layout: application
title: KDE publie les applications de KDE en version 18.08.1
version: 18.08.1
---
06 Septembre 2018. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../18.08.0'>applications 18.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus d'une douzaine de corrections de bogues apportent des améliorations à Kontact, Cantor, Gwenview, Okular, Umbrello et bien d'autres.

Les améliorations incluses sont :

- Le composant « KIO-MTP » ne se plante plus quand le périphérique est en cours d'accès par une autre application.
- L'envoi de courriels dans KMail utilise un mot de passe lorsque spécifié grâce à la demande de mot de passe.
- Okular se souvient maintenant du mode de la barre latérale après l'enregistrement des documents « PDF ».
