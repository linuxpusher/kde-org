---
date: '2013-12-18'
hidden: true
title: Les applications de KDE 4.12 font un grand pas en avant dans la gestion des
  informations personnelles et de nombreuses améliorations.
---
La communauté KDE est fière d'annoncer la nouvelle version majeure des applications KDE proposant de nouvelles fonctionnalités et des corrections. Cette version intègre de très nombreuses améliorations pour la suite KDE PIM, apportant de bien meilleures performances et plusieurs nouvelles fonctionnalités. Kate améliore la productivité pour les développeurs Python et propose une première prise en charge des macros VIM. Les applications pour l'éducation et les jeux bénéficient de nombreuses fonctionnalités variées.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

L'éditeur de texte graphique le plus performant, Kate, a de nouveau reçu des contributions sur le complètement du code, cette fois-ci en introduisant <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>les capacités de mise en correspondance avancée, traitant les abréviations et les mises en correspondances partielles dans les classes</a>. Par exemple, le nouveau code mettrait en correspondance le mot « Qualident » avec « Qualifieldidentifier ». Kate prend également en charge <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>les macros Vim</a>. Le meilleur de tout cela est que ces améliorations sont compatibles avec KDevelop et toutes les autres applications utilisant la technologie Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

L'afficheur Okular prend maintenant en charge <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>les marges des imprimantes</a> et la gestion audio et vidéo pour le format « epub », permet d'effectuer de meilleures recherches et peut maintenant traiter plus de transformations à partir des métadonnées images « Exif ». Dans l'outil de diagramme « UML Umbrello », les associations peuvent maintenant être <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>dessinées avec différentes calques</a> et Umbrello ajoute un signal visuel si le composant graphique est documenté.

L'outil de gestion de données privées KGpg donne plus d'informations aux utilisateurs et KWallet Manager, l'outil pour enregistrer les mots de passe peut maintenant les <a href='http://www.rusu.info/wp/?p=248'>stocker au format GPG</a>, Konsole possède une nouvelle fonctionnalité : « Ctrl » + clic pour lancer directement les URLs à partir de la console. Il peut maintenant aussi <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>lister les processus lorsque l'application se termine</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit offre la possibilité <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>d'adapter automatiquement la taille du contenu à la résolution du bureau</a>. Le gestionnaire de fichiers Dolphin propose de nombreuses améliorations de performance pour le tri et l'affichage des fichiers en réduisant sa consommation de mémoire et en étant plus rapide. KRDC a pris en charge les reconnexions automatiques avec « VNC ». « KDialog » fournit désormais un accès aux messages détaillées d'avertissement et d'erreur pour permettre de faire des scripts encore plus précis. Le module externe « OTR » de Kopete a été mis à jour et le protocole « Jabber » prend désormais en charge la norme « XEP-0264 » pour les vignettes de transfert de fichiers. Au delà de ces fonctionnalités, d'importants travaux ont été effectués pour nettoyer le code et supprimer les avertissements de compilation.

### Jeux et logiciels éducatifs

Les jeux KDE ont été améliorés de nombreuses manières. KReversi est désormais fondé sur <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>QML et Qt Quick</a> afin d'offrir une expérience de jeu plus fluide. KNetWalk a également été <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>converti</a> avec les même bénéfices ainsi que la possibilité de définir des grilles avec une hauteur et une largeur personnalisées. Konquest possède désormais un nouveau joueur IA appelé « Becai ».

Les applications destinées à l'éducation ont connu quelques changements majeurs. KTouch<a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduit une prise en charge personnalisée des leçons et de nombreux nouveaux cours</a>; KStars utilise un nouveau <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>module d'alignement pour telescopes</a> plus précis. Vous trouverez une vidéo sur YouTube de ses nouvelles fonctions <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'> ici </a>. Cantor qui offrait une interface puissante et accessible pour une variété de modules mathématiques possède maintenant des moteurs <a href='http://blog.filipesaraiva.info/?p=1171'>pour Python2 et Scilab</a>. Apprenez-en plus à propos du puissant module Scilab <a href='http://blog.filipesaraiva.info/?p=1159'> ici </a>. Marble ajoute l'intégration à « OwnCloud » (les configurations sont accessibles dans les préférences) et ajoute le support au rendu superposé. KAlgebra rend possible l'exportation au format « PDF » de tracés 3D, permettant une manière plaisante de partager votre travail. Et finalement bien que ce soit tout aussi important, plusieurs bogues ont été corrigés dans les différentes applications de KDE orientées vers l'éducation.

### Courrier électronique, agenda et informations personnelles

KDE PIM, les applications utilisées pour traiter le courrier électronique, l'agenda et autres informations personnelles a reçu beaucoup d'améliorations.

En commençant par le client de courrier électronique KMail, la prise en charge<a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'> d'AdBlock </a> est maintenant disponible (lorsque le « HTML » est activé) et améliore la prise en charge pour le repérage des arnaques de type "scam" en développant les adresses « URL » abrégées. Un nouvel agent « Akonadi » appelé « FolderArchiveAgent » permet aux utilisateurs d'archiver les courriels lus dans des dossiers spécifiques et l'interface utilisateur de la fonction « Envoyer plus tard » a été épurée. KMail bénéficie aussi d'une prise en charge améliorée du filtre « Sieve ». Le module « Sieve » permet de filtrer le courrier électronique au niveau du serveur et vous pouvez maintenant <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'> créer et modifier les filtres sur les serveurs</a> et <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'> et convertir des filtres « KMail » existants en filtres sur le serveur</a>. La prise en charge du format « mbox » de KMail <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>a aussi été améliorée</a>.

Dans d'autres applications, plusieurs changements rendent le travail plus facile et agréable. Un nouvel outil fait son apparition, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>ContactThemeEditor</a> permettant la création de thèmes « Grantlee » pour KAddressBook, lors de l'affichage des contacts. Le carnet d'adresses permet maintenant afficher des aperçus avant impression. KNotes a profité de beaucoup <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>de travail pour la correction de bogues</a>. L'outil Blogilo pour les blogs peut maintenant prendre en charge les traductions et il y a une grande quantité de correctifs et d'améliorations dans l'ensemble des applications « PIM » de KDE.

Profitant à toutes les applications, beaucoup de travail a été effectué au niveau de la performance, de la stabilité et de l'adaptabilité <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'> du cache des données sous-jacent au PIM de KDE</a>, réglant <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>des problèmes de fonctionnement pour « PostgreSQL » sous la dernière version de Qt 4.8.5</a>. Et il y a maintenant un nouvel outil en ligne de commandes, le « calendarjanitor » qui parcourt les données de calendrier à la recherche d'incidences problématiques et ajoute une boîte de dialogue pour les recherches de bogues. Remerciements très spéciaux à Laurent Montel pour le travail qu'il effectue sur les éléments PIM de KDE !

#### Installation des applications de KDE

Les logiciels de KDE, y compris toutes ses bibliothèques et ses applications, sont disponibles gratuitement sous des licences « Open Source ». Ils fonctionnent sur diverses configurations matérielles, sur des architectures processeurs comme « ARM » ou « x86 » et sur des différents systèmes d'exploitation. Ils utilisent tout type de gestionnaire de fenêtres ou environnements de bureaux. A côté des systèmes d'exploitation Linux ou reposant sur UNIX, vous pouvez trouver des versions « Microsoft Windows » de la plupart des applications de KDE sur le site <a href='http://windows.kde.org'>Logiciels KDE sous Windows</a> et des versions « Apple Mac OS X » sur le site <a href='http://mac.kde.org/'>Logiciels KDE sous Mac OS X</a>. Des versions expérimentales des applications de KDE peuvent être trouvées sur Internet pour diverses plate-formes mobiles comme « MeeGo », « MS Windows Mobile » et « Symbian » mais qui sont actuellement non prises en charge. <a href='http://plasma-active.org'>Plasma Active</a> est une interface utilisateur pour une large variété de périphériques comme des tablettes et d'autres matériels mobiles. <br />

Les logiciels KDE peuvent être obtenus sous forme de code source et de nombreux formats binaires à l'adresse <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a>. Ils peuvent être aussi obtenus sur <a href='/download'>CD-ROM</a> ou avec n'importe quelle distribution <a href='/distributions'>majeure de systèmes GNU / Linux et UNIX</a> publiée à ce jour.

##### Paquets

Quelques fournisseurs de systèmes Linux / Unix mettent à disposition gracieusement des paquets binaires de 4.12.0 pour certaines versions de leurs distributions. Dans les autres cas, des bénévoles de la communauté le font aussi.

##### Emplacements des paquets

Pour obtenir une liste courante des paquets binaires disponibles, connus par l'équipe de publication de KDE, veuillez visiter le <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Wiki de la communauté</a>.

Le code source complet pour 4.12.0 peut être <a href='/info/4/4.12.0'>librement téléchargé</a>. Les instructions pour la compilation et l'installation des logiciels KDE 4.12.0 sont disponibles à partir de la <a href='/info/4/4.12.0#binary'>Page d'informations 4.12.0</a>.

#### Configuration minimale du système

Pour bénéficier au maximum de ces nouvelles versions, l'utilisation d'une version récente de Qt est recommandée, comme la version 4.8.4. Elle est nécessaire pour garantir un fonctionnement stable et performant, puisque certaines améliorations apportées aux logiciels de KDE ont été réalisées dans les bibliothèques Qt sous-jacentes.

Pour bénéficier au maximum des capacités des logiciels KDE, l'utilisation des tout derniers pilotes graphiques pour votre système est recommandée, car ceux-ci peuvent grandement améliorer votre expérience d'utilisateur, à la fois dans les fonctionnalités optionnelles que dans les performances et la stabilité en général.
