---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE Ships KDE Applications 15.04.2
layout: application
title: KDE Ships KDE Applications 15.04.2
version: 15.04.2
---

June 2, 2015. Today KDE released the second stability update for <a href='../15.04.0'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 30 recorded bugfixes include improvements to gwenview, kate, kdenlive, kdepim, konsole, marble, kgpg, kig, ktp-call-ui and umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.20, KDE Development Platform 4.14.9 and the Kontact Suite 4.14.9.