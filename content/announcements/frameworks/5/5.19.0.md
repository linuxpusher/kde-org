---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Attica

- Simplify attica plugin look-up and initialization

### Breeze Icons

- Many new icons
- Add missing mimetype icons from oxygen icon set

### Extra CMake Modules

- ECMAddAppIcon: Use absolute path when operating on icons
- Make sure the prefix is looked-up on Android
- Add a FindPoppler module
- Use PATH_SUFFIXES in ecm_find_package_handle_library_components()

### KActivities

- Don't call exec() from QML (bug 357435)
- KActivitiesStats library is now in a separate repository

### KAuth

- Also perform preAuthAction for Backends with AuthorizeFromHelperCapability
- Fix DBus service name of polkit agent

### KCMUtils

- Fix High-DPI issue in KCMUtils

### KCompletion

- KLineEdit::setUrlDropsEnabled method can not be marked as deprecated

### KConfigWidgets

- add a "Complementary" color scheme to kcolorscheme

### KCrash

- Update docs for KCrash::initialize. Application developers are encourage to call it explicitly.

### KDeclarative

- Clean up dependencies for KDeclarative/QuickAddons
- [KWindowSystemProxy] Add setter for showingDesktop
- DropArea: Fix correctly ignoring dragEnter event with preventStealing
- DragArea: Implement grabbing delegate item
- DragDropEvent: Add ignore() function

### KDED

- Revert BlockingQueuedConnection hack, Qt 5.6 will contain a better fix
- Make kded register under aliases specified by the kded modules

### KDELibs 4 Support

- kdelibs4support requires kded (for kdedmodule.desktop)

### KFileMetaData

- Allow querying for a file's origin URL

### KGlobalAccel

- Prevent crash in case dbus is not available

### KDE GUI Addons

- Fix listing of available palettes in color dialog

### KHTML

- Fix detection of icon link type (aka "favicon")

### KI18n

- Reduce use of gettext API

### KImageFormats

- Add kra and ora imageio plugins (read-only)

### KInit

- Ignore viewport on current desktop on init startup information
- Port klauncher to xcb
- Use an xcb for interaction with KStartupInfo

### KIO

- New class FavIconRequestJob in new lib KIOGui, for favicons retrieval
- Fix KDirListerCache crash with two listers for an empty dir in the cache (bug 278431)
- Make Windows implementation of KIO::stat for file:/ protocol error out if the file doesn't exist
- Don't assume that files in read-only dir can't be deleted on Windows
- Fix .pri file for KIOWidgets: it depends on KIOCore, not on itself
- Repair kcookiejar autoload, the values got swapped in 6db255388532a4
- Make kcookiejar accessible under the dbus service name org.kde.kcookiejar5
- kssld: install DBus service file for org.kde.kssld5
- Provide a DBus service file for org.kde.kpasswdserver
- [kio_ftp] fix display of file/directory modification time/date (bug 354597)
- [kio_help] fix garbage sent when serving static files
- [kio_http] Try NTLMv2 authentication if the server denies NTLMv1
- [kio_http] fix porting bugs which broke caching
- [kio_http] Fix NTLMv2 stage 3 response creation
- [kio_http] fix waiting until the cache cleaner listens to the socket
- kio_http_cache_cleaner: don't exit on startup if cache dir doesn't exist yet
- Change DBus name of kio_http_cache_cleaner so it doesn't exit if the kde4 one is running

### KItemModels

- KRecursiveFilterProxyModel::match: Fix crash

### KJobWidgets

- Fix crash in KJob dialogs (bug 346215)

### Package Framework

- Avoid finding the same package multiple times from different paths

### KParts

- PartManager: stop tracking a widget even if it is no longer top level (bug 355711)

### KTextEditor

- Better behaviour for "insert braces around" autobrace feature
- Change option key to enforce new default, Newline at End of File = true
- Remove some suspicious setUpdatesEnabled calls (bug 353088)
- Delay emitting of verticalScrollPositionChanged until all stuff is consistent for folding (bug 342512)
- Patch updating tag substitution (bug 330634)
- Only update the palette once for the change event belonging to qApp (bug 358526)
- Append newlines at EOF by default
- Add NSIS syntax highlighting file

### KWallet Framework

- Duplicate the file descriptor while opening the file to read the env

### KWidgetsAddons

- Fix buddy widgets working with KFontRequester
- KNewPasswordDialog: use KMessageWidget
- Prevent crash-on-exit in KSelectAction::~KSelectAction

### KWindowSystem

- Change licence header from "Library GPL 2 or later" to "Lesser GPL 2.1 or later"
- Fix crash if KWindowSystem::mapViewport is called without a QCoreApplication
- Cache QX11Info::appRootWindow in eventFilter (bug 356479)
- Get rid of QApplication dependency (bug 354811)

### KXMLGUI

- Add option to disable KGlobalAccel at compilation time
- Repair path to app shortcut scheme
- Fix listing of shortcut files (wrong QDir usage)

### NetworkManagerQt

- Re-check connection state and other properties to be sure they are actual (version 2) (bug 352326)

### Oxygen Icons

- Remove broken linked files
- Add app icons from the kde applications
- Add breeze places icons into oxygen
- Sync oxygen mimetype icons with breeze mimetype icons

### Plasma Framework

- Add a property separatorVisible
- More explicit removal from m_appletInterfaces (bug 358551)
- Use complementaryColorScheme from KColorScheme
- AppletQuickItem: Don't try to set initial size bigger than parent size (bug 358200)
- IconItem: Add usesPlasmaTheme property
- Don't load toolbox on types not desktop or panel
- IconItem: Try to load QIcon::fromTheme icons as svg (bug 353358)
- Ignore check if just one part of size is zero in compactRepresentationCheck (bug 358039)
- [Units] Return at least 1ms for durations (bug 357532)
- Add clearActions() to remove every applet interface action
- [plasmaquick/dialog] Don't use KWindowEffects for Notification window type
- Deprecate Applet::loadPlasmoid()
- [PlasmaCore DataModel] Don't reset model when a source is removed
- Fix margin hints in opague panel background SVG
- IconItem: Add animated property
- [Unity] Scale Desktop icon size
- the button is compose-over-borders
- paintedWidth/paintedheight for IconItem

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
