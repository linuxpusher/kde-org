---
qtversion: 5.15.2
date: 2022-07-09
layout: framework
libCount: 83
---


### Breeze Icons

* Create edit-image icon that's a symlink to tool_imageeffects
* Add new okular icon
* Create sidebar-show-symbolic symlink (bug 455247)
* applets/256: Add dark version of analogclock

### Extra CMake Modules

* ECMDeprecationSettings: enable warnings by default
* ECMAddQch: let doxygen update the config file instead of "outdated" warnings
* Fix SHOW_DEPRECATIONS option not getting respected when function is called
* Change -Wundef warning to an error

### KCalendarCore

* Don't drop custom properties with non-text value types
* Add calendar loading state property
* Fix UTC offset timezone loading with latest Qt 5.15.x
* Change the xCalFormat classes to use a hierarchical dptr

### KConfigWidgets

* Add edit-clear-list icon to the Clear List action

### KCoreAddons

* xdg drag and drop portal support
* Install KMemoryInfo headers

### KDeclarative

* kquickcontrols: give main button a description
* kquickcontrols: give Clear button a tooltip
* qmlcontrols: make `isPlatformX11` and `isPlatformWayland` constant
* qmlcontrols: add `isPlatformX11` and `isPlatformWayland` in `KWindowSystemProxy`
* qmlcontrols: add `Pad` fill mode in `QImageItem` (bug 389623)
* [KeySequenceItem] Replace attached object ToolTip with an instance

### KDED

* Shorten KSycoca update delay

### KDocTools

* catalog: Avoid needlessly copying a list to be returned
* Allow build with nix package manager

### KGlobalAccel

* Launch app in terminal when Terminal=true (bug 455117)
* Fix D-Bus de/marshalling KGlobalAccel::MatchType (bug 454704)

### KHolidays #

* Fix reporting invalid sun event times
* Update Indonesian holidays

### KI18n

* Accept null strings from QML

### KImageFormats

* PSD header checks according to specifications
* Improved detection of alpha channel on CMYK images
* Fix Alpha + testcase images
* Basic support to CMYK 8/16 bits (not fully tested)
* jxl: support both old 0.6.1 and new 0.7.0 libjxl API
* avif: read performance improvements

### KIO

* Rename ioslave_defaults to ioworker_defaults
* Don't treat KJob::UserDefinedError as unknown error (bug 456055)
* move global enum to worker nomenclature
* [webrunner] Add debian as new keyword for webrunner
* [knewfilemenu] Don't forcibly change file extension (bug 456091)
* [knewfilemenu] Write Name when creating Link files
* [knewfilemenu] Always add .desktop suffix when creating link file
* Pass JobUiDelegate's window to created dialogs
* Deprecated global file class code in KFileWidget
* KRecentDirs: Deprecate reading/writing to global file
* [kfileplacesmodel] Cache device display name
* PreviewJob: Add note about plugins being cached internally
* KRecentDocuments: Improve indentation in recentlyused.xbel (bug 456046)
* Add template for empty file (bug 297003)
* We need it when we build with strict compile
* Don't leak the slave's worker thread
* filewidgets: update location text after selected files are renamed (bug 455327)
* Add "Get more Apps in Discover" button to kopenwithdialog
* New worker API
* KPropertiesDialog: fix saving changes when editing a .desktop file symlink (bug 450727)

### Kirigami

* Fix and improve template for KAppTemplate
* NavigationTabBar: Add recolorIcons property
* Make JavaScript cleaner and stricter
* BannerImage: Fix `empty` property definition
* PagePoolAction: Fix typeof check
* SwipeListItem: warn users when they try to override the padding (bug 453959)
* template: add method to restore window geometry
* OverlaySheet: Check if isMobile to set footer extra margin
* OverlayDrawer: Make sure the handle is gone when it's hidden (bug 450902)
* Fix checkable labels
* ActionTextField: Fix RTL layout and actions rows
* ActionTextField: Bump QML imports
* PasswordField: Sync code to PlasmaExtras
* Dont't change title color when page is not active
* Fix race: Object (m_item) is operated on before it is assigned
* Use creationContext of the Component as the starting point
* Sync changes from PlasmaExtras PasswordField to disable Ctrl-Z
* Fix link to theme doc

### KNewStuff

* KNewStuffWidgets: generate QCH docs

### KService

* add aliasfor custom property

### KTextEditor

* AppCommands: Do not quit application when last document or all documents are closed (bug 454924)
* Fix leak of KateCompletionWidget::m_argumentHintTree
* Allow to disable KAuth
* KateThemeConfig: when copying a scheme, put the current name in the line-edit
* Utilize ECMDeprecationSettings to manage deprecate Qt API
* avoid that saveAs uses filename as directory (bug 454648)
* allow to disable the autoreload if content is in git
* Fix occurence highlighting not working with custom line height

### KUnitConversion

* Don't add empty string keys in the units map

### KWayland

* Implement PlasmaWindowModel::itemData to expose all the roles

### KWidgetsAddons

* avoid margin if we have no text set

### KWindowSystem

* Make enum Q_ENUM
* Implement _GTK_SHOW_WINDOW_MENU (bug 454756)

### KXMLGUI

* Fix QWhatsThis links not being clickable
* Allow to load rc file in kf6

### Plasma Framework

* FrameItemNode: Remove unnecessary attribute
* Expose the edit mode to the applets
* Units: Fix sizeForLabels double-scaling icons with Plasma scaling (bug 454131)
* Update plugins.qmltypes for `org.kde.plasma.core`
* Skip test for old style theme metadata format when building without deprecations
* Revert "Use QT_FEATURE_foo to detect opengl* support, and TARGET for glesv2"
* wallpaperinterface: call `setSource` before `setContextProperty`
* Add back the WindowsGoBelow panel behavior (bug 455138)
* breeze/widgets: Make analog clock follow colorscheme (bug 377935)

### Purpose

* [plugins/email] Port to KEMailClientLauncherJob

### QQC2StyleBridge

* iterate only over visible children in menu
* Avoid a crash with kscreenlocker_greet or any other QGuiApplication
* Make StackView obey the global animation speed setting (bug 395324)

### Solid

* avoid segfault if qobject_cast fails
* upower: Allow displaying Apple Magic Trackpad charge level
* skip non-FileSystem StorageVolumes in storageAccessFromPath

### Syntax Highlighting

* Handle case when heredoc closing identifier is indented with space or tabulation (bug 353162)
* cmake.xml: Updates for CMake 3.24
* Add OrderWithRequires and RemovePathPostfixes keywords to RPM Spec
* Adjust repo's own includes (bug 453759)

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
