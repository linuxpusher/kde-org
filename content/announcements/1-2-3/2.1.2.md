---
aliases:
- ../announce-2.1.2
custom_about: true
custom_contact: true
date: '2001-04-30'
description: The KDE Project today announced the release of kdelibs 2.1.2, a security
  and bugfix release of the core KDE libraries. The other core KDE packages, including
  kdebase, have not been updated.
title: KDE 2.1.2 Release Announcement
---

FOR IMMEDIATE RELEASE

<H3 ALIGN="center">SECURITY: New KDE Libraries Released</H3>

KDE Adds Security and Bug Fixes to Core Libraries

The <a href="/">KDE
Project</a> today announced the release of kdelibs 2.1.2,
a security and bugfix release of the core KDE libraries. The other
core KDE packages, including kdebase, have not been updated. The KDE Project
recommends that all KDE users upgrade to kdelibs 2.1.2 and KDE 2.1.1.

This release provides the following fixes:

<ul>
<li><em>Security fixes</em>:</li>
<ul>
<li><em>KDEsu</em>.  The KDEsu which shipped with earlier releases of KDE 2
writes a (very) temporary but world-readable file with authentication
information.  A local user can potentially abuse this behavior to gain
access to the X server and, if KDEsu is used to perform tasks that require
root-access, can result in comprimise of the root account.</li>
</ul>
<li><em>Bug fixes</em>:</li>
<ul>
<li><em>kio_http</em>.  Fixed problems with "protocol for http://x.y.z died unexpectedly" and with proxy authentication with Konqueror.</li>
<li><em>kparts</em>.  Fixed crash in KOffice 1.1 when splitting views.</li>
<li><em>khtml</em>.  Fixed memory leak in Konqueror.  Fixed minor HTML
rendering problems.</li>
<li><em>kcookiejar</em>.  Fixed minor problems with HTTP cookies.</li>
<li><em>kconfig</em>.  Fixed problem with leading/trailing spaces in
configuration values.</li>
<li><em>kdebug</em>.  Fixed memory leak in debug output.</li>
<li><em>klineedit</em>.   Fixed problem with klineedit emitting "return
pressed" twice.</li>
</ul>
</ul>

For more information about the KDE 2.1 series, please see the
<a href="/announcements/announce-2.1.1">KDE 2.1.1
press release</a> and the <a href="/info/1-2-3/2.1.1">KDE
2.1.1 Info Page</a>, which is an evolving FAQ about the latest stable release.
Information on using anti-aliased fonts with KDE is available
<a href="http://dot.kde.org/984693709/">here</a>.

#### Downloading and Compiling kdelibs 2.1.2

The source package for kdelibs 2.1.2 (including a diff file against 2.1.1) is
available for free download at
<a href="http://ftp.kde.org/stable/2.1.2/distribution/src/">http://ftp.kde.org/stable/2.1.2/distribution/src/</a>
or in the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. KDE 2.1.2 requires
qt-2.2.3, which is available from
<a href="http://www.trolltech.com/">Trolltech</a> at
<a href="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</a>
under the name <a href="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.3.tar.gz">qt-x11-2.2.3.tar.gz</a>,
although
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.4.tar.gz">qt-2.2.4</a>or
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</a>is recommended (for anti-aliased fonts,
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</a>and <a href="ftp://ftp.xfree86.org/pub/XFree86/4.0.3/">XFree 4.0.3</a> or
newer is required).
kdelibs 2.1.2 will not work with versions of Qt older than 2.2.3.

For further instructions on compiling and installing KDE, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>.

#### Installing Binary Packages

Some distributors choose to provide binary packages of KDE for certain
versions of their distribution. Some of these binary packages for
kdelibs 2.1.2 will be available for free download under
<a href="http://ftp.kde.org/stable/2.1.2/distribution/">http://ftp.kde.org/stable/2.1.2/distribution/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you have any questions, please read the
<a href="http://dot.kde.org/986933826/">KDE Binary Packages Policy</a>).

kdelibs 2.1.2 requires qt-2.2.3, the free version of which is available
from the above locations usually under the name qt-x11-2.2.3, although
qt-2.2.4 or qt-2.3.0 is recommended (for anti-aliased fonts,
qt-2.3.0 and XFree 4.0.3 or newer is required).
KDE 2.1.2 will not work with versions of Qt older than 2.2.3.

At the time of this release, pre-compiled packages are available for:

<ul>
<li><a href="http://www.caldera.com/">Caldera</a> eDesktop 2.4: <a href="http://ftp.kde.org/stable/2.1.2/distribution/Caldera/eDesktop-2.4/">i386</a></li>

<li><a href="http://www.redhat.com/">RedHat Linux</a>:  7.1:  <a href="http://ftp.kde.org/stable/2.1.2/distribution/RedHat/7.1/i386/">i386</a></li>

<li><a href="http://www.suse.com/">SuSE Linux</a> (<a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/README">README</a>):
<ul>
<li>7.1:  <a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/7.1/">i386</a>, <a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/sparc/7.1/">Sparc</a> and <a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/ppc/7.1/">PPC</a></li>
 
<li>7.0:  <a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/7.0/">i386</a>, <a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/ppc/7.0/">PPC</a>, and <a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/s390/">S390</a></li>
<li>6.4:  <a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/6.4/">i386</a></li>
<li>6.3:  <a href="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/6.3/">i386</a></li>
</li>
</ul>
<li>Tru64 Systems:  <a href="http://ftp.kde.org/stable/2.1.2/distribution/Tru64/">4.0e,f,g, or 5.x</a> (<a href="http://ftp.kde.org/stable/2.1.2/distribution/Tru64/README.Tru64">README</a>)</li>

</ul>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages may become available over the
coming days and weeks.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.

KDE and all its components are available for free under
Open Source licenses from the KDE <a href="http://ftp.kde.org/">server</a>
and its <a href="http://www.kde.org/mirrors.html">mirrors</a> and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.
As a result of the dedicated efforts of hundreds of translators,
KDE is available in <a href="http://i18n.kde.org/teams/distributed.html">34
languages and dialects</a>. KDE includes the core KDE libraries, the core
desktop environment (including
<a href="http://konqueror.kde.org/">Konqueror</a>), developer packages
(including <a href="http://www.kdevelop.org/">KDevelop</a>), as well as the
over 100 applications from the other standard base KDE packages
(administration, games, graphics, multimedia, network, PIM and utilities).

For more information about KDE, please visit KDE's
<a href="http://www.kde.org/whatiskde/">web site</a>.
More information about KDE 2 is available in two
(<a href="http://devel-home.kde.org/~granroth/LWE2000/index.html">1</a>,
<a href="http://mandrakesoft.com/~david/OSDEM/">2</a>) slideshow
presentations and on
<a href="/">KDE's web site</a>, including an evolving
<a href="/info/1-2-3/2.1">FAQ</a> to answer questions about
migrating to KDE 2.1 from KDE 1.x,
<a href="http://dot.kde.org/984693709/">anti-aliased font tutorials</a>, a
number of
<a href="/screenshots/kde2shots">screenshots</a>, <a href="http://developer.kde.org/documentation/kde2arch.html">developer information</a> and
a developer's
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</a>.

<hr /><font SIZE=2>

_Trademarks Notices._
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
</font>

<hr /><table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](mailto:pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(44) 1225 837409

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>
    
  [konold@kde.org](mailto:konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
</table>
