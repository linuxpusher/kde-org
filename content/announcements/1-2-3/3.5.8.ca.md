---
aliases:
- ../announce-3.5.8
custom_about: true
date: '2007-10-16'
title: Anunci de publicació de KDE 3.5.8
---

<h3 align="center">
   El Projecte KDE publica la vuitena versió que millora les traduccions i els serveis
   de l'escriptori líder del programari lliure.
</h3>

<p align="justify">
  El KDE 3.5.8 incorpora traduccions a 65 idiomes, millores al conjunt de programes 
  de gestió d'informació personal del KDE i a altres aplicacions.
</p>

<p align="justify">
  Avui, el <a href="/">Projecte KDE</a> ha 
  anunciat la disponibilitat immediata del KDE 3.5.8,
  una versió de manteniment de l'última generació de l'escriptori <em>lliure</em> més
  avançat i potent per al GNU/Linux i altres UNIX. El KDE té ara traducció a 
  <a href="http://l10n.kde.org/stats/gui/stable/">65 idiomes</a>,
  la qual cosa el fa disponible per a més persones que la majoria dels
  programes no lliures, i pot ser ampliat fàcilment a altres comunitats que vulguin contribuir al projecte de codi obert.
</p>

<p align="justify">
    Encara que el focus principal dels desenvolupadors es centra en acabar el KDE 4.0, la sèrie estable 3.5 
    és encara l'escriptori que escull la majoria. És estable, provat i ben mantingut.
    La versió 3.5.8 amb el seu centenar de correccions d'errors torna a millorar l'experiència dels usuaris.
    Les principals millores del KDE 3.5.8 són:
        
  <ul>
      <li>
      Millores al Konqueror i el seu component web KHTML. S'han corregit errors en la gestió
      de connexions HTTP i KHTML ha millorat la seva compatibilitat amb algunes
      característiques de CSS per millorar el compliment dels estàndards.
      </li>
      <li>
      Al paquet kdegraphics, hi ha hagut una gran quantitat de correccions en el visor
      de PDF del KDE i a Kolourpaint, una aplicació de dibuix.
      </li>
      <li>
      El conjunt d'aplicacións KDE PIM ha rebut una gran quantitat de millores, des del KMail, client de correu del KDE, al
      KOrganizer, l'aplicació d'organització.
      </li>
  </ul>
</p>

<p align="justify">
  Per a una llista més detallada de millores des de la 
  <a href="/announcements/announce-3.5.7">versió KDE 3.5.7</a>
  del 22 de maig del 2007, si us plau, consulteu la pàgina de canvis del 
  <a href="/announcements/changelogs/changelog3_5_7to3_5_8">KDE 3.5.8</a>.
</p>

<p align="justify">
  El KDE 3.5.8 es distribueix amb un escriptori bàsic i quinze paquets addicionals 
  (gestió d'informació personal, administració, xarxes, educació, utilitats,  multimèdia, jocs, art, 
  desenvolupament web, i altres). Les eines del KDE més 
  premiades i les aplicacions estan disponibles en <strong>65 idiomes</strong>.
</p>

<h4>
  Distribucions amb el KDE
</h4>
<p align="justify">
  La majoria de distribucions de Linux i sistemes operatius UNIX no incorporaran
  immediatament les noves versions de KDE, sinó que integraran els paquets del KDE
  3.5.8 en les seves properes versions. Consulteu
  <a href="/distributions">aquesta llista</a>
  per veure quines distribucions porten el KDE.
</p>

<h4>
  Instal·lació de paquets binaris del KDE 3.5.8
</h4>
<p align="justify">
  <em>Creadors de paquets</em>.
  Alguns proveïdors de sistemes operatius han proporcionat amablement els
  paquets binaris del KDE 3.5.8 per a algunes versions de les seves distribucions,
  en altres casos han estat voluntaris de la comunitat els que ho han fet. 
  Alguns d'aquests paquets es poden descarregar lliurement del servidor de 
  descàrregues del KDE a 
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.8/">download.kde.org</a>.
  En properes setmanes poden haver-hi paquets nous així com millores als paquets
  disponibles. 
</p>

<p align="justify">
  <a id="package_locations"><em>Ubicació dels paquets</em></a>.
  Consulteu la pàgina d'informació del 
  <a href="/info/1-2-3/3.5.8">KDE 3.5.8</a> per obtenir una llista dels paquets 
  binaris disponibles actualment dels que s'ha informat al Projecte KDE.
</p>

<h4>
  Com compilar el KDE 3.5.8
</h4>
<p align="justify">
  <a id="source_code"></a><em>Codi font</em>.
  El codi complet del KDE 3.5.8 es pot
  <a href="http://download.kde.org/stable/3.5.8/src/">descarregar 
  lliurement</a>. Les instruccions de com compilar i instal·lar el KDE 3.5.8
  estan disponibles a la pàgina d'informació del <a href="/info/1-2-3/3.5.8">KDE
  3.5.8</a>.
</p>

<h4>
  Com col·laborar amb el KDE
</h4>
<p align="justify">
  El KDE és un projecte de
  <a href="http://www.gnu.org/philosophy/free-sw.html">programari lliure</a>
  que existeix i creix gràcies a molts voluntaris que donen el seu temps i
  esforç. El KDE sempre busca nous voluntaris i contribucions, ja sigui per
  programar, corregir o informar d'errors, escriure documentació, traduir, 
  ajudar en la promoció, aportar diners, etc. Totes les contribucions s'accepten 
  amb agraïment. Si us plau, per a més informació llegiu <a href="/community/donations/">Com col·laborar amb el KDE</a>.</p>

<p align="justify">
Esperem tindre notícies vostres ben aviat!
</p>

<h4>
  Quant al KDE
</h4>
<p align="justify">
  El KDE és un projecte <a href="/community/awards/">premiat</a> i independent format per
  <a href="/people/">centenars</a> de desenvolupadors, traductors, artistes i
  altres professionals d'arreu del món, col·laborant gràcies a Internet per
  crear un entorn d'escriptori i d'oficina de lliure distribució, sofisticat,
  personalitzable i estable que utilitza una arquitectura basada en components,
  transparent a la xarxa i ofereix una magnífica plataforma de desenvolupament.</p>

<p align="justify">
  El KDE proporciona un escriptori madur i estable que inclou un navegador 
  (<a href="http://konqueror.kde.org/">Konqueror</a>), un conjunt d'utilitats de
  gestió d'informació personal (<a href="http://kontact.org/">Kontact</a>), un
  conjunt d'aplicacions d'oficina complet
  (<a href="http://www.koffice.org/">KOffice</a>), una gran quantitat
  d'aplicacions i utilitats de xarxa, i un eficient i intuïtiu entorn integrat de
  desenvolupament (<a href="http://www.kdevelop.org/">KDevelop</a>).</p>

<p align="justify">
  KDE és la prova real que el model de desenvolupament de programari lliure
  "estil Basar" pot crear tecnologies de primer nivell a la par o superiors que
  el més complex del programari comercial.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Notes en quant a marques registrades.</em>
  KDE<sup>&#174;</sup> i el logo K Desktop Environment<sup>&#174;</sup> són
  marques registrades de KDE e.V.

  Linux és una marca registrada de Linus Torvalds.

  UNIX és una marca registrada de The Open Group als Estats Units i d'altres
  països.

  Totes les altres marques registrades i copyrights esmentats en aquest anuncis
  són propietat dels respectius propietaris.
  </font>
</p>

<hr />
