---
aliases:
- ../announce-3.5.4
custom_about: true
custom_contact: true
date: '2006-08-02'
title: Anúncio de Lançamento do KDE
---

<h3 align="center">
   Projecto KDE Lança Quarta Versão de Serviço e Tradução Do Livre Software Desktop
</h3>

<p align="justify">
  KDE 3.5.4 oferece traduções em 65 idiomas,
  realçando o suporte a dispositivos removíveis
  e melhorias no motor HTML (KHTML).
</p>

<p align="justify">
  O  <a href="/">Projecto KDE</a>
  anuncia hoje a disponibilidade imediata do KDE 3.5.4, uma nova versão de manutenção
  para a última geração do mais avançado e potente livre desktop para GNU/Linux e
  outros UNIXes. O KDE suporta agora 65 idiomas, sendo-o mais acessível às pessoas do
  que a maioria do software proprietário e pode facilmente extender-se de modo a suportar
  outras através de comunidades que desejem contribuir para o projecto open source.
</p>

<p align="justify">
  Melhorias incluídas:
</p>

<ul>
  <li>Suporte de remoção de dispositivos melhorada no Linux (os utilizadores podem agora montar todos os dispositivos suportados pelo <a href="http://www.freedesktop.org/wiki/Software/hal">HAL da FreeDesktop</a> e controlar como este o faz)
  </li>
  <li>Optimizações de velocidade no <a href="http://konsole.kde.org">Konsole</a> e no <a href="http://kate-editor.org">Kate</a></li>
  <li>Múltiplas férias podem agora começar na mesma data no <a href="http://korganizer.kde.org">KOrganizer</a></li>
  <li>Muitas correcções no motor HTML do <a href="http://www.konqueror.org">Konqueror</a>, o KHTML</li>
  <li>O dialogo para enviar certificados SSL por parte do cliente está agora mais usável</li>
  <li>O <a href="http://knetworkconf.sourceforge.net">KNetworkConf</a> suporta agora o Fedora Core 5 e gere melhor chaves WEP</li>
</ul>

<p align="justify">
  Mais de 10 novas funcionalidades foram adicionadas e mais de 100 bugs
  corrigidos. Para uma lista mais detalhada das melhorias desde o KDE 3.5.3
  em Maio 2006, por favor consulte a
  <a href="/announcements/changelogs/changelog3_5_3to3_5_4">lista de alterações do KDE 3.5.4</a>.
</p>

<p align="justify">
  O KDE 3.5.4 vem com um desktop básico e com outros quinze pacotes
  (Gestor de Informação Pessoal, administração, redes, educação,
  utilitários, multimédia, jogos, trabalhos gráficos,
  desenvolvimento web e mais).
</p>

<h4>
  Instalando os Pacotes Binários do KDE 3.5.4
</h4>
<p align="justify">
  <em>Criadores de Pacotes</em>.
  Alguns vendedores de sistemas operativos fornecem gentilmente pacotes
  binários do KDE 3.5.4 para algumas versões das suas distribuições,
  e em outros casos comunidades voluntárias também o fazem. Alguns
  desses pacotes binários estão disponíveis gratuitamente para download
  pelo servidor de downloads do KDE em
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/">download.kde.org</a>.
  Adicionalmente pacotes binários, assim como actualizações para os pacotes
  agora disponíveis, podem vir a estar disponíveis durante as próximas semanas.
</p>

<p align="justify">
  <a id="package_locations"><em>Posições dos Pacotes</em></a>.
  Para uma lista actual dos pacotes binários disponíveis em 
  que o Projecto KDE foi informado, por favor visite a
  <a href="/info/1-2-3/3.5.4">Página de Informações do KDE 3.5.4</a>.
</p>

<h4>
  Compilando o KDE 3.5.4
</h4>
<p align="justify">
  <a id="source_code"></a><em>Código-fonte</em>.
  O código-fonte completo para o KDE 3.5.4 pode ser
  <a href="http://download.kde.org/stable/3.5.4/src/">livremente transferido</a>.
  Instruções de compilação e instalação do KDE 3.5.4 estão disponíveis na
  <a href="/info/1-2-3/3.5.4">Página de Informações do KDE 3.5.4</a>.
</p>

<h4>
  Suportando o KDE
</h4>
<p align="justify">
O KDE é um projecto <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
que existe e cresce apenas devido à ajuda de muitos voluntários que doam o seu tempo e esforço.
O KDE está sempre à procura de novos voluntários e contribuidores, que possam ajudar programando,
corrigindo ou reportando erros, escrevendo documentação, traduzindo, promovendo, doando dinheiro,
etc. Todas as contribuições são apreciadas e ansiosamente aceites. Para mais informações leia por
favor a página <a href="/community/donations/">Suportando o KDE</a>.</p>

<h4>
  Sobre o KDE
</h4>
<p align="justify">
  KDE é um projecto <a href="/community/awards/">premiado</a>, independente com centenas de programadores,
  tradutores, artistas e outros profissionais colaborando pela Internet para criar e distribuir
  gratuitamente um sofisticado, personalizado e estável desktop e ambiente de escritório. Utiliza
  uma arquitectura flexível baseada em componentes, redes transparentes e oferece uma óptima
  plataforma de desenvolvimento.</p>

<p align="justify">
  O KDE oferece um estável e maduro desktop incluindo um magnífico browser
  (<a href="http://konqueror.kde.org/">Konqueror</a>), uma suite de gestor
  de informações pessoais (<a href="http://kontact.org/">Kontact</a>), uma
  completa suite de escritório (<a href="http://www.koffice.org/">KOffice</a>),
  inúmeras aplicações de redes e utilitários e um eficiente e
  intuitivo ambiente IDE de desenvolvimento
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE é a prova real que o modelo de software de desenvolvimento Open Source
  “estilo-Bazaar” pode criar tecnologias com rendimento superior às complexas
  aplicações proprietárias existentes.
</p>

<hr  />

<h4>Contactos de imprensa</h4>
<table cellpadding="10" align="center"><tr valign="top" >
<td>

<b>África</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Telefone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />

</td>

<td>
<b>Asia and India</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Telefone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Telefone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>América do Norte</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Telefone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Telefone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>América do Sul</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Telefone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>
