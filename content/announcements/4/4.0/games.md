---
custom_about: true
custom_contact: true
hidden: true
title: 'KDE 4.0 Visual Guide: Game'
---

<p>
<img src="/announcements/4/4.0/images/games.png" align="right"  hspace="10"/>
The KDE Games community has put in tremendous effort porting and reworking many games
from KDE 3 and also adding several new, exciting games to KDE 4.0.<br />
Highlights in the games include more intuitive gameplay, polished graphics and better
resolution independence. You can run games fullscreen as well as in small windows.<br />

Most of the artwork has been redone, giving the games in KDE 4.0 a very polished and
modern look.

</p>

<h2>KGoldrunner - an arcade game</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kgoldrunner.png">
<img src="/announcements/4/4.0/kgoldrunner_thumb.png" class="img-fluid">
</a> <br/>
<em>Collect gold nuggets in KGoldrunner</em>
</div>
<br/>

<p>
KGoldrunner is a retro arcade game with a puzzle flavor. 
It has hundreds of levels where pieces of gold must be 
collected, with enemies in hot pursuit.
</p>

<h2>KFourInLine - a board game</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kfourinline.png">
<img src="/announcements/4/4.0/kfourinline_thumb.png" class="img-fluid">
</a> <br/>
<em>Connect four pieces in a row in KFourInLine</em>
</div>
<br/>

<p>
KFourInLine is a board game for two players based on the 
Connect-Four game. The players try to build up a row of 
four pieces using different strategies.
</p>

<h2>LSkat - a card game</h2>

<div class="text-center">
<a href="/announcements/4/4.0/lskat.png">
<img src="/announcements/4/4.0/lskat_thumb.png" class="img-fluid">
</a> <br/>
<em>Play Skat</em>
</div>
<br/>

<p>
Lieutenant Skat (from German "Offiziersskat") is a fun and 
engaging card game for two players, where the second player 
is either a live opponent, or a built-in artificial intelligence.
</p>

<h2>KJumpingCube - a dice game</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kjumpingcube.png">
<img src="/announcements/4/4.0/kjumpingcube_thumb.png" class="img-fluid">
</a> <br/>
<em>Play dice with KJumpingCube</em>
</div>
<br/>

<p>
KJumpingcube is a simple dice-driven tactical game. The playing 
area consists of squares containing points. Players move by 
clicking on either a vacant square, or on their own square.
</p>

<h2>KSudoku - a logic game</h2>

<div class="text-center">
<a href="/announcements/4/4.0/ksudoku.png">
<img src="/announcements/4/4.0/ksudoku_thumb.png" class="img-fluid">
</a> <br/>
<em>Exercise your brain with KSudoku</em>
</div>
<br/>

<p>
KSudoku is a logic-based symbol placement puzzle. The player has 
to fill a grid so that each column, row as well as each square 
block on the game field contains only one instance of each symbol.
</p>

<h2>Konquest - a strategy game</h2>

<div class="text-center">
<a href="/announcements/4/4.0/konquest.png">
<img src="/announcements/4/4.0/konquest_thumb.png" class="img-fluid">
</a> <br/>
<em>Konquer other planets in Konquest</em>
</div>
<br/>

<p>
Players conquer other planets by 
sending ships to them. The goal is to build an interstellar empire 
and ultimately conqueror all other player's planets.
</p>

<p>
You can find more information about the above games and many more on 
the newly updated <a href="http://games.kde.org">KDE Games</a> website.
</p>
<table width="100%">
	<tr>
		<td width="50%">
				<a href="../education">
				<img src="/announcements/4/4.0/images/education-32.png" />
				Previous page: Educational Applications
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="../guide">Overview
				<img src="/announcements/4/4.0/images/star-32.png" /></a>
		</td>
	</tr>
</table>