---
title: Delovna okolja Plasma 4.9 – osnovne izboljšave
date: "2012-12-05"
hidden: true
---

<p>
KDE z veseljem najavlja takojšnjo razpoložljivost različice 4.9 delovnih okolij Plasma Desktop in Plasma Netbook. Nova različica je doživela izboljšave obstoječih zmožnosti in prinaša večje nove zmožnosti.
</p>
<h2>Upravljalnik datotek Dolphin</h2>
<p>
KDE-jev zmogljiv upravljalnik datotek Dolphin sedaj premore gumba za pomikanje nazaj in naprej ter preimenovanje imen datotek znotraj vrstice. Dolphin po novem lahko prikazuje tudi metapodatke kot so ocene, oznake, avtorja, dimenzije slik, velikosti datotek in podobno. Po teh metapodatkih je možno tudi razvrščanje in združevanje v skupine. Dodan je bil vstavek za sistem upravljanja z izvorno kodo Mercurial, ki se pridružuje že obstoječi podpori za git, Subversion in CVS. Uporabniški vmesnik programa je doživel nekaj manjših izboljšav, med katerimi so boljše podokno z mesti ter izboljšano iskanje in usklajevanje trenutnega mesta s podoknom terminala.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-dolphin_.png">
	<img src="/announcements/4/4.9.0/kde49-dolphin_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Posnemovalnik terminala Konsole</h2>
<p>
Iz programa Konsole lahko sedaj s pomočjo KDE-jevih spletnih bližnjic poiščete izbrano besedilo. Ko neko mapo povlečete in spustite v okno Konsole, je sedaj v priročnem meniju na voljo nova možnost »Spremeni mapo v«. Uporabniki imate po novem več nadzora pri organiziranju oken terminala, saj lahko <strong>zavihek z vleko odcepite</strong> od okna in tako ustvarite novo okno. Obstoječe zavihke je moč podvojiti, tako da uporabijo isti profil. Pri zagonu Konsole je mogoče nadzorovati vidnost menijske vrstice in vrstice z zavihki. Če vam gre pisanje skriptov dobro od rok, lahko z njimi spreminjate imena zavihkov.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole1.png">
	<img src="/announcements/4/4.9.0/kde49-konsole1-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole2.png">
	<img src="/announcements/4/4.9.0/kde49-konsole2-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Upravljalnik oken KWin</h2>
<p>
Veliko je bilo narejenega na KDE-jevem upravljalniku oken KWin. Med izboljšavami so tako majhne spremembe, kot sta dvigovanje oken med preklapljanjem in pomoč za nastavitve, ki so specifične za posamezno okno; kot tudi večje spremembe, na primer izboljšane nastavitve preklapljanja med okni in pohitritve grafičnih učinkov. KWin je po novem bolje povezan z Dejavnostmi, dodana je bila na primer možnost nastavljanja pravila za prikaz posameznih oken v izbranih dejavnostih. V splošnem je bilo veliko dela posvečenega izboljšanju kakovosti in pohitritvam.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-window-behaviour_settings.png">
	<img src="/announcements/4/4.9.0/kde49-window-behaviour_settings_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Dejavnosti</h2>
<p>
Dejavnosti so sedaj veliko temeljiteje integrirane v delovno okolje. V Dolphinu, Konquerorju in gradniku Prikaz map lahko sedaj datoteke povežete z izbranimi dejavnostmi. Prikaz map lahko tako na primer prikazuje le datoteke, ki so povezane s trenutno dejavnostjo. Nova je možnost šifriranja zasebnih dejavnosti.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-link-files-to-activities.png">
	<img src="/announcements/4/4.9.0/kde49-link-files-to-activities-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<p>
Dodana je bila podpora za protokol MPRIS2, ki omogoča oddaljeno upravljanje večpredstavnostnih predvajalnikov in prikaz podatkov o predvajani vsebini. Z mešalnikom KMix lahko zato sedaj nadzorujete posamezne zvočne tokove in iz različnih gradnikov za namizje, kakršen je »Sedaj se predvaja«, upravljate s predvajalniki. Podpora za MPRIS je bila dodana v predvajalnik zvoka JuK in predvajalnik videa Dragon.
</p>
<p>
V delovnih okoljih je tudi več manjših sprememb, na primer prenos več gradnikov in drugih delov okolij na novo tehnologijo Qt QML. Izboljšani minipredvajalnik za Plasmo vključuje pogovorno okno z lastnostmi skladbe in izboljšano filtriranje. Meni za zaganjanje programov Kickoff je sedaj moč uporabljati tudi s tipkovnico. Gradnik za upravljanje z omrežnimi povezavami je doživel izboljšave prikaza in uporabnosti. Veliko sprememb je doživel tudi gradnik za podatke o javnem prevozu.
</p>

<h4>Namestitev okolij Plasma</h4>

<p align="justify">
KDE-jeva programska oprema, ki vključuje vse knjižnice in programe, je prosto na voljo pod licencami za svobodno in odprtokodno prgramsko opremo. KDE-jeva programska oprema teče na raznih strojnih konfiguracijah in procesorskih arhitekturah kot so MIPS, ARM in x86. Deluje tudi na več operacijskih sistemih in s poljubnim upravljalnikom oken ali v poljubnem namiznem okolju. Poleg inačic za GNU/Linux in ostale operacijske sisteme temelječe na UNIX-u lahko večino programov dobite tudi za Windows (oglejte si stran projekta <a href="http://windows.kde.org/">KDE-jevih programov za Windows</a>) in Mac OS X (<a href="http://mac.kde.org/">KDE-jevi programi za Mac</a>). Na spletu je moč najti tudi razne preizkusne inačice KDE-jevih programov za mobilne platforme kot sta na primer MeeGo in Symbian. Te niso podprte. <a href="http://plasma-active.org/">Plasma Active</a> je uporabniška izkušnja za širši spekter naprav kot so tablični računalniki in druga mobilna strojna oprema.
<br />
KDE-jevo programsko opremo je v obliki izvorne kode in raznih binarnih formatih moč dobiti s strani <a
href="http://download.kde.org/stable/4.9.0/">download.kde.org</a>, na <a href="/download">zgoščenki</a> ali kot del <a href="/distributions">večjih distribucij sistema GNU/Linux ali sistemov UNIX</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketi</em></a>.
  Nekateri ponudniki operacijskega sistema GNU/Linux in sistemov UNIX so dali na razpolago binarne pakete različice 4.9.0 za nekatere različice svojih distribucij. V drugih primeri so to storili prostovoljci. <br />
  Nekateri binarni paketi so za prost prenos na voljo s KDE-jeve strani <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">download.kde.org</a>.
  Dodatni binarni paketi in posodobitve obstoječih bodo na voljo v prihodnjih tednih.
<a id="package_locations"><em>Lokacije paketov</em></a>.
Za trenuten seznam razpoložljivih binarnih paketov, o katerih je bila skupnost KDE obveščena, obiščite <a href="/info/4/4.9.0">4.9to spletno stran</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Celotno izvorno kodo za različico 4.9.0 lahko <a href="/info/4/4.9.0">prosto prenesete</a>.
Navodila za prevajanje izvorne kode in nameščanje KDE-jeve programske opreme različice 4.9.0 so na voljo na
<a href="/info/4/4.9.0#binary">4.9.0tej spletni strani</a>.
</p>

<h4>
Sistemske zahteve
</h4>
<p align="justify">
Da bi ta različica delovala kar najbolje, priporočamo uporabo najnovejše različice knjižnice Qt. To je potrebno za stabilnost in hitrost, saj so bile nekatere izboljšave opravljene na nivoju ogrodja Qt.<br />
Za čimboljše doživetje pri uporabi vseh zmožnosti KDE-jevih programov priporočamo tudi uporabo najnovejših gonilnikov za grafično kartico. Najnovejši gonilniki lahko izboljšajo funkcionalnost, hitrost in stabilnost.
</p>




<h2>Druge današnje najave:</h2>

<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Novi in izboljšani KDE-jevi programi 4.9</a></>

<p>
Med nove ali izboljšane KDE-jeve programe  spadajo, Okular, Kopete, Kontact, izobraževalni programi in igre. Za podrobnosti si oglejte <a href="../applications">najavo za KDE-jeve programe</a>.
</p>

<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>KDE-jeva razvojna platforma 4.9</a></h2>
<p>
Današnja izdaja KDE-jeve razvojne platforme vsebuje popravke, druge izboljšave kakovosti in omrežnih zmožnosti ter priprave na ogrodje KDE Frameworks 5. Za podrobnosti si oglejte <a href="../platform">najavo za KDE-jevo razvojno platformo</a>.
</p>
