---
aliases:
- ../announce-4.3.3
date: '2009-11-03'
description: KDE Community Ships Third Update to KDE 4.3 Series.
title: KDE 4.3.3 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE 4.3.3 Out Now: Clockwork
</h3>

<p align="justify">
  <strong>
KDE Community Ships Third Translation and Service Release of the 4.3
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
Like the ticking of a Swiss watch, every month the KDE team brings you a new release. <a href="/announcements/4/4.3.3">November's edition of KDE</a> is a bugfix and translation update to KDE 4.3. With the KDE 4 series picking up in popularity, we're happy to encourage even more people to give KDE 4 another spin -- or just upgrade your existing KDE to KDE 4.3.3. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. Users around the world will appreciate that KDE 4.3.3 is more completely translated. KDE 4 is already translated into more than 50 languages, with more to come.
</p>
<p>
KDE 4.3.3 has a number of improvements that will make your life just a little bit better. Some of KWin's effects have been smoothed and freed of visual glitches, JuK should now be more stable, KDE PIM has seen its share of improvements while in the back-rooms of KDE, the developers are working hard on porting all applications to the new Akonadi storage and cache. The <a href="/announcements/changelogs/changelog4_3_2to4_3_3">changelog</a> has more, if not exhaustive, lists of the improvements since KDE 4.3.2.
</p>

<div class="text-center">
	<a href="/announcements/4/4.3.0/images/kde430-desktop.png">
	<img src="/announcements/4/4.3.0/images/kde430-desktop_thumb.png" class="img-fluid" alt="KDE 4.3.0 Release Notes">
	</a> <br/>
	<em>The KDE 4.3 Desktop</em>
</div>
<br/>

<p align="justify">
Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE 4.3.3, you can browse the Subversion log.
KDE 4.3.3 also ships a more complete set of translations for many of the 50+ supported languages.
<p />
To find out more about the KDE 4.3 desktop and applications, please refer to the
<a href="/announcements/4.3/">KDE 4.3.0</a>,
<a href="/announcements/4.2/">KDE 4.2.0</a>,
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE 4.3.3 is a recommended update for everyone running KDE 4.3.2 or earlier versions.
<p />

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.3.3/">download.kde.org</a> and can
also be obtained on <a href="/download">CD-ROM</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing KDE 4.3.3 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.3.3
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.3.3/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.3.3">KDE 4.3.3 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.3.3
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.3.3 may be <a
href="http://download.kde.org/stable/4.3.3/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.3.3
  are available from the <a href="/info/4/4.3.3#binary">KDE 4.3.3 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>


