---
custom_about: true
custom_contact: true
hidden: true
title: Desktop Improves User Experience
---

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../guide">
                <img src="/announcements/4/4.2.0/images/star-32.png" />
                Overview
                </a>        
        </td>
        <td align="right" width="50%">
                <a href="../applications">Next page: Basic applications
                <img src="/announcements/4/4.2.0/images/applications-32.png" /></a>
        </td>
    </tr>
</table>

<p>The KDE Desktop Workspace consists of a group of core applications including
Plasma, KWin, KRunner and SystemSettings.
</p>

<p>Plasma allows you to organize your workspace however you want. It has become
easier to place the panels, add and move widgets around and configure your
desktop.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/desktop.png">
	<img src="/announcements/4/4.2.0/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The Desktop in KDE 4.2</em>
</div>
<br/>

<p>
<div align="right">
<img src="/announcements/4/4.2.0/images/plasma-logo.jpg" align="right" />
</div>

<strong>New Plasma widgets </strong>have been added, including Pastebin. You can drag and drop
text and images to a Pastebin website. The link is returned to your clipboard.
You can then send the link to others so that they can view the files you
uploaded. Other new widgets include several system monitors, a special character
selector, quicklaunch, news feeds, file previews, a calendar and even a simple
webbrowser. Thousands more widgets are now available on Plasma thanks to support
for Google Gadgets; Mac OS X Dashboard widgets are also available. Widgets can
be written in Ruby and Python; JavaScript and C++ are already supported. You can
expect that many more Plasma widgets will become available in the coming months.

</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/plasma-other-widgets.png">
	<img src="/announcements/4/4.2.0/plasma-other-widgets_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Some other Plasma widgets</em>
</div>
<br/>

<p><strong>Adding or moving a widget is easier than ever before</strong>. Choose the "Add Widgets"
option in the Plasma menu, and drag your choice to the desktop or a panel. They
will automatically resize depending on the room available. Widgets can be locked
and unlocked, so that they are not moved inadvertently. You unlock the desktop
by clicking on the Plasma icon (in the top-right by default) and choosing
"Unlock Widgets". Right-clicking the desktop also gives you the ability to lock
and unlock widgets. With widgets unlocked, you can rearrange your widgets and
move the Plasma icon to where you want it. The Plasma desktop will help you
arrange widgets. It is easy to resize them.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/plasma-add-widget.png">
	<img src="/announcements/4/4.2.0/plasma-add-widget.png" class="img-fluid">
	</a> <br/>
	<em>Adding a widget to Plasma</em>
</div>
<br/>

<p>If you need fast access to a certain widget (for example to quickly tweet about
what you are doing), you can now <strong>assign a shortcut </strong>to that widget. Access the
configuration of the widget using the wrench on the mouse-over sidebar or
right-click on the widget and choose the settings. Choose the option "Keyboard
Shortcut" and click on the shortcut button (which now says "None"). Enter the
key sequence you want to use to activate the widget. If there is a conflict with
another shortcut, you will be notified, and given a choice to undo or try
another shortcut. To reassign this shortcut to this widget, choose "Reassign".
</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/plasma-add-shortcut.png">
	<img src="/announcements/4/4.2.0/plasma-add-shortcut.png" class="img-fluid">
	</a> <br/>
	<em>You can configure shortcuts for every applet</em>
</div>
<br/>

<p>If you often put files and folders on the desktop while you work on them, use
the <strong>Folderview widget</strong>. When you drop a folder on the desktop, Folderview offers
you the choice of a new folderview or an icon. If you choose folderview, the
files in that folder are at the tip of your hand right away. When you are done
with that folder, you can remove the Folderview widget - the files are still on
the original (even remote like ftp) location. You could also choose to show an
icon instead of a folderview. Clicking on the icon will open the folder with the
Dolphin file manager.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/plasma-folderview.png">
	<img src="/announcements/4/4.2.0/plasma-folderview.png" class="img-fluid">
	</a> <br/>
	<em>The Folder View applet can display the contents of directories on your harddisk and on the network</em>
</div>
<br/>

<p>The Folderview widget remembers where you placed icons. It can also show
previews of the files. Click with your right mouse button on the name area at
the top of a folderview or use the configure icon (wrench) on the popout menu,
which is activated when you move your mouse over the Folderview widget. You can
choose any folder location, and configure it however you want it--large icons
with lots of text, or small icons with a single line of text. You can filter
files the folderview should show you, matching a string or a filetype, or hiding
certain files.
</p>

<p><strong>Desktop configuration </strong>is simple. Click with your right mouse button on the
desktop and choose "Appearance Settings", or use the Plasma icon to access the
configuration menu. Choosing a Folderview widget as your desktop gives you the
traditional desktop. In the future, more types of desktops will be developed.
You can give each desktop a name. Plasma gives you the ability to zoom out and
choose from several desktops. Naming them makes it easy to zoom in on the
desktop you want at the moment. A Plasma widget gives you a list of your
desktops to help switch between them.
</p>

<div class="text-center">
	<em>Plasma configuration screencast </em><br/>
<a href="http://blip.tv/file/1651256?filename=Sebasje-ThePlasmaDesktopShellInKDE42312.ogv">Ogg Theora version</a></div>
<br/>

<p>In the configuration screen, you can choose the theme Plasma should use for
widgets, and how you want to display the background. The look of Plasma can be
fine tuned further using a new System Settings module, <strong>Desktop Theme Details</strong>.
One option for the background is to show a wallpaper or a slideshow. This is
built upon a plugin system; in the future, you will have more options like an
interactive Mandelbrot fractal which lets you zoom and move around, and a
similarly interactive Marble world globe.
</p>

<p>Clicking the Plasma icon on the right of a panel brings up Panel Control. You
can change the height or location by dragging the buttons in the middle of the <strong>Panel
Controller</strong>. The "Add Widgets" button will bring you the list of available widgets
for the panel. You can drag and drop them, or use the "Add Widget" button.
Change the location of widgets on the panel with drag &amp; drop, or just click them
once, move them to the appropriate location and click again. An icon showing
four arrows will indicate which widget you are moving. You can drag and drop
widgets from the panel to the desktop or other panels and back again. The width
of the panel can be adjusted using the small 'ruler' icons on the bottom of
Panel Control. Grab them and drag them left or right (or up and down on a
vertical panel) to change the size of your panel. In the "More Settings" menu,
you will find options to change the alignment of the panel or get it back to
full width. You can also make the panel hide automatically. The "Lock Widgets"
button closes the configuration bar and locks the widgets in place. Click on an
empty space on the Panel or use the "Panel Options" submenu to unlock the
widgets again.
</p>

<p>Another new but still experimental feature is <strong>widgets on the screensaver</strong>. To
enable this, go to SystemSettings, choose "Desktop" and go to the "Screen Saver"
configuration. You can enable "Allow widgets on screen saver" on the bottom, and
click "Setup..." to add the widgets. Use the "Add Widgets" button, select
widgets and put them on the screen. Useful widgets are for example the Now
Playing widget and the Leave a Note widget. These widgets can be accessed by
anyone using your computer while your screen is locked. Using "Appearance
Settings" you can select the transparency of the widgets and hide them unless
somebody moves the mouse or uses the keyboard. If they do, the widgets will
appear and the "unlock computer" dialog appears. If the user clicks "Cancel", he
or she can then interact with the widgets you enabled on the desktop and thus
pause and skip songs or leave you notes.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/panel-controller.png">
	<img src="/announcements/4/4.2.0/panel-controller_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The panel controller lets you adjust size and location and behavior of the panel</em>
</div>
<br/>

<p>You now have more flexibility in <strong>choosing how your tasks are presented</strong>. Click
with your right mouse button on the taskbar and choose "Taskmanager Settings" to
access the configuration menu. You can change the way applications are grouped,
or use drag and drop to group them yourself. If multiple windows are grouped
behind a single taskbar entry, you will see an arrow with the number of grouped
windows. When you increase the height of the panel, the Taskbar can adjust
itself to a larger area by showing multiple rows of tasks. If you want, you can
force a certain number of rows. You can have it show tasks from all desktops, or
limit it to show only tasks from the current screen (helpful if you have a
multi-monitor setup). You can move a window from one desktop to another by
dropping it on the Pager widget.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/taskbar-configuration.png">
	<img src="/announcements/4/4.2.0/taskbar-configuration.png" class="img-fluid">
	</a> <br/>
	<em>In the taskbar's settings, you can set up the behavior of the taskbar in the panel</em>
</div>
<br/>

<p><strong>The System Tray</strong>, the area with small icons (usually on the right hand side of the
panel) has seen several changes. You can have the system tray hide certain icons
by default. Do a right mouse click just left of the most left icon on the
system tray and choose "System Tray Settings"  to access the configuration. By
selecting an icon and clicking the arrow pointing right you move the icon to the
list of hidden icons. A small arrow will appear on the left of the system tray.
Clicking it will show you the hidden icons.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/systray-progress.png">
	<img src="/announcements/4/4.2.0/systray-progress.png" class="img-fluid">
	</a> <br/>
	<em>The system tray now also shows progress of longer-running tasks and notifications</em>
</div>
<br/>

<p>The system tray will now gather <strong>notifications and track the progress </strong>of all kinds
of jobs from all KDE applications, giving you a convenient and consistent
location to see what is going on. For example, when a file is being transferred,
you will see a little icon on the right of the system tray. A
file transfer dialog will pop up above it, and disappear when the file transfer
is complete. Click on the computer icon to see what transfers are running or
what notifications have recently been shown. If you want to monitor the progress
more closely, you can detach the progress dialog. Grab it by the title bar and
move it onto the destop workspace. It will remain there until the notification
times out, the download finishes or you decide to re-attach it by clicking the
icon just left of the close button. When there are no more transfers and
notifications, the icon in the system tray hides itself again.
</p>

<p>KRunner provides faster access to your applications, files and websites.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/plasma-wos.png">
	<img src="/announcements/4/4.2.0/plasma-wos_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Plasma widgets can now also be used on the screensaver</em>
</div>
<br/>

<p>KRunner, the "Run command..." dialog lets you <strong>quickly start applications, find
documents or visit websites</strong>. KRunner has extended functionality through several
new plugins, including spell checking, Konqueror browser history, power
management control through PowerDevil, KDE Places, Recent Documents, and the
ability to start specific sessions of the Kate editor, Konqueror and Konsole.
The converter plugin now also supports quickly converting between units of
speed, mass and distances. The configure menu allows you to select just the
plugins you want.</p>

<p>You access KRunner by pressing the key combination <em>"ALT-F2"</em>. KRunner will show
up as a simple text input field. Start typing, and it will seek out to find what
you want. You can type <em>"= 2352 / 56"</em> and it will show you the answer. Type a
name of an application, and you can start it by hitting the enter key or choose
between the options KRunner shows you. If you have the plugins enabled, you can
<strong>change power management settings </strong>by just typing in "Power",
<strong>find entries in your
addressbook </strong>by typing in their names, or <strong>search through your music library</strong>
thanks to the Amarok Runner. Recent documents, Konsole and Kate sessions and
Konqueror history are available as well. <strong>Converting between units </strong>can be done by
typing into KRunner what you want: "135 km in miles" will show you 83.88.
Clicking the result will copy it to the clipboard so you can paste it anywhere.
If you type "35 euro in" KRunner will even offer you a link to the official
exchange rates on the European Central Bank site. Many types of data can be
converted like area, length, mass, speed and volume. You can find more
information about KRunner and on how to use it on <a
href="http://userbase.kde.org/Tutorials/Krunner">this Userbase page</a></p>

<p>By clicking the second icon from the left you will be brought to the <strong>System
Activity screen</strong>, which shows a list of running applications and processes.
KSysguard, the application behind the System Activity screen, gained several new
ways of controlling and monitoring the running applications. You can change
their priority ("Renice Process"), terminate them and monitor their input and
output by doing a right mouse click on the application and choosing the
appropriate action. The search field on the top will help you find the process
you are looking for.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/krunner-system-activity.png">
	<img src="/announcements/4/4.2.0/krunner-system-activity_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KRunner gives quick access to your system status and process manager</em>
</div>
<br/>

<p><strong>KRunner also has an alternative interface which is task-oriented</strong>. To use it, go
to the configuration dialog using the wrench icon on the left of the KRunner
dialog. Here you can configure what KRunner will match as you type. Under the
"User Interface" tab, you can choose between command-oriented and task-oriented
user interfaces. The task-oriented interface is named QuickSand. If you type in
a word, matches will appear, and you can use the left and right keys to choose
one of them. QuickSand also shows a dialog on the right of the interface with a
list of the results where you can use the up and down buttons to select an item.
Once you have selected one and hit enter, QuickSand will show the item and
associated actions. You can choose an action to execute with enter. Using the
left key, you can go back to the list. Many matches don't have actions yet, they
will be added as work on QuickSand progresses. Read more about QuickSand in the
<a href="http://commit-digest.org/issues/2008-09-21/">commit-digest</a> or this
<a href="http://rbitanga.blogspot.com/">developer blog</a>.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/krunner-quicksand.png">
	<img src="/announcements/4/4.2.0/krunner-quicksand.png" class="img-fluid">
	</a> <br/>
	<em>QuickSand, the task-based interface is a different UI for KRunner</em>
</div>
<br/>

<p>More information about Plasma and KRunner can be found on <a
href="http://userbase.kde.org/Plasma">Userbase</a>.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/kwin-present-windows.png">
	<img src="/announcements/4/4.2.0/kwin-present-windows.png" class="img-fluid">
	</a> <br/>
	<em>KWin's present windows effect makes for easy window switching</em>
</div>
<br/>

<h2>The Window Manager</h2>

<p>KWin is the window manager for the KDE desktop workspace. It gives you complete
control over your windows, making sure they're not in the way, but aiding you in
your tasks. Click with your right mouse button on a window decoration or use
System Settings to access the <strong>new and easier configuration </strong>of KWin. Here you can
enable effects to manage your windows more efficiently. For example, you can use
"Present Windows" as the effect for "window switching". Instead of showing you
icons or very small previews of your applications, it arranges them in a grid so
you can quickly choose the right one. Just use the <em>"ALT-TAB"</em> key sequence. With
the ALT key pressed, you can use tab to cycle through the windows or just
activate the right one using the mouse.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/kwin-settings.png">
	<img src="/announcements/4/4.2.0/kwin-settings.png" class="img-fluid">
	</a> <br/>
	<em>Compositing effects can be set up in KWin's settings module</em>
</div>
<br/>

<p>Employing <strong>motion dynamics and compositing</strong>, KWin provides your windows with a
more natural feel. <strong>New desktop effects </strong>like the "Cube Desktop Switcher" and
"Magic Lamp Minimize Animation" have been added. Improvements in the existing
effects make window management smoother than ever. KWin has gotten many
performance improvements to enable these effects even on lower-end hardware.
Checks have been built in to ensure that users with low-end hardware are not
presented with an unusable setup. KWin uses <strong>3D effects by default </strong>if it detects
the computer is capable of running them reasonably. You can take control and
disable these checks or change settings (Such as the global animation speed) to
fine tune your experience. When your computer is under heavy load, KWin will
automatically disable the compositing temporarily to keep things running
smoothly, and notify you. You can re-enable compositing by pressing
"SHIFT+ALT+F12". Finally, <strong>support for multiple screen handling has been improved</strong>
in KWin and Plasma using the new Kephal Library. You can move maximized windows
between Xinerama screens; the addition of external screens is handled better
than before.</p>

<div class="text-center">
	<em>Desktop Effect in KWin</em><br/>
<a href="http://blip.tv/file/1650884?filename=Sebasje-WindowManagementInKDE42153.ogv">Ogg Theora version</a></div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.2.0/systemsettings.png">
	<img src="/announcements/4/4.2.0/systemsettings.png" class="img-fluid">
	</a> <br/>
	<em>All settings of KDE are collated in System Settings</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.2.0/krunner-desktopsearch.png">
	<img src="/announcements/4/4.2.0/krunner-desktopsearch.png" class="img-fluid">
	</a> <br/>
	<em>First signs of desktop search integration</em>
</div>
<br/>

<p><strong>KDE 4.2 supports your mobile life.</strong> Owners of netbooks and other small devices
will appreciate the work on making applications usable on small screens. Many
configuration screens like those in Konqueror and Kontact have been redesigned
to fit. Another benefit for mobile users is that power usage has been reduced
all throughout the KDE software. The frequent wakeups from the core applications
like Plasma and KWin have been eliminated, making sure you get the most from
your battery.</p>

<p>Besides these improvements, PowerDevil introduces a new way of <strong>managing power</strong>.
Instead of being a separate tool, PowerDevil is an integral part of KDE. You
don't have to run a specific application to have access to its functionality. It
is configurable within System Settings (under the Advanced tab), and can be
controlled with a Plasma widget on the panel or desktop if you want.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/powerdevil.png">
	<img src="/announcements/4/4.2.0/powerdevil_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Power Management configuration and Plasma applet</em>
</div>
<br/>

<p><strong>A new printer configuration tool</strong> makes adding, removing and configuring printers
easier. A system tray applet (printer-applet) helps monitoring and controlling
print jobs. In order to configure your printer, select "Printing", under the
"System" category of the K menu. By selecting "New Printer", the program will
ask you for the type of printer (e.g., Local, networked...) and then prompt you to
select your manufacturer and printer model. Additionally, you can provide your
own print driver if you wish. Also, you can review the printers installed on
your system, print test pages and clean print heads (if your model allows that).
Lastly, you can configure some advanced settings of the printer system.</p>

<p><strong>ConsoleKit</strong>, a Freedesktop.org framework, has been integrated into the KDE
Display Manager (KDM). The Display Manager displays the login screen when you
boot up your computer, and manages user sessions. ConsoleKit enables some
multi-user features like fast user switching and keeps track of user sessions in
a desktop agnostic way.</p>
<table width="100%">
    <tr>
        <td width="50%">
                <a href="../guide">
                <img src="/announcements/4/4.2.0/images/star-32.png" />
                Overview
                </a>        
        </td>
        <td align="right" width="50%">
                <a href="../applications">Next page: Basic applications
                <img src="/announcements/4/4.2.0/images/applications-32.png" /></a>
        </td>
    </tr>
</table>