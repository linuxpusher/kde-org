---
aliases:
- ../announce-4.3.2
date: '2009-10-06'
description: KDE Community Ships Second Update to KDE 4.3 Series.
title: KDE 4.3.2 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE 4.3.2 Stabilizes Free Desktop
</h3>

<p align="justify">
  <strong>
KDE Community Ships Second Translation and Service Release of the 4.3
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
Another month has passed since the release of KDE 4.3.0, so today the  <a href="/">KDE
Community</a> announces the immediate availability of KDE 4.3.2, a
bugfix, translation and maintenance update for the latest generation
of the most advanced and powerful
free desktop. KDE 4.3.2 is a monthly update to <a href="../4.3.0/">KDE 4.3</a>. It
ships with a desktop workspace and many cross-platform applications such as administration
programs, network tools, educational applications, utilities, multimedia software, games, artwork,
development tools and more. KDE's award-winning tools and applications are
available in more than 50 languages.</p>

<h4>
  <a id="changes">Enhancements</a>
</h4>
<p align="justify">

KDE 4.3.2 brings a nice number of bugfixes, some critical, some just fixing small annoyances:

<ul>
    <li>Many crashers have been fixed in KDE's core libraries, bringing more stability to all applications </li>
    <li>KWin's window compositing effects have been further stabilized by fixing a number of bugs in effect plugins such as the famous coverswitch window switcher</li>
    <li>The biggest number of bugs for this release has been fixed in KMail, KDE's email client, making it more reliable and usable by correcting a number of display issues</li>
    <li>Saving files over themselves works again in Okular, KDE's document viewer</li>
</ul>
As usual, the <a href="/announcements/changelogs/changelog4_3_1to4_3_2">changelog</a> provides more -- if not exhaustive -- information about improvements in KDE 4.3.2. The KDE team recommends KDE 4.3.2 for everyone running an earlier version of KDE.
</p>

<div class="text-center">
	<a href="/announcements/4/4.3.0/images/kde430-desktop.png">
	<img src="/announcements/4/4.3.0/images/kde430-desktop_thumb.png" class="img-fluid" alt="KDE 4.3.0 Release Notes">
	</a> <br/>
	<em>The KDE 4.3 Desktop</em>
</div>
<br/>

<p align="justify">
Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE 4.3.2, you can browse the Subversion log.
KDE 4.3.2 also ships a more complete set of translations for many of the 50+ supported languages.
<p />
To find out more about the KDE 4.3 desktop and applications, please refer to the
<a href="/announcements/4.3/">KDE 4.3.0</a>,
<a href="/announcements/4.2/">KDE 4.2.0</a>,
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE 4.3.2 is a recommended update for everyone running KDE 4.3.1 or earlier versions.
<p />

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.3.2/">download.kde.org</a> and can
also be obtained on <a href="/download">CD-ROM</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing KDE 4.3.2 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.3.2
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.3.2/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.3.2">KDE 4.3.2 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.3.2
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.3.2 may be <a
href="http://download.kde.org/stable/4.3.2/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.3.2
  are available from the <a href="/info/4/4.3.2#binary">KDE 4.3.2 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>


