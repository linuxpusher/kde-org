---
aliases:
- ../4.2
date: '2009-01-27'
description: KDE Community Ships Second Major Update To Leading Free Software Desktop.
title: KDE 4.2.0 Release Announcement
---

<h3 align="center">
  KDE Community Improves User Experience with KDE 4.2
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (Codename: <em>"The Answer"</em>) Brings Improved Desktop User Experience,
    Applications and Development Platform
  </strong>
</p>

<p align="justify">
The <a href="/">KDE
Community</a> today announced the immediate availability of <em>"The Answer"</em>,
(a.k.a KDE 4.2.0), readying the Free Desktop for end users. KDE 4.2 builds on the technology
introduced with KDE 4.0 in January 2008. After the release of KDE 4.1, which was aimed at
casual users, the KDE Community is now confident we have a compelling offering for the
majority of end users.
</p>

<p><strong><a href="./guide">See the Visual Guide To KDE 4.2</a></strong> for the details on the innovations in 4.2 or read on for an overview.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/desktop.png">
	<img src="/announcements/4/4.2.0/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The KDE 4.2 Desktop</em>
</div>
<br/>

<h3>
    Desktop Improves User Experience
</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
        Further refinement of the Plasma desktop interface makes organizing
        your workspace easier.
        <strong>New and improved applets</strong> include a Quicklauncher, weather information,
        newsfeeds, comics, quick file sharing via "pastebin" services. Plasma applets can
        now be used on top of the screensaver, for example for leaving a note while the
        owner is absent.
        Plasma optionally acts as <strong>traditional, file manager-like desktop</strong>.
        Previews for the file icons and persistant locations of icons have been added. <br />
        The Plasma Panel <strong>now groups tasks</strong> and display multiple rows. The
        enhanced system tray now <strong>tracks longer running
        tasks</strong> such as downloads. System and application
        <strong>notifications</strong> are displayed in a unified fashion via the system tray.
        To save space, system tray icons can now be hidden. The panel can now
        <strong>automatically hide</strong> in order to free up screen space. Widgets can
        be displayed in panels as well as on the desktop.<br />
        </p>
    </li>
    <li>
        <p align="justify">
        KWin offers smooth and efficient window management. In KDE 4.2 it employs
        <strong>motion physics</strong> to give a natural feel to old and <strong>new
        effects</strong> like the "Cube" and "Magic Lamp". KWin only enables desktop
        effects in the default setup on computers that are able to handle them.
        <strong>Easier configuration</strong> allows the user to select different
        effects as window switcher, making changing windows most efficient.
        </p>
    </li>
    <li>
        <p align="justify">
        New and improved workspace tools increase productivity. PowerDevil supports
        mobile life by bringing modern and unobtrusive <strong>power management</strong>
        to laptops and mobile devices. Ark offers smart <strong>extraction and
        creation</strong> of archives, and the new printer tools allow the user to easily
        <strong>manage printers</strong> and print jobs.
        </p>
    </li>
</ul>
Moreover, support for several new languages have been added expanding the
number of users for whom KDE is available in their native language by
roughly 700 million people. Newly supported languages include are Arabic,
Icelandic, Basque, Hebrew, Romanian, Tajik and
several Indian languages (Bengali India, Gujarati, Kannada,
Maithili, Marathi) indicating a rise in popularity in this
part of Asia.
</p>

<div class="text-center">
	<em>The Plasma desktop interface in KDE 4.2</em><br/>
<a href="http://blip.tv/file/get/Sebasje-ThePlasmaDesktopShellInKDE42312.ogv">Ogg Theora version</a></div>
<br/>

<h3>
    Applications Leap Forward
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        File management becomes faster and more efficient. The Dolphin file manager now
        has a slider to easily <strong>adjust the icon size</strong>.
        Further user interface improvements include <strong>tooltips with
        previews</strong> and a capacity indicator for removable media devices. These
        changes have also been applied to the <strong>file dialogs</strong> in KDE, making
        it easier to spot the right file.
        </p>
    </li>
    <li>
        <p align="justify">
        KMail's email list views have been reworked by a Google Summer of Code student.
        The user can now configure the <strong>display of additional information</strong>
        optimizing the workflow for each individual folder. Support for <strong>IMAP and
        other protocols</strong> has also been improved making KMail much faster.
        </p>
    </li>
    <li>
        <p align="justify">
        Web browsing becomes better. The Konqueror web browser improves support for
        <strong>scalable vector graphics</strong> and receives many performance
        enhancements. A <strong>new find dialog</strong> makes for less intrusive searching
        inside webpages. Konqueror now <strong>shows your bookmarks</strong> on start-up.
        </p>
    </li>
</ul>
</p>

<div class="text-center">
	<em>Window Management in KDE 4.2</em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagementInKDE42153.ogv">Ogg Theora version</a></div>
<br/>

<h3>
    Platform Accelerates Development
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        <strong>Enhanced Scripting Support</strong>. Plasma widgets can now be written
        in JavaScript, Python and Ruby. These widgets can be distributed online via
        web-services and online collaboration tools such as OpenDesktop.org.
        GoogleGadgets can be used in Plasma, and support for Mac OS X dashboard
        widgets has been further improved.
        </p>
    </li>
    <li>
        <p align="justify">
        Technology previews of various KDE applications for <strong>Windows</strong> and
        <strong>Mac OS X</strong> are available, some applications are nearing release
        quality already, while others need some work depending on the functionality they
        implement. Support for OpenSolaris is upcoming and
        approaching stable quality. KDE4 on FreeBSD continues to mature.
        </p>
    </li>
    <li>
        <p align="justify">
        After Qt will be released under the terms of the <strong>LGPL</strong>, both
        the KDE libraries and beneath it Qt are available under those more relaxed
        licensing terms, making for a more compelling platform for commercial
        software development.
        </p>
    </li>
</ul>
</p>

<h4>
  Installing KDE 4.2.0
</h4>
<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.2.0/">download.kde.org</a> and can
also be obtained on <a href="/download">CD-ROM</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.2.0
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.2.0">KDE 4.2.0 Info
Page</a>.
</p>

<p align="justify">
Performance problems with the <em>NVidia</em> binary graphics driver have been
<a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">resolved</a> in the
latest beta releases of the driver available from NVidia.
</p>

<h4>
  Compiling KDE 4.2.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.2.0 may be <a
href="http://download.kde.org/stable/4.2.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.2.0
  are available from the <a href="/info/4/4.2.0#binary">KDE 4.2.0 Info
Page</a>.
</p>

<h4>
    Spread the Word
</h4>
<p align="justify">
The KDE team encourages everybody to spread the word on the Social Web as well.
Submit stories to websites, use channels like delicious, digg, reddit, twitter,
identi.ca. Upload screenshots to services like Facebook, FlickR,
ipernity and Picasa and post them to appropriate groups. Create screencast,
upload them to YouTube, Blip.tv, Vimeo and others. Do not forget to tag uploaded
material with the <em>tag <strong>kde42</strong></em> so it is easier
for everybody to find the
material, and for the KDE team to compile reports of coverage for the KDE 4.2
announcement. This is the first time the KDE team is attempting a coordinated
effort to use social media for their messaging. Help us spreading the word, be
part of it.
</p>
<p>
On web forums, inform people about KDE's new compelling features,
help others getting started with their new desktop, help us spread information.
</p>

<center>
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/software/KDE_4_2_released_2"><img src="/announcements/buttons/digg.gif" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com"><img src="/announcements/buttons/reddit.gif" /></a>
    </td>
    <td>
        <a href="http://www.twitter.com"><img src="/announcements/buttons/twitter.gif" /></a>
    </td>
    <td>
        <a href="http://www.identi.ca"><img src="/announcements/buttons/identica.gif" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde42/"><img src="/announcements/buttons/flickr.gif" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde42"><img src="/announcements/buttons/youtube.gif" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com"><img src="/announcements/buttons/facebook.gif" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde42"><img src="/announcements/buttons/delicious.gif" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></style>
</center>


