---
aliases:
- ../../plasma-5.4.0
changelog: 5.3.2-5.4.0
date: 2015-08-25
layout: plasma
figure:
  src: /announcements/plasma/5/5.4.0/plasma-screen-desktop-2-shadow.png
---

{{% i18n_date %}}

Today KDE releases a feature release of the new version of Plasma 5.

This release of Plasma brings many nice touches for our users such as much improved high DPI support, KRunner auto-completion and many new beautiful Breeze icons. It also lays the ground for the future with a tech preview of Wayland session available. We're shipping a few new components such as an Audio Volume Plasma Widget, monitor calibration tool and the User Manager tool comes out beta.

{{<figure src="/announcements/plasma/5/5.4.0/plasma-screen-audiovolume-shadows.png" alt="The new Audio Volume Applet" class="text-center" width="600px" caption="The new Audio Volume Applet">}}

### New Audio Volume Applet

Our new Audio Volume applet works directly with PulseAudio, the popular sound server for Linux, to give you full control over volume and output settings in a beautifully designed simple interface.

{{<figure src="/announcements/plasma/5/5.4.0/plasma-screen-dashboard-2-shadow.png" alt="The new Dashboard alternative launcher" class="text-center" width="600px" caption="The new Dashboard alternative launcher">}}

### Application Dashboard alternative launcher

Plasma 5.4 brings an entirely new fullscreen launcher Application Dashboard in kdeplasma-addons: Featuring all features of Application Menu it includes sophisticated scaling to screen size and full spatial keyboard navigation. The new launcher allows you to easily and quickly find applications, as well as recently used or favorited documents and contacts based on your previous activity.

{{<figure src="https://kver.files.wordpress.com/2015/07/image10430.png" alt="Just some of the new icons in this release" class="text-center" width="600px" caption="Just some of the new icons in this release">}}

### Artwork Galore

Plasma 5.4 brings over 1400 new icons covering not only all the KDE applications, but also providing Breeze themed artwork to apps such as Inkscape, Firefox and LibreOffice providing a more integrated, native feel.

{{<figure src="/announcements/plasma/5/5.4.0/plasma-screen-krunner-shadow.png" alt="KRunner" class="text-center" width="600px" caption="KRunner">}}

### KRunner history

KRunner now remembers your previous searches and automatically completes from the history as you type.

{{<figure src="/announcements/plasma/5/5.4.0/plasma-screen-nm-graph-shadow.png" alt="Network Graphs" class="text-center" width="600px" caption="Network Graphs">}}

### Useful graphs in Networks applet

The Networks applet is now able to display network traffic graphs. It also supports two new VPN plugins for connecting over SSH or SSTP.

### Wayland Technology Preview

With Plasma 5.4 the first technology preview of a Wayland session is released. On systems with free graphics drivers it is possible to run Plasma using KWin, Plasma's Wayland compositor and X11 window manager, through <a href="https://en.wikipedia.org/wiki/Direct_Rendering_Manager">kernel mode settings</a>. The currently supported feature set is driven by the needs for the <a href="https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform"> Plasma Mobile project</a> and more desktop oriented features are not yet fully implemented. The current state does not yet allow to use it as a replacement for Xorg based desktop, but allows to easily test it, contribute and watch tear free videos. Instructions on how to start Plasma on Wayland can be found in the <a href='https://community.kde.org/KWin/Wayland#Start_a_Plasma_session_on_Wayland'>KWin wiki pages</a>. Wayland support will improve in future releases with the aim to get to a stable release soon.

### Other changes and additions

- Much improved high DPI support
- Smaller memory footprint
- Our desktop search got new and much faster backend
- Sticky notes adds drag &amp; drop support and keyboard navigation
- Trash applet now works again with drag &amp; drop
- System tray gains quicker configurability
- The documentation has been reviewed and updated
- Improved layout for Digital clock in slim panels
- ISO date support in Digital clock
- New easy way to switch 12h/24h clock format in Digital clock
- Week numbers in the calendar
- Any type of item can now be favorited in Application Menu (Kicker) from any view, adding support for document and Telepathy contact favorites
- Telepathy contact favorites show the contact photo and a realtime presence status badge
- Improved focus and activation handling between applets and containment on the desktop
- Various small fixes in Folder View: Better default sizes, fixes for mouse interaction issues, text label wrapping
- The Task Manager now tries harder to preserve the icon it derived for a launcher by default
- It's possible to add launchers by dropping apps on the Task Manager again
- It's now possible to configure what happens when middle-clicking a task button in the Task Manager: Nothing, window close, or launching a new instance of the same app
- The Task Manager will now sort column-major if the user forces more than one row; many users expected and prefer this sorting as it causes less task button moves as windows come and go
- Improved icon and margin scaling for task buttons in the Task Manager
- Various small fixes in the Task Manager: Forcing columns in vertical instance now works, touch event handling now works on all systems, fixed a visual issue with the group expander arrow
- Provided the Purpose framework tech preview is available, the QuickShare Plasmoid can be used, making it easy to share files on many web services.
- Monitor configuration tool added
- kwallet-pam is added to open your wallet on login
- User Manager now syncs contacts to KConfig settings and the User Account module has gone away
- Performance improvements to Application Menu (Kicker)

- Various small fixes to Application Menu (Kicker): Hiding/unhiding
  apps is more reliable, alignment fixes for top panels, 'Add to
  Desktop' against a Folder View containment is more reliable,
  better behavior in the KActivities-based Recent models

- Support for custom menu layouts (through kmenuedit) and menu separator items in Application Menu (Kicker)

- Folder View has improved mode when in panel (<a href="https://blogs.kde.org/2015/06/04/folder-view-panel-popups-are-list-views-again">blog</a>)
- Dropping a folder on the Desktop containment will now offer creating a Folder View again
