---
date: 2021-06-22
changelog: 5.22.1-5.22.2
layout: plasma
youtube: HdirtBXW8bI
asBugfix: true
draft: false
---

+ Discover: Flatpak: properly notify about updates. [Commit.](http://commits.kde.org/discover/2a3c864386758e6bc8e973002323020f037ec2fb) Fixes bug [#438670](https://bugs.kde.org/438670)
+ [Task Manager] Show window title in tooltip when player title is different. [Commit.](http://commits.kde.org/plasma-desktop/2e499ee4a244777648c1ae621499c8f39ec23cd3)
+ KWin: Wayland: Implement activities window rule. [Commit.](http://commits.kde.org/kwin/6be5055bdf99f85c1c6eee180451536ee64c79ba) Fixes bug [#439022](https://bugs.kde.org/439022)
