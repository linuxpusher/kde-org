---
aliases:
- ../../plasma-5.18.3
changelog: 5.18.2-5.18.3
date: 2020-03-10
layout: plasma
peertube: cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5
figure:
  src: /announcements/plasma/5/5.18.0/plasma-5.18.png
asBugfix: true
---

- libkscreen: handle when backend fails to load/initialize. <a href="https://commits.kde.org/libkscreen/ff98585ea5541012b68604e34b7fec383a487cd9">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D27625">D27625</a>
- Discover Flatpak: build with older libflatpaks. <a href="https://commits.kde.org/discover/5cae00d1dbd94a584c9c63f7ff7fb5f893b228e4">Commit.</a>
- Discover: Make sure we don't crash. <a href="https://commits.kde.org/discover/1b0992a5375f2243d1c8fdb2ac5efdb991d619bd">Commit.</a>
