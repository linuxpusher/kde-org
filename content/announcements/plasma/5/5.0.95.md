---
aliases:
- ../../plasma-5.0.95
date: '2014-09-30'
description: KDE Ships Beta for Plasma 5's Second Release.
layout: plasma
title: KDE Ships Beta for Plasma 5's Second Release
---

{{<figure src="/announcements/plasma/5/5.1.0/qt4_widgets_plus_icontasks.jpg" class="text-center float-right ml-3" caption="KDE Platform 4 apps now themed to fit in with Plasma 5" width="600px" >}}

September 30, 2014. Today KDE releases the beta for the second release of Plasma 5. <a href='http://kde.org/announcements/plasma5.0/index.php'>Plasma 5</a> was released three months ago with many feature refinements and streamlining the existing codebase of KDE's popular desktop for developers to work on for the years to come.

This release is for testers to find bugs before our second release of Plasma 5.

## Some New Features

{{<figure src="/announcements/plasma/5/5.1.0/alt_switcher.jpg" class="text-center" caption="Applet Switcher" width="600px" >}}

Plasma panels have new switchers to easily swap between different widgets for the same task. You can select which application menu, clock or task manager you want with ease.

The icons-only task manager is back for those who want a clean panel.

{{<figure src="/announcements/plasma/5/5.1.0/icons_task_manager.jpg" class="text-center" caption="Icons-only Task Manager" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.1.0/plasma-lookandfeel.png" class="text-center" caption="A new System Settings module lets you switch between desktop themes." width="600px" >}}

{{<figure src="/announcements/plasma/5/5.1.0/qt4_widgets_plus_icontasks.jpg" class="text-center" caption="Breeze Theme for Qt 4" width="600px" >}}

A new Breeze widget theme for Qt 4 lets applications written with KDE Platform 4 fit in with your Plasma 5 desktop.
