---
aliases:
- ../../plasma-5.5.1
changelog: 5.5.0-5.5.1
date: 2015-12-15
layout: plasma
figure:
  src: /announcements/plasma/5/5.5.0/plasma-5.5.png
---

{{% i18n_date %}}

Today KDE releases a bugfix update to Plasma 5, versioned 5.5.1. <a href='https://www.kde.org/announcements/plasma-5.5.0.php'>Plasma 5.5</a> was released in last week with many feature refinements and new modules to complete the desktop experience. We are experimenting with a new release schedule with bugfix releases started out frequent and becoming less frequent. The first two bugfix releases come out in the weeks following the initial release and future ones at larger increments.

{{< i18n "annc-plasma-bugfix-worth-1" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

- Freeze on Plasma/Wayland startup fixed (unblock signals in child processes). <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=14b9046ad2ae7d4b9e3ffda996b2112fae3690c4">Commit.</a> Fixes bug <a href="https://bugs.kde.org/356580">#356580</a>. Code review <a href="https://git.reviewboard.kde.org/r/126361">#126361</a>
- KWin: Windows don't lose their 'active state' while being moved.
- Many fixes in full screen launcher application dashboard.
