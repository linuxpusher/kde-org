---
aliases:
- ../../plasma-5.8.95
changelog: 5.8.5-5.8.95
date: 2017-01-12
layout: plasma
title: Plasma 5.9 Beta Kicks off 2017 in Style.
figure:
  src: /announcements/plasma/5/5.9.0/plasma-5.9.png
---

{{% i18n_date %}}

Today KDE releases the beta of this year’s first Plasma feature update, Plasma 5.9. While this release brings many exciting new features to your desktop, we'll continue to provide bugfixes to Plasma 5.8 LTS.

## Be even more productive

{{<figure src="/announcements/plasma/5/5.9.0/spectacle-notification.png" alt="Spectacle screenshot notifications can now be dragged into e-mail composers (including web mail) " class="text-center" width="600px" caption="Spectacle screenshot notifications can now be dragged into e-mail composers (including web mail)">}}

In our ongoing effort to make you more productive with Plasma we added interactive previews to our notifications. This is most noticeable when you take a screenshot using Spectacle's global keyboard shortcuts (Shift+Print Scr): you can drag the resulting file from the notification popup directly into a chat window, an email composer or a web browser form, without ever having to leave the application you're currently working with. Drag and drop was improved throughout the desktop, with new drag and drop functionality to add widgets directly to the system tray. Widgets can also be added directly from the full screen Application Dashboard launcher.

{{<figure src="/announcements/plasma/5/5.9.0/icon-dialog.png" alt="Icon Widget Properties " class="text-center" width="600px" caption="Icon Widget Properties">}}

The icon widget that is created for you when you drag an application or document onto your desktop or a panel sees the return of a settings dialog: you can now change the icon, label text, working directory, and other properties. Its context menu now also sports an 'Open with' section as well as a link to open the folder the file it points to is located in.

{{<figure src="/announcements/plasma/5/5.9.0/mute.png" alt="Muting from Panel Task Manager " class="text-center" width="600px" caption="Muting from Panel Task Manager">}}

Due to popular demand we implemented switching between windows in Task Manager using Meta + number shortcuts for heavy multi-tasking. Also new in Task Manager is the ability to pin different applications in each of your activities. And should you rather want to focus on one particular task, applications currently playing audio are marked in Task Manager similar to how it’s done in modern web browsers. Together with a button to mute the offending application, this can help you stay focused.

{{<figure src="/announcements/plasma/5/5.9.0/kickoff-krunner-result.png" alt="Search Actions " class="text-center" width="600px" caption="Search Actions">}}

The Quick Launch applet now supports jump list actions, bringing it to feature parity with the other launchers in Plasma. KRunner actions, such as “Run in Terminal” and “Open containing folder” are now also shown for the KRunner-powered search results in the application launchers.

A new applet was added restoring an earlier KDE 4 feature of being able to group multiple widgets together in a single widget operated by a tabbed interface. This allows you to quickly access multiple arrangements and setups at your fingertips.

## More streamlined visuals

{{< video src-ogv="/announcements/plasma/5/5.9.0/breeze-scrollbar.ogv" caption="New Breeze Scrollbar Design" >}}

Improvements have been made to the look and feel of the Plasma Desktop and its applications. Scroll bars in the Breeze style, for instance, have transitioned to a more compact and beautiful design, giving our applications a sleek and modern look.

## Global Menus

{{<figure src="/announcements/plasma/5/5.9.0/global-menus-widget.png" alt="Global Menus in a Plasma Widget " class="text-center" width="600px" caption="Global Menus in a Plasma Widget">}}

{{<figure src="/announcements/plasma/5/5.9.0/global-menus-window-bar.png" alt="Global Menus in the Window Bar " class="text-center" width="600px" caption="Global Menus in the Window Bar">}}

Global Menus have returned. KDE's pioneering feature to separate the menu bar from the application window allows for new user interface paradigm with either a Plasma Widget showing the menu or neatly tucked away in the window bar.

{{<figure src="/announcements/plasma/5/5.9.0/task-manager-tooltips.png" alt="Neat Task Manager Tooltips " class="text-center" width="600px" caption="Neat Task Manager Tooltips">}}

Task Manager tooltips have been redesigned to provide more information while being significantly more compact. Folder View is now able to display file emblems which are used, for example, to indicate symlinks. Overall user experience when navigating and renaming files has been greatly improved.

## More powerful Look and Feel import & export

{{%youtube id="RX5HwumKPP8"%}}

The global Look and Feel desktop themes now support changing the window decoration as well – the 'lookandfeelexplorer' theme creation utility will export your current window decoration to the theme you create.

If you install, from the KDE store, themes that depend on other artwork packs also present on the KDE store (such as Plasma themes and Icon themes) they will be automatically downloaded, in order to give you the full experience intended by the theme creator.

## New network configuration module

{{<figure src="/announcements/plasma/5/5.9.0/network-configuration.png" alt="Network Connections Configuration" class="text-center" width="600px" caption="Network Connections Configuration">}}

A new configuration module for network connections has been added to System Settings, using QML and bringing a new fresh look. Design of the module is inspired by our network applet, while the configuration functionality itself is based on the previous Connection Editor. This means that although it features a new design, functionality remains using the proven codebase.

## Wayland

{{%youtube id="_kQwFpZoDZY"%}}

Wayland has been an ongoing transitional task, getting closer to feature completion with every release. This release makes it even more accessible for enthusiastic followers to try Wayland and start reporting any bugs they might find. Notable improvements in this release include:

An ability to take screenshots or use a color picker. Fullscreen users will be pleased at borderless maximized windows.
Pointers can now be confined by applications, gestures are supported (see video right) and relative motions used by games were added. Input devices were made more configurable and now save between sessions. There is also a new settings tool for touchpads.

Using the Breeze style you can now drag applications by clicking on an empty area of the UI just like in X. When running X applications the window icon will show up properly on the panel. Panels can now auto-hide. Custom color schemes can be set for windows, useful for accessibility.
