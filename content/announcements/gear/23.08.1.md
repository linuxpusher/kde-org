---
date: 2023-09-14
appCount: 120
image: true
layout: gear
---
+ gwenview: Fix navigation with side mouse buttons ([Commit](http://commits.kde.org/gwenview/9422e2129c5059df232849b4638f2dd8527a66d3))
+ kio-extras: Thumbnail: Fix heap-use-after-free in AudioCreator::create ([Commit](http://commits.kde.org/kio-extras/cf5d29ae48c627d6299638a5c535f5d8c2ae36fa), fixes bug [#469458](https://bugs.kde.org/469458))
+ akonadi-calendar: Use correct identity when sending iTIP counter-proposal ([Commit](http://commits.kde.org/akonadi-calendar/89a05bc3db6cd14172fe4222a3f33b1ab18a80e7), fixes bug [#458524](https://bugs.kde.org/458524))
