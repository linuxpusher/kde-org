---
date: 2022-10-13
appCount: 120
image: true
layout: gear
---
+ ark: Fix incompatibility with original 7-Zip ([Commit](http://commits.kde.org/ark/6187ddc4bed6b9503406c3b68ddfeec0358e7d99), fixes bug [#456797](https://bugs.kde.org/456797))
+ kdeconnect: Prevent a crash if there's no audio devices ([Commit](http://commits.kde.org/kdeconnect-kde/c6c6b1f44323ddd7c22e9d366f37ce84bc907621), fixes bug [#454917](https://bugs.kde.org/454917))
+ kio-extras: Compatibility with Samba >= 4.16 ([Commit](https://invent.kde.org/network/kio-extras/-/commit/95ebf9a66c4f503dd41205abadd7a522e758d974), fixes bug [#453090](https://bugs.kde.org/453090))
