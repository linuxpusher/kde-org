---
aliases:
- ../../fulllog_releases-21.08.1
title: KDE Gear 21.08.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Add a missing static storage class. [Commit.](http://commits.kde.org/akonadi/6cf482d3788fa908111dc7e67e3723d809b441cb) 
{{< /details >}}
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Ensure created ical resources syncs initial config to storage. [Commit.](http://commits.kde.org/akonadi-calendar/2e888a69bc1874510615e31aba5f8f77d6b1287c) 
{{< /details >}}
{{< details id="akonadi-calendar-tools" title="akonadi-calendar-tools" link="https://commits.kde.org/akonadi-calendar-tools" >}}
+ Ensure created ical resources syncs initial config to storage. [Commit.](http://commits.kde.org/akonadi-calendar-tools/cc4713f82782ea8d3563419a43321fb78a617eb2) 
{{< /details >}}
{{< details id="akonadiconsole" title="akonadiconsole" link="https://commits.kde.org/akonadiconsole" >}}
+ Improve the Database Browser tab. [Commit.](http://commits.kde.org/akonadiconsole/5cc54fccce07ce02d967102fabc69d5e36e2b89b) 
+ Fix the displayed job creation time. [Commit.](http://commits.kde.org/akonadiconsole/363ffa95a7460923fe4453bc82c92f6175602f94) 
{{< /details >}}
{{< details id="calendarsupport" title="calendarsupport" link="https://commits.kde.org/calendarsupport" >}}
+ Revert "Remove unneeded superclass from PrintPlugin". [Commit.](http://commits.kde.org/calendarsupport/38d94ba9f8a03ee9ae602f36de74b71fb9694297) 
+ Remove unneeded superclass from PrintPlugin. [Commit.](http://commits.kde.org/calendarsupport/ff9d663d67d0640815d154d6acf023f632052243) 
+ Remove dead print plugins. [Commit.](http://commits.kde.org/calendarsupport/bed9a075d186f4a1010d98318c7ff06f16264299) 
+ Fix Clazy warnings. [Commit.](http://commits.kde.org/calendarsupport/1aa8114655316222bb90b73f6f571f867dfbad30) 
+ Fix Clazy warnings. [Commit.](http://commits.kde.org/calendarsupport/9d15fba73f183ec77afd36a99ab0266734d91f45) 
+ Make events stand out from holidays when printing. [Commit.](http://commits.kde.org/calendarsupport/b295cd8dffbf571ca42ffa18bb57fecb846f1a07) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Filter the items that have changed. [Commit.](http://commits.kde.org/dolphin/127e446ba09c1294f57e50bcfc574b6c8a311370) 
+ FoldersPanel: Fix inline renaming. [Commit.](http://commits.kde.org/dolphin/1d125057cb6318bca795fd4a153c2675c881a9d1) Fixes bug [#441124](https://bugs.kde.org/441124)
+ Fix selecting file always opening new instance. [Commit.](http://commits.kde.org/dolphin/542e2a214a48a0eba6938381f1e043a37909f200) Fixes bug [#440663](https://bugs.kde.org/440663)
+ Avoid crash on start when help actions are restricted through the Kiosk system. [Commit.](http://commits.kde.org/dolphin/7f4756aecba48f7b828301d9f8256d6376660855) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Don't find KIO on Android. [Commit.](http://commits.kde.org/elisa/86cab3ac902c9a27943b525516f3ccd07b6897b1) 
+ Fix in-app seek shortcuts. [Commit.](http://commits.kde.org/elisa/dd0e1a1c8caf9c8bb462ce2380ae496a43e3e7cc) Fixes bug [#441485](https://bugs.kde.org/441485)
+ Fix broken "Files" view. [Commit.](http://commits.kde.org/elisa/618cf9b589ef9dd58e3ec3dd7450a80f10e118f8) 
+ Don't show "Show in Folder" buttons for radio streams. [Commit.](http://commits.kde.org/elisa/c0487093790ab0aabe62e7a0d1d0fed519854fa8) Fixes bug [#439280](https://bugs.kde.org/439280)
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Allow separate ListView configurations. [Commit.](http://commits.kde.org/eventviews/924bbce9b6590028fb122af2a3b729951a02275b) 
+ Preserve sorting in the ListView. [Commit.](http://commits.kde.org/eventviews/59afb8ff462a5da831a8daf3ebe064846f556504) Fixes bug [#441530](https://bugs.kde.org/441530)
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Fix build due to -Werror=undef. [Commit.](http://commits.kde.org/incidenceeditor/30a3fca472a2b236da6045b1d306b1f5db337a49) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Properly encode mail attachments received via FairEmail share intents. [Commit.](http://commits.kde.org/itinerary/eeae2741511f531c973b575522c091899b854265) 
+ Rename icon according to Flatpak convention. [Commit.](http://commits.kde.org/itinerary/39b36262ce1f24e38e7e6a66b7fa98eb351930bd) 
+ Add breeze as fallback icon theme. [Commit.](http://commits.kde.org/itinerary/a03d29172419cb634f31572e8e722dbb82e89379) 
+ Add 21.08.0 changelog. [Commit.](http://commits.kde.org/itinerary/8ac238ae66bc3f2804701eaefbce1e10e20377f3) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Bug 441660: Fix crash when KAlarm is launched while already running. [Commit.](http://commits.kde.org/kalarm/ac7402f51c1079d03ac2256fd152ec7740e2eddf) Fixes bug [#441660](https://bugs.kde.org/441660)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Semantic highlighter: Use a timer to reduce amount of requests being sent. [Commit.](http://commits.kde.org/kate/e25a08defe360ada7d7e52933fa9e4a459a7b9f1) 
{{< /details >}}
{{< details id="kdebugsettings" title="kdebugsettings" link="https://commits.kde.org/kdebugsettings" >}}
+ Fix list of group name. [Commit.](http://commits.kde.org/kdebugsettings/23bb2b50e34144045b41de7290a4a40003662954) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Compile MLT with GCC 9 - fixes brightness effect corruption. [Commit.](http://commits.kde.org/kdenlive/46dc123e2e820d8441d7d73caaac766b5f084fe8) 
+ Fix mix crossfade sometimes using wrong order (starting at 100% and ending at 0%) instead of reverse. [Commit.](http://commits.kde.org/kdenlive/7f5e10cc0215e3b46d101a21c96d2a68ef343afd) 
+ Upgrade document version and fix custom affine effects for MLT 7 when. [Commit.](http://commits.kde.org/kdenlive/d8d79dd59aa4e45e884f6a36c1594aa0d199f2fc) 
+ Make it possible to import mlt rect keyframes to frei0r.alphaspot. [Commit.](http://commits.kde.org/kdenlive/8b5f75d2d5e26df1a7428e1a6bdd347408771873) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Re-add servicetype for pimeventsplugin. [Commit.](http://commits.kde.org/kdepim-addons/3fb106d71338339dcf589f396a8d101ffd1cebff) Fixes bug [#440449](https://bugs.kde.org/440449)
+ Fix autotest. [Commit.](http://commits.kde.org/kdepim-addons/bf9bbc86b6e9fd4204ec03db66ff1f8ecc6c344b) 
+ Fix autotest. [Commit.](http://commits.kde.org/kdepim-addons/74a851109593ab09e40abade0624e97d01d2e810) 
+ Allow to cancel action. [Commit.](http://commits.kde.org/kdepim-addons/00454bfa6e21a281958669b7693e7942f204dbb6) 
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ It seems that code was not build before adding to MR. [Commit.](http://commits.kde.org/kdepim-runtime/727b21a15e6e5a1f6c0af40edbce19a307eab11d) 
{{< /details >}}
{{< details id="kig" title="kig" link="https://commits.kde.org/kig" >}}
+ Fix the kig build after kwindowsystem changes. [Commit.](http://commits.kde.org/kig/7e20f81c334fa306340c3af2884cc4eea9838fda) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add Airdo confirmation mail extractor script. [Commit.](http://commits.kde.org/kitinerary/307aba44f2eec00db4294d60f0350863e301e25e) 
+ Extract SNCF Ouigo confirmation emails. [Commit.](http://commits.kde.org/kitinerary/7fb1c4a632b59c6636b04efc6076656cc0659207) Fixes bug [#441361](https://bugs.kde.org/441361)
+ Extract Ryanair PDF boarding passes. [Commit.](http://commits.kde.org/kitinerary/b2e43b77e083fe36d71525f62d9902e2a1ec8a13) 
+ Actually add the MAV extractor. [Commit.](http://commits.kde.org/kitinerary/711480927c593f3612c1e112876f1a00f0cf0e38) 
+ Add basic MAV (Hungarian state railway) domestic ticket extractor. [Commit.](http://commits.kde.org/kitinerary/95b8d125a4dce891fe9efaaee0f05a16a4f20727) 
+ Add an alternative way of decoding RCT2 reservation data. [Commit.](http://commits.kde.org/kitinerary/63fd74cdf392ea968aed5bc34561ed589b65e3a0) 
+ Reproduce whitespaces a bit more correctly when layouting U_TLAY text. [Commit.](http://commits.kde.org/kitinerary/455458e1a45314f024822890aa86482e67dfd499) 
+ Let RCT2 type detection ignore whitespaces and support Hungarian IRTs. [Commit.](http://commits.kde.org/kitinerary/fc877ba47889d4884a297b0e302cd44fd0f0edd7) 
+ Add a watchdog timer to interrupt long running extractor scripts. [Commit.](http://commits.kde.org/kitinerary/76863a4a9779769283cdce164a84a5e354fa9cd4) 
+ Add Regionado pkpass extractor script. [Commit.](http://commits.kde.org/kitinerary/423df5a9f13788c1ef7f221cf0d7840bd28a1cb2) 
{{< /details >}}
{{< details id="klickety" title="klickety" link="https://commits.kde.org/klickety" >}}
+ Remove double content rating. [Commit.](http://commits.kde.org/klickety/b25e9024a6b5106738590716a7e38b02daaa633f) 
{{< /details >}}
{{< details id="klines" title="klines" link="https://commits.kde.org/klines" >}}
+ Remove double content rating. [Commit.](http://commits.kde.org/klines/188be2febba120e537dcd199e5d4f645d4c0a0af) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Assign parentWidget before creating plugin. [Commit.](http://commits.kde.org/kmail/20eebbb74915d501df0603b6cc322ab5e2609870) 
{{< /details >}}
{{< details id="kmail-account-wizard" title="kmail-account-wizard" link="https://commits.kde.org/kmail-account-wizard" >}}
+ Ensure created resources/agents reconfigure with up-to-date stored config. [Commit.](http://commits.kde.org/kmail-account-wizard/0afeb5ff2a5d39a1ba9012457a64c3420f2b36b0) 
{{< /details >}}
{{< details id="kmailtransport" title="kmailtransport" link="https://commits.kde.org/kmailtransport" >}}
+ Fix creation of invalid negative transport ids by cast uint32 -> int. [Commit.](http://commits.kde.org/kmailtransport/ac4988b0fb423260031b08220894f4a5601d1888) 
{{< /details >}}
{{< details id="kmix" title="kmix" link="https://commits.kde.org/kmix" >}}
+ Fixes crash thanks to Check if MD (MixDevice) exist on GlobalMaster or not. [Commit.](http://commits.kde.org/kmix/a4d5d52a2fed6012490318e9e7d8a73353a30883) 
{{< /details >}}
{{< details id="knetwalk" title="knetwalk" link="https://commits.kde.org/knetwalk" >}}
+ Remove double content rating. [Commit.](http://commits.kde.org/knetwalk/04f655db9b9736ebbefd20d001b8d72a3f6442b0) 
{{< /details >}}
{{< details id="kolourpaint" title="kolourpaint" link="https://commits.kde.org/kolourpaint" >}}
+ Use the QImageReader::read variant that gives you an image even if it thinks it failed. [Commit.](http://commits.kde.org/kolourpaint/7466945e821419140f3cd02be7e90946ad61c4ee) Fixes bug [#441554](https://bugs.kde.org/441554)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Only emit empty() in SessionFinished() when in TabbedNavigation mode. [Commit.](http://commits.kde.org/konsole/a731cd883304c2e00dce5210bc929a59d49736c2) Fixes bug [#441206](https://bugs.kde.org/441206)
+ When closing a session, don't close the whole window if there are splits. [Commit.](http://commits.kde.org/konsole/2591a9489a4d3a43c7a7f00764e9f84822d4946c) See bug [#440976](https://bugs.kde.org/440976)
+ Fix MainWindow size when there is no saved size. [Commit.](http://commits.kde.org/konsole/c78edfbac49852cec40efd5cbe73c341bc06c5ab) See bug [#437791](https://bugs.kde.org/437791)
+ Fix KXmlGUI toolbars; and Konsole MainWindow size. [Commit.](http://commits.kde.org/konsole/fb7f838fd3138a39aea3bcb2e91f923741587137) See bug [#430036](https://bugs.kde.org/430036). See bug [#439339](https://bugs.kde.org/439339). Fixes bug [#436471](https://bugs.kde.org/436471)
+ Fix crash when setting blur effect. [Commit.](http://commits.kde.org/konsole/f24dd6acc28393ba6f731be1360731c01a9a1ef0) Fixes bug [#439871](https://bugs.kde.org/439871)
+ Prevent window "flashing" when closing the last session. [Commit.](http://commits.kde.org/konsole/c8d60923728fb7b16eca613a44ee47e90ab86c72) Fixes bug [#432077](https://bugs.kde.org/432077)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Consistent conventional member names. [Commit.](http://commits.kde.org/korganizer/decde8a69997799587ede9ef6bc7b646a1425020) 
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Work around CMake Protobuf support not dealing with missing protoc. [Commit.](http://commits.kde.org/kosmindoormap/255b2b180710110e81ed1d26ff5cc4dee23b1fb2) 
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Fix autotest. [Commit.](http://commits.kde.org/kpimtextedit/dd5be14d5012634154fea401753d73aa37471f62) 
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Work around CMake Protobuf support not dealing with missing protoc. [Commit.](http://commits.kde.org/kpublictransport/559f92503bc5a4a1bc05d51b62a020ebbfba6d43) 
{{< /details >}}
{{< details id="libksane" title="libksane" link="https://commits.kde.org/libksane" >}}
+ Try every possible resolution value for preview. [Commit.](http://commits.kde.org/libksane/ccb0c77e3226c09a4a00fff412dc55476daf5269) Fixes bug [#440932](https://bugs.kde.org/440932)
{{< /details >}}
{{< details id="libksieve" title="libksieve" link="https://commits.kde.org/libksieve" >}}
+ SieveEditorHelpHtmlWidgetTest: handle 0x0-sized window failing expectations. [Commit.](http://commits.kde.org/libksieve/9818e389c461ca9468c1a15d4510a6b106afe3ba) 
+ SieveDateSpinBox: fix result of code() for weekdays. [Commit.](http://commits.kde.org/libksieve/c14049267141d43536b390185c05794188ab85ee) 
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Fix CryptoUtils::assembleMessage() to not add bogus text/plain content-type. [Commit.](http://commits.kde.org/mailcommon/17197c848caec74d070b2c3e05f87799aaf0c0c5) 
+ Fix FilterActionEncryptTest, workaround now expired key in test data. [Commit.](http://commits.kde.org/mailcommon/181068766c489475307a4cf7a0f120772ab40d2c) 
+ Filter tests: adapt to kmime enforcing explicit Content-Type. [Commit.](http://commits.kde.org/mailcommon/fb0184d65524745adc11bf239dc6af60fd06d970) 
+ FavoriteProxyTest: adapt test for dropping option on favourites. [Commit.](http://commits.kde.org/mailcommon/8d0bfbda8da72f68b9c793e7d262b11b98c9076f) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix build with the CI now using the standard Akonadi test controls. [Commit.](http://commits.kde.org/messagelib/e0136da6dda9c8798e06f352493ab0a92465d097) 
+ Adapt MessageFactoryTest::test_multipartAlternative to new replyAsHtml flag. [Commit.](http://commits.kde.org/messagelib/b0d1d8ff5b785cbff7b783ac7bdbee9658bb1f51) 
+ AutocryptHeadersJob: ensure reproducable order of Autocrypt-Gossip headers. [Commit.](http://commits.kde.org/messagelib/3ee82827cadb95f25d4a26f3da07af767b9d86b0) 
+ Fix MainTextJobTest::testHtmlWithImages. [Commit.](http://commits.kde.org/messagelib/07bc3977caf2c3c07c1a4ebf6d08dfb1491043cb) 
+ MessageFactoryNG: also replace ${} wrapper from MDN templates. [Commit.](http://commits.kde.org/messagelib/98ba4b3382712aad9e5096fecc0db071f9e1cab3) 
+ Fix false positive for url "https://www.google.com/search?q=%5C". [Commit.](http://commits.kde.org/messagelib/ee84101b36b1ea130c39a5bc9c9b3c471bb4edfb) See bug [#440635](https://bugs.kde.org/440635)
+ Fix autotest. [Commit.](http://commits.kde.org/messagelib/53a3cd1cf9a93fe1afef7bce8dba43945b4366ff) 
+ Fix autotest. [Commit.](http://commits.kde.org/messagelib/6eae4cbfc4ec84ffd662414668de2fd39193409b) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Stamps: Fix final showing the expired stamp. [Commit.](http://commits.kde.org/okular/6b9a15fb1ad017517ce7abb66dba6be9105b1e6a) Fixes bug [#441134](https://bugs.kde.org/441134)
+ Textpage: Don't use the page bounding rect as initial text rect. [Commit.](http://commits.kde.org/okular/bed447f76926002574752a2a18678edd1203d403) 
+ CI: silence clazy warning now that there's a new clazy version. [Commit.](http://commits.kde.org/okular/b747f0feb17829cf4fc628531cf62d70cc4c437e) 
{{< /details >}}
{{< details id="parley" title="parley" link="https://commits.kde.org/parley" >}}
+ Fix several memory leaks. [Commit.](http://commits.kde.org/parley/a3dd810b2c52cd7ac7d6ee63019717bd2c62679f) 
+ Fix assert with Qt debug build in QWidget destructor. [Commit.](http://commits.kde.org/parley/1a18decb4465469bd0bb82411014ba71bdbcfbb1) 
+ Provide sanitizer build option. [Commit.](http://commits.kde.org/parley/db1322e8e32f19af979e9e2702ffb592494c67e0) 
{{< /details >}}
{{< details id="pim-data-exporter" title="pim-data-exporter" link="https://commits.kde.org/pim-data-exporter" >}}
+ Fix export tests by adding a dummy exportdatatype.xml file again. [Commit.](http://commits.kde.org/pim-data-exporter/dfec7e96c5a024571fd4a2d1fdb0ea38cd5ecbc3) 
+ Allow to save/restore confirmbeforedeletingrc. [Commit.](http://commits.kde.org/pim-data-exporter/2e48b059d63e34ed6bf08e8defb2d66bd31b77a9) 
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Ensure created resources reconfigure with up-to-date stored config. [Commit.](http://commits.kde.org/pimcommon/2c59c4b1a43e943ba4e2e36bfd7b80debac89465) 
{{< /details >}}
{{< details id="print-manager" title="print-manager" link="https://commits.kde.org/print-manager" >}}
+ Fix plugin name in .desktop file. [Commit.](http://commits.kde.org/print-manager/64e93502238a1a4e249809a356c413457a4d4deb) Fixes bug [#426834](https://bugs.kde.org/426834)
{{< /details >}}
{{< details id="skanlite" title="skanlite" link="https://commits.kde.org/skanlite" >}}
+ Fix image saving when preview is not shown. [Commit.](http://commits.kde.org/skanlite/a8034a8dc2de3a0987ebf7fa57c21704e30e42e9) Fixes bug [#440970](https://bugs.kde.org/440970)
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ ExportManager: delay copy to clipboard. [Commit.](http://commits.kde.org/spectacle/bca68c17191f27723b6d96c4767dca5ef021087a) Fixes bug [#421974](https://bugs.kde.org/421974)
+ Wayland: when screens have same dpr use NativeSize. [Commit.](http://commits.kde.org/spectacle/a91f88d5a1ec6e08f9c5f142ed4bc1421d6d32bf) Fixes bug [#440226](https://bugs.kde.org/440226)
+ Don't show an error message when user cancels screenshot in progress on Wayland. [Commit.](http://commits.kde.org/spectacle/dd88afd37bae085813c31191ba00273350be6e28) 
{{< /details >}}
