---
aliases:
- ../../fulllog_releases-20.04.3
hidden: true
release: true
releaseDate: 2020-07
title: Release Service 20.04.3 Full Log Page
type: fulllog
version: 20.04.3
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Add .gitlab-ci.yml to run builds for PRs to stable branch. <a href='http://commits.kde.org/akonadi/1c8cd7013c1f350f9a684c3882e62c019b503d8c'>Commit.</a> </li>
<li>Fix build. <a href='http://commits.kde.org/akonadi/2abcb75b66b8132da567d9e78dd7d2b654fc16d8'>Commit.</a> </li>
<li>Server: fix fetching of attributes with empty data. <a href='http://commits.kde.org/akonadi/8e61c1c4f15568f1d4e4d5f35252b85e6a6d6b35'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>Fix crash when we select a unknown type. <a href='http://commits.kde.org/akonadi-contacts/52c3009ade50020f3da4684e65c189856e36d22b'>Commit.</a> </li>
<li>Fix mem leak found by ASAN. <a href='http://commits.kde.org/akonadi-contacts/ae7c0b7600808426c853f0fb5966da1c56f59bb0'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Fix show "switch_application_language". <a href='http://commits.kde.org/akregator/0f5f7023eaea0b512c5ad349e299d18c1182ab87'>Commit.</a> </li>
<li>Update Planet KDE feed to current atomic one. <a href='http://commits.kde.org/akregator/43aaa5da640356bd896c0d6bf1fa39dfa542e88b'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Use the canonical docs.kde.org URLs. <a href='http://commits.kde.org/cantor/30eadb602dcfc8d95f7b8baf222479d931b867ea'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Write correct value for "Open in application" script execution setting. <a href='http://commits.kde.org/dolphin/7a6956da997a39a22c1f08138674459c3c3aa32b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421294'>#421294</a></li>
<li>Fix file preview for desktop files with absolute icon paths. <a href='http://commits.kde.org/dolphin/37df39b93bf23b89ca760d4dd793788833d9a3e1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423326'>#423326</a></li>
<li>Generalize Player protocol support. <a href='http://commits.kde.org/dolphin/f2c09faf00e5375bc698732953572841933bb74d'>Commit.</a> </li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Hide]</a></h3>
<ul id='uleventviews' style='display: block'>
<li>Sort by date/time correctly in the Event List view. <a href='http://commits.kde.org/eventviews/9abd4418ab7d69600a7a838de7b1d5fde6ddd35c'>Commit.</a> </li>
<li>Display incidences at the time they are dragged to in the agenda view. <a href='http://commits.kde.org/eventviews/2757fd52ad3217a5ac47357ada352dd2e770b0c7'>Commit.</a> </li>
<li>Display recurring to-dos on their due date/time in the agenda view. <a href='http://commits.kde.org/eventviews/19aa3fdeea4c6312b80c959e86317639861b14fe'>Commit.</a> </li>
<li>Display all-day to-dos on their due date in the agenda view. <a href='http://commits.kde.org/eventviews/06b1cede77c8c62c24281935bf404b62f06d1c94'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338302'>#338302</a>. Fixes bug <a href='https://bugs.kde.org/417982'>#417982</a></li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Generate a journal entry when the completion percentage is set to 100%. <a href='http://commits.kde.org/incidenceeditor/6b5c5a64c2a41117df126e26e2859cb59155f2c5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423121'>#423121</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Fix CMake module search. <a href='http://commits.kde.org/juk/2079a775efdaa0f8548f992de2aa15f02cb0bfe2'>Commit.</a> </li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix CMake module search. <a href='http://commits.kde.org/k3b/564e54e703afd9cded4c894b64f16ff9c482be7f'>Commit.</a> </li>
</ul>
<h3><a name='kaccounts-integration' href='https://cgit.kde.org/kaccounts-integration.git'>kaccounts-integration</a> <a href='#kaccounts-integration' onclick='toggle("ulkaccounts-integration", this)'>[Hide]</a></h3>
<ul id='ulkaccounts-integration' style='display: block'>
<li>Bump libkaccounts so version. <a href='http://commits.kde.org/kaccounts-integration/839bce4ba5399fd5924f7316f8df402a86cb4262'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422191'>#422191</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Fix possible corruption of autostart.desktop file if disk is full. <a href='http://commits.kde.org/kalarm/3f52a74e4a009622828c6c91e2f979b9ee564058'>Commit.</a> </li>
</ul>
<h3><a name='kdeconnect-kde' href='https://cgit.kde.org/kdeconnect-kde.git'>kdeconnect-kde</a> <a href='#kdeconnect-kde' onclick='toggle("ulkdeconnect-kde", this)'>[Hide]</a></h3>
<ul id='ulkdeconnect-kde' style='display: block'>
<li>Add explanatory comment. <a href='http://commits.kde.org/kdeconnect-kde/bc5863f68fcd88f7895ef4838c6dd44274e6f7fb'>Commit.</a> </li>
<li>Prefer qdbus-qt5 if found. <a href='http://commits.kde.org/kdeconnect-kde/b699372d38c0035558b82efeb42336c0f0da16d8'>Commit.</a> </li>
</ul>
<h3><a name='kdenetwork-filesharing' href='https://cgit.kde.org/kdenetwork-filesharing.git'>kdenetwork-filesharing</a> <a href='#kdenetwork-filesharing' onclick='toggle("ulkdenetwork-filesharing", this)'>[Hide]</a></h3>
<ul id='ulkdenetwork-filesharing' style='display: block'>
<li>Cmake: remove deprecation timebomb. <a href='http://commits.kde.org/kdenetwork-filesharing/6406fc160fe2ab041550b50a324a6f03deef7dac'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix drop in timeline from clip monitor possible crash and disappearing clip (if mouse released above track headers). <a href='http://commits.kde.org/kdenlive/211212f70fb2813e6608d94ec7df1510ea2b2bdf'>Commit.</a> </li>
<li>Fix working on project with proxy only. <a href='http://commits.kde.org/kdenlive/703c0878ec9ecb153d7bf6ca331077a5f4341ee3'>Commit.</a> </li>
<li>Ensure we have a valid context before drawing keyframes. <a href='http://commits.kde.org/kdenlive/7ee8a90754b1f966bb5449e37a3a13192140ffeb'>Commit.</a> </li>
<li>Don't attempt activating a monitor if it is hidden. <a href='http://commits.kde.org/kdenlive/3051f9f235eafd2af0a4c86c361c67ad04538417'>Commit.</a> See bug <a href='https://bugs.kde.org/422849'>#422849</a></li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/kdepim-addons/c4e34a3eda121b4a5eb1d95af8457b6b5c55303d'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Reduce kf5. <a href='http://commits.kde.org/kdepim-runtime/3f9d8abc761781f8417e8fc340c8ac30014c54cf'>Commit.</a> </li>
</ul>
<h3><a name='kdesdk-kioslaves' href='https://cgit.kde.org/kdesdk-kioslaves.git'>kdesdk-kioslaves</a> <a href='#kdesdk-kioslaves' onclick='toggle("ulkdesdk-kioslaves", this)'>[Hide]</a></h3>
<ul id='ulkdesdk-kioslaves' style='display: block'>
<li>Don't disable API in future version in release branches. <a href='http://commits.kde.org/kdesdk-kioslaves/0167ac54bcb9b087be664da5088a3b71d38a1ad8'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Use the selected export keyserver to upload keys, not the input one. <a href='http://commits.kde.org/kgpg/b251b24f0e70dc6b5cd2bf75552688b0cacf7bbf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422611'>#422611</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Fix the CMake module search order. <a href='http://commits.kde.org/kio-extras/ecdc2558c11bce98701b735da03336f6beb44a96'>Commit.</a> </li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Extract seat reservation information from Ouigo confirmations. <a href='http://commits.kde.org/kitinerary/bc8b78d47829da0bec4facdff3bf1a740751e5ae'>Commit.</a> See bug <a href='https://bugs.kde.org/404451'>#404451</a></li>
<li>Adapt to ZXing changing its library target name. <a href='http://commits.kde.org/kitinerary/8391d99b88154fd6ba6389cb21e33f13a3a2a047'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix show "switch_application_language". <a href='http://commits.kde.org/kmail/c085d1123adbe41de3bc19e171e1c693cefa43ce'>Commit.</a> </li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Hide]</a></h3>
<ul id='ulknotes' style='display: block'>
<li>Remove duplicate margin. <a href='http://commits.kde.org/knotes/cd6decc50c44752dfedf9890b2fbf7fb31e92b6d'>Commit.</a> </li>
</ul>
<h3><a name='kompare' href='https://cgit.kde.org/kompare.git'>kompare</a> <a href='#kompare' onclick='toggle("ulkompare", this)'>[Hide]</a></h3>
<ul id='ulkompare' style='display: block'>
<li>Don't disable deprecated in future versions in release branches. <a href='http://commits.kde.org/kompare/b657424dd0be3628e3a210e1d8e527fddf7aefba'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Make the search bar usable after showing KMessageWidget. <a href='http://commits.kde.org/konsole/784c22ec2c064749508b1e8eee7cd32834e9805a'>Commit.</a> </li>
<li>Don't add extra newlines when pasting from GTK applications. <a href='http://commits.kde.org/konsole/6b6a1e552492a25217cded1d83c25a652e5b249a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421480'>#421480</a></li>
<li>Fix shortcuts for Yakuake grow-terminal actions. <a href='http://commits.kde.org/konsole/9bb2c059b1802fcf39059b976fb76daf2bbfb89f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415164'>#415164</a></li>
</ul>
<h3><a name='kontact' href='https://cgit.kde.org/kontact.git'>kontact</a> <a href='#kontact' onclick='toggle("ulkontact", this)'>[Hide]</a></h3>
<ul id='ulkontact' style='display: block'>
<li>Add separator. <a href='http://commits.kde.org/kontact/ff653b436bac103d576f4fd98935cca572f87191'>Commit.</a> </li>
<li>Fix show "switch_application_language". <a href='http://commits.kde.org/kontact/693ac5cfd31b88409cfe608824c12593cb7e9be0'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Add autotest for bug  found by David. <a href='http://commits.kde.org/kpimtextedit/b99bd4a67257f9a02224161fd4d8292eba75f6ab'>Commit.</a> </li>
<li>Fix generate html text (bug found by david). <a href='http://commits.kde.org/kpimtextedit/09c28867067f34cb4e1035f89b0bffd3239a0162'>Commit.</a> </li>
<li>Fix mem leak. <a href='http://commits.kde.org/kpimtextedit/28bc63cc505592822c59e93c5e39633798b00950'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/kpimtextedit/a1f7f537303d832fe23a204a9ce2e9a0c61dfb3c'>Commit.</a> </li>
</ul>
<h3><a name='ktimer' href='https://cgit.kde.org/ktimer.git'>ktimer</a> <a href='#ktimer' onclick='toggle("ulktimer", this)'>[Hide]</a></h3>
<ul id='ulktimer' style='display: block'>
<li>Fix command lines with arguments. <a href='http://commits.kde.org/ktimer/0be87585e6f9584a904d9a60bd3d03f9a8375c21'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/332490'>#332490</a></li>
</ul>
<h3><a name='kwalletmanager' href='https://cgit.kde.org/kwalletmanager.git'>kwalletmanager</a> <a href='#kwalletmanager' onclick='toggle("ulkwalletmanager", this)'>[Hide]</a></h3>
<ul id='ulkwalletmanager' style='display: block'>
<li>Properly pass the arguments to QProcess::startDetached(). <a href='http://commits.kde.org/kwalletmanager/9cf4ab73cd9b6eb38ddf8f55dca797510de513f0'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Fix mem leak found by ASAN. <a href='http://commits.kde.org/libksieve/e3c13637b10adabb00c8ba9c24923319d9c33e3e'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Fix hidden items in template directory. <a href='http://commits.kde.org/lokalize/202676f805fd3dca629282f95d1090a5d392c453'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422488'>#422488</a></li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Fix mem leak found by asan. <a href='http://commits.kde.org/mailcommon/f761cd0a1c7d3a2bff571ca1fe205ae46ce01a5e'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Hide]</a></h3>
<ul id='ulmarble' style='display: block'>
<li>Fix the reference to docs.kde.org from marble-qt. <a href='http://commits.kde.org/marble/552cb9ae1f34482d1ec56532a703e0d820856286'>Commit.</a> </li>
<li>Add missing Qt5WebChannel dependency. <a href='http://commits.kde.org/marble/c062504a2864680fee9c81ea46e7613315de038a'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix mem leak found by ASAN. <a href='http://commits.kde.org/messagelib/b96be01bebde1121a998e2db25e5bcbd131d1759'>Commit.</a> </li>
<li>Make sure that we don"t see [vcard] when we don't have it. <a href='http://commits.kde.org/messagelib/ef8a93713d63cff47bcebbfd19b196fb84f8fe6e'>Commit.</a> </li>
<li>Fix crash when node is empty. <a href='http://commits.kde.org/messagelib/f919863d6e391372f85309ffcaa582d89f049403'>Commit.</a> </li>
<li>Add more check about css imported from message. <a href='http://commits.kde.org/messagelib/752e180447d451021fba9807a223cd74d9381961'>Commit.</a> </li>
<li>Don't allow to use div in head. <a href='http://commits.kde.org/messagelib/b5a4ec86df70205a6cf8e524e51c5888d073ffe4'>Commit.</a> </li>
<li>We don't need to add all testcase when we use qt >= 5.14. <a href='http://commits.kde.org/messagelib/605ab089fc91f9b9beb005d39cbb5610a9762472'>Commit.</a> </li>
<li>NavigationTypeRedirect was added to qt5.14. <a href='http://commits.kde.org/messagelib/33753bebd8379b6219f0cd75182a2f01c3f2ede7'>Commit.</a> </li>
<li>Exclude redirect too. <a href='http://commits.kde.org/messagelib/229a289c86085a5cc563054175a1d761b78c96e2'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Increase kjs requirement for using the timeout check. <a href='http://commits.kde.org/okular/795c7cb01fe2dc77d66cec701e915447203bbafd'>Commit.</a> </li>
<li>Allow scaling like 150% on Windows and default to Breeze style on Windows/macOS. <a href='http://commits.kde.org/okular/3311bcb949a51f1004491e5857117e30a08297ce'>Commit.</a> </li>
<li>Fix/Workaround file where we can't enter values in some fields. <a href='http://commits.kde.org/okular/d78cbb79c98e01fc524ec91fb099ec8ff98cc1fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421508'>#421508</a></li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Hide]</a></h3>
<ul id='ulpim-data-exporter' style='display: block'>
<li>Fix logic. <a href='http://commits.kde.org/pim-data-exporter/59581fb53fe72f42315dabb8d17144df708dc09b'>Commit.</a> </li>
<li>Fix import resource name. <a href='http://commits.kde.org/pim-data-exporter/29944a8793c9ba5e375455d1f165fbe9f523d336'>Commit.</a> </li>
</ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Hide]</a></h3>
<ul id='ulpimcommon' style='display: block'>
<li>Extract email address directly. <a href='http://commits.kde.org/pimcommon/17ddf3e20584cb3559e8f0f77b529a46353a1fca'>Commit.</a> </li>
</ul>
<h3><a name='rocs' href='https://cgit.kde.org/rocs.git'>rocs</a> <a href='#rocs' onclick='toggle("ulrocs", this)'>[Hide]</a></h3>
<ul id='ulrocs' style='display: block'>
<li>Cmake: fix version in config header. <a href='http://commits.kde.org/rocs/f36bb0e2a6a288b5553023f85183cec5d4bacee5'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Fix 'Class Diagram Widget Text Truncated Upon Re-Opening XMI' on Windows 10. <a href='http://commits.kde.org/umbrello/15bb50e2ec5c6438eb13c012de1ccf177f3e98e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423660'>#423660</a></li>
</ul>
<h3><a name='yakuake' href='https://cgit.kde.org/yakuake.git'>yakuake</a> <a href='#yakuake' onclick='toggle("ulyakuake", this)'>[Hide]</a></h3>
<ul id='ulyakuake' style='display: block'>
<li>Fix maximization behaviour. <a href='http://commits.kde.org/yakuake/40326335554565cbc5a1ed9938828d60fa7cd214'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414049'>#414049</a></li>
<li>Fix shortcut conflict for session closing. <a href='http://commits.kde.org/yakuake/7a08d95b659af3614391df32d80bf2ca90e1077f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/319172'>#319172</a></li>
</ul>
