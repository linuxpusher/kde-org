---
aliases:
- ../../fulllog_applications-16.08.3
hidden: true
title: KDE Applications 16.08.3 Full Log Page
type: fulllog
version: 16.08.3
---

<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Show]</a></h3>
<ul id='ulakonadi-calendar' style='display: none'>
<li>Fix CalendarBase::items() only working for invalid Collection. <a href='http://commits.kde.org/akonadi-calendar/d338ae8d54dfd3a72bda5846fdf0afbce99b83b6'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Show]</a></h3>
<ul id='ulakonadi-search' style='display: none'>
<li>Fetch full Item payload. <a href='http://commits.kde.org/akonadi-search/d03e37b9e13046fcec34469cf9a05c935f2d3580'>Commit.</a> </li>
<li>Respect the IndexPolicyAttribute settings for indexing. <a href='http://commits.kde.org/akonadi-search/c4296b665e327e4ea5c3fbd991df1d71b24d5fc1'>Commit.</a> </li>
<li>Revert "Fix a memory leak introduced in 1e70d63a943". <a href='http://commits.kde.org/akonadi-search/1a26abcf15b7bd2f42beb7b86a12ae13980eba0a'>Commit.</a> </li>
<li>Fix a memory leak introduced in 1e70d63a943. <a href='http://commits.kde.org/akonadi-search/5aefe02b83a6d73dbd048337d2ba54fee4bbd67c'>Commit.</a> See bug <a href='https://bugs.kde.org/363741'>#363741</a></li>
</ul>
<h3><a name='analitza' href='https://cgit.kde.org/analitza.git'>analitza</a> <a href='#analitza' onclick='toggle("ulanalitza", this)'>[Show]</a></h3>
<ul id='ulanalitza' style='display: none'>
<li>Fix regressed feature. <a href='http://commits.kde.org/analitza/9669ef43bdd73543509317b10de56493faf250db'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352526'>#352526</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Cliinterface: fix crash when aborting an ExtractJob. <a href='http://commits.kde.org/ark/5e61417e02b2543357151825f96a4d7279674607'>Commit.</a> </li>
<li>Libarchive: fix data loss when aborting AddJobs and Deletejobs. <a href='http://commits.kde.org/ark/d1444e74574dd4c57455ab0dd554da105ac37498'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365869'>#365869</a></li>
<li>Libarchive: properly kill DeleteJobs. <a href='http://commits.kde.org/ark/d587749dbdb1f13cd497934d15388488bc199c2b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365870'>#365870</a></li>
<li>Allow extraction of read-only archives. <a href='http://commits.kde.org/ark/6f0e4ead6f33f4a6ae2530c551952e8443bd3c8b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370948'>#370948</a></li>
<li>Stop crashing when batch extracting invalid archives. <a href='http://commits.kde.org/ark/33c5874b39a1182402d0869b74417c50195ec37f'>Commit.</a> See bug <a href='https://bugs.kde.org/370959'>#370959</a>. See bug <a href='https://bugs.kde.org/370986'>#370986</a></li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Show]</a></h3>
<ul id='ulartikulate' style='display: none'>
<li>Fix menubar show/hide functionality. <a href='http://commits.kde.org/artikulate/4cbe29cf1918f32edca05433a9aafe51594e8e51'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>[Places Item Edit Dialog] Fix accepting dialog with Return. <a href='http://commits.kde.org/dolphin/95e1505c9f01bdae2c1157be1bb6fa5b991c8fd4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129213'>#129213</a></li>
</ul>
<h3><a name='dolphin-plugins' href='https://cgit.kde.org/dolphin-plugins.git'>dolphin-plugins</a> <a href='#dolphin-plugins' onclick='toggle("uldolphin-plugins", this)'>[Show]</a></h3>
<ul id='uldolphin-plugins' style='display: none'>
<li>[git plugin] Correct revert action arguments. <a href='http://commits.kde.org/dolphin-plugins/63b37b28a5d352f0800de78f5d44824d526a3fcb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129272'>#129272</a></li>
</ul>
<h3><a name='kajongg' href='https://cgit.kde.org/kajongg.git'>kajongg</a> <a href='#kajongg' onclick='toggle("ulkajongg", this)'>[Show]</a></h3>
<ul id='ulkajongg' style='display: none'>
<li>--nokde: search tilesets also in /usr/share/kmahjongglib. <a href='http://commits.kde.org/kajongg/f001f0c77518a7548ef327dd8fb9d729104b6992'>Commit.</a> </li>
<li>Kajongg did not show icons anymore when running with using the PyKDE bindings. <a href='http://commits.kde.org/kajongg/959273fa79e9071bbb7778b3d6ebf9290a21e82c'>Commit.</a> </li>
<li>PyKDE does not work on Kubuntu 16.10, see KDE bug 370998. <a href='http://commits.kde.org/kajongg/7629cc90b4f45ec8dd4153c887b133c95a972c3d'>Commit.</a> </li>
<li>Players on server need no voice. <a href='http://commits.kde.org/kajongg/6de3e345498736a80fc179d0cda665c2e5f8a10f'>Commit.</a> </li>
</ul>
<h3><a name='kbreakout' href='https://cgit.kde.org/kbreakout.git'>kbreakout</a> <a href='#kbreakout' onclick='toggle("ulkbreakout", this)'>[Show]</a></h3>
<ul id='ulkbreakout' style='display: none'>
<li>KBreakout: Fix brick count. <a href='http://commits.kde.org/kbreakout/dd0118db78d361bf58c03212c7b6f0fac9c9369f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128556'>#128556</a></li>
</ul>
<h3><a name='kcachegrind' href='https://cgit.kde.org/kcachegrind.git'>kcachegrind</a> <a href='#kcachegrind' onclick='toggle("ulkcachegrind", this)'>[Show]</a></h3>
<ul id='ulkcachegrind' style='display: none'>
<li>Kcachegrind.desktop: X-DBUS-ServiceName=org.kde.kcachegrind. <a href='http://commits.kde.org/kcachegrind/e5932b6d000894e7684a059759550e3ba4b40550'>Commit.</a> </li>
</ul>
<h3><a name='kcolorchooser' href='https://cgit.kde.org/kcolorchooser.git'>kcolorchooser</a> <a href='#kcolorchooser' onclick='toggle("ulkcolorchooser", this)'>[Show]</a></h3>
<ul id='ulkcolorchooser' style='display: none'>
<li>Don't use native color picker. <a href='http://commits.kde.org/kcolorchooser/1f35c6d3dffbdfbbd7679e6b9a6297bd421abb3e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129151'>#129151</a></li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>KRecursiveFilterProxyModel: fix QSFPM corruption due to filtering out rowsRemoved signal. <a href='http://commits.kde.org/kdelibs/e58d4813b9eb6125bad83cc4fd576d5d75d31c4b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128428'>#128428</a>. See bug <a href='https://bugs.kde.org/349789'>#349789</a></li>
<li>Don't show overwrite dialog if file name is empty. <a href='http://commits.kde.org/kdelibs/0cc054d1c6336f1a4c7604d437d15de076a50293'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/120127'>#120127</a></li>
<li>Fix KUrl::isRelativeUrl to allow all RFC3986 characters in scheme. <a href='http://commits.kde.org/kdelibs/a0f36a80ac76a0b23f1bf53fc43bd6e8191048b1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129208'>#129208</a></li>
<li>Support newer hunspell versions in FindHUNSPELL.cmake. <a href='http://commits.kde.org/kdelibs/2ab2745eb01f73355c490ac8d5d1837dec84fd6c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128600'>#128600</a></li>
<li>Fix HAVE_TRUNC cmake check. <a href='http://commits.kde.org/kdelibs/0c642ae95dacf894e50630ffcc1961ad1e4e0322'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129119'>#129119</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix error message (cannot create directory) when opening archived project. <a href='http://commits.kde.org/kdenlive/084294034dd909caaffa87893c36b3fe37e5bae4'>Commit.</a> </li>
<li>Fix incorrect Url handling in archive feature. <a href='http://commits.kde.org/kdenlive/2871dee6c51c827f41ad074c5a5d6b7435c69e08'>Commit.</a> See bug <a href='https://bugs.kde.org/367705'>#367705</a></li>
<li>Switch from kdelibs4 sound to Plasma 5 sound files. <a href='http://commits.kde.org/kdenlive/c0b0591e3e21144ec5eae7639aa9d921f059a50b'>Commit.</a> </li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Increase version here too. <a href='http://commits.kde.org/kdepim/43c8faf26f152d2a885e83e2bf58d7c0c4e52ae4'>Commit.</a> </li>
<li>Fix Bug 372073 - Kontact and KMail 5.3.2 are showing the wrong version in the help->introduction page. <a href='http://commits.kde.org/kdepim/09d172cffdd1b72a46ff7ddf2b008a3750eec2ed'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372073'>#372073</a></li>
<li>Update changelog. <a href='http://commits.kde.org/kdepim/81367c49dee5d2fa3f451eb4372e690fb36fe40c'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim/7e8983344938bd59bfaf534737f0b614994008b3'>Commit.</a> </li>
<li>Don't add new tag when name is empty and we use return key. <a href='http://commits.kde.org/kdepim/6bc78e9ab1a0d4f55ba60a863862d7f8604dd49a'>Commit.</a> </li>
<li>Backport : remove double margin. <a href='http://commits.kde.org/kdepim/90ae2110b3d940bff606b8c2553da0f88b13a3c1'>Commit.</a> </li>
<li>Backport remove double margin. <a href='http://commits.kde.org/kdepim/182f4d8e410dc6417f7a8eabcd2f41483ef753e8'>Commit.</a> </li>
<li>Fix warning message about QCoreApplication::arguments. <a href='http://commits.kde.org/kdepim/18c35bf197275d9aad647df04dae55d3d5284775'>Commit.</a> </li>
<li>Remove deprecated calls. <a href='http://commits.kde.org/kdepim/f3a9366ad1d1993908d754b2276224121014e14f'>Commit.</a> </li>
<li>Bug 371628: Fix crash when a second instance of KAlarm is started. <a href='http://commits.kde.org/kdepim/6bd68f98d675e30fab24713e63dfe72cdfd6f4f5'>Commit.</a> </li>
<li>Backport refresh display attributes. <a href='http://commits.kde.org/kdepim/5f24ff15fd73fc90d5b04461dd0f0d161675bdd7'>Commit.</a> </li>
<li>Fix update menu action. <a href='http://commits.kde.org/kdepim/8679424ade7f5def4311280bbe0782217f92d3b7'>Commit.</a> </li>
<li>Backport fix Bug 371703 - Not saving "Sender ID" and "Port" in "Network preferences". <a href='http://commits.kde.org/kdepim/cbfcd564d0500a109140c98c4b430fb81018c759'>Commit.</a> See bug <a href='https://bugs.kde.org/371703'>#371703</a></li>
<li>Bug 362962: Fix default calendar files not being created on first run. <a href='http://commits.kde.org/kdepim/72b42951fc44bccf5dcbfa0a986f39e6e3b3e86e'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Show]</a></h3>
<ul id='ulkdepim-addons' style='display: none'>
<li>Don't display CC/BCC when there are not defined. <a href='http://commits.kde.org/kdepim-addons/849284dda124b2ec18f5b2eabbf232fa3f9f9621'>Commit.</a> </li>
<li>PimEventsPlugin: switch from ETMCalendar to custom EventModel. <a href='http://commits.kde.org/kdepim-addons/c921a14e0a30e13342e94cce74d4ad1bbb3a64c5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369606'>#369606</a>. Fixes bug <a href='https://bugs.kde.org/368832'>#368832</a></li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Bug 370627: ignore temporary files created in calendar directory. <a href='http://commits.kde.org/kdepim-runtime/4cd6df12ffc7cc7455f986e87c0d79d00d3123fa'>Commit.</a> </li>
<li>Bug 370627: ignore temporary files created in calendar directory. <a href='http://commits.kde.org/kdepim-runtime/36009232d38e517b34816a014958a58fabd886a0'>Commit.</a> </li>
<li>Drop unused dep: KF5Syndication. <a href='http://commits.kde.org/kdepim-runtime/ca256c6884837a7ebcb344b4997e5f54d4437c15'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129193'>#129193</a></li>
</ul>
<h3><a name='kdgantt2' href='https://cgit.kde.org/kdgantt2.git'>kdgantt2</a> <a href='#kdgantt2' onclick='toggle("ulkdgantt2", this)'>[Show]</a></h3>
<ul id='ulkdgantt2' style='display: none'>
<li>Increase version. <a href='http://commits.kde.org/kdgantt2/47c49a6f02258252c21870b7e7f1a3b5dcb4cf83'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>[filenamesearch] Add kded module to watch file changes. <a href='http://commits.kde.org/kio-extras/0c9abdaf0a1be2d52f88eb021bf626c25b102a25'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129297'>#129297</a></li>
</ul>
<h3><a name='klettres' href='https://cgit.kde.org/klettres.git'>klettres</a> <a href='#klettres' onclick='toggle("ulklettres", this)'>[Show]</a></h3>
<ul id='ulklettres' style='display: none'>
<li>Drop unused dependency. <a href='http://commits.kde.org/klettres/59335eef20ea6864d197c9ee5b44d8ea34d891bb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129195'>#129195</a></li>
</ul>
<h3><a name='kmines' href='https://cgit.kde.org/kmines.git'>kmines</a> <a href='#kmines' onclick='toggle("ulkmines", this)'>[Show]</a></h3>
<ul id='ulkmines' style='display: none'>
<li>Fix clearing question marks at game win. <a href='http://commits.kde.org/kmines/f3455686463c0e4c79c1c7ae376d21ef05cdd9c8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128462'>#128462</a>. Fixes bug <a href='https://bugs.kde.org/354900'>#354900</a></li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Show]</a></h3>
<ul id='ulkolourpaint' style='display: none'>
<li>Add null check for mimeData retrieved from QClipboard. <a href='http://commits.kde.org/kolourpaint/4b280d0942b4c7b0248c96d0077ebab8c5a2673d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129171'>#129171</a></li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>Fix typo in CMakeLists.txt option. <a href='http://commits.kde.org/konsole/5080af97975b8ab57562d87df389066434da0203'>Commit.</a> </li>
</ul>
<h3><a name='kstars' href='https://cgit.kde.org/kstars.git'>kstars</a> <a href='#kstars' onclick='toggle("ulkstars", this)'>[Show]</a></h3>
<ul id='ulkstars' style='display: none'>
<li>Switch from kdelibs4 sound files to Plasma 5 sound files. <a href='http://commits.kde.org/kstars/dca06d4444dc4a28cd856f5c899c16460175bf73'>Commit.</a> </li>
</ul>
<h3><a name='kwalletmanager' href='https://cgit.kde.org/kwalletmanager.git'>kwalletmanager</a> <a href='#kwalletmanager' onclick='toggle("ulkwalletmanager", this)'>[Show]</a></h3>
<ul id='ulkwalletmanager' style='display: none'>
<li>Fix XML export regression. <a href='http://commits.kde.org/kwalletmanager/956552dcb5beea41238a858cbe77570b21076b15'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368314'>#368314</a></li>
</ul>
<h3><a name='kwordquiz' href='https://cgit.kde.org/kwordquiz.git'>kwordquiz</a> <a href='#kwordquiz' onclick='toggle("ulkwordquiz", this)'>[Show]</a></h3>
<ul id='ulkwordquiz' style='display: none'>
<li>Switch from kdelibs4 sounds to Plasma 5 sounds. <a href='http://commits.kde.org/kwordquiz/a91d084881c06885539b6d3d0815892939c1f006'>Commit.</a> </li>
</ul>
<h3><a name='libkdegames' href='https://cgit.kde.org/libkdegames.git'>libkdegames</a> <a href='#libkdegames' onclick='toggle("ullibkdegames", this)'>[Show]</a></h3>
<ul id='ullibkdegames' style='display: none'>
<li>Fix theme selector theme lookup. <a href='http://commits.kde.org/libkdegames/42897f89a03c2f46ff388df688f9a23675688908'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129238'>#129238</a></li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Show]</a></h3>
<ul id='ullibksieve' style='display: none'>
<li>Fix parsing error when we have < or > in comment. <a href='http://commits.kde.org/libksieve/916f2dfb3b00c2dad0e4678a09cadb45ccfa8e92'>Commit.</a> </li>
<li>Add failed script. <a href='http://commits.kde.org/libksieve/125d96a8608824a97182a7526566a059058035ba'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Show]</a></h3>
<ul id='ullokalize' style='display: none'>
<li>Write dates with european-arabic numbers instead with arabic-arabic numbers when on an arabic locale. <a href='http://commits.kde.org/lokalize/52026c5bcdfd9a32184f4a22cb1c80562d59a6e6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129115'>#129115</a></li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Show]</a></h3>
<ul id='ulmailcommon' style='display: none'>
<li>Remove double margin. <a href='http://commits.kde.org/mailcommon/a63be6918d29becbbae28bdc239cb5bd776d0512'>Commit.</a> </li>
<li>Fix crash when we open dialog. <a href='http://commits.kde.org/mailcommon/66fb9b8a048afabbaaedbc863b742eee6ecaa6c6'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Fix broken check for name & note field existance in ShpRunner. <a href='http://commits.kde.org/marble/285d4e8131145b6f98a94906e4543758c4e66006'>Commit.</a> </li>
<li>Update links to wiki pages. <a href='http://commits.kde.org/marble/e0f82e593793cd161bff5468a4c5d41fa840f30e'>Commit.</a> </li>
<li>Version bump to 0.25.3 (stable release). <a href='http://commits.kde.org/marble/5d3012acd2416e2017f4b2cd38bd1f80c2eca79f'>Commit.</a> </li>
<li>Fix popupitem sizes to match (hardcoded layout sizes of) content. <a href='http://commits.kde.org/marble/07e9ebc140a771e8f6bdb5ff7db759dfa6e1e0b3'>Commit.</a> </li>
<li>Fix string puzzle with labels in statusbar. <a href='http://commits.kde.org/marble/1c5fd888051b9b40a85b47b4ae1f7ad0ff73ac6e'>Commit.</a> </li>
<li>Remove code of past-end-of-line marble-mobile. <a href='http://commits.kde.org/marble/5f0b0b99fa60363c9ec07d44a7ac19fd595ef740'>Commit.</a> </li>
<li>Revert "do not install extra license files". <a href='http://commits.kde.org/marble/24f25c94c659d6a6220c85fd813c5e6deb9ad22f'>Commit.</a> </li>
<li>Extract UI string also from PluginInterface.h (& set non-QObject context). <a href='http://commits.kde.org/marble/f063bf64eaddf39df42b6ec451af1d2351a5b7c9'>Commit.</a> </li>
<li>Initial fix for webpopup appearance. <a href='http://commits.kde.org/marble/9065c71d1f20784c204c4250fd287a459aeeb7f8'>Commit.</a> </li>
<li>Make labels in statusbar localizable, use localized ones for size calculation. <a href='http://commits.kde.org/marble/f14e3c953aaa97eec90d17060fd7f08703ddf43a'>Commit.</a> </li>
<li>Align elevation edit widget to the top in EditPlacemarkDialog. <a href='http://commits.kde.org/marble/7162c3756dc9c68c47d2f52c431ee7f4d9b5df68'>Commit.</a> </li>
<li>Fix memory leak: delete OsmRelationEditorDialog also on cancelling. <a href='http://commits.kde.org/marble/e42ad090fd6fc871308001ac2a4f89dedb5d189a'>Commit.</a> </li>
<li>Fix: make Cancel button work in OsmRelationEditorDialog. <a href='http://commits.kde.org/marble/237afbd50f2e53ab6e169869607f2f68a0ed6570'>Commit.</a> </li>
<li>Fix: use currentIndex instead of comparing currentText against english version. <a href='http://commits.kde.org/marble/a65c523e09267aff3ddac74677fbcba3dc75d998'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Don't ask twice overwrite filename. <a href='http://commits.kde.org/messagelib/e2a629bd5a0f08f85509620db7bf0562a6f2d10c'>Commit.</a> </li>
<li>Remove redundant calls to setContextMenuPolicy(Qt::DefaultContextMenu) in test program. <a href='http://commits.kde.org/messagelib/4814202f536fafa7c709c2ecdcffed46d45e67ba'>Commit.</a> </li>
<li>Fix: clicking on HTML Side Bar can no longer toggle html/plain text. <a href='http://commits.kde.org/messagelib/925145c7bb7c5a44bf5d9ad414179432d7c9aafa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367997'>#367997</a>. Code review <a href='https://git.reviewboard.kde.org/r/129266'>#129266</a></li>
<li>Fix Bug 363721 - Kmail Message Headers Interpretting UTC Time after latest update. <a href='http://commits.kde.org/messagelib/e6796719c5de570338689c4258ec722a3474a7ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363721'>#363721</a></li>
<li>Fix Bug 369392 - Custom date display for message list not applied until restart. <a href='http://commits.kde.org/messagelib/44f6cf6293776590ca2dae82aefe86a0f33fc5b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369392'>#369392</a></li>
<li>Fix Bug 368498 - Kmail does not open links with target="_blank". <a href='http://commits.kde.org/messagelib/1109b48ef544914353c87e1eb47cf08bafceeabc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368498'>#368498</a></li>
<li>Fix: Attachments are included as plain text in replied mails. <a href='http://commits.kde.org/messagelib/bf247fc9e802a39b60c8828019401569f6ad99fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368060'>#368060</a></li>
<li>Add templateparser test for reply mails. <a href='http://commits.kde.org/messagelib/aa7f10c06fcad3c0d90395405f439b1748f78333'>Commit.</a> </li>
<li>Fix Bug 370452 - Message viewer: New citation renderer swallows newlines in citation. <a href='http://commits.kde.org/messagelib/ca0982e07525ebe068a65ed89e4ae1d95834e3fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370452'>#370452</a></li>
<li>Add shit+w to close current tab. <a href='http://commits.kde.org/messagelib/5382d78b7412a304bfe35569ad4b52da8d233826'>Commit.</a> </li>
<li>Fix Bug 369072 - Regression kMail 5.3.x: Plain-text quoting display is messed up. <a href='http://commits.kde.org/messagelib/9c764eaa3995be89b3bcb94822ec9446d47a9b1c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369072'>#369072</a></li>
<li>Not necessary to store in private class. <a href='http://commits.kde.org/messagelib/dc1955de42c5c04ef3b9184609bb01e697acde55'>Commit.</a> </li>
<li>Use path and not data for icons. It's more easy to create autotest. <a href='http://commits.kde.org/messagelib/f4755ee0a6e757fc2d5aec9a1e01dc0fd5e13f62'>Commit.</a> </li>
<li>Add autotest for bug-369072. <a href='http://commits.kde.org/messagelib/b0a24038934c35740807e74b243510a967feaeb7'>Commit.</a> </li>
<li>Allow to test quote renderer. <a href='http://commits.kde.org/messagelib/4b318c16148c63a01f36b3b2a79fd3a49e53c581'>Commit.</a> </li>
<li>Allow to test rendered with quote marker. <a href='http://commits.kde.org/messagelib/3a7bef2a60f07aa8a4c823826f8cb17bfad81fe5'>Commit.</a> </li>
<li>Start to fix quoting display. <a href='http://commits.kde.org/messagelib/c812e79789d6991fc011d8e93b46efec89ac4544'>Commit.</a> See bug <a href='https://bugs.kde.org/369072'>#369072</a></li>
<li>Don't show entry when they are empty. <a href='http://commits.kde.org/messagelib/b660502d1e4bc077416e288c7fbc016ac6c9cf74'>Commit.</a> </li>
<li>Fix Bug 369376 - Show date in message view part. <a href='http://commits.kde.org/messagelib/a7c1b75aa36bd5a093700c3b50b1b74a75d2470b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369376'>#369376</a></li>
<li>Bug 369572 - Regression kMail 5.3.1 QtWebEngine: HTML mail printing not possible if plain text view is preferred over HTML. <a href='http://commits.kde.org/messagelib/8c304e1ed6c63d33c4dc63991298a7eb1c72b93e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369572'>#369572</a></li>
</ul>
<h3><a name='minuet' href='https://cgit.kde.org/minuet.git'>minuet</a> <a href='#minuet' onclick='toggle("ulminuet", this)'>[Show]</a></h3>
<ul id='ulminuet' style='display: none'>
<li>Add an arcconfig so that one can easily arc diff. <a href='http://commits.kde.org/minuet/458dea76ddb5ab0cf95b7b4cbd82555ebdc12576'>Commit.</a> </li>
<li>Use the METAINFODIR variable from ECM instead of hardcoding appdata paths. <a href='http://commits.kde.org/minuet/ce10cb30786106bd4c7a5f16992580e0b0bf224d'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Add a unit test for the kpart, for bug 365523. <a href='http://commits.kde.org/okteta/cdebba645086fd36296eec54b651005c757d7481'>Commit.</a> </li>
<li>Fix crash in KPart: remove current model from controllers before deleting it. <a href='http://commits.kde.org/okteta/1c14d2d0545c14097e3e443a0ddb5fcfda6e5cc0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365523'>#365523</a></li>
<li>Bump version to 0.20.3. <a href='http://commits.kde.org/okteta/9eafa4b0396c4d7581e8a22c0f88fb8e5e739a00'>Commit.</a> </li>
<li>Enable HiDpi pixmaps with the Okteta app. <a href='http://commits.kde.org/okteta/ea7f30339efbc2f9ee0c2c420c8c5aa1f83591cb'>Commit.</a> </li>
<li>Workaround slowness of search jobs & Co. by buggy QEventDispatcherGlib. <a href='http://commits.kde.org/okteta/c31a665b8fc4be456fd29fc3303b81e21a1cbd87'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365047'>#365047</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'On moving widgets not selected indirect associations are only moved on mouse release'. <a href='http://commits.kde.org/umbrello/a7bda8a813aa23fe9d3299a1d76105806553a778'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372176'>#372176</a></li>
<li>Fix 'Moving several objects does not move lines between them'. <a href='http://commits.kde.org/umbrello/b410ab727ae1ebf7f864e136dda796a580aaddaf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372107'>#372107</a></li>
<li>Add wrapper for WidgetBase::m_baseType and enforce valid range. <a href='http://commits.kde.org/umbrello/2ed4f7baedb1766452a5b08ffaf882ab09dcdbcf'>Commit.</a> See bug <a href='https://bugs.kde.org/371990'>#371990</a></li>
<li>Fix 'Lost class and interface names when open the project'. <a href='http://commits.kde.org/umbrello/7c761dac4ba3a90e4cfce99d653992f77d5c7147'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371495'>#371495</a></li>
<li>Fix 'Umbrello does not import c++ datatype references'. <a href='http://commits.kde.org/umbrello/371b78c2d2cfffe78a5a9440ef0e56bed3180fa0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371400'>#371400</a></li>
<li>Codegenerators: add enum literal value. <a href='http://commits.kde.org/umbrello/10e0bb87f2c7932f6f2e48bf7874c3934a7c3ad6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127001'>#127001</a></li>
</ul>