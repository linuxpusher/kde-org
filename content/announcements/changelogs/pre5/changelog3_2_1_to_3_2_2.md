---
aliases:
- ../changelog3_2_1_to_3_2_2
hidden: true
title: KDE 3.2.1 to 3.2.2 Changelog
---

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.2.1 and 3.2.2 releases.
Nevertheless it should be considered as incomplete.
</p>

<h3>arts</h3><ul>
<li>ALSA: Make aRts work through DMix-devices (<a href="http://bugs.kde.org/show_bug.cgi?id=70802">#70802</a>)</li>
</ul>

<h3>kdelibs</h3><ul>
<li>Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76618">CR at end of line in source file</a></li>
<li>Make the kalyptus script install with the executable bit set</li>
<li>Respect "EffectsEnabled" for tearoff handle setting</li>
</ul>

<h3>kdeaddons</h3><ul>
<li>konqueror/uachanger: List of possible Konqueror browser user agent strings are now sorted</li>
</ul>

<h3>kdeadmin</h3><ul>
</ul>

<h3>kdeartwork</h3><ul>
</ul>

<h3>kdebase</h3><ul>
<li>kappfinder: Fixed button order</li>
<li>kdesktop: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=74244">switching desktops incurrs (seemingly) unrequired cpu load</a></li>
<li>konqueror: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=67689">textposition of bookmark toolbar not saved</a></li>
<li>konqueror: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=78049">[testcase] HTTP-EQUIV=&quot;refresh&quot; not working with weird formating. </a></li>
<li>konqueror (khtml): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=71162">When dragging a URL link with an image, the image is pasted and not the link URL</a></li>
<li>konqueror (khtml): Basic support for the <a href="http://bugs.kde.org/show_bug.cgi?id=45788">HTML ACCESSKEY attribute.</a></li>
<li>konqueror (khtml): Ctrl only suspends Shift+arrows scrolling.</a></li>
<li>konqueror (khtml event): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=65553">dragging an attachment icon to desktop copies icon!</a></li>
<li>konqueror: No "Actions" submenu with only one entry</li>
<li>konqueror: Shift inverses "open tabs in background" setting for Ctrl+left mouse button and "Open in New Tab" in context menu</li>
<li>kicker: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=43936">tooltips sometimes not disappearing.</a></li>
<li>konsole: Added support for Scroll Up (SU) and Scroll Down (SD)</li>
<li>konsole: Better compatibility with xterm/XF86 4.4.0</li>
<li>konsole: Fixed sending of large blocks</li>
<li>konsole: Show session menu when you move the mouse. (<a href="http://bugs.kde.org/show_bug.cgi?id=77873">BR77873</a>)
<li>Make various scripts install with the executable bit set</li>
<li>Fix various libraries&apos; install locations and versioning</li>
</ul>

<h3>kdebindings</h3><ul>
<li>kdejava and qtjava: Don&apos;t install internal headers</li>
</ul>

<h3>kdeedu</h3><ul>
<li>Kig: fix some untranslated UI strings.</li>
<li>kstars: fixes to LX200 telescope driver.</li>
<li>kstars: fixed some broken URLs for object-specific links.</li>
</ul>

<ul>
</ul>

<h3>kdegames</h3><ul>
<li>kpat: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=22630">Highscores</a></li>
<li>kpat: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72659">[PATCH] Remove deprecated KLineEditDlg</a></li>
<li>kpat: Disable the Hint/Demo/Redeal actions in case a game cannot be won anymore</li>
<li>artwork: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=40738">KFoulEggs icon</a></li>
<li>docs: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72223">Error in kpatience documentation</a></li>
<li>docs: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72349">Errors in kreversi doc</a></li>
<li>docs: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72379">typo in ktron documentation</a></li>
<li>configure: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77669">Extraneous message in admin/Makefile.common using --prefix</a></li>
</ul>

<h3>kdegraphics</h3><ul>
<li>kiconedit: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=61921">[PATCH] Adds large current color viewer to palette toolbar</a></li>
<li>kiconedit: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=61935">Visual difference between a gray and transparent pixel</a></li>
<li>kpdf: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=70947">KPDF can&apos;t open files whose names have non-ASCII letters</a></li>
<li>ksvg: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=75065">Transformed symbols sometimes disappear</a></li>
<li>configure: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77669">Extraneous message in admin/Makefile.common using --prefix</a></li>
<li>kghostview: Fix the executable linking against the part, by adding a common library.</li>
</ul>

<h3>kdemultimedia</h3><ul>
<li>KRec: Corrected calculation of position in KB.</li>
<li>kio (audiocd): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=39145">kio-audiocd gives no error message when device permissions are wrong</a></li>
<li>kio (audiocd): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=63174">kaudiocreator error &quot;/By Track/Track 01.wav&quot; should start with &quot;audiocd:/&quot;</a></li>
<li>kmid: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=58083">KMid doesn&apos;t support international characters (AT ALL; I&apos;ve checked source code and there are errors)</a></li>
<li>juk: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=63770">Not adding m3u, pls... lists when refreshing directory</a></li>
<li>juk: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=67598">Show Total running time of selection/playlist</a></li>
<li>juk: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=68244">extra option (random play) in popup-window</a></li>
<li>juk: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72906">TimeLeft widget listens to too many signals</a></li>
<li>juk: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=73998">Juk crashed when I was retagging a bunch of songs and started to play with search</a></li>
<li>Fix <a href="http://bugs.kde.org/show_bug.cgi?id=74403">FLAC dependency isn&apos;t mentioned anywhere and can cause compilation errors</a></li>
<li>Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76477">kdemultimedia build fails with parallel make</a></li>
<li>configure: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77669">Extraneous message in admin/Makefile.common using --prefix</a></li>
<li>kmix: Fix the executable linking against the part, by adding a common library.</li>
</ul>

<h3>kdenetwork</h3><ul>
<li>kopete (Cryptography Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=54743">enable/disable encryption per message</a></li>
<li>kopete: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=55932">suppression of buddy notification on connection</a></li>
<li>kopete (Jabber Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=63425">resource identifiers truncated from Jabber &quot;to&quot; addresses</a></li>
<li>kopete (Jabber Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=63588">Packets from foreign clients are duplicated</a></li>
<li>kopete (Main Application): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=67523">isReachable() is not called when clicking on the contact icon</a></li>
<li>kopete (Jabber Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=69396">String bug in account editor</a></li>
<li>kopete (ICQ and AIM Plugins): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=71654">[PATCH] ICQ User Info for temporary contacts is not automatically loaded</a></li>
<li>kopete: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72410">Refresh icon cache when KIconLoader settings change</a></li>
<li>kopete (MSN Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72417">&quot;This user has me on his contact list&quot; is inaccurate</a></li>
<li>kopete (IRC Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72526">can&apos;t log into ANY IRC server</a></li>
<li>kopete: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76323">no mainWindow restore on exec kopete</a></li>
<li>kopete (Jabber Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76441">Registering new Account dialog in Jabber does not update username</a></li>
<li>kopete (MSN Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76520">Kopete logs into MSN, connects and pops up current contacts and mail, and then disconnects.  It will not stay logged in.</a></li>
<li>kopete (Main Application): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76570">Selecting several contacts and dragging them only moves one</a></li>
<li>kopete (SMS Plugin): Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77334">sms sending blocked by kopete determining user is &apos;offline&apos;</a></li>
<li>kopete: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77593">Type in Kopete handbook</a></li>
<li>kpf: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=55973">Incorrect mimetypes for css and other fileformats.</a></li>
<li>krdc: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=69637">KRDC does not send key clicks to the server until &quot;special keys&quot; is used</a></li>
<li>knewsticker: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77693">SIGFPE crash when scrolling speed is set to 1</a></li>
<li>configure: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77669">Extraneous message in admin/Makefile.common using --prefix</a></li>
<li>configure: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=78207">The fix of the bug 72081, reported on 3.1.94, is missing in 3.2.1</a></li>
<li>kwifimanager: make it compile with libiw-dev (27+)</li>
<li>kwifimanager: auto-scaling statistics window, should now work for every chipset out there</li>
<li>kwifimanager: control center module saves settings</li>
<li>Make various scripts install with the executable bit set</li>
</ul>

<a name="pim"><h3>kdepim</h3></a>

<ul>
<li>kalarm: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=78179">Ctrl+Q must QUIT the application and not hide it to system tray</a></li>
<li>kalarm: Update time entry field after editing when mouse cursor leaves it.</li>
<li>kalarm: Prevent undeleted recurring alarms being triggered immediately.</li>
<li>kalarm: Do not allow alarms to be undeleted if they are completely expired.</li>
<li>kmail: Improved look with other styles than Keramik.</li>
<li>KNode: Don't save accounts before they have a valid id (<a href="http://bugs.kde.org/show_bug.cgi?id=70539">#70539</a>)</li>
<li>KNotes:
<ul>
<li>fixed <a href="http://bugs.kde.org/show_bug.cgi?id=65744">#65744</a>,
          <a href="http://bugs.kde.org/show_bug.cgi?id=76034">#76034</a>,
          <a href="http://bugs.kde.org/show_bug.cgi?id=77783">#77783</a>:
    session management: properly restore note windows on KDE startup</li>
<li>fixed <a href="http://bugs.kde.org/show_bug.cgi?id=72888">#72888</a>,
          <a href="http://bugs.kde.org/show_bug.cgi?id=73404">#73404</a>,
          <a href="http://bugs.kde.org/show_bug.cgi?id=75558">#75558</a>,
          <a href="http://bugs.kde.org/show_bug.cgi?id=78292">#78292</a>:
    much more robust conversion of old config files, now hopefully absolutely no black notes anymore (grave bug)</li>
<li>finished rich text support</li>
<li>fixed Tab key handling in RT mode: do not give away focus to the font combo box</li>
<li>fixed #75793: <a href="http://bugs.kde.org/show_bug.cgi?id=75793">moved the toolbar to the bottom of the note to avoid jumping of the content</a></li>
<li>make KNotes interpret the text as rich text when switching from pain text to rich text</li>
<li>fixed bug that made the html source show up in a text note when switching from rich text to
    plain text and then restarting KNotes</li>
<li>make mailing a note possible again at all - KMail now needs --body and not --msg as argument</li>
<li>convert rich text notes to plain text before emailing them</li>
<li>fixed #60841: <a href="http://bugs.kde.org/show_bug.cgi?id=60841">include the title as subject by default when mailing a note</a></li>
<li>fixed a bug that changes in the note default configuration will never be used</li>
<li>fixed #68127: <a href="http://bugs.kde.org/show_bug.cgi?id=68127">notes are not really deleted if deleting is the last action before quitting KNotes</a></li>
<li>KNotes is now network enabled regarding config files: only use KIO::NetAccess for file handling</li>
<li>fixed menu update after a color change of a note</li>
<li>fixed jumping to another desktop when opening the global config dialog when there are no notes on the current desktop</li>
<li>fixed #72657: <a href="http://bugs.kde.org/show_bug.cgi?id=72657">restore and handle skipTaskbar property properly</a></li>
<li>fixed #74469: <a href="http://bugs.kde.org/show_bug.cgi?id=74469">RMB menu locks whole desktop if opened on a modified note</a></li>
<li>fixed #74558: <a href="http://bugs.kde.org/show_bug.cgi?id=74558">do not crash every second time a note is saved</a></li>
<li>fixed #71685: <a href="http://bugs.kde.org/show_bug.cgi?id=71685">encode '\' - it was possible to write control sequences in KNotes, e.g. \n.</a></li>
<li>fixed <a href="http://bugs.kde.org/show_bug.cgi?id=75507">#75507</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=76126">#76126</a>: handle (load) tabs correctly</li>
<li>no attachments in the journals anymore, the config file names have always been the uid of the note anyway</li>
<li>use KConfig XT for default values</li>
<li>use XML GUI for all actions</li>
</ul>
</li>
<li>Make various scripts install with the executable bit set</li>
</ul>

<h3>kdesdk</h3><ul>
<li>cervisia: Partly fix <a href="http://bugs.kde.org/show_bug.cgi?id=77440">Don't execute shell scripts when the user used the edit file action</a></li>
<li>cervisia: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=78800">Look harder whether a directory really is under CVS control</a></li>
<li>configure: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77669">Extraneous message in admin/Makefile.common using --prefix</a></li>
<li>Umbrello: aggregation/composition &amp; inheritance hierachy sometimes (<a href="http://bugs.kde.org/show_bug.cgi?id=72615">#72615</a>)</li>
<li>Umbrello: Unable to change the specification of an operation (<a href="http://bugs.kde.org/show_bug.cgi?id=73926">#73926</a>)</li>
<li>Umbrello: "New operation" works the wrong way around in collaboration diagrams (<a href="http://bugs.kde.org/show_bug.cgi?id=76506">#76506</a>)</li>
<li>Umbrello: SQL code generator generates "circular reference" foreign keys (<a href="http://bugs.kde.org/show_bug.cgi?id=77377">#77377</a>)</li>
<li>Umbrello: Drag-n-drop uses associations incorrectly (<a href="http://bugs.kde.org/show_bug.cgi?id=78192">#78192</a>)</li>
<li>Umbrello: code generation omits many operations (<a href="http://bugs.kde.org/show_bug.cgi?id=78525">#78525</a>)</li>
<li>Umbrello: compilation for Tru64 fixed</li>
<li>Umbrello: fix loading of XMI files</li>
<li>Umbrello: fix backspace handling in documentation window</li>
<li>Umbrello: support for package nesting on CPP import</li>
</ul>

<h3>kdetoys</h3><ul>
<li>configure: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77669">Extraneous message in admin/Makefile.common using --prefix</a></li>
</ul>

<h3>kdeutils</h3><ul>
<li>kwalletmanager: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=71142">no popup for kwallet in system tray</a></li>
<li>kwalletmanager: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=71143">kwallet in system tray does not provide configure option</a></li>
<li>kwalletmanager: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=71851">&quot;Launch wallet manager&quot; in kwalletmanager prefs does not make sense</a></li>
<li>kwalletmanager: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=73894">When changing a value the &apos;Save&apos; button is not activated</a></li>
<li>kgpg: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=75440">kgpg exports gpg warnings within public key</a></li>
<li>kgpg: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76388">not possible to store keys/config in a different location</a></li>
<li>kgpg: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76427">kgpg editor doesn&apos;t use KDE&apos;s default fixed width font. Breaks layout</a></li>
<li>kgpg: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=78178">Qtrl+Q must QUIT the application and not hide it to system tray icon</a></li>
<li>configure: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77669">Extraneous message in admin/Makefile.common using --prefix</a></li>
</ul>

<h3>quanta</h3><ul>
<li>VPL: don't lose the comment text from inside a comment</li>
<li>make removal of top folders added with "New Top Folder" possible [<a href="http://bugs.kde.org/show_bug.cgi?id=76498">#76498</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=76573">#76573</a>]</li>
<li>never crash when invoking the CSS editor on an empty document</li>
<li>don't crash when invoking the CSS editor after a &#060;style> without the closing &#060;/style></li>
<li>other CSS editor invocation fixes</li>
<li>don't change the tab name if saving under a new name failed</li>
<li>use the correct quotation and case when inserting a link to a file from the treeviews [<a href="http://bugs.kde.org/show_bug.cgi?id=76663">#76663</a>]</li>
<li>fix preview when preview prefix is used</li>
<li>remove the &#060; and > from the misc. tag</li>
<li>don't crash when parsing an included file [<a href="http://bugs.kde.org/show_bug.cgi?id=76478">#76478</a>]</li>
<li>don't break the opening tag when some new tag is pasted inside an existing one</li>
<li>don't insert the closing tag for a Tag action if it is not requested to do so</li>
<li>performance improvement: parse the included files less often</li>
</ul>

<h3>kdevelop</h3><ul>
<li>Fixed: A case where selected items that disappeared due to filesystem events could crash the FileTree.</li>
<li>Fixed: Several language parsers were updated to use QRegExp instead of the deprecated KRegExp. <a href=""http://bugs.kde.org/show_bug.cgi?id=73754">#73754</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=63973">#63973</a> </li>
<li>Fixed: <a href="http://bugs.kde.org/show_bug.cgi?id=74570">74570: Custom makefiles project doesn't support "GNUmakefile" file name</a> (Don't try to outguess make.)</li>
<li>Fixed: <a href="http://bugs.kde.org/show_bug.cgi?id=75172">75172: Changing projects with unsaved work</a> (Don't set recent project marker when user cancels project change.)</li>
<li>Fixed: <a href="http://bugs.kde.org/show_bug.cgi?id=75943">75943: Crash during directory scan</a> (Don't crash when we don't have permission to read a directory.)</li>
<li>Fixed: <a href="http://bugs.kde.org/show_bug.cgi?id=77278">77278: Grep component fails to search with spaces in path</a> </li>
<li>Fixed: <a href="http://bugs.kde.org/show_bug.cgi?id=78776">78776: Crash in CustomProjectPart::populateProject populating new project</a> </li>
</ul>