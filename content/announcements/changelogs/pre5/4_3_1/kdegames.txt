------------------------------------------------------------------------
r1006966 | kleag | 2009-08-04 19:51:32 +0000 (Tue, 04 Aug 2009) | 1 line

Corrects bug 191551
------------------------------------------------------------------------
r1007236 | lueck | 2009-08-05 11:00:32 +0000 (Wed, 05 Aug 2009) | 1 line

fix i18n bug: the translation catalog has to be inserted before showing the window
------------------------------------------------------------------------
r1007579 | coates | 2009-08-06 00:51:18 +0000 (Thu, 06 Aug 2009) | 2 lines

Fixed a problem with the implementation of the vaporizer. The robots should *not* get a turn to move after the vaporizer is used.

------------------------------------------------------------------------
r1009382 | coates | 2009-08-09 21:15:58 +0000 (Sun, 09 Aug 2009) | 2 lines

Change the icon for the "Drop" action from "legalmoves" to "game-endturn". It turns out the "legalmoves" icon is provided by KReversi, so if distros package the games individually, this icon can break. "game-endturn" isn't the best icon for the task, but it's certainly better than a broken one.

------------------------------------------------------------------------
r1010046 | coates | 2009-08-11 14:00:19 +0000 (Tue, 11 Aug 2009) | 6 lines

Backport of r1010036.

Correct yet another issue with shortcut handling. The movement actions were being stored in a separate KActionCollection so that they could be shown in the Configure Shortcuts dialog and not in the Configure Toolbars dialog. It turns out this broke a couple things. Changes to these shortcuts weren't being saved between sessions and the new shortcut scheme functionality was crashing. Now, for simplicity's sake all actions are stored in the same collection.

CCBUG:202468

------------------------------------------------------------------------
r1010640 | scripty | 2009-08-13 03:16:21 +0000 (Thu, 13 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011223 | scripty | 2009-08-14 03:20:36 +0000 (Fri, 14 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011981 | aacid | 2009-08-16 14:38:29 +0000 (Sun, 16 Aug 2009) | 5 lines

backport r1011979 | aacid | 2009-08-16 16:30:25 +0200 (Sun, 16 Aug 2009) | 3 lines

QGraphicsScene does not like items being deleted without being removed and that cuases lots of redraws
BUGS: 199354

------------------------------------------------------------------------
r1012029 | aacid | 2009-08-16 16:42:36 +0000 (Sun, 16 Aug 2009) | 3 lines

backport optimizations from trunk
BUG: 204061

------------------------------------------------------------------------
r1012150 | aacid | 2009-08-16 23:12:11 +0000 (Sun, 16 Aug 2009) | 4 lines

Backport r1012148 | aacid | 2009-08-17 01:10:47 +0200 (Mon, 17 Aug 2009) | 2 lines

Another optimization, no need to call linesFromSquare if countBorderLines already does it

------------------------------------------------------------------------
r1012789 | scripty | 2009-08-18 03:20:24 +0000 (Tue, 18 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1013733 | coolo | 2009-08-20 16:10:22 +0000 (Thu, 20 Aug 2009) | 2 lines

if this goes wrong, it's all david's fault

------------------------------------------------------------------------
r1014494 | scripty | 2009-08-23 03:27:05 +0000 (Sun, 23 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1015171 | aacid | 2009-08-24 18:58:49 +0000 (Mon, 24 Aug 2009) | 2 lines

Add some i18n here, still puzzle like

------------------------------------------------------------------------
r1015640 | scripty | 2009-08-26 02:50:54 +0000 (Wed, 26 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1015838 | ianw | 2009-08-26 12:28:24 +0000 (Wed, 26 Aug 2009) | 1 line

Fix some problems in the record/replay feature.
------------------------------------------------------------------------
r1015841 | ianw | 2009-08-26 12:30:29 +0000 (Wed, 26 Aug 2009) | 1 line

Fix a typo.
------------------------------------------------------------------------
r1016199 | scripty | 2009-08-27 03:36:19 +0000 (Thu, 27 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
