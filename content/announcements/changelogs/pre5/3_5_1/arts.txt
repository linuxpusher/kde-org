2005-12-01 19:45 +0000 [r484751]  mueller

	* branches/arts/1.5/arts/x11/x11globalcomm_impl.cc: fix strict
	  aliasing warning

2005-12-22 23:20 +0000 [r490743]  adridg

	* branches/arts/1.5/arts/mcop/mcoputils.cc: Interesting how you can
	  use *printf() functions without a stdio.h

2006-01-19 15:56 +0000 [r500182]  coolo

	* branches/arts/1.5/arts/configure.in.in: updating version

