------------------------------------------------------------------------
r1168440 | pino | 2010-08-26 17:38:52 +0100 (dj, 26 ago 2010) | 3 lines

bump okular version to 0.11.1
bump patch version of combickbook, plucker, and fax generators, as they got one fix each

------------------------------------------------------------------------
r1167958 | jmueller | 2010-08-25 20:36:48 +0100 (dc, 25 ago 2010) | 1 line

Fix red tint of dng files created from canon raws. Use the minimum of the per channel black points introduced with LibRaw 0.10 as overall blackpoint.
------------------------------------------------------------------------
r1167827 | mitchell | 2010-08-25 13:33:02 +0100 (dc, 25 ago 2010) | 1 line

Patch KDE Security Advisory 2010-08-25-1 and CVE-2010-2575
------------------------------------------------------------------------
r1165381 | cgilles | 2010-08-19 08:36:36 +0100 (dj, 19 ago 2010) | 2 lines

backport commit #1165379 from trunk

------------------------------------------------------------------------
r1164873 | cgilles | 2010-08-17 21:32:08 +0100 (dt, 17 ago 2010) | 2 lines

backport XMP support from trunk

------------------------------------------------------------------------
r1164203 | cgilles | 2010-08-16 09:42:26 +0100 (dl, 16 ago 2010) | 2 lines

backport commit #1164067

------------------------------------------------------------------------
r1163988 | cgilles | 2010-08-15 14:43:56 +0100 (dg, 15 ago 2010) | 2 lines

backport commit #1163982 to KDE 4.5 branch

------------------------------------------------------------------------
r1163569 | aacid | 2010-08-14 13:00:24 +0100 (ds, 14 ago 2010) | 6 lines

Backport r1163568 | aacid | 2010-08-14 12:59:49 +0100 (Sat, 14 Aug 2010) | 3 lines

make sure we wrap correctly when zoooming too
BUGS: 247797
FIXED-IN: 4.5.1

------------------------------------------------------------------------
r1162804 | aacid | 2010-08-12 19:07:42 +0100 (dj, 12 ago 2010) | 4 lines

backport r1162803 | aacid | 2010-08-12 19:07:15 +0100 (Thu, 12 Aug 2010) | 2 lines

Typo fix by Radu Voicilas

------------------------------------------------------------------------
r1162337 | aacid | 2010-08-11 22:07:11 +0100 (dc, 11 ago 2010) | 4 lines

backport r1162332 | aacid | 2010-08-11 22:01:33 +0100 (Wed, 11 Aug 2010) | 2 lines

fix docu

------------------------------------------------------------------------
r1161860 | pino | 2010-08-11 00:50:12 +0100 (dc, 11 ago 2010) | 5 lines

when archiving, resolve the symlink if the document path is that

BUG: 245243
FIXED-IN: 4.5.1

------------------------------------------------------------------------
r1161850 | pino | 2010-08-11 00:12:39 +0100 (dc, 11 ago 2010) | 5 lines

save the original "data" pointer and delete[] the copy
this way, if we move "data" (for ghostscript/PC research faxes -- see FAXMAGIC) we are always sure to free the right memory buffer
BUG: 247300
FIXED-IN: 4.5.1

------------------------------------------------------------------------
r1160617 | pino | 2010-08-08 15:52:45 +0100 (dg, 08 ago 2010) | 5 lines

do not process the user events while executing the rar processes

CCBUG: 245499
FIXED-IN: 4.5.1

------------------------------------------------------------------------
r1160616 | pino | 2010-08-08 15:52:00 +0100 (dg, 08 ago 2010) | 2 lines

SVN_SILENT remove svn:mergeinfo properties

------------------------------------------------------------------------
