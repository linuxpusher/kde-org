2006-10-10 09:55 +0000 [r594149]  binner

	* branches/KDE/3.5/kdewebdev/kdewebdev.lsm: fix for next sed run

2006-10-10 21:22 +0000 [r594303]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/main.cpp: bump version

2006-10-10 22:10 +0000 [r594318]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.cpp: - fix
	  a crash when clicking in the table empty area - correctly mark
	  the link as timed out - fix the timeout icon

2006-10-10 23:46 +0000 [r594350]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/documentrootdialog.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.cpp:
	  - set the focus on the right widget - don't crash with an empty
	  document root

2006-10-11 15:13 +0000 [r594543]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/dtd/dtdparser.cpp: Give
	  line/column numbers if the DTD we are parsing has errors.

2006-10-18 09:14 +0000 [r596711]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Regression fix: show
	  the Find in Files when KFileReplace is installed. Was broken due
	  to the commit for #132530. CCBUG: 132530

2006-11-24 20:31 +0000 [r607486]  annma

	* branches/KDE/3.5/kdewebdev/doc/quanta/extending-quanta.docbook:
	  CCBUG=137812

2006-11-30 10:22 +0000 [r609347]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/components/debugger/gubed/quantadebuggergubed.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: (char *)0L is not
	  the same as 0, at least on some systems (mine and a freebsd).
	  Fixes crashes in the debugger. BUG: 137483

2006-12-06 20:53 +0000 [r611122]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix crash when
	  editing a <style> CSS area.

2006-12-16 23:35 +0000 [r614245]  lueck

	* branches/KDE/3.5/kdewebdev/doc/xsldbg/credits.docbook: fixed a
	  wrong placed </para> entity

2006-12-19 12:36 +0000 [r614899]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectupload.cpp:
	  Avoid an Abort and Cancel button as it confuses translators.
	  Still not the best, but I missed the message freeze, so I had to
	  use an existing string. If I don't forget I will use a better
	  string in the next release. BUG: 135203

2006-12-23 12:10 +0000 [r615950]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/kommander.kdevelop: Use the
	  correct encoding in the file dialogs. BUG: 138343

2007-01-03 13:11 +0000 [r619457]  schafer

	* branches/KDE/3.5/kdewebdev/kimagemapeditor/kimedialogs.cpp:
	  Exchanged onclick and onDblClick when applying the values of the
	  corresponding fields to the attributes of the html tag. BUG:
	  139548

2007-01-08 09:36 +0000 [r621091]  schafer

	* branches/KDE/3.5/kdewebdev/kimagemapeditor/kimagemapeditor.cpp:
	  Fixed a code line that can crash when the desired font is not
	  installed. Removed the hard-coded font name Luxi Sans and take
	  the default font instead. BUG:125145

2007-01-08 16:02 +0000 [r621313]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectupload.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/dtds.cpp: Don't lose the
	  modifications for the upload profile if this was the first
	  profile in the list and for some reason there is no existing
	  "default" profile. In that case the first one is taken as the
	  default.

2007-01-09 18:21 +0000 [r621763]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectlist.h,
	  branches/KDE/3.5/kdewebdev/quanta/project/rescanprj.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectnewlocal.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp: Fix
	  symlink handling in Project Rescan, New Project adding
	  files/folders. BUG: 138107

2007-01-09 18:57 +0000 [r621772]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/lib/qextfileinfo.h: Fix symlink
	  handling in Project Rescan, New Project adding files/folders.

2007-01-09 19:36 +0000 [r621786]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Do not set the
	  current project mark to a project that could not be opened. BUG:
	  137186

2007-01-09 20:46 +0000 [r621820]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Allow closing of an
	  untitle editor tab when an image is previewed inside. BUG: 134534

2007-01-09 21:01 +0000 [r621827]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/components/csseditor/styleeditor.cpp:
	  Be permissive and allow editing empty tags inside the CSS style
	  editor. Make the CSS button work on Core & i18n tab for
	  not-yet-inserted tags. CCBUG: 135524

2007-01-14 22:44 +0000 [r623503]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix problems with
	  the automatic backup system: - regression fix: make it work, as
	  probably it got broken in 3.5.5 completely - hopefully the final
	  fix for #111049: do not allow growing of quantarc until it
	  crashes Quanta

2007-01-14 23:08 +0000 [r623517]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/VERSION,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h: 3.5.6

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

