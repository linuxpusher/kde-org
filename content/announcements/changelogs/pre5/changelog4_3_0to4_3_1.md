---
aliases:
- ../changelog4_3_0to4_3_1
hidden: true
title: KDE 4.3.1 Changelog
---

<h2>Changes in KDE 4.3.1</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_3_1/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when editing toolbars. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=200815">200815</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013099&amp;view=rev">1013099</a>. </li>
      </ul>
      </div>
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix "Copy file after rename uses old file name" bug. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=195385">195385</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1012564&amp;view=rev">1012564</a>. </li>
        <li class="bugfix ">Renaming a remote file during a copy operation was broken. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204322">204322</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013228&amp;view=rev">1013228</a>. </li>
        <li class="bugfix ">Fix KTar's handling of large files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203414">203414</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013638&amp;view=rev">1013638</a>. </li>
        <li class="bugfix crash">Fix crash when typing a device name that is not in the list. See SVN commit <a href="http://websvn.kde.org/?rev=1016509&amp;view=rev">1016509</a>. </li>
      </ul>
      </div>
      <h4><a name="ksendbugmail">ksendbugmail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Repair sending of bug reports by email for apps not on bugs.kde.org. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182384">182384</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1016323&amp;view=rev">1016323</a> and <a href="http://websvn.kde.org/?rev=1016339&amp;view=rev">1016339</a>. </li>
      </ul>
      </div>
      <h4><a name="klauncher">klauncher</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix klauncher not using translated Name entries to replace %c when launching .desktop files. See SVN commit <a href="http://websvn.kde.org/?rev=1015210&amp;view=rev">1015210</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_3_1/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="search plugins">search plugins</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix grec search url. See SVN commit <a href="http://websvn.kde.org/?rev=1014811&amp;view=rev">1014811</a>. </li>
        <li class="bugfix ">Honor Charset key in web shortcuts. See SVN commit <a href="http://websvn.kde.org/?rev=1015945&amp;view=rev">1015945</a>. </li>
      </ul>
      </div>
      <h4><a name="oxygen style">Oxygen style</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix combo boxes drawing their labels twice. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202701">202701</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1008050&amp;view=rev">1008050</a>. </li>
      </ul>
      </div>
      <h4><a name="kio_fish">kio_fish</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make kio_fish work again (for most people). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=189235">189235</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1010416&amp;view=rev">1010416</a>. </li>
      </ul>
      </div>
      <h4><a name="libkonq">libkonq</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Don't append .desktop when creating a link to URL. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=197840">197840</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1016350&amp;view=rev">1016350</a>. </li>
      </ul>
      </div>
      <h4><a name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when doing "Close all other tabs". Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183486">183486</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1016318&amp;view=rev">1016318</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Ensure that wobbly windows animation finishes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=201244">201244</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1004452&amp;view=rev">1004452</a>. </li>
        <li class="bugfix ">Fix shadows in window decoration preview. See SVN commit <a href="http://websvn.kde.org/?rev=1005232&amp;view=rev">1005232</a>. </li>
        <li class="bugfix ">Show window decoration during fade out. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=201780">201780</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013297&amp;view=rev">1013297</a>. </li>
        <li class="bugfix ">Repaint also the shadow when a window gets repainted. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=201596">201596</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013298&amp;view=rev">1013298</a>. </li>
        <li class="bugfix ">Fix broken window shading with OpenGL compositing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=195593">195593</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013299&amp;view=rev">1013299</a>. </li>
        <li class="bugfix ">Fix incorrect drawing of decoration preview. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203842">203842</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1015363&amp;view=rev">1015363</a>. </li>
        <li class="bugfix ">Fix stretching of windows in wobbly effect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=198559">198559</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1015539&amp;view=rev">1015539</a>. </li>
        <li class="bugfix ">Make opacity rules actually do something. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=167138">167138</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1015801&amp;view=rev">1015801</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_3_1/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kontact.kde.org/kmail/" name="kmail">KMail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make custom font settings for the message list work again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=178402">178402</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1011071&amp;view=rev">1011071</a>. </li>
        <li class="bugfix ">Fix display of some HTML newsletters with images. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=187989">187989</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1005078&amp;view=rev">1005078</a>. </li>
        <li class="bugfix ">Support all parts of a mailto URL from command line. See SVN commit <a href="http://websvn.kde.org/?rev=1005081&amp;view=rev">1005081</a>. </li>
        <li class="bugfix ">Correctly decode dropped mailto URLs. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=138725">138725</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1007476&amp;view=rev">1007476</a>. </li>
        <li class="bugfix ">Improve support for external editors. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169092">169092</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1006445&amp;view=rev">1006445</a>, <a href="http://websvn.kde.org/?rev=1006454&amp;view=rev">1006454</a>, <a href="http://websvn.kde.org/?rev=1006460&amp;view=rev">1006460</a> and <a href="http://websvn.kde.org/?rev=1006762&amp;view=rev">1006762</a>. </li>
        <li class="bugfix ">Correctly add the default domain to email addresses. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=136407">136407</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1006790&amp;view=rev">1006790</a>. </li>
        <li class="bugfix ">Fix rendering problem with the HTML warning. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=137643">137643</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1007463&amp;view=rev">1007463</a>. </li>
        <li class="bugfix ">When switching the identity in the composer, update the signing preference. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=51410">51410</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1007465&amp;view=rev">1007465</a>. </li>
        <li class="bugfix ">Correctly escape the HTML in message tooltips. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=52223">52223</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1007467&amp;view=rev">1007467</a>. </li>
        <li class="bugfix ">Don't show HTML code in the progress manager for some IMAP mails. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204765">204765</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1014683&amp;view=rev">1014683</a>. </li>
        <li class="bugfix ">Fix word wrap when using a custom quote sign. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=55021">55021</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1007468&amp;view=rev">1007468</a>. </li>
        <li class="bugfix ">Set the focus to the correct composer fields when opening it. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=87549">87549</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1007470&amp;view=rev">1007470</a> and <a href="http://websvn.kde.org/?rev=1007472&amp;view=rev">1007472</a>. </li>
        <li class="bugfix ">When pressing the up key in the first line of the editor in the composer, always jump to the subject field. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204780">204780</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1014440&amp;view=rev">1014440</a>. </li>
        <li class="bugfix ">Fix an infinite recursion when switching to a newly created folder. See SVN commits <a href="http://websvn.kde.org/?rev=1008994&amp;view=rev">1008994</a> and <a href="http://websvn.kde.org/?rev=1008995&amp;view=rev">1008995</a>. </li>
        <li class="bugfix ">Make dropping images from KSnapshot work again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202101">202101</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1009398&amp;view=rev">1009398</a>. </li>
        <li class="bugfix ">Make the copy action in the stand-alone reader window work again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203220">203220</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1010579&amp;view=rev">1010579</a>. </li>
        <li class="bugfix ">Show all encodings in the composer again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202294">202294</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1010587&amp;view=rev">1010587</a>. </li>
        <li class="bugfix ">Fix deleting of attachments in encapsulated messages. See SVN commits <a href="http://websvn.kde.org/?rev=1010874&amp;view=rev">1010874</a> and <a href="http://websvn.kde.org/?rev=1010879&amp;view=rev">1010879</a>. </li>
        <li class="bugfix ">Display digest mails correctly. See SVN commit <a href="http://websvn.kde.org/?rev=1011083&amp;view=rev">1011083</a>. </li>
        <li class="bugfix ">Display mailman messages correctly. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=53668">53668</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1011085&amp;view=rev">1011085</a>. </li>
        <li class="bugfix ">Expand the full thread when expanding by shortcut. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182910">182910</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1011919&amp;view=rev">1011919</a>. </li>
        <li class="bugfix ">Ignore subject prefixes when sorting by subject. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203104">203104</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1012121&amp;view=rev">1012121</a>. </li>
        <li class="bugfix ">Show the icon in the header for icon-only columns in the message list. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183517">183517</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1012125&amp;view=rev">1012125</a>. </li>
        <li class="bugfix ">Make grouping by receiver work. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204193">204193</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1012550&amp;view=rev">1012550</a>. </li>
        <li class="bugfix crash">Don't crash when editing a mail with two embedded images in the same line. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204214">204214</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013338&amp;view=rev">1013338</a>. </li>
        <li class="bugfix ">Don't unhide all folders when dragging over a folder in the folder list. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204257">204257</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013402&amp;view=rev">1013402</a>. </li>
        <li class="bugfix ">Correctly parse a date that contains uppercase october. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=150620">150620</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1014199&amp;view=rev">1014199</a>. </li>
      </ul>
      </div>
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Enable the edit alarm dialog OK button only when changes have been made. See SVN commit <a href="http://websvn.kde.org/?rev=1011612&amp;view=rev">1011612</a>. </li>
          <li class="improvement">Shift-Delete now deletes alarms without showing confirmation prompt. See SVN commit <a href="http://websvn.kde.org/?rev=1004271&amp;view=rev">1004271</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when restoring alarms on login. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203957">203957</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1013042&amp;view=rev">1013042</a> and <a href="http://websvn.kde.org/?rev=1013437&amp;view=rev">1013437</a>. </li>
        <li class="bugfix ">Make volume settings work for audio files. See SVN commits <a href="http://websvn.kde.org/?rev=1011655&amp;view=rev">1011655</a> and <a href="http://websvn.kde.org/?rev=1011677&amp;view=rev">1011677</a>. </li>
        <li class="bugfix ">Fix bad email addresses when sending email alarms. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=197496">197496</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1008964&amp;view=rev">1008964</a>. </li>
        <li class="bugfix ">Fix KMail dependent functions not being included in build. See SVN commit <a href="http://websvn.kde.org/?rev=1005723&amp;view=rev">1005723</a>. </li>
        <li class="bugfix ">Improve command line option validation and remove unnecessary restrictions on combining options. See SVN commit <a href="http://websvn.kde.org/?rev=1004268&amp;view=rev">1004268</a>. </li>
        <li class="bugfix ">When selecting a mail in the 'Find Messages' window, correctly update the selection in the message list. See SVN commit <a href="http://websvn.kde.org/?rev=1016458&amp;view=rev">1016458</a>. </li>
        <li class="bugfix ">Fix inconsistent selection behavior in "Find Messages" window. See SVN commit <a href="http://websvn.kde.org/?rev=1016436&amp;view=rev">1016436</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeartwork"><a name="kdeartwork">kdeartwork</a><span class="allsvnchanges"> [ <a href="4_3_1/kdeartwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kscreensaver">KScreenSaver</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make some strings translatable. See SVN commit <a href="http://websvn.kde.org/?rev=1008772&amp;view=rev">1008772</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="4_3_1/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://games.kde.org/game.php?game=ksquares" name="ksquares">ksquares</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make KSquares faster. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=199354">199354</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=204061">204061</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1011981&amp;view=rev">1011981</a> and <a href="http://websvn.kde.org/?rev=1012029&amp;view=rev">1012029</a>. </li>
      </ul>
      </div>
      <h4><a href="http://games.kde.org/game.php?game=ksirk" name="ksirk">ksirk</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make some strings translatable. See SVN commit <a href="http://websvn.kde.org/?rev=1015171&amp;view=rev">1015171</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_3_1/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kolourpaint">kolourpaint</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix opening images with an http url. See SVN commit <a href="http://websvn.kde.org/?rev=1010218&amp;view=rev">1010218</a>. </li>
      </ul>
      </div>
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not start a search again on F3 if we are already running it. See SVN commit <a href="http://websvn.kde.org/?rev=1009823&amp;view=rev">1009823</a>. </li>
        <li class="bugfix ">Make Okular PS part have higher precedence than Karbon so that print preview works. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204925">204925</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1015189&amp;view=rev">1015189</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeplasma-addons"><a name="kdeplasma-addons">kdeplasma-addons</a><span class="allsvnchanges"> [ <a href="4_3_1/kdeplasma-addons.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="magnifique">magnifique</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make the Comment line translatable. See SVN commit <a href="http://websvn.kde.org/?rev=1008840&amp;view=rev">1008840</a>. </li>
      </ul>
      </div>
      <h4><a name="weatherstation">weatherstation</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Correctly format the time string. See SVN commit <a href="http://websvn.kde.org/?rev=1009723&amp;view=rev">1009723</a>. </li>
      </ul>
      </div>
      <h4><a name="libconversion">libconversion</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make sure that applets using the lib (unitconverter) have tranlated units. See SVN commit <a href="http://websvn.kde.org/?rev=1013503&amp;view=rev">1013503</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="4_3_1/kdesdk.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kate">kate</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make strings in the symbol viewer translatable. See SVN commit <a href="http://websvn.kde.org/?rev=1009261&amp;view=rev">1009261</a>. </li>
      </ul>
      </div>
      <h4><a name="kompare">Kompare</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Add some APIs required for KDevelop 4 to the Kompare libraries. See SVN commit <a href="http://websvn.kde.org/?rev=1016107&amp;view=rev">1016107</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Only enable OK in the URL dialog when a URL is entered. See SVN commit <a href="http://websvn.kde.org/?rev=1009995&amp;view=rev">1009995</a>. </li>
        <li class="bugfix ">Fix the radiobuttons for diff format selection. See SVN commit <a href="http://websvn.kde.org/?rev=1009998&amp;view=rev">1009998</a>. </li>
        <li class="bugfix crash">Fix a crash due to incorrect use of iterators. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204880">204880</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1016097&amp;view=rev">1016097</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_3_1/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Respect the short date format set by the user. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=201180">201180</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1002678&amp;view=rev">1002678</a>. </li>
        <li class="bugfix ">Make network actions available when the network state is unknown. See SVN commit <a href="http://websvn.kde.org/?rev=1012454&amp;view=rev">1012454</a>. </li>
        <li class="bugfix ">Improve some context menu descriptions to have proper singular/plural wording. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=200713">200713</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1013345&amp;view=rev">1013345</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/kdelirc" name="kdelirc">kdelirc</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix Handling of profile IDs in kcmlirc Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204611">204611</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1014739&amp;view=rev">1014739</a>. </li>
      </ul>
      </div>
    </div>