commit e83907379219d7ff4fcd8c05f787c4dcd078d27e
Author: Aaron Seigo <aseigo@kde.org>
Date:   Mon Apr 4 14:50:11 2011 +0200

    fix initial players source setting, rid uneeded updateSourceEvent

commit 937c7245f0f4058d2b335d160617c1cdb2398cb7
Author: Andriy Rysin <arysin@gmail.com>
Date:   Sat Apr 2 22:27:37 2011 -0400

    Fix null pointer crash when no rules found; add unit test
    BUG: 269961

commit 9390572f7b97bb9c2fc9b4f141b8886a91c1a438
Author: Dirk Mueller <mueller@kde.org>
Date:   Fri Apr 1 12:53:26 2011 +0200

    bump version

commit aaf776b6536a6997d36739c5977de629a8985531
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Mar 31 11:23:33 2011 +0200

    link to kutils

commit c7e476d19d548a815491ede7c46300a9f913ca57
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Mar 31 11:21:21 2011 +0200

    showingWidgetExplorer and showingActivityManager

commit b239193e10195b13943686f39a186cd29fbf7e24
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Mar 31 11:17:32 2011 +0200

    improve autounhide with autorehide
    
    due to unmergable branches, this was done "by hand" :/

commit b5e2414d6f3952f72ea9c44bff123b8d5ce20e71
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Mar 31 11:17:16 2011 +0200

    make panelcontroller able to switch states properly

commit 46e59b0c1b1ebc20eea213ca1d3601ea85970fcd
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Mar 31 10:54:36 2011 +0200

    remove a duplicated entry, patch by esigra

commit 402a654ff4cd9ed4a1e1cfcddb0091f1aafad687
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Mar 30 15:43:13 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 0914101fb8faa6ed99eb9d552476472053348e75
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Mar 29 20:15:07 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 6b033466e0ce885eea3715eae70d7c08bac0bb4c
Author: Ingomar Wesp <ingomar@wesp.name>
Date:   Tue Mar 29 05:54:14 2011 +0200

    Backport to 4.6: Provide a proper minimumSize hint (preferredWidth for
    horizontal form factors, preferredHeight for vertical ones).
    
    CCBUG: 264606
    CCBUG: 269419

commit b54588830c038d5233250a92ace8da5c6034703c
Author: John Tapsell <johnflux@gmail.com>
Date:   Mon Mar 28 22:51:23 2011 +0100

    KSysguardProcessList - Use the standard find key to focus on the search box

commit 339722d45c308782b52eaf2979bb419c8df23475
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 24 21:42:55 2011 +0000

    When a process has been selected to be killed, flash it in red

commit e80cd99c7c09914445064f1369ee59a8ed5043ea
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 24 20:21:30 2011 +0000

    Add an option to forcibly kill a process, but only show if you have already tried to kill the process

commit d07d84bfc0303a9a8b6c37b078a812f868936c11
Author: John Tapsell <johnflux@gmail.com>
Date:   Wed Mar 23 23:59:44 2011 +0000

    Processes - Introduce a lastError() function to find out why a killProcess() etc function call failed
    
    Now we can, and do, only ask for the root password if killing a process
    failed beacuse we have insufficient privillages.

commit c834b79d800637d2844e2bb7e7fb9e09e7a6b73b
Author: John Tapsell <johnflux@gmail.com>
Date:   Wed Mar 23 23:55:34 2011 +0000

    Prevent memory leak and strange error message when killing a process

commit 1a5799ea7883f8caf022d690d8ab6d1aec1cdf14
Author: John Tapsell <johnflux@gmail.com>
Date:   Tue Mar 22 20:52:46 2011 +0000

    Change the "fake process" pid to -1 instead of 0.
    
    (Backport - includes the commits from master:
    
    * Use C style only comments
    * ProcessFilter - Fix stupid crash that I introduced yesterday when switching the root-pid to -1
    * Fix more cases where tracerpid should be set to -1 not 0"

commit e307e865ed314e2903b576713202088bf9816df2
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Mon Mar 21 18:48:16 2011 +0200

    Add various Process counts.
    
    Add Idle, Running, Sleeping, Stopped, Zombie, Waiting and Locked
    process counts.
    
    FreeBSD supports upto 99999 running processes however that number
    may change in the future and it makes Line plotting scale badly so
    do not set upper limits on any of the process count sensors.

commit 7ac08fa8e7651242fdd970383c499357c1169ce5
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Mon Mar 21 18:02:51 2011 +0200

    Move Process sensors into category/module.
    
    As there are now 4 sensors (ps, pscount, lastpid and procspawn) and
    more on the way there are sufficient number to create a new category.

commit 240bbc3dd9be814119e493d67874ffec6ab61b3b
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Mon Mar 21 16:24:05 2011 +0200

    Add Process Spawn Count sensor.
    
    This sensor is the number of processes spawned.  Existing processes
    are not counted (or uncounted).

commit cf6f47fc3a038c641e0aa567c8a707e6b42571c3
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Mon Mar 21 16:21:10 2011 +0200

    Replace kvm_getswapinfo(3) with sysctl call.
    
    This change removes the dependancy on libkvm and allows all sensors
    to run from unprivilaged userspace (before access to /dev/kmem was
    required to get swap, and other, sensors).
    
    kvm_getswapinfo_sysctl is a heavily modified function from libkvm,
    however due to the extensive changes I did not use its original
    license.

commit e44463d6a3a180d92a3ea931f5f7816c1dc29b96
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Mon Mar 21 14:20:39 2011 +0000

    Fix license to GPLv2 or later, with email permission from David

commit a4e8a0c78e2545035ccf7ec68eae7213a59b356a
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Mon Mar 21 10:32:50 2011 +0200

    Expand support for Terabytes and Petabytes
    
    Petabyte capable storage should appear within the next 15 years

commit 6a301c6ecabae66ac30e77e834a077f560cb90de
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Mon Mar 21 11:04:16 2011 +0200

    Do not display kernel process.
    
    Currently KSysGuard assumes PID!=0 and will crash when receiving a
    PID==0 so do not show such a process (which, under FreeBSD is the
    kernel).

commit 20e59a72c2c80ce56a9176949bcd87732afec095
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Mon Mar 21 13:40:47 2011 +0000

    ksysguardd FreeBSD - no longer crashes when /dev/kmem is inaccessable.
    
    * Inform user about reduced sensors when /dev/kmem is not accessable
    * Correctly indent changes code

commit cb2cf1cad08b8832e49e750dfbf282e587c69b6c
Author: John Tapsell <johnflux@gmail.com>
Date:   Mon Mar 21 08:46:33 2011 +0000

    Detect new network interfaces, and new partitions
    
    BUG:263696

commit c3a5b8bacb543ec7b68b4fa3383c4607143ddcc3
Author: John Tapsell <johnflux@gmail.com>
Date:   Sun Mar 20 21:53:04 2011 +0000

    Do not put commas in the uid number - for consistency

commit 3c9388a4aed97180aa0ee7dd844d5b1ddc2215ce
Author: John Tapsell <johnflux@gmail.com>
Date:   Sun Mar 20 21:45:53 2011 +0000

    Do not include bond0 etc network interfaces, otherwise we can count the same network traffic twice
    
    BUG:267692

commit 8f5ea0893616f7a4af64a3ae4ee264a6414f2132
Author: John Tapsell <johnflux@gmail.com>
Date:   Sun Mar 20 13:21:41 2011 +0000

    Update to new /sys/class
    
    This updates fan and thermal to the new /sys/class API, but doesn't
    update the battery stuff yet
    
    BUG:262654

commit 2b976e36a8d8a1328d4cfb3075038031c7e3de11
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 18 23:48:06 2011 +0000

    Remove unused variables and functions in acpi.c

commit 27012889e9b0fcccabad12dc9cdbabc415a51bbf
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 18 17:00:23 2011 +0000

    Fix that IO fields don't change the unit
    
    BUG:263542

commit de71b01793df19d81c915910bd63f10d9bf22001
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 18 16:26:40 2011 +0000

    Make it possible to remove a ProcessController
    
    CCBUG: 267280

commit 06471cc16172df567452fbd99ba084e1d6630ada
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 18 15:53:49 2011 +0000

    Fix if the user creates a second Processes Table
    
    BUG:267280

commit 3904e2e67a7cdc9bc11890e7889d4e67bb7db62f
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 18 15:07:51 2011 +0000

    Be a bit more relaxed about bad values from custom clients

commit 17ecd1657bf25f36b92553f646f0af37bf3eb40a
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 18 14:18:41 2011 +0000

    Fix tooltip changing for the wrong window

commit ca5413d4425e487610e20a4744dc2a7b9f0f932e
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 20:25:27 2011 +0000

    ListView - add a context menu to set kb/mb/gb

commit d227734ad652d41136335e1243019778980ac147
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 20:25:09 2011 +0000

    Fix memory leak with the menu

commit f7b9a2af457de10c6319e489e7fba922d4484d8d
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 16:37:29 2011 +0000

    ListView - white space changes

commit ad9f05734d2c010b8858b41a97272091df2acc44
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 16:35:28 2011 +0000

    Always display ListView memory in bytes

commit 59426b77104db0c225f7ea1e164114fc7d4fd5af
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 16:28:54 2011 +0000

    Add support for "percentage" in the ListView, and fix sorting in the ListView

commit a06f4aa0174bc68323c5a8b461bfb0924501d9d4
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 14:27:55 2011 +0000

    Return disk sizes in bytes instead of blocks
    
    Also replace deprecated statfs with statvfs

commit 4a07a130014a38a642fc9c1be6bb8b2ab2fc1c63
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 14:24:23 2011 +0000

    Fix white space SensorBrowser

commit 51baca7cfa1976c0111a35d7feb15b962873e713
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 14:13:02 2011 +0000

    Rename .cc files to .cpp
    
    (Including - Unbreak Messages.sh patch in master)

commit 85672fe144894524e11217e81e0b3a948b4a81bd
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 17 13:59:44 2011 +0000

    SensorBrowser - fix crash when the daemon disconnects.

commit 5ba1e06d4f26bc868e8c6f6615b3cf22f56e1b36
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Thu Mar 17 08:56:41 2011 +0000

    FreeBSD support - A large number of improvements to the FreeBSD support

commit 03bce3a6480ac98d990b1e4ba6ce95d07f5505d1
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Thu Mar 17 08:52:42 2011 +0000

    Add some translation strings
    
    ReviewedBy: John Tapsell

commit c0a96afb1a5532adc41b80f9c83ebd94ef7eb204
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Thu Mar 17 08:50:35 2011 +0000

    Do not show bridge and usbus network devices
    
    ReviewedBy: John Tapsell

commit 34d012a31436be9f9548f464a265bd62f566d3fc
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Thu Mar 17 08:44:03 2011 +0000

    ListView - add support for "KB" column type, to indicate a memory column
    
    ReviewedBy: John Tapsell

commit 08b40c575bca1f475110612e8004bf0dcb5e06d2
Author: John Tapsell <johnflux@gmail.com>
Date:   Wed Mar 16 15:50:44 2011 +0000

    Let us be able to close System Activity with the usual ctrl-Q shortcut
    
    BUG:268646

commit 743898b6cbf2b010441ea94fa314b61e3aaa2a1e
Author: John Tapsell <john.tapsell.ext@basyskom.de>
Date:   Fri Mar 11 14:08:42 2011 +0000

    Explicitly declare another variable
    
    BUG:268014

commit 31082e73d7334e5f50be96c2483d3f35d90b2fd8
Author: John Tapsell <john.tapsell.ext@basyskom.de>
Date:   Fri Mar 11 11:10:22 2011 +0000

    Renice dialog - Fix showing two dialogs when the renice/kill/etc fails

commit 9a944e834870fed7d870dc07b73a62da0eda5d6b
Author: John Tapsell <john.tapsell.ext@basyskom.de>
Date:   Fri Mar 11 10:12:04 2011 +0000

    Explicitly declare variable, to fix with newest QWebKit
    
    BUG:268014

commit 9f10ec3b14cf106da6302e5fec6d2b8d009e9412
Author: John Tapsell <john.tapsell.ext@basyskom.de>
Date:   Thu Mar 10 12:50:48 2011 +0000

    Fix compile on solaris
    
    Patch thanks to tropikhajma
    BUG: 255844

commit d44c3347cf67ee18c151e747955056036f319a12
Author: Martin Gräßlin <mgraesslin@kde.org>
Date:   Mon Mar 28 18:09:54 2011 +0200

    Remove KWin Blacklist update script
    
    Blacklist is not used anymore and script was missing shebang.
    So just remove it completely - no use in updating the kwinrc with
    obsoleted data.

commit 7dace6332bfcf50bee67e254cef2ceeaeb17faf2
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Mar 28 14:04:52 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 1ba3b9ff8211847ab84dd93ee51a0451d8e87849
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Mar 27 16:04:33 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 9ee656c206677d2ccf3dcca0079eff9bd3d91ec3
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sun Mar 27 12:22:13 2011 +0200

    Make sure widget is not a Dock "title Widget" (passed via QDockWidget::setTitleBarWidget)
    before setting as dragable.
    
    CCBUG: 269410

commit 5e7ff34bb2652913952af882505aa7110b3addc2
Merge: 9e541f6 883d96a
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sat Mar 26 16:30:30 2011 -0300

    Merge branch 'KDE/4.6' of git://anongit.kde.org/kde-workspace into KDE/4.6

commit 9e541f642fa5cb4b913a607a87ced49d8acfa561
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sat Mar 26 16:29:31 2011 -0300

    Using the correct guard for this header.

commit 883d96a442c5b0c7a7829e7b53cb7b8d08d4b35b
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 25 16:01:34 2011 +0000

    KSignalPlotter - fix the signal plotter going darker after you view the settings

commit 1c3239acd617a7d436974f4274b1fa8c8d71879f
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 18 16:50:27 2011 +0000

    Do not crash if we cannot open vmstat
    
    BUG:263853

commit a696f0aed05844d2281b35fdb83723aae50e15c2
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 25 08:34:04 2011 +0000

    Add back support for Stacked Beams
    
    BUG:232624

commit 4798035cfb56b477dc70d6f589b97b5926acc9bd
Author: John Tapsell <johnflux@gmail.com>
Date:   Fri Mar 25 11:08:30 2011 +0000

    SignalPlotter - Fix support for stacked graphs
    
    CCBUG:232624

commit 973daba7fb809f7487d7e007bf932c381f28ca31
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Mar 24 00:08:08 2011 +0000

    Fix crash when renicing a process, and it is killed
    
    BUG:254772

commit fc5993dde16f7403f10e171d13193a0db9473aaf
Author: John Tapsell <johnflux@gmail.com>
Date:   Wed Mar 23 23:30:34 2011 +0000

    Fix crash when killing a process
    
    BUG:269094
    BUG:261251

commit bed639322cbf0216c3c528338c665c386a323a3a
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Mar 20 14:10:17 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 629e17d18526ccfae3d3579066e8be927a92437d
Author: Martin Gräßlin <mgraesslin@kde.org>
Date:   Sun Mar 20 13:22:19 2011 +0100

    Fix Blur behind in Dashboard Effect
    
    By making Dashboard Effect a fullscreen effect the blurring got
    accidentially broken (blur ignores fullscreen effects). Reenabling
    by setting the force blur role on the dashboard window(s).
    BUG: 259797
    FIXED-IN: 4.6.2

commit 26a5f4dc3ec42616a13eee4f0112a22b6361c241
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Fri Mar 18 23:20:09 2011 +0100

    fix dialogparent implementation
    
    CCBUG: 267349

commit 672a08ad631155c57b225c6ac64310b19bc1ba12
Author: Martin Gräßlin <mgraesslin@kde.org>
Date:   Fri Mar 18 18:06:03 2011 +0100

    Repaint area between offset and window in sliding popups
    
    As this area was not repainted it caused visual garbage during
    animationgs. Most visible with yakuake and a panel at the top.
    Offset in that case is the upper screenedge while yakuake is
    positioned below the panel. The area of the panel did not get
    repainted causing the garbage.
    BUG: 264765
    FIXED-IN: 4.6.2

commit 675f3657d9f7fd2d1987506bfa73eee1b39d8acb
Author: Martin Gräßlin <mgraesslin@kde.org>
Date:   Thu Mar 17 19:10:27 2011 +0100

    Implement default handler in Dashboard config
    
    BUG: 253599
    FIXED-IN: 4.6.2

commit f2cf9bb8b821bc87b467b6b4fee3ce46bc1a9733
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Mar 16 14:33:02 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit d0cf7ff6da4c6f8bcc684dd6378e2af09d36bb06
Author: Jacopo De Simoi <wilderkde@gmail.com>
Date:   Tue Mar 15 22:17:26 2011 +0100

    Fix jerky animations for the XRender backend
    
    The code controlling timing for the animations
    assumes that all painting is finished when the
    Scene has finished painting. Since the X protocol
    is asyncronous we need to call XSync (and not XFlush)
    to make sure all painting has finished.
    Tested for more than 2 weeks without issues.

commit 0025327dff6d7eaebffd8563d888631bc9fac481
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Mar 15 15:49:05 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit d863e0e4273554de61bc8f3190e3512280207767
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Mar 14 14:53:09 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 68fc9687b99ad04598d037f4bf3bb23a15c0ffef
Author: John Tapsell <johnflux@gmail.com>
Date:   Mon Mar 14 10:21:15 2011 +0000

    Fix crash when mixing summation and non-summation sensors
    
    Also fix the tooltip to make it look better when mixing summation and
    non-summation sensors
    
    BUG:223095

commit 1ea5f3e670b514991dd8c6475f2fcf4e8cfb1b98
Author: Script Kiddy <scripty@kde.org>
Date:   Sat Mar 12 14:35:11 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 0069abf1e4531df4389add699ff048f2a2776d41
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Fri Mar 11 18:08:45 2011 -0300

    iSend the correct suspend type mode for suspendSession action.
    
    BUG: 268149
    FIXED-IN: 4.6.2

commit 8ee1461949e387e254e67dad17b013cb52741279
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Fri Mar 11 16:04:13 2011 -0300

    Enable advanced options for Ups Batteries in the plasmoid.

commit 4f5cbcb37594b29f8e9c77cf63d2b5d106540eb7
Author: Lukas Tinkl <lukas@kde.org>
Date:   Fri Mar 11 14:54:40 2011 +0100

    fix Wifi Capability

commit ca2956e0ca977fb49d4e3679f5556bc066fabdb7
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Mar 11 14:28:09 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 7b802f29427167fd3fd27d8c08fc767ff92d7925
Merge: a05dc9a c3b28d9
Author: Thomas Zander <zander@kde.org>
Date:   Wed Mar 9 19:59:44 2011 +0100

    Merge commit 'origin/4.6' into 4.6

commit a05dc9a5e89dc1b76564ffcd724f6ccd96909401
Author: Aaron Seigo <aseigo@kde.org>
Date:   Wed Mar 9 10:44:34 2011 +0100

    Int, not Integer

commit ad36815a8277ed1ea8de04b04ce69b7af37dce2b
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Mar 8 15:44:30 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 941b0e313292438909342c5be3f04a2152aa13da
Author: Aaron Seigo <aseigo@kde.org>
Date:   Mon Mar 7 18:11:59 2011 +0100

    check that we hava an item to query
    
    BUG:267888

commit 4b56a712c08a3bd4319af995fb27d3d27f8cb3a3
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Mon Mar 7 14:38:21 2011 +0100

    force WA_TranslucentBackground only on QTipLabel, rather than on Qt::ToolTip type of windows.
    
    CCBUG: 267837 254473

commit c3b28d90cf2c5f9efb57bdc764361c6a596f1749
Author: Alex Fiestas <afiestas@kde.org>
Date:   Fri Mar 4 23:26:12 2011 +0100

    adjustSize of the Stack Widget once the StackDialog is hidden
    
    Right now a notification with a long title will create a very long
    NotificationWidget and that widget will resize the stackdialog to
    its size. The problem is that once the large notification group is
    closed, the StackDialog will continue having the notification width
    and it won't resize to a normal size.
    
    This patch calls to "adjustSize" when the stackdialog doesn't
    have any notification, so it will be resized to the right size.

commit c75a00284dd8aa87a1f3b921b38b475c0245c263
Author: Alex Fiestas <afiestas@kde.org>
Date:   Sun Mar 6 00:38:51 2011 +0100

    Do not "Stick" the notification to the applet on savePosition
    
    When the user move the notification to its desired place the
    saved position should be what the user wants.

commit 03f0c22e8539f27c1ffa482313795e04345bb228
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Mon Mar 7 01:18:13 2011 -0300

    Adding missing emit.

commit 21b05c15b3c1f230556f37887a9f72b995f1f7bc
Author: Thomas Zander <zander@kde.org>
Date:   Sun Mar 6 13:31:10 2011 +0100

    Usability fixes
    
    Make the edit area disabled until the user selects he wants custom
    positions.
    Its a source of confusion if the user can edit the positions but doesn't
    see anything happen in his decorations upon apply.  This gives immediate
    feedback that the checkbox should be enabled.

commit 1e0971e3252d1511f07bbd5d52ba4860a729117b
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Mar 6 14:31:04 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 181c58c354f15fb1bda91ea6582f2baf3975c7f2
Author: Lim Yuen Hoe <yuenhoe86@gmail.com>
Date:   Sat Mar 5 16:35:39 2011 +0800

    Make panel properly resize to fit in screen when reducing screen resolution.
    
    CCBUG: 209962

commit a58c0a17217b14e2e47415cfdfb1e8bb0b61a4d3
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sat Mar 5 19:39:00 2011 +0100

    default to poweroff for HaltCmd on linux
    
    now that linux can have more bsd-like shutdown implementations (both in
    form of gentoo and of systemd), it makes sense to treat it the same.
    
    BUG: 267490
    FIXED-IN: 4.6.2

commit 4654efa2396359cc52af97a4db04b8d8f3cdfe82
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sat Mar 5 18:05:03 2011 +0100

    revert wallpaper to "air"
    
    why is this called oxygen-air and the screenshot shows an "air"
    background? right: because the wallpaper is "ethais".
    and why is this in kdebase? right again: because the wallpaper is in
    kdeartwork.
    
    uhm, maybe not right, after all. whatever the artistic vision behind
    this is, the current state is technically bogus. consider moving this
    theme to kdeartwork as a whole and making adjustments there.
    
    BUG: 262554
    FIXED-IN: 4.6.2

commit 5a0a5ced670199ee4d6b3d25d9e96fbabb227f05
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sat Mar 5 17:57:02 2011 +0100

    purge ethais theme (which was copied 1:1 to horos)
    
    apparently artists were incapable of properly renaming ...

commit 9200c92ec1ab2e124f70a2b5536f71c6d4c87baa
Author: Script Kiddy <scripty@kde.org>
Date:   Sat Mar 5 15:18:08 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit d40194bf7b0947facae8d08b146972f5f348a761
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sat Mar 5 00:01:16 2011 -0300

    Backporting
    http://commits.kde.org/kde-workspace/549c933a0cdfa3556b016d7172e5aa09fc63f7d2
    
    Fix to I18N_ARGUMENT_MISSING problem in kwin when moving/resizinz a window.

commit d409abdbef8dd8b72bb2f311fcaa7456bdc2404b
Author: Luboš Luňák <l.lunak@suse.cz>
Date:   Fri Mar 4 16:22:23 2011 +0100

    do not show hostname in titlebar if it's FQDN of localhost

commit 4985776ac4ad88e5ef978276ee47e80a1ff28a54
Author: Script Kiddy <scripty@kde.org>
Date:   Thu Mar 3 14:35:36 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 935ffeba4a4e7b88f0585101d51d21786ac022ff
Author: Andriy Rysin <arysin@gmail.com>
Date:   Wed Mar 2 17:52:27 2011 -0500

    Support xkbConfigRegistry version; don't merge variant name with
    layout in version>=1.1

commit a04924a50b385a40800b4fcea370227214ef5bd9
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Mar 2 14:44:39 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 0182fae5be88ea87c9490fa513d11438d387755d
Author: John Layt <john@layt.net>
Date:   Sun Feb 27 16:55:43 2011 +0000

    Plasma Calendar: Update copy of Akonadi calendar model
    
    The akonadi calendar model is required to understand the akonadi
    calendar data, but is not yet publically available.  This updates
    the model to the latest version from kdepim 4.6 to include a number
    of bug fixes.

commit 226c69c40de9db1b749c663cb08c97de9ed57f8b
Author: John Layt <john@layt.net>
Date:   Sun Feb 27 16:43:15 2011 +0000

    Plasma Calendar: Re-enable akonadi event updates
    
    Fix the plasma calendar to set the right flag to indicate a refresh of
    the akonadi events is required.  This should fix the problem, but while
    I have confirmed this does now send a query to akonadi again, I don't
    get any events back, but this si probably an artifact of my test
    environment, it should work ok once in a full desktop/akonadi
    environment.
    
    CCBUG: 258125

commit 5c7add026ba1a0048ba884217f7454998164dcf4
Author: John Layt <john@layt.net>
Date:   Thu Feb 3 13:40:00 2011 +0000

    Include KHolidays translation catalog in plasma calendar and clocks
    
    Fix missing catalog
    
    CCBUG: 264867

commit 4f61298feb25bcb49ebb87043b87840eadad7d8b
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Mar 1 15:51:15 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 9112e153de81e25269081aef45a328f0a34f16d0
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Feb 28 14:58:14 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit b6c9aae2601a84b1b2fbe32fe9a62b6cc4790d8f
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sun Feb 27 19:13:50 2011 +0100

    remove now unused variable (amend c437b772)

commit 90522fa12f7de9256ec5a378a13bb80b98e08f0e
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sun Feb 27 19:06:43 2011 +0100

    fix font defaults
    
    of course it was extraordinarily clever of me to remove support for qt2
    font specs from the config parser without adjusting the defaults.
    
    BUG: 258968
    FIXED-IN: 4.6.2

commit 675358bff4c3c8f2d01479de9503a75b875c3654
Merge: bcf7b0e 50509ef
Author: Farhad Hedayati Fard <hf.farhad@gmail.com>
Date:   Sun Feb 27 21:11:38 2011 +0330

    Merge branch 'KDE/4.6' of git.kde.org:kde-workspace into KDE/4.6

commit bcf7b0ee8044b0c8bb8a45221868e0fc4ea71d66
Author: Farhad Hedayati Fard <hf.farhad@gmail.com>
Date:   Sun Feb 27 21:07:42 2011 +0330

    focus on search box when widget explorer is opened.
    REVIEW: 100757

commit 50509ef24bbdc20f5a80c65e84d06e20630e8481
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sun Feb 27 17:42:27 2011 +0100

    fix capitalization of Xfce 4
    
    it's not meant to mean "XForms common environment" any more
    
    BUG: 257051
    FIXED-IN: 4.6.2

commit a39070efd2747df08624cc1c3a9142d1546ee24f
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sun Feb 27 17:42:33 2011 +0100

    adjust to INSTALLed file permissions

commit 77c664729119f2c24a907939c8758bef8ca58f63
Author: Script Kiddy <scripty@kde.org>
Date:   Sat Feb 26 14:15:22 2011 +0100

    SVN_SILENT made messages (.desktop file)
