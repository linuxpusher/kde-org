------------------------------------------------------------------------
r1225073 | schwarzer | 2011-03-17 08:18:26 +1300 (Thu, 17 Mar 2011) | 3 lines

Fix score display not being updated on new game.

BUG: 268585
------------------------------------------------------------------------
r1225074 | schwarzer | 2011-03-17 08:18:29 +1300 (Thu, 17 Mar 2011) | 3 lines

Fix swapped rotation direction.

BUG: 268581
------------------------------------------------------------------------
r1225886 | wrohdewald | 2011-03-24 23:21:11 +1300 (Thu, 24 Mar 2011) | 9 lines

Backport from trunk r1225885

for upper and lower player, do not show yellow popups over their name
    
move the popups more to the middle of the field
   
BUG:269293


------------------------------------------------------------------------
r1226415 | scripty | 2011-03-30 07:16:07 +1300 (Wed, 30 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1226563 | scripty | 2011-03-31 02:44:25 +1300 (Thu, 31 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1226604 | schwarzer | 2011-03-31 14:12:47 +1300 (Thu, 31 Mar 2011) | 9 lines

backport or r1226078: Fix message being shown behind the bricks.

The bug was that message boxes were displayed in the background,
especially behind the bricks making the text unreadable.
The function TextItem::show() now explicitely raises the
KGameCanvasItem before showing it.

BUG: 261017

------------------------------------------------------------------------
r1226659 | schwarzer | 2011-04-01 09:11:33 +1300 (Fri, 01 Apr 2011) | 2 lines

Version bumps for bug fixes.

------------------------------------------------------------------------
