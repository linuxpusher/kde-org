2008-09-27 10:34 +0000 [r865272]  annma <annma@localhost>:

	* branches/KDE/4.1/kdeedu/ktouch/keyboards/uk.winkeys.keyboard.xml
	  (added): backport of new Ukrainian keyboard

2008-09-27 15:57 +0000 [r865444]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdeedu/doc/kig/index.docbook,
	  branches/KDE/4.1/kdeedu/doc/step/tutorials.docbook,
	  branches/KDE/4.1/kdeedu/doc/kalzium/index.docbook,
	  branches/KDE/4.1/kdeedu/doc/kstars/flux.docbook,
	  branches/KDE/4.1/kdeedu/doc/kstars/retrograde.docbook,
	  branches/KDE/4.1/kdeedu/doc/kgeography/index.docbook,
	  branches/KDE/4.1/kdeedu/doc/kmplot/introduction.docbook,
	  branches/KDE/4.1/kdeedu/doc/kstars/quicktour.docbook,
	  branches/KDE/4.1/kdeedu/doc/kstars/fitsviewer.docbook,
	  branches/KDE/4.1/kdeedu/doc/kalgebra/index.docbook,
	  branches/KDE/4.1/kdeedu/doc/kstars/equinox.docbook: documentation
	  backport for 4.1.3 from trunk

2008-09-28 15:33 +0000 [r865671]  ewoerner <ewoerner@localhost>:

	* branches/KDE/4.1/kdeedu/kalzium/src/detailinfodlg.cpp: Backport
	  fix for "Kalzium does not show spectrum after switching to an
	  element without spectrum" CCBUG: 168830

2008-10-02 06:19 +0000 [r866868]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdeedu/marble/src/plasmoid/plasma-applet-kworldclock.desktop,
	  branches/KDE/4.1/kdeedu/parley/plasmoid/engine/plasma-dataengine-parley.desktop,
	  branches/KDE/4.1/kdeedu/kig/kig/kig.desktop,
	  branches/KDE/4.1/kdeedu/kalzium/src/kalzium.desktop,
	  branches/KDE/4.1/kdeedu/step/step/step.desktop,
	  branches/KDE/4.1/kdeedu/kwordquiz/src/kwordquiz.desktop,
	  branches/KDE/4.1/kdeedu/kalgebra/src/kalgebra.desktop,
	  branches/KDE/4.1/kdeedu/kgeography/src/kgeography.desktop,
	  branches/KDE/4.1/kdeedu/kanagram/src/kanagram.desktop,
	  branches/KDE/4.1/kdeedu/kwordquiz/src/kwordquiz.notifyrc,
	  branches/KDE/4.1/kdeedu/marble/src/marble.desktop: SVN_SILENT
	  made messages (.desktop file)

2008-10-08 07:15 +0000 [r869115]  nielsslot <nielsslot@localhost>:

	* branches/KDE/4.1/kdeedu/kturtle/src/canvas.cpp: Give the printed
	  text the same color as the turtle's pen.

2008-10-08 16:41 +0000 [r869262]  ewoerner <ewoerner@localhost>:

	* branches/KDE/4.1/kdeedu/marble/data/placemarks/elevplacemarks.cache,
	  branches/KDE/4.1/kdeedu/marble/data/placemarks/elevplacemarks.kml:
	  Backport fix position of Mont Blanc

2008-10-09 10:02 +0000 [r869506]  ewoerner <ewoerner@localhost>:

	* branches/KDE/4.1/kdeedu/marble/src/lib/PlaceMarkManager.cpp,
	  branches/KDE/4.1/kdeedu/marble/src/marble_part.cpp,
	  branches/KDE/4.1/kdeedu/marble/src/lib/GridMap.cpp: Backport
	  fixes for bugs revealed by code checking tool Exporting to png
	  should work again

2008-10-14 06:17 +0000 [r871196]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdeedu/parley/plasmoid/engine/plasma-dataengine-parley.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-16 20:21 +0000 [r872299]  gladhorn <gladhorn@localhost>:

	* branches/KDE/4.1/kdeedu/parley/src/entry-dialogs/audiowidget.cpp:
	  backport r872297 for KDE 4.1.3 (fix sound crash) CCBUG: 172431

2008-10-24 06:13 +0000 [r875342]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdeedu/marble/src/marble.desktop: SVN_SILENT
	  made messages (.desktop file)

2008-10-29 21:39 +0000 [r877592]  aacid <aacid@localhost>:

	* branches/KDE/4.1/kdeedu/kgeography/data/world.png: Give Hungary
	  the correct color, discovered by Russel Riley

