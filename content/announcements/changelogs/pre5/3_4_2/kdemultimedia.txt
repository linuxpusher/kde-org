2005-05-24 12:08 +0000 [r417715]  adrian

	* juk/playlistitem.h: fix compile with current gcc

2005-05-26 09:00 +0000 [r418308]  esken

	* kmix/mdwslider.cpp: Backport to KDE_3_4 branch of the fix for the
	  weird KMix Panel-Applet channel-hide bug. I received one
	  acknowledge, that the fix works, so I will close the bug reports.
	  If it will get accepted for inclusion, it will ship with
	  KDE3.4.1, otherwise with KDE3.4.2. BUGS: 90930 BUGS: 96821

2005-05-26 12:32 +0000 [r418364]  larkang

	* libkcddb/httplookup.cpp, libkcddb/client.cpp: Backport: Some
	  valgrinding: Pass size to QString::fromUtf8() calls since
	  QByteArray isn't null-terminated Return directly after successful
	  lookup in slotResult since the object can be deleted when
	  emitting readReady().

2005-05-26 19:22 +0000 [r418495]  larkang

	* libkcddb/asynchttplookup.cpp, libkcddb/asynccddbplookup.h,
	  libkcddb/httplookup.cpp, libkcddb/asyncsmtpsubmit.h,
	  libkcddb/asynchttplookup.h, libkcddb/client.cpp,
	  libkcddb/httplookup.h, libkcddb/asynchttpsubmit.cpp,
	  libkcddb/cddb.h, libkcddb/cdinfo.cpp,
	  libkcddb/asynccddbplookup.cpp, libkcddb/asyncsmtpsubmit.cpp,
	  libkcddb/asynchttpsubmit.h, libkcddb/synchttplookup.cpp:
	  Backport: Make Lookup and Submit inherit QObject, and use
	  deleteLater() to delete the objects. Don't call job->error() on
	  deleted KIO::Job's

2005-06-01 09:55 +0000 [r420755]  larkang

	* libkcddb/submit.h, libkcddb/lookup.h, libkcddb/cddb.h: Backport:
	  Don't break BC

2005-06-02 17:27 +0000 [r421282]  carewolf

	* akode/lib/player.cpp, akode/lib/frametostream_decoder.cpp,
	  akode/lib/streamtoframe_decoder.cpp, akode/lib/audiobuffer.h,
	  akode/lib/player.h, akode/lib/audiobuffer.cpp: Backport of recent
	  bug fixes

2005-06-04 17:13 +0000 [r422162-422161]  bero

	* akode/lib/player.cpp: Fix compile if NDEBUG is #defined

	* akode/lib/player.cpp: Better fix for NDEBUG issue

2005-06-13 08:04 +0000 [r424802]  larkang

	* kscd/cddbdlg.cpp, kscd/kscd.cpp, kscd/cddbdlg.h, kscd/kscd.h:
	  Backport: Don't ask which cddb-entry to choose when there are
	  multiple entries available and the user just edited one of them

2005-06-13 09:14 +0000 [r424819]  carewolf

	* akode/lib/decoderpluginhandler.cpp: Backport of wav fix

2005-06-17 03:49 +0000 [r426349]  thiago

	* kscd/libwm/plat_linux_cdda.c: Backporting the bugfix in two
	  weeks, as promised. I will not apply the reporter's patch because
	  it's too intrusive. Attachment #11304 is a workaround for glibc
	  bug, so fix glibc instead BUG:106667

2005-06-25 20:19 +0000 [r428926]  adridg

	* kioslave/audiocd/audiocd.cpp: Handle the case where there is
	  already an audiocdencoder.h installed in system include paths
	  (like from a previous install of kdemm) -- BACKPORT

2005-06-25 21:07 +0000 [r428942-428941]  mcbride

	* doc/kscd/kscd6.png, doc/kscd/kscd16.png, doc/kscd/kscd9.png,
	  doc/kscd/index.docbook, doc/kscd/kscd.png, doc/kscd/kscd11.png,
	  doc/kscd/kscd2.png, doc/kscd/kscd12.png, doc/kscd/kscd3.png,
	  doc/kscd/kscd13.png, doc/kscd/kscd14.png, doc/kscd/kscd5.png:
	  Updates to kscd documentation: - Added documentation for new
	  dialog entries - Updated screenshots for new look since last
	  update - Added CD troubleshooting section for people who need
	  help getting their player working

	* doc/kscd/kscdannounc.png (added): Forgot to add a new screenshot

2005-06-27 15:18 +0000 [r429401]  binner

	* kdemultimedia.lsm: 3.4.2 preparations

2005-07-10 00:50 +0000 [r433206]  mpyne

	* juk/googlefetcher.cpp, juk/googlefetcher.h, juk/Makefile.am:
	  Backport fix for bug 108297 (JuK crashes when getting cover for
	  album with quotes) to KDE 3.4. The patch itself is non-trivial,
	  but the code itself is markedly less brittle now so I feel pretty
	  good about this. Also the feature in question is quite seriously
	  broken without this fix due to a slight change in the Google
	  results output.

2005-07-10 15:07 +0000 [r433360]  larkang

	* kscd/kscd.cpp: Make sure the discid is always 8 characters BUG:
	  108572

2005-07-10 23:46 +0000 [r433489]  esken

	* kmix/mixer_hpux.h, kmix/mixer_backend.h (added),
	  kmix/mixertoolbox.h, kmix/kmixerwidget.cpp, kmix/volume.cpp,
	  kmix/kmix.cpp, kmix/mixer_none.h, kmix/mixer_oss.cpp,
	  kmix/mixer_sun.cpp, kmix/viewbase.h, kmix/mixer_alsa9.cpp,
	  kmix/kmixdockwidget.cpp, kmix/kmixapplet.cpp,
	  kmix/kmixtoolbox.cpp, kmix/dialogselectmaster.h (added),
	  kmix/mixdevice.h, kmix/mixer.cpp, kmix/mixer_irix.h,
	  kmix/dialogviewconfiguration.h, kmix/mixer_hpux.cpp,
	  kmix/mdwslider.cpp, kmix/mixertoolbox.cpp, kmix/mixer_backend.cpp
	  (added), kmix/kmixctrl.cpp, kmix/mixer_none.cpp,
	  kmix/viewbase.cpp, kmix/kmixerwidget.h, kmix/KMixApp.cpp,
	  kmix/dialogselectmaster.cpp (added), kmix/kmixprefdlg.cpp,
	  kmix/kmix-platforms.cpp, kmix/mixdevice.cpp, kmix/kmix.h,
	  kmix/mixer_oss.h, kmix/mixer_sun.h, kmix/mixer_irix.cpp,
	  kmix/viewapplet.cpp, kmix/kmixdockwidget.h, kmix/kmixapplet.h,
	  kmix/main.cpp, kmix/Makefile.am, kmix/viewdockareapopup.cpp,
	  kmix/mixer_alsa.h, kmix/mixer.h, kmix/version.h: Backport of the
	  trunk version. It fixes several bugs and fulfills some wishes as
	  noted below: BUGS:90930 BUGS:96821 BUGS:100173 BUGS:101742
	  BUGS:105476 BUGS:105650 BUGS:87778 BUGS:98818 BUGS:101870
	  BUGS:108043 FEATURE:42643 FEATURE:55078 FEATURE:105241 GUI:The
	  change of orientation will be adopted on the next start of KMix.

2005-07-11 00:10 +0000 [r433493]  esken

	* kmix/kmixerwidget.cpp, kmix/Makefile.am: Remove "viewgrid"
	  referneces (they are only part of trunk, and not contained in
	  branch)

2005-07-20 10:51 +0000 [r436844-436843]  binner

	* kmix/version.h: bugfixes/features => new version

	* kmix/version.h: svnrevertlast, version already increased from
	  2.4.1 to 2.6 for this bugfix release

2005-07-20 11:29 +0000 [r436848]  coolo

	* libkcddb/cdinfodialogbase.ui: fixuifiles

2005-07-21 16:09 +0000 [r437357]  binner

	* juk/playlistsearch.h, juk/playlistsearch.cpp: Backport SVN commit
	  434275 by hermier: Workaround GCC-3.3 bug of -fexceptions with
	  templates and visibility.

