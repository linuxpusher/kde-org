------------------------------------------------------------------------
r838527 | scripty | 2008-07-28 06:45:03 +0200 (Mon, 28 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r838773 | aacid | 2008-07-28 19:44:09 +0200 (Mon, 28 Jul 2008) | 2 lines

Backport r838771 kbreakout/trunk/KDE/kdegames/kbreakout/src/gameengine.cpp: we want the shortcut translated

------------------------------------------------------------------------
r838777 | aacid | 2008-07-28 19:47:44 +0200 (Mon, 28 Jul 2008) | 2 lines

Backport r838774 kblackbox/trunk/KDE/kdegames/kblackbox/kbbmainwindow.cpp: we want the shortcut translated

------------------------------------------------------------------------
r839901 | scripty | 2008-07-31 06:50:41 +0200 (Thu, 31 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r841306 | scripty | 2008-08-03 06:48:51 +0200 (Sun, 03 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r841783 | scripty | 2008-08-04 06:54:57 +0200 (Mon, 04 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r842295 | mlaurent | 2008-08-05 08:59:40 +0200 (Tue, 05 Aug 2008) | 3 lines

Backport:
clean up

------------------------------------------------------------------------
r843164 | lueck | 2008-08-06 17:37:24 +0200 (Wed, 06 Aug 2008) | 1 line

add missing X-DocPath entry to make the docs visible in khelpcenter
------------------------------------------------------------------------
r843166 | lueck | 2008-08-06 17:47:28 +0200 (Wed, 06 Aug 2008) | 1 line

add missing X-DocPath entry to make the doc visible in khelpcenter
------------------------------------------------------------------------
r845591 | scripty | 2008-08-12 07:00:05 +0200 (Tue, 12 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r846763 | scripty | 2008-08-14 07:12:27 +0200 (Thu, 14 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r847712 | scripty | 2008-08-16 08:06:12 +0200 (Sat, 16 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r848138 | scripty | 2008-08-17 07:03:31 +0200 (Sun, 17 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r848848 | dimsuz | 2008-08-18 16:57:16 +0200 (Mon, 18 Aug 2008) | 2 lines

Backported two bugfixes from trunk (r848840)

------------------------------------------------------------------------
r853063 | scripty | 2008-08-27 08:04:43 +0200 (Wed, 27 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r853476 | ereslibre | 2008-08-27 21:18:41 +0200 (Wed, 27 Aug 2008) | 2 lines

Backport of removal of setAutoSaveSettings redundant call. This is already called by setupGUI

------------------------------------------------------------------------
r853480 | ereslibre | 2008-08-27 21:22:00 +0200 (Wed, 27 Aug 2008) | 2 lines

Backport of rev 853479. Redundant call removal

------------------------------------------------------------------------
r853591 | kkofler | 2008-08-28 01:46:20 +0200 (Thu, 28 Aug 2008) | 3 lines

Fix #160419: Bovo doesn't draw placed marks, patch by David Benjamin.
(backport rev 853590 from trunk)
CCBUG: 160419
------------------------------------------------------------------------
r853710 | scripty | 2008-08-28 08:06:37 +0200 (Thu, 28 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
