------------------------------------------------------------------------
r1226990 | mkraus | 2011-04-04 09:00:47 +1200 (Mon, 04 Apr 2011) | 2 lines

backport of commit 1226984
(fix visual bug in the infosidebar introduced with the port to KGameRenderer)
------------------------------------------------------------------------
r1227127 | mkraus | 2011-04-05 11:24:38 +1200 (Tue, 05 Apr 2011) | 2 lines

backport of commit 1227126
(fix visual bug in the infooverlay introduced with the port to KGameRenderer)
------------------------------------------------------------------------
r1227894 | mkraus | 2011-04-14 10:02:47 +1200 (Thu, 14 Apr 2011) | 1 line

backport commit 1227893 (bugix for phonon sound fallback)
------------------------------------------------------------------------
r1228141 | coates | 2011-04-16 15:05:50 +1200 (Sat, 16 Apr 2011) | 9 lines

Backport of commit 1228140.

Make the Spider solver check all cards are face up before moving runs.

This was simply a missing check.

CCBUG:265857
FIXED-IN:SC4.6.3

------------------------------------------------------------------------
