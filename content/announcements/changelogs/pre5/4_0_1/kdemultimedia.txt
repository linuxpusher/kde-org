2008-01-05 20:11 +0000 [r757752]  uwolfer

	* branches/KDE/4.0/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  Backport r757750: Additional fix for NetBSD. Patch by Mark
	  Davies. (Bug #154990)

2008-01-08 22:27 +0000 [r758722-758721]  mpyne

	* branches/KDE/4.0/kdemultimedia/juk/cache.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/cache.h: Backport fix for
	  binary incompatible loading of JuK playlists/cache to 4.0. This
	  will break loading of 4.0.0 playlists until sanity checking is
	  added.

	* branches/KDE/4.0/kdemultimedia/juk/main.cpp: Bump JuK version in
	  4.0 branch.

2008-01-09 03:21 +0000 [r758771]  mpyne

	* branches/KDE/4.0/kdemultimedia/juk/playlist.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/cache.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/historyplaylist.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/upcomingplaylist.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/playlistsearch.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/searchplaylist.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/juk-exception.h (added),
	  branches/KDE/4.0/kdemultimedia/juk/playlistbox.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/folderplaylist.cpp: Allow JuK
	  to load "playlists" files created from KDE 4.0.0. Will
	  forwardport to trunk.

2008-01-09 04:03 +0000 [r758777]  mpyne

	* branches/KDE/4.0/kdemultimedia/juk/viewmode.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/playlist.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/playlist.h,
	  branches/KDE/4.0/kdemultimedia/juk/dynamicplaylist.h: Backport
	  JuK icon name fixes to 4.0 branch.

2008-01-09 04:08 +0000 [r758778]  mpyne

	* branches/KDE/4.0/kdemultimedia/juk/cache.cpp: Backport JuK fix
	  allowing the collection to be saved after the first time it was
	  saved. (i.e. now JuK remembers the changes you made to your music
	  collection on the second time you use it too, yay!)

2008-01-12 00:57 +0000 [r760146]  mpyne

	* branches/KDE/4.0/kdemultimedia/juk/playlistcollection.cpp: Fix
	  JuK bug causing Play Queue to be created twice on startup, and
	  ensure changes to Play Queue get saved on shutdown.

2008-01-12 03:39 +0000 [r760166-760165]  mpyne

	* branches/KDE/4.0/kdemultimedia/juk/systemtray.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/systemtray.h: Backport fix to
	  move track announcement popup to the right location to 4.0.

	* branches/KDE/4.0/kdemultimedia/juk/systemtray.cpp,
	  branches/KDE/4.0/kdemultimedia/juk/covermanager.cpp: Backport fix
	  for track announcement popup cover art size to 4.0.

2008-01-12 03:55 +0000 [r760168]  mpyne

	* branches/KDE/4.0/kdemultimedia/juk/playlist.cpp: Backport column
	  visibility loading fix to 4.0.

2008-01-15 09:23 +0000 [r761626-761622]  gateau

	* branches/KDE/4.0/kdemultimedia/kmix/mdwslider.cpp: Define a
	  minimum size for the volume slider. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdemultimedia@759568

	* branches/KDE/4.0/kdemultimedia/kmix/viewsliders.cpp: Nicer
	  separator. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdemultimedia@759569

	* branches/KDE/4.0/kdemultimedia/kmix/viewsliders.cpp,
	  branches/KDE/4.0/kdemultimedia/kmix/mdwslider.cpp: Don't add
	  extent spaces unless necessary. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdemultimedia@759570

	* branches/KDE/4.0/kdemultimedia/kmix/dialogviewconfiguration.cpp,
	  branches/KDE/4.0/kdemultimedia/kmix/dialogselectmaster.cpp,
	  branches/KDE/4.0/kdemultimedia/kmix/kmixprefdlg.cpp: Fix double
	  margins in dialogs. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdemultimedia@759571

	* branches/KDE/4.0/kdemultimedia/kmix/mdwslider.cpp: Oups,
	  setFixedWidth(), not setFixedHeight(). Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdemultimedia@761608

2008-01-15 10:17 +0000 [r761674-761673]  gateau

	* branches/KDE/4.0/kdemultimedia/kmix/mdwslider.cpp: Reset
	  playbackSpacer size the same way captureSpacer is resetted.
	  Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdemultimedia@761661

	* branches/KDE/4.0/kdemultimedia/kmix/viewsliders.cpp: Make sure
	  the sliders are correctly aligned on startup. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdemultimedia@761662

2008-01-28 14:56 +0000 [r767696]  alexmerry

	* branches/KDE/4.0/kdelibs/cmake/modules/FindFlac.cmake,
	  branches/KDE/4.0/kdemultimedia/kioslave/audiocd/plugins/flac/encoderflac.cpp:
	  Backport fix for FLAC > 1.1.2. CCBUG: 153503

