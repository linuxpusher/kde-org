2006-03-19 14:49 +0000 [r520230]  cniehaus

	* branches/KDE/3.5/kdeedu/kalzium/src/data/data.xml: Techneticum is
	  artificial CCBUG:112404 CCBUG:123906

2006-03-27 12:24 +0000 [r523110]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/pt-br.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ua-basic.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/training/russian_long.ktouch.xml
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/training/bulgarian.ktouch.xml
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatisticsdata.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/bg.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/dvorak-es.svg (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ua-winkeys.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/fi.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatistics.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru-typewriter.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/training/Makefile.am,
	  branches/KDE/3.5/kdeedu/ktouch/training/finnish_for_kids.ktouch.xml
	  (added), branches/KDE/3.5/kdeedu/ktouch/src/ktouchui.rc,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyboard.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/ChangeLog,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru-basic.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/training/bulgarian_long.ktouch.xml
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/ru2.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/ru3.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/src/main.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am,
	  branches/KDE/3.5/kdeedu/ktouch/training/russian2ktouch.xml
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/hu.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru-winkeys.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ua-typewriter.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/training/finnish.ktouch.xml
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/training/slovenian.ktouch.xml
	  (added): KTouch - New version 1.5.1 including bug fixes and new
	  lectures/keyboards.

2006-03-31 03:14 +0000 [r524731]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/ChangeLog,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchleveldata.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchlecture.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchtrainer.cpp: KTouch -
	  new features and bugfixes

2006-04-02 04:55 +0000 [r525449]  ghorwin

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvoctraintable.cpp:
	  KVocTrain - added bug fix - adjustment of row heights when new
	  lines are inserted

2006-04-03 02:30 +0000 [r525862]  ghorwin

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvoctraintable.cpp:
	  KVocTrain - change from STL to Qt style

2006-04-04 00:43 +0000 [r526170]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouchleveldata.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchlecture.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/training/slovenian.ktouch.xml:
	  KTouch - small bug fixes and new default mini level

2006-04-04 05:19 +0000 [r526184]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchutils.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchutils.h: KTouch -
	  Feature wish #99947 implemented, also training file list is now
	  sorted.

2006-04-05 04:14 +0000 [r526612]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/pt.br.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/fr.a.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de-2.keyboard (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru.3.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ua-winkeys.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru.typewriter.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/uk.winkeys.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/ru3.keyboard
	  (removed), branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/nn.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru-winkeys.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/dvorak_es.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/dvorak_se.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru.winkeys.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/fr.swiss.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/pt-br.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ua-basic.keyboard
	  (removed), branches/KDE/3.5/kdeedu/ktouch/keyboards/ru.2.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/dvorak.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/en.dvorak.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/sv.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/de.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/number.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de_neo.keyboard
	  (removed), branches/KDE/3.5/kdeedu/ktouch/keyboards/dvorak-es.svg
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/uk.typewriter.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/es.dvorak.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de.swiss.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/uk.basic.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru-typewriter.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de-ch2.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de.keypad.keyboard
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru-basic.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru.basic.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/se.keyboard
	  (removed), branches/KDE/3.5/kdeedu/ktouch/keyboards/fr_a.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de.neo.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ru2.keyboard (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/creating_keyboard_layouts.txt
	  (removed), branches/KDE/3.5/kdeedu/ktouch/keyboards/no.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/sv.dvorak.keyboard
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/de-ch.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/ua-typewriter.keyboard
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/fr_ch.keyboard
	  (removed): KTouch - show language names instead of keyboard file
	  names

2006-04-05 04:59 +0000 [r526618]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/training/dvorak_ABCD.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/fr.a.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/training/russian_long.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchprefgenerallayout.ui,
	  branches/KDE/3.5/kdeedu/ktouch/training/catalan.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/spanish.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/nederlands_junior.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/french.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/danish.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/german.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchlectureeditor_dlg.ui,
	  branches/KDE/3.5/kdeedu/ktouch/training/dvorak_es.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/finnish_for_kids.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/nederlands.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/italian.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchlecture.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/training/dvorak.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/french2.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouch.kcfg,
	  branches/KDE/3.5/kdeedu/ktouch/training/danish2.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/norwegian.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/english.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchlectureeditor.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/training/german2.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/german3.ktouch.xml,
	  branches/KDE/3.5/kdeedu/ktouch/training/finnish.ktouch.xml:
	  KTouch - Changing default font to "Monospace"

2006-04-08 23:54 +0000 [r527642]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchpreftraininglayout.ui,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchprefgenerallayout.ui,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatus.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouch.h,
	  branches/KDE/3.5/kdeedu/ktouch/extras/sliding_line.odg (added),
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatus.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatistics.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src,
	  branches/KDE/3.5/kdeedu/ktouch/ChangeLog,
	  branches/KDE/3.5/kdeedu/ktouch/graphics/keyboard_grid.png
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyboardwidget.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchtrainer.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchprefkeyboardlayout.ui,
	  branches/KDE/3.5/kdeedu/ktouch/src/main.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouch.kcfg,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchslideline.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyboardwidget.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatistics_dlg.ui,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchslideline.h: KTouch -
	  several bug fixes and new features, see ChangeLog

2006-04-09 23:23 +0000 [r528059]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatisticsdata.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouch.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchcoloreditor.cpp (added),
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkey.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchcoloreditor.h (added),
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkey.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchui.rc,
	  branches/KDE/3.5/kdeedu/ktouch/src,
	  branches/KDE/3.5/kdeedu/ktouch/ChangeLog,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyconnector.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchcolorscheme.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchlecture.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyboardwidget.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchtrainer.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchcoloreditor_dlg.ui
	  (added),
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchprefcolorslayout.ui,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouch.kcfg,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchslideline.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchcolorscheme.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyconnector.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyboardwidget.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/Makefile.am,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeys.cpp: KTouch - added
	  color scheme editor and related changes

2006-04-10 04:47 +0000 [r528092]  ghorwin

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/de.keypad.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/ChangeLog,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/en.dvorak.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/bg.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de.neo.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyboardwidget.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/pl.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchslideline.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyboardwidget.h,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/dk.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/de.swiss.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/en.keyboard: KTouch -
	  fixed wrong keyboard layouts

2006-04-10 16:50 +0000 [r528304]  jschaub

	* branches/KDE/3.5/kdeedu/kalzium/src/kalziumtip.h,
	  branches/KDE/3.5/kdeedu/kalzium/src/kalziumtip.cpp: Don't stop
	  moving the KalziumTip if the mouse hovers it.

2006-04-12 18:10 +0000 [r529194-529192]  saxton

	* branches/KDE/3.5/kdeedu/kmplot/kmplot/View.cpp: Backport of fix
	  for incorrect integral calculation (bug 121051).

	* branches/KDE/3.5/kdeedu/kmplot/kmplot/View.cpp,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/parser.cpp,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/kminmax.cpp: Backport some
	  crash fixes (null pointers, derefencing empty lists) and use of
	  initialized variables.

2006-04-12 18:22 +0000 [r529197]  saxton

	* branches/KDE/3.5/kdeedu/kmplot/kmplot/View.cpp,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/parser.cpp,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/parser.h: Backport bug fix:
	  Small numbers were stored as strings using "e", e.g. "1.22e-6".
	  As "e" is a constant, this was parsed as "(1.22*e)-6". Now,
	  numbers are stored as e.g. "1.22*10^-6".

2006-04-12 18:27 +0000 [r529199]  saxton

	* branches/KDE/3.5/kdeedu/kmplot/kmplot/View.cpp: Backport bug fix:
	  Use [-pi,pi] interval as default interval for parametric
	  equations instead of using the current x-range.

2006-04-12 18:33 +0000 [r529203]  saxton

	* branches/KDE/3.5/kdeedu/kmplot/kmplot/xparser.cpp: Backport
	  bugfix: make default function color wrap around after 10
	  functions had been created (before, was stopping at the 10th
	  function and just returning the same color).

2006-04-14 08:28 +0000 [r529702]  deller

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am: remove
	  trailing slash on last line (fix configure
	  error:ktouch/keyboards/Makefile.am:40: trailing backslash on last
	  line)

2006-04-17 18:03 +0000 [r530816-530815]  bram

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.kcfg: I don't like
	  typos introduced during a message freeze.

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.kcfg: One more typo. :(

2006-04-27 14:34 +0000 [r534603]  mutlaqja

	* branches/KDE/3.5/kdeedu/kstars/kstars/fitshistogram.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/indidevice.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/fitsprocess.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/indistd.cpp: Thorsten's
	  patch

2006-05-03 20:40 +0000 [r537074]  aacid

	* branches/KDE/3.5/kdeedu/kbruch/src/taskwidget.cpp: backport
	  r536390

2006-05-03 20:44 +0000 [r537077]  aacid

	* branches/KDE/3.5/kdeedu/kbruch/src/factorizedwidget.cpp: Don't
	  leak on paintEvent CID #1892

2006-05-08 09:18 +0000 [r538540]  mueller

	* branches/KDE/3.5/kdeedu/kbruch/src/rationalwidget.cpp,
	  branches/KDE/3.5/kdeedu/kbruch/src/ratiowidget.cpp,
	  branches/KDE/3.5/kdeedu/kbruch/src/resultwidget.cpp: backport
	  memleak fixlets

2006-05-16 23:04 +0000 [r541679]  mueller

	* branches/KDE/3.5/kdeedu/kiten/xjdxgen.c: fix double free (CID
	  2194)

2006-05-21 10:27 +0000 [r543105]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/main.cpp: as Carsten said:
	  and let be a new minor version!

2006-05-21 18:48 +0000 [r543313]  lueck

	* branches/KDE/3.5/kdeedu/ktouch/training/german.ktouch.xml: fixed
	  spelling error in german lecture CCMAIL:nive@nivalis.org

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

