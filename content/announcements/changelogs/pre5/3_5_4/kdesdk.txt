2006-05-29 17:32 +0000 [r546298]  cloose

	* branches/KDE/3.5/kdesdk/cervisia/checkoutdlg.cpp: Clear branch
	  tag combobox before adding newly fetched list. BUG: 125075

2006-06-03 10:44 +0000 [r547766]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/petaltree2uml.cpp:
	  support nested components

2006-06-04 14:17 +0000 [r548033]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pythonwriter.cpp:
	  EBN says the "@since" and "@deprecated" doxygen tags should not
	  be used in the API documention for major versions.

2006-06-04 14:23 +0000 [r548035]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodeoperation.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubywriter.cpp:
	  EBN says do not compare a QString to "". Instead use the
	  .isEmpty() method.

2006-06-04 15:53 +0000 [r548114]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerationpolicy.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeparameter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/template.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/datatype.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/xmlschemawriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/classpropdlg.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/hierarchicalcodeblock.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/overwritedialogue.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenobjectwithtextblocks.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/codeeditor.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierlistitem.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeclassfield.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/configurable.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/worktoolbar.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/classifierinfo.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/enum.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/template.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javawriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javaclassifiercodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenobjectwithtextblocks.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeoperation.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppsourcecodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/enum.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entityattribute.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/operation.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodegenerationpolicypage.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/floatingtextwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifiercodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerator.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/ownedcodeblock.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/tclwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classparser/cpptree2uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubyclassifiercodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodegenerator.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeaccessormethod.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/umltemplatedialog.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/entityattribute.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/operation.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javacodeoperation.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/codeviewerdialog.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/classifierlistpage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/stereotype.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubycodegenerationpolicypage.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/model_utils.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javaantcodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodegenerationpolicy.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/enumliteral.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppheadercodedocument.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/hierarchicalcodeblock.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javaclassdeclarationblock.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/attribute.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/classoptionspage.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeclassfield.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppcodegenerationpolicypage.cpp:
	  fix spelling errors admonished by EBN

2006-06-05 04:16 +0000 [r548269]  danxuliu

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/codegenerationwizard.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/codegenerationwizardbase.ui,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/codegenerationwizard.cpp:
	  Attachment 16051 enables selection of multiple classes in "Select
	  classes" dialog. FEATURE: 126485

2006-06-11 21:12 +0000 [r550471]  reitelbach

	* branches/KDE/3.5/kdesdk/poxml/parser.cpp: Remove "appendix" from
	  the cuttingtags list again since it generates bad msgids on
	  template creation. CCMAIL:kde-i18n-doc@kde.org
	  CCMAIL:lueck@hube-lueck.de

2006-06-15 22:19 +0000 [r551891]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/aligntoolbar.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.h: sync
	  with trunk

2006-06-15 22:52 +0000 [r551903]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/javaimport.cpp:
	  parseStmt(): Handle "new" and sequemce of statements at state
	  member declaration. BUG:129107

2006-06-15 23:08 +0000 [r551912]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/VERSION,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: update

2006-06-16 17:48 +0000 [r552136]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/umlnamespace.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/listpopupmenu.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: Make proper
	  Uml::ListView_Type lvt_EnumLiteral. BUG:129252

2006-06-17 01:01 +0000 [r552216]  danxuliu

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppwriter.cpp:
	  Added missed ".h" to the name of the superclass header files
	  included in subclass header files.

2006-06-17 22:16 +0000 [r552478]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/adaimport.cpp:
	  parseStmt(): Fix setting of attribute initial value.

2006-06-17 23:33 +0000 [r552494]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/php5writer.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/jswriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerator.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/rubywriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pythonwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/javawriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/xmlschemawriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/tclwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/cppwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/adawriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerator.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/phpwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/simplecodegenerator.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/sqlwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/perlwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/aswriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/idlwriter.cpp:
	  findFileName(), overwritableName(): Fix return value to include
	  extension. Thanks Daniel for your commit 552216 which hints at
	  the problem.

2006-06-23 14:54 +0000 [r554219]  kleag

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators/docbookgenerator.h
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/common.ent
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umbrelloui.rc,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators/main.cpp
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/xmi2docbook.xsl
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators/docbookgenerator.cpp
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators/Makefile.am
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/configure.in.in: First version
	  of documentation generators

2006-06-24 12:41 +0000 [r554565]  goutte

	* branches/KDE/3.5/kdesdk/kbabel/kbabel/kbabelview.cpp: Do not try
	  to be smarter than the KSpell dialog on where it should be
	  created. BUG:16776

2006-06-24 17:41 +0000 [r554672]  goutte

	* branches/KDE/3.5/kdesdk/kbabel/kbabel/main.cpp,
	  branches/KDE/3.5/kdesdk/kbabel/ChangeLog,
	  branches/KDE/3.5/kdesdk/kbabel/kbabeldict/main.cpp,
	  branches/KDE/3.5/kdesdk/kbabel/catalogmanager/main.cpp: Use the
	  new URL for KBabel's web site: http://kbabel.kde.org

2006-06-24 18:29 +0000 [r554679]  goutte

	* branches/KDE/3.5/kdesdk/kbabel/common/kbproject.cpp: Allow to
	  open empty files as KBabel project files CCBUG:128939 (That is
	  not exactly the same bug, but it has the same symptom.)

2006-06-28 08:11 +0000 [r555673]  coolo

	* branches/KDE/3.5/kdesdk/scripts/svn2dist: avoid that trap for
	  others

2006-06-29 20:46 +0000 [r556295]  kleag

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/common.ent (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/docgenerators
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umbrelloui.rc,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/xmi2docbook.xsl
	  (removed), branches/KDE/3.5/kdesdk/umbrello/umbrello/main.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/configure.in.in: Withdraw of
	  freeze-breaking changes. Sorry...

2006-06-29 21:23 +0000 [r556306]  kleag

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/main.cpp: Removed
	  remaining traces from doc generators

2006-07-01 21:54 +0000 [r556909]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/javaimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: skipToClosing(): Add
	  check for QString::null return value from advance(). BUG:130093

2006-07-03 03:07 +0000 [r557369]  danxuliu

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierwidget.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifierwidget.cpp:
	  Fixes to some issues changing interface into class and vice
	  versa. BUG:79433

2006-07-12 13:29 +0000 [r561550]  danxuliu

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/main.cpp: Added support
	  for relative paths when exporting images from command line BUG:
	  130600

2006-07-12 15:40 +0000 [r561582]  danxuliu

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/main.cpp: Simplified
	  creation of directory URL to export images to. Thanks to Carsten
	  Pfeiffer for the suggestion. CCBUG: 130600

2006-07-15 19:51 +0000 [r562790]  shaforo

	* branches/KDE/3.5/kdesdk/kbabel/common/diff.cpp,
	  branches/KDE/3.5/kdesdk/kbabel/common/catalog.cpp,
	  branches/KDE/3.5/kdesdk/kbabel/kbabel/main.cpp,
	  branches/KDE/3.5/kdesdk/kbabel/common/diff.h: Word-by-word string
	  difference algorithm implementation (to replace old charDiff).
	  http://shafff.narod.ru/wordDiff-nq8.png - old (on the top) and
	  new algorithms. As one can see there is still an issue with
	  newlines (in that msgid only "<mousebutton>left</mousebutton>
	  mouse button" was changed to "&RMB;"), but it's not in diff.cpp
	  and about.addCredit("Eva Brucherseifer", I18N_NOOP("String
	  difference algorithm implementation"), "eva@kde.org"); ->
	  about.addCredit("Eva Brucherseifer", I18N_NOOP("String distance
	  algorithm implementation"), "eva@kde.org"); cause she contributed
	  stringdistance.cpp, not diff.cpp CCMAIL: nicolasg@snafu.de
	  CCMAIL: kde-i18n-doc@kde.org

2006-07-15 22:49 +0000 [r562827]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/activitywidget.h:
	  remove excess scope prefix

2006-07-15 23:17 +0000 [r562838]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/javaimport.cpp:
	  parseStmt(): Check for m_klass being NULL before dereferencing
	  it. Thanks to Clinton Grant for spotting this. CCBUG:130735

2006-07-15 23:21 +0000 [r562840]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/import_utils.cpp:
	  createGeneralization(UMLClassifier*, UMLClassifier*): If the
	  child is an interface then the parent must also be an interface.
	  Thanks to JP Fournier for helping. BUG:130793

2006-07-15 23:27 +0000 [r562841]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/assocrules.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog:
	  allowAssociation(Association_Type, UMLWidget*, UMLWidget*, bool)
	  case at_Activity: avoid illegal static_cast of widgetB to
	  ActivityWidget when it is really a ForkJoinWidget. BUG:129914

2006-07-15 23:34 +0000 [r562844]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/javaimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: parseStmt(): Use
	  m_currentAccess at call to Import_Utils::insertMethod(). Thanks
	  to JP Fournier for the patch. BUG:130794

2006-07-15 23:52 +0000 [r562848]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/javaimport.cpp:
	  parseStmt(): Patch by JP Fournier cleans up constructor detection
	  and token lookahead logic. CCCBUG:130792

2006-07-16 07:34 +0000 [r562901]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/THANKS: It doesn't crash or
	  freeze for me now, thanks again JP. BUG:130792

2006-07-16 13:23 +0000 [r563016]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/settingsdlg.cpp:
	  Disabling newcodegen for the time being to avoid a slew of
	  redundant bug reports. Notes to assignee of #84739: 1) To work on
	  the bug, #define the symbol BUG84739_FIXED. 2) Make sure all PRs
	  marked duplicate of #84739 pass. CCBUG:84739

2006-07-16 15:45 +0000 [r563062]  shaforo

	* branches/KDE/3.5/kdesdk/kbabel/common/diff.cpp,
	  branches/KDE/3.5/kdesdk/kbabel/common/catalog.cpp,
	  branches/KDE/3.5/kdesdk/kbabel/common/diff.h: fix
	  whole-line-coloring issue by deleting unneeded legacy code that
	  took ~1 mln cpu cycles per msg in diff mode CCMAIL:
	  nicolasg@snafu.de

2006-07-16 20:16 +0000 [r563149]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/nativeimportbase.cpp
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/pythonimport.cpp
	  (removed), branches/KDE/3.5/kdesdk/umbrello/umbrello/entity.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/umlwidgetcolorpage.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/adaimport.cpp
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/associationwidget.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pythonimport.cpp
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/nativeimportbase.cpp
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/javaimport.h
	  (removed), branches/KDE/3.5/kdesdk/umbrello/umbrello/cppimport.h
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/cppimport.cpp
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/umldoc.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classparser/cpptree2uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umllistview.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlobject.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/listpopupmenu.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/javaimport.h
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/adaimport.h
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/Makefile.am
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/import_utils.h
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classimport.cpp
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/idlimport.cpp
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/import_utils.h
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.h
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.h
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/classimport.h
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/javaimport.cpp
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/idlimport.h
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/cppimport.cpp
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/optionstate.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlnamespace.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/model_utils.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/javaimport.cpp
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/nativeimportbase.h
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/pythonimport.h
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/dialogs/umlattributedialog.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classifier.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/adaimport.h (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/adaimport.cpp
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/petaltree2uml.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pythonimport.h
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/nativeimportbase.h
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/cppimport.h
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/import_utils.cpp
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/import_utils.cpp
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/codegenfactory.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.cpp
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.cpp
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/classimport.h
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/classimport.cpp
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/idlimport.h
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/idlimport.cpp
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/optionstate.cpp
	  (added): Move all code import related files to a separate
	  subdirectory, codeimport. Prepare for Pascal import and
	  generator. ATM they are cheap copies from Ada, adaptation to
	  Pascal is up next. Also move the {set,get}OptionState() methods
	  from UMLApp to Settings because of a crash (that should have
	  happened long ago...) CCBUG:114547 CCBUG:107347

2006-07-16 20:52 +0000 [r563157]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/javaimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: Patches from JP
	  Fournier fix setting of method staticness and package visibility.
	  BUG:130926 BUG:130932

2006-07-17 07:54 +0000 [r563287]  tstaerk

	* branches/KDE/3.5/kdesdk/scripts/add_trace.pl: Works with
	  koffice/kspread/kspread_style.cc now

2006-07-17 08:05 +0000 [r563289]  tstaerk

	* branches/KDE/3.5/kdesdk/scripts/add_trace.pl: there may be a
	  MyconstVar

2006-07-17 22:47 +0000 [r563598]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/classparser/cpptree2uml.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/classparser/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/classparser (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/cppimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/classparser
	  (added), branches/KDE/3.5/kdesdk/umbrello/umbrello/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/classparser/cpptree2uml.cpp:
	  move directory classparser to codeimport/

2006-07-17 23:01 +0000 [r563603]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/kdevcppparser
	  (added),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/Makefile.am,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/cppimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/classparser
	  (removed),
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/kdevcppparser/Makefile.am:
	  rename directory classparser to kdevcppparser

2006-07-17 23:05 +0000 [r563604]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/Makefile.am: oops,
	  forgot this in r563603

2006-07-18 05:05 +0000 [r563652]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/javaimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog: parseStmt(): The
	  UMLObject found by createUMLObject might originally have been
	  created as a placeholder with a type of ot_Class but if is really
	  an interface, then we need to change it. Thanks to JP Fournier
	  for the fix. BUG:131006

2006-07-20 02:54 +0000 [r564394]  danxuliu

	* branches/KDE/3.5/kdesdk/umbrello/ChangeLog: Added missed changes
	  in the ChangeLog.

2006-07-20 19:38 +0000 [r564664]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.cpp:
	  Basic adaptations to Pascal syntax. Up next: PascalImport.
	  CCBUG:114547

2006-07-22 10:30 +0000 [r565067]  woebbe

	* branches/KDE/3.5/kdesdk/cervisia/Makefile.am,
	  branches/KDE/3.5/kdesdk/cervisia/version.h: at least one bug was
	  fixed -> increase version

2006-07-22 15:05 +0000 [r565158]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.cpp:
	  Generate protected and private attributes. Extend list of
	  reserved words.

2006-07-23 06:49 +0000 [r565343]  tstaerk

	* branches/KDE/3.5/kdesdk/scripts/add_trace.pl: handle comments
	  within signature

2006-07-23 07:17 +0000 [r565346-565345]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/idlimport.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/javaimport.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pythonimport.h:
	  cosmetics

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/uml.cpp:
	  activeLanguageIsCaseSensitive(): Pascal is not.

2006-07-23 07:22 +0000 [r565347]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/javaimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/nativeimportbase.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pythonimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/adaimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/nativeimportbase.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/idlimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.h,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/adaimport.h:
	  NativeImportBase::preprocess(): Handle multi-line comments in a
	  programming language independent way.
	  NativeImportBase::{setMultiLineComment, setMultiLineAltComment}:
	  New.

2006-07-23 08:04 +0000 [r565351]  tstaerk

	* branches/KDE/3.5/kdesdk/scripts/add_trace.pl: Do not treat
	  keyword const as a variable

2006-07-23 09:27 +0000 [r565366]  tstaerk

	* branches/KDE/3.5/kdesdk/scripts/add_trace.pl: backporting #483411

2006-07-23 09:58 +0000 [r565373]  tstaerk

	* branches/KDE/3.5/kdesdk/scripts/add_trace.pl: Cannot expect to
	  have kdebug included everywhere

2006-07-23 11:11 +0000 [r565397]  tstaerk

	* branches/KDE/3.5/kdesdk/scripts/add_trace.pl: Now gets along with
	  all of kspread

2006-07-23 13:19 +0000 [r565431]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.h:
	  Basic adaptations to Pascal syntax, still untested. Up next: Test
	  it. CCBUG:114547

2006-07-23 14:02 +0000 [r565476]  coolo

	* branches/KDE/3.5/kdesdk/kdesdk.lsm: preparing KDE 3.5.4

2006-07-23 15:45 +0000 [r565506-565505]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/pascalwriter.cpp:
	  defaultDatatypes(): Add yet more.

	* branches/KDE/3.5/kdesdk/umbrello/uml.lsm: update

2006-07-23 16:04 +0000 [r565522]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.h:
	  Start testing.

2006-07-24 04:21 +0000 [r565676]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/import_utils.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/javaimport.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/import_utils.h,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/javaimport.h:
	  Apply attachment id=17105 from JP Fournier 2006-07-24 > Basically
	  the idea is that when a new type in encountered an attempt is
	  made > to import the new type. This way associations and packages
	  are created > correctly. Most of this change is limited to the
	  java import class, however > in import_utils: > - added a new
	  version of insertAttribute that accepts a UMLClassifier* >
	  instead of a string > - adjusted createGeneralization to be able
	  to deal with realization > Kudos to JP for his many JavaImport
	  fixes (to appear in version 1.5.4.) BUG:131270

2006-07-24 04:26 +0000 [r565677]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codeimport/pascalimport.cpp:
	  Set the PascalImport to work - roughly. PascalImport right now
	  suffers from the same problems as JavaImport did concerning
	  placeholder class scoping. TODO: See how JP Fournier's fix to bug
	  131270 (r565676) can be applied to PascalImport. BUG:114547

