------------------------------------------------------------------------
r1170237 | sitter | 2010-08-31 21:31:47 +1200 (Tue, 31 Aug 2010) | 4 lines

backport r1167379

put in the more appropriate hardware category

------------------------------------------------------------------------
r1170243 | sitter | 2010-08-31 21:43:35 +1200 (Tue, 31 Aug 2010) | 4 lines

Backport of r1170241

Addition to the PyQt 4.7 fixes of r1167347

------------------------------------------------------------------------
r1174635 | scripty | 2010-09-13 14:35:47 +1200 (Mon, 13 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1175012 | pino | 2010-09-14 09:27:39 +1200 (Tue, 14 Sep 2010) | 4 lines

rename as hicolor, so they can be used when the current icon theme is not oxygen

they are already installed in the data dir of kuser, so there's no pollution of the global hicolor icon theme

------------------------------------------------------------------------
r1175350 | lueck | 2010-09-15 07:48:32 +1200 (Wed, 15 Sep 2010) | 1 line

remove unused screenshots
------------------------------------------------------------------------
r1178840 | scripty | 2010-09-24 14:42:28 +1200 (Fri, 24 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180118 | scripty | 2010-09-27 15:52:58 +1300 (Mon, 27 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
