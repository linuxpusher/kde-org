2007-05-16 18:51 +0000 [r665378]  weidendo

	* branches/KDE/3.5/kdeaddons/konq-plugins/fsview/treemap.cpp: Fix
	  bug 145454

2007-06-23 09:53 +0000 [r679191]  mlaurent

	* branches/KDE/3.5/kdeaddons/kate/tabbarextension/Makefile.am: Fix
	  extract i18n into .h file

2007-09-25 21:45 +0000 [r717037]  dfaure

	* branches/KDE/3.5/kdeaddons/konq-plugins/dirfilter/dirfilterplugin.cpp:
	  Konqueror performance patch 2: don't force mimetype resolution
	  from the dirfilter plugin, so that delayed-mimetype-determination
	  has some chance of working

2007-09-26 14:16 +0000 [r717276]  dfaure

	* branches/KDE/3.5/kdeaddons/konq-plugins/dirfilter/dirfilterplugin.cpp:
	  Konqueror performance patch 3: iconName() calls
	  determineMimeType() too; use the mimetype icon instead (not a
	  possibly-file-specific icon).

2007-09-28 13:53 +0000 [r718346]  mueller

	* branches/KDE/3.5/kdeaddons/konq-plugins/mediarealfolder/kio_media_realfolder:
	  fix shell insertion vulnerability (novell 326912)

2007-10-01 20:50 +0000 [r719763]  alund

	* branches/KDE/3.5/kdeaddons/kate/filetemplates/plugin/filetemplates.cpp:
	  A bit embarrassing, using any file as a template has been broken
	  for 3 years - since jowenn switched the plugin to the new
	  template system... Fix is reviewed and tested by dominik haumann

2007-10-08 11:05 +0000 [r722959]  coolo

	* branches/KDE/3.5/kdeaddons/kdeaddons.lsm: updating lsm

