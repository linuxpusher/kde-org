2007-05-15 19:17 +0000 [r665076]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/jni.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/jni_md.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/obsolete/protypes.h
	  (added), branches/KDE/3.5/kdebase/nsplugins/sdk/npupp.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/jri.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/jri_md.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/obsolete (added),
	  branches/KDE/3.5/kdebase/nsplugins/sdk/prtypes.h (added),
	  branches/KDE/3.5/kdebase/nsplugins/sdk/README (removed),
	  branches/KDE/3.5/kdebase/nsplugins/sdk/npapi.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/jritypes.h,
	  branches/KDE/3.5/kdebase/nsplugins/sdk/prcpucfg.h (added):
	  Replace the Netscape plugins SDK files with ones from Mozilla
	  which have a more friendly license.

2007-05-15 19:38 +0000 [r665080]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/sdk/prcpucfg.h: Newer version
	  of this file to support also x64-86. Could you please check now
	  the support is the same like after r651938 (I'll do the same in
	  trunk)? CCMAIL:woebbeking@kde.org CCMAIL:gb.public@free.fr

2007-05-16 14:35 +0000 [r665307]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/sdk/npapi.h: Go back to
	  NP_VERSION_MINOR being 11, just in case. To quote Maksim: I know
	  that having a lower one is fine --- it implies potentially
	  smaller function structures, and higher version may require
	  implementing more stuff. The reason I asked is that I did
	  something similar internally when working on npruntime support,
	  and I know flash 7 would crash with some value of
	  NP_VERSION_MINOR, but I am not sure whether it was 13 or 14.

2007-05-18 15:24 +0000 [r666023]  mueller

	* branches/KDE/3.5/kdebase/khelpcenter/khc_indexbuilder.cpp,
	  branches/KDE/3.5/kdebase/khelpcenter/khc_indexbuilder.h: enter
	  the event loop at least once, otherwise kapp->quit doesn't quit

2007-05-18 18:19 +0000 [r666082]  ossi

	* branches/KDE/3.5/kdebase/kdesktop/lock/autologout.cc: backport:
	  don't shutdown at autologout.

2007-05-18 18:54 +0000 [r666094]  ossi

	* branches/KDE/3.5/kdebase/kcontrol/screensaver/scrnsave.cpp:
	  backport: make the LockGrace maximum consistent with the actual
	  maximum in kdesktop_lock.

2007-05-25 12:40 +0000 [r668198]  lunakl

	* branches/KDE/3.5/kdelibs/kinit/Makefile.am,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdelibs/kinit/start_kdeinit.c,
	  branches/KDE/3.5/kdelibs/kinit/start_kdeinit_wrapper.c (added):
	  Fix the problem that the setuid kdeinit wrapper causes unsetting
	  some variables like LD_LIBRARY_PATH, I knew there would be
	  somebody who'd know what to do with it - add yet another wrapper
	  that saves the environment and use it again from withing the
	  setuid wrapper.

2007-05-28 10:19 +0000 [r669004-669001]  mueller

	* branches/KDE/3.5/kdebase/kicker/applets/minipager/pagerapplet.h:
	  the usual "daily unbreak compilation"

	* branches/KDE/3.5/kdebase/kioslave/man/kio_man.cpp,
	  branches/KDE/3.5/kdebase/kioslave/sftp/sftpfileattr.h,
	  branches/KDE/3.5/kdebase/kioslave/floppy/kio_floppy.h,
	  branches/KDE/3.5/kdebase/kioslave/thumbnail/imagecreator.h,
	  branches/KDE/3.5/kdebase/kioslave/man/kmanpart.h,
	  branches/KDE/3.5/kdebase/kioslave/sftp/process.h,
	  branches/KDE/3.5/kdebase/kioslave/thumbnail/djvucreator.h,
	  branches/KDE/3.5/kdebase/kioslave/media/libmediacommon/medium.h,
	  branches/KDE/3.5/kdebase/kioslave/nfs/kio_nfs.h,
	  branches/KDE/3.5/kdebase/kioslave/floppy/program.h,
	  branches/KDE/3.5/kdebase/kioslave/thumbnail/cursorcreator.h:
	  various things I had uncommitted on disk

	* branches/KDE/3.5/kdebase/kwin/options.cpp: compiler warning--

2007-05-29 14:32 +0000 [r669488]  lunakl

	* branches/KDE/3.5/kdebase/konsole/konsole/main.cpp: Real
	  transparency support without side effects, if qt-copy patch #0078
	  is available (#83974).

2007-05-30 18:36 +0000 [r669899]  lunakl

	* branches/KDE/3.5/kdebase/kicker/applets/minipager/pagerapplet.h,
	  branches/KDE/3.5/kdelibs/kdecore/netwm.cpp,
	  branches/KDE/3.5/kdebase/kwin/workspace.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/netwm_def.h,
	  branches/KDE/3.5/kdelibs/kdecore/netwm_p.h,
	  branches/KDE/3.5/kdelibs/kdecore/netwm.h,
	  branches/KDE/3.5/kdebase/kwin/workspace.h,
	  branches/KDE/3.5/kdebase/kwin/events.cpp,
	  branches/KDE/3.5/kdebase/kicker/applets/minipager/pagerapplet.cpp:
	  Backport _NET_DESKTOP_LAYOUT support from trunk. This will allow
	  mixing kde3/kde4 KWin and also fix #146040. BUG: 146040

2007-06-01 08:48 +0000 [r670383]  mueller

	* branches/KDE/3.5/kdebase/pics/wallpapers/triplegears.jpg.desktop:
	  fix non-UTF8 encoding even though UTF-8 was declared

2007-06-05 15:20 +0000 [r671822]  dfaure

	* branches/KDE/3.5/kdebase/kdesktop/xautolock.cc: sshht

2007-06-06 09:18 +0000 [r672169]  lunakl

	* branches/KDE/3.5/kdebase/kwin/workspace.cpp: Set usable desktop
	  layout default if none is given.

2007-06-06 14:38 +0000 [r672252]  mueller

	* branches/KDE/3.5/kdebase/kcheckpass/kcheckpass.c: make sure that
	  there are no strange integer overflows

2007-06-10 20:03 +0000 [r673664]  craig

	* branches/KDE/3.5/kdebase/kcontrol/fonts/kxftconfig.cpp: Use
	  00kde.conf as root config file. BUG:146637

2007-06-13 15:16 +0000 [r674933]  mueller

	* branches/KDE/3.5/kdebase/kcontrol/kfontinst/viewpart/kfontview.desktop:
	  fix desktop file to validate

2007-06-13 16:17 +0000 [r674951]  winterz

	* branches/KDE/3.5/kdebase/kioslave/smtp/command.cc,
	  branches/KDE/3.5/kdebase/kioslave/pop3/pop3.cc: Fix bug "SASL
	  Authentication fails if another client of sasl is loaded" Patch
	  by Andreas. Thanks! BUGS: 146582

2007-06-16 03:24 +0000 [r676115]  gamaral

	* branches/KDE/3.5/kdebase/kate/app/katesession.cpp: Local
	  KDialogBase gets destroyed insided KMessageDialog and causes
	  crash.

2007-06-17 16:40 +0000 [r676759]  mlaurent

	* branches/KDE/3.5/kdebase/kdepasswd/kcm/main.cpp: Fix mem leak

2007-06-19 08:09 +0000 [r677489]  dfaure

	* branches/KDE/3.5/kdebase/konqueror/konq_mainwindow.cc: Make "new
	  tab" faster when the current view isn't an html view, e.g. when
	  it's a directory view. (it used to load a directory view in the
	  new tab and then replace it immediately with khtml)

2007-06-19 16:16 +0000 [r677645]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/client.c: backport: malloc
	  the groups array with the proper size - i found NGROUPS to be
	  unexpectedly large.

2007-06-21 17:25 +0000 [r678557]  binner

	* branches/KDE/3.5/kdebase/konqueror/kfmclient_dir.desktop,
	  branches/KDE/3.5/kdebase/kate/data/kate.desktop,
	  branches/KDE/3.5/kdebase/kate/data/kwrite.desktop,
	  branches/KDE/3.5/kdebase/kcontrol/kthememanager/installktheme.desktop,
	  branches/KDE/3.5/kdebase/kdeprint/printmgr/printers.desktop,
	  branches/KDE/3.5/kdebase/konqueror/kfmclient_war.desktop,
	  branches/KDE/3.5/kdebase/kcontrol/kicker/uninstall.desktop,
	  branches/KDE/3.5/kdebase/konsole/konsolesu.desktop,
	  branches/KDE/3.5/kdebase/ksysguard/gui/ksysguard.desktop: fix
	  invalid .desktop files

2007-06-23 09:16 +0000 [r679179]  mlaurent

	* branches/KDE/3.5/kdebase/konqueror/Makefile.am: fix extract i18n
	  into keditbookmarks/*.h

2007-06-23 18:56 +0000 [r679365]  ossi

	* branches/KDE/3.5/kdebase/kcheckpass/checkpass_pam.c: backport:
	  new PAM versions don't call openlog() themselves

2007-06-24 20:32 +0000 [r679750]  jschaub

	* branches/KDE/3.5/kdebase/kicker/libkicker/kickertip.cpp: enable
	  tooltips again, after user clicked kickertip away

2007-06-24 20:54 +0000 [r679760]  jschaub

	* branches/KDE/3.5/kdebase/kicker/libkicker/kickertip.cpp: readd
	  m_frameTimer.stop()

2007-06-29 09:09 +0000 [r681524]  ossi

	* branches/KDE/3.5/kdebase/kdm/kfrontend/kgreeter.cpp,
	  branches/KDE/3.5/kdebase/kdm/kfrontend/kgreeter.h: workaround bug
	  #107408 by disabling the menu button on failed login. with qt4,
	  otoh, it Just Works (TM).

2007-07-09 11:06 +0000 [r685636]  ossi

	* branches/KDE/3.5/kdebase/kdm/kfrontend/krootimage.cpp: backport:
	  change wallpaper at startup already

2007-07-12 12:26 +0000 [r686916]  tyrerj

	* branches/KDE/3.5/kdebase/kappfinder/apps/Internet/javaws.desktop:
	  Adding icon to javaws.desktop

2007-07-13 14:44 +0000 [r687409]  lunakl

	* branches/KDE/3.5/kdebase/kicker/applets/minipager/pagerapplet.cpp:
	  Handle viewport properly with Xinerama.

2007-07-14 19:38 +0000 [r687933]  porten

	* branches/KDE/3.5/kdebase/konqueror/konq_combo.cc: show the
	  beginning of urls rather than the end.

2007-07-15 18:02 +0000 [r688299]  porten

	* branches/KDE/3.5/kdebase/konqueror/konq_combo.cc: added comment
	  as suggested by dirk

2007-07-16 09:13 +0000 [r688530]  mueller

	* branches/KDE/3.5/kdebase/kioslave/smtp/smtp.cc,
	  branches/KDE/3.5/kdebase/kioslave/smtp/command.cc,
	  branches/KDE/3.5/kdebase/kioslave/pop3/pop3.cc: clean up the mess
	  introduced by the SASL update patches and make pop3 work again by
	  removing the local GETOPT hook and not registering global hooks.
	  They're not supposed to be used anyway

2007-07-16 11:56 +0000 [r688602]  binner

	* branches/KDE/3.5/kdebase/khelpcenter/kcmhelpcenter.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/kcontrol/modules.cpp: ensure
	  that kcmshell matching the running KDE version is called

2007-07-16 20:34 +0000 [r688783]  mlaurent

	* branches/KDE/3.5/kdebase/kappfinder/main.cpp: Return a value

2007-07-27 16:39 +0000 [r693283]  helio

	* branches/KDE/3.5/kdebase/kcontrol/arts/arts.cpp: - Write init
	  options for arts. Missing for long time :-/

2007-07-27 16:46 +0000 [r693291]  helio

	* branches/KDE/3.5/kdebase/libkonq/kfileivi.cc: - Avoid preview
	  crashes when view is invalid

2007-07-27 16:59 +0000 [r693299]  helio

	* branches/KDE/3.5/kdebase/kwin/kwin.kcfg: - Adding missing
	  xinerama options on kcfg

2007-07-27 17:05 +0000 [r693306]  helio

	* branches/KDE/3.5/kdebase/kcontrol/crypto/Makefile.am: - Define
	  libdir for crypt module in case of use a diff standard libdir,
	  most common on case of lib64

2007-07-27 17:11 +0000 [r693310]  helio

	* branches/KDE/3.5/kdebase/kcontrol/kthememanager/kthememanager.cpp:
	  - Prevent theme manager to enable apply theme in invalid
	  directory

2007-07-27 17:16 +0000 [r693312]  helio

	* branches/KDE/3.5/kdebase/kwin/kwin.kcfg: - Extra <group> closing

2007-07-27 17:23 +0000 [r693314]  helio

	* branches/KDE/3.5/kdebase/kdesktop/init/Home.desktop,
	  branches/KDE/3.5/kdebase/kdesktop/init/System.desktop: - Show
	  home and system kde icons only on kde if other desktops share
	  same tree

2007-07-27 17:32 +0000 [r693315]  helio

	* branches/KDE/3.5/kdebase/kscreensaver/KBlankscreen.desktop,
	  branches/KDE/3.5/kdebase/kscreensaver/KRandom.desktop: - Show
	  specific screensavers only on kde

2007-07-27 19:38 +0000 [r693353]  helio

	* branches/KDE/3.5/kdebase/kdesktop/init/Home.desktop,
	  branches/KDE/3.5/kdebase/kdesktop/init/System.desktop: -
	  OnlyShowIn is a list. need terminates in ;. Thanks to Coolo to
	  point this

2007-07-27 19:41 +0000 [r693354]  helio

	* branches/KDE/3.5/kdebase/kscreensaver/KBlankscreen.desktop,
	  branches/KDE/3.5/kdebase/kscreensaver/KRandom.desktop: -
	  OnlyShowIn is a list. need terminates in ;. Thanks to Coolo to
	  point this

2007-07-29 21:07 +0000 [r694035]  mlaurent

	* branches/KDE/3.5/kdebase/kcontrol/crypto/Makefile.am: Forward
	  port: there is some i18n in .h file

2007-08-02 12:58 +0000 [r695581]  lunakl

	* branches/KDE/3.5/kdebase/kwin/kwin.kcfg: Revert r693299, there
	  are no such options in 3.5 branch. CCMAIL: helio@kde.org

2007-08-02 21:52 +0000 [r695756]  mueller

	* branches/KDE/3.5/kdebase/konsole/konsole/main.cpp,
	  branches/KDE/3.5/kdebase/konsole/konsole/konsole.h: don't
	  redefine package/version, causes build error with gcc 4.3

2007-08-10 10:57 +0000 [r698562]  dfaure

	* branches/KDE/3.5/kdebase/konqueror/konq_mainwindow.cc,
	  branches/KDE/3.5/kdebase/konqueror/konq_viewmgr.cc: When loading
	  a new url into an existing view, don't update the location bar
	  immediately, but when starting to load it into the view for real.
	  This fixes: - click on link which opens in new app: the url was
	  shown temporarily and then reverted - constant redirects would
	  keep updating the location bar url, masking the current url But
	  we still show the url immediately when loading a url into a new
	  window or tab, for early user feedback. Also fixed
	  double-part-activation bug which was clearing up the url
	  temporarily during startup (and slowing down startup, too, since
	  AFAICS it was doing xmlgui-building twice for KHTML).

2007-08-13 14:40 +0000 [r699566]  hmeine

	* branches/KDE/3.5/kdebase/konqueror/konq_viewmgr.cc: extending
	  David's fix from r698562 to some more use cases by preventing
	  multiple calls to emitActivePartChanged() in a central place.
	  Especially, this fixes the aanoying delayed locationbar clearing
	  after pressing Ctrl-N (new tab) and typing right away. (Approved
	  by dfaure)

2007-08-21 19:01 +0000 [r703013]  deller

	* branches/KDE/3.5/kdebase/kcontrol/usbview/usb.ids,
	  branches/KDE/3.5/kdebase/kcontrol/usbview/usbdb.cpp: Bugfix:
	  Prefer distribution provided usb.ids file over the usb.ids file
	  which is shipped with KDE. Additionally update KDE's usb.ids file
	  to latest available version. BUG: 148943

2007-08-23 15:04 +0000 [r703895]  lunakl

	* branches/KDE/3.5/kdebase/kwin/events.cpp: No idea why I removed
	  this in r603293. BUG: 149011

2007-08-23 15:11 +0000 [r703898]  lunakl

	* branches/KDE/3.5/kdebase/kwin/events.cpp: When removing grabs
	  from the set, first grab all, don't assume it's there from
	  somewhen before.

2007-08-24 09:42 +0000 [r704198]  lunakl

	* branches/KDE/3.5/kdebase/kwin/activation.cpp,
	  branches/KDE/3.5/kdebase/kwin/workspace.h,
	  branches/KDE/3.5/kdebase/kwin/events.cpp: In mouse focus
	  policies, change active window only when the mouse actually
	  moves, not as a result of window changes (#92290).

2007-08-27 12:55 +0000 [r705205]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/plugin_paths.h (added),
	  branches/KDE/3.5/kdebase/nsplugins/pluginscan.cpp,
	  branches/KDE/3.5/kdebase/nsplugins/Makefile.am,
	  branches/KDE/3.5/kdebase/nsplugins/kcm_nsplugins.cpp,
	  branches/KDE/3.5/kdebase/nsplugins/plugin_paths.cpp (added):
	  Defaulting to no scanning for new plugins is nice from
	  performance point of view, but it's completely unreasonable to
	  expect the user to run the check manually. Check timestamps to
	  find out if a full scan is needed. BUG: 126744

2007-08-27 14:44 +0000 [r705238]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/konqhtml/pluginopts.cpp,
	  branches/KDE/3.5/kdebase/nsplugins/plugin_paths.cpp: Update note
	  about keeping files in sync.

2007-08-27 15:52 +0000 [r705262]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/kcm_nsplugins.cpp: Slightly
	  better the case when paths change.

2007-08-27 20:34 +0000 [r705371]  helio

	* branches/KDE/3.5/kdebase/nsplugins/Makefile.am: - Same source on
	  binary and library

2007-08-28 14:03 +0000 [r705752]  lunakl

	* branches/KDE/3.5/kdebase/kicker/taskmanager/taskmanager.cpp:
	  Avoid reports about X errors.

2007-08-30 09:16 +0000 [r706401]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/Makefile.am: *** Warning:
	  Linking the shared library ../nsplugins/kcm_nsplugins.la against
	  the *** static library ../nsplugins/libpluginpaths.a is not
	  portable!

2007-09-03 04:30 +0000 [r707819]  adawit

	* branches/KDE/3.5/kdebase/kcontrol/kio/kcookiespolicies.cpp: - Do
	  not loose the domain-specific cookie policies when cookie support
	  is disabled and re-enabled again. BUG:117302

2007-09-03 05:28 +0000 [r707822]  adawit

	* branches/KDE/3.5/kdebase/kcontrol/kio/kcookiesmanagement.h,
	  branches/KDE/3.5/kdebase/kcontrol/kio/kcookiesmanagement.cpp:
	  When delete all is pressed, only delete the visible cookies if
	  the view is filtered. BUG:113497

2007-09-03 05:37 +0000 [r707823]  adawit

	* branches/KDE/3.5/kdebase/kcontrol/kio/kcookiesmanagement.cpp:
	  Undo the removal of m_bDeleteAll from ::reset

2007-09-04 09:29 +0000 [r708238]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/background/bgrender.cpp: Avoid
	  infinite loop.

2007-09-06 18:49 +0000 [r709189]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/session.c: backport: don't
	  attempt to auto-login after a post-session shutdown confirmation

2007-09-06 20:07 +0000 [r709204]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/session.c: backport: fix up
	  tdiff handling a bit.

2007-09-07 10:08 +0000 [r709355]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/client.c: backport:
	  passwd/shadow (non-pam, non-aix) auth: don't virtually reveal
	  that a user does not exist.

2007-09-07 11:37 +0000 [r709384]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/client.c: backport:
	  passwd/shadow (non-pam, non-aix) auth: explicitly detect disabled
	  accounts for better debug.

2007-09-07 14:10 +0000 [r709438]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/Makefile.am,
	  branches/KDE/3.5/kdebase/kdm/backend/client.c: backport: exclude
	  disabled accounts (in particular system accounts) from
	  NoPassUsers=*

2007-09-08 19:33 +0000 [r709978]  marchand

	* branches/KDE/3.5/kdebase/kcontrol/ebrowsing/plugins/ikws/searchproviders/deb.desktop:
	  backport fix for 149501, update to new debian URL

2007-09-08 22:43 +0000 [r710023]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/client.c: fix compile. kde 3
	  is weird. :)

2007-09-14 09:28 +0000 [r712356]  hasso

	* branches/KDE/3.5/kdebase/kioslave/man/man2html.cpp: Backport
	  712309 and 712343. - Add DragonFly (Dx) mdoc(7) macro. - Sections
	  for BSD systems according to mdoc(7).

2007-09-17 15:08 +0000 [r713445]  lunakl

	* branches/KDE/3.5/kdebase/ksmserver/legacy.cpp: One more app for
	  the broken Mozilla apps family.

2007-09-19 14:12 +0000 [r714420]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/session.c: backport: fix
	  CVE-2007-4569 and memleaks

2007-09-21 14:20 +0000 [r715221]  mueller

	* branches/KDE/3.5/kdebase/kdmlib/kgreet_winbind.cpp,
	  branches/KDE/3.5/kdebase/kdmlib/kgreet_winbind.h: - don't use
	  --all-domains, only use --trusted-domains and --own-domain -
	  regularly poll for new domains, on startup the domain
	  registration is not finished yet

2007-09-21 20:23 +0000 [r715318]  dfaure

	* branches/KDE/3.5/kdebase/kioslave/tar/tar.cc: Fix wrong redirect
	  when clicking on a symlink in tar:/ (#149903) Extraction still
	  copies the target -- that's by design in kio due to #5601; tricky
	  problem...

2007-09-22 10:40 +0000 [r715536]  aacid

	* branches/KDE/3.5/kdebase/kate/app/katesession.cpp,
	  branches/KDE/3.5/kdebase/kate/app/katesession.h: Session Chooser
	  fixes: Do not disable quit button when no session is selected but
	  the open session button Pressing X now triggers the open session
	  button, when it should trigger quit button Approved by dhaumann

2007-09-25 17:32 +0000 [r716946]  dfaure

	* branches/KDE/3.5/kdebase/kioslave/thumbnail/imagethumbnail.desktop:
	  Fix invalid separator (found when listing all mimetypes with
	  previews, and "image/cgm;image/fax-g3" was in the list)

2007-09-25 20:08 +0000 [r716991]  dfaure

	* branches/KDE/3.5/kdebase/libkonq/konq_popupmenu.cc: had this
	  lying around: resolve media: and system: urls to local files in
	  the popup menu when looking for servicemenus and
	  .desktop-file-provided-actions

2007-09-26 12:24 +0000 [r717253]  lunakl

	* branches/KDE/3.5/kdebase/kdesktop/xautolock.cc: Don't allow the X
	  builtin screensaver to activate when our screensaver is blocked
	  by DPMS turned off.

2007-09-27 09:17 +0000 [r717643]  lunakl

	* branches/KDE/3.5/kdebase/kdesktop/lock/lockprocess.cc: Fix race
	  condition reported by Danny Baumann <dannybaumann@web.de> - when
	  creating a new X window for the screensaver, it is temporarily
	  mapped while not being override_redirect, causing race
	  conditions.

2007-10-01 09:50 +0000 [r719539]  lunakl

	* branches/KDE/3.5/kdebase/nsplugins/sdk/npapi.h: Put back r651938
	  (proper bit width for 32bit types). BUG: 150241

2007-10-04 11:07 +0000 [r721013]  dfaure

	* branches/KDE/3.5/kdebase/konqueror/konq_mainwindow.cc: Fix crash
	  when the window is deleted from onmousedown, which takes effect
	  inside the RMB popupmenu event loop. BUG: 149736

2007-10-08 11:05 +0000 [r722962]  coolo

	* branches/KDE/3.5/kdebase/kdebase.lsm: updating lsm

2007-10-08 11:08 +0000 [r722979-722978]  coolo

	* branches/KDE/3.5/kdebase/konqueror/version.h: update for tag

	* branches/KDE/3.5/kdebase/startkde: update for tag

