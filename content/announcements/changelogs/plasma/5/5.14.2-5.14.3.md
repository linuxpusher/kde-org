---
aliases:
- /announcements/plasma-5.14.2-5.14.3-changelog
hidden: true
plasma: true
title: Plasma 5.14.3 Complete Changelog
type: fulllog
version: 5.14.3
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- Link \*-resize to the right icons. <a href='https://commits.kde.org/breeze/eb3498d9a797e3e21f0e722ecc0c9507c6f1b8ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15950'>D15950</a>
- [cursors] Also link for Breeze Snow. <a href='https://commits.kde.org/breeze/6e2a3815cbc44393ab2366c72e1f79b7fa412b99'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15920'>D15920</a>
- [Cursors] link _-resize to _\_corner cursor. <a href='https://commits.kde.org/breeze/0383cd9adc75c3149b4bd0799dd98b44b6244040'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399194'>#399194</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15889'>D15889</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Snap: improve transaction status. <a href='https://commits.kde.org/discover/e2c1814b06d37d5c274f6354e15f40bcba60e9b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396351'>#396351</a>
- Use the locale to create the release date string. <a href='https://commits.kde.org/discover/a3485f6b306fab8b903cefa9d4e7bb15e9964b0d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400314'>#400314</a>
- Pk: readability. <a href='https://commits.kde.org/discover/8529597eae75c31bf2affd1b5210e6a59995801e'>Commit.</a>
- Flatpak: use the path to identify a local remote. <a href='https://commits.kde.org/discover/892095df5d44f4adaea5bf29f2f52284bc06041a'>Commit.</a>
- Flatpak: fix freeze. <a href='https://commits.kde.org/discover/0be86e272533a0ce8bcc7d7046436fedef7b29d8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400212'>#400212</a>
- Flatpak: use pkgconfig targets instead of using variables. <a href='https://commits.kde.org/discover/d9ccf1d41fc3265aee9e01eebbc090b163fefe07'>Commit.</a>
- Flatpak: fix warning. <a href='https://commits.kde.org/discover/a7fccfbaea8e91e8589098ca07aebe6cd0e1bb30'>Commit.</a>
- Fwupd: improve fwupd lookup. <a href='https://commits.kde.org/discover/2fe3d58fc652fc9957f063cbbd0722dc1cf45730'>Commit.</a>
- Fwupd: Cancel job upon close. <a href='https://commits.kde.org/discover/15f9044ea8958ed13b17403c14ba3079ca8c02eb'>Commit.</a>
- Fwupd: don't report passive errors about unsupported devices. <a href='https://commits.kde.org/discover/945273ffdc909eed112e94c7258726f8f41b2026'>Commit.</a>
- Fwupd: fix error handling logic. <a href='https://commits.kde.org/discover/1e309086880788997a4d3d368a03884160511451'>Commit.</a>
- Fwupd: --output. <a href='https://commits.kde.org/discover/4cda66dc1aec0dc2176e9fcb652ec9dba69a101a'>Commit.</a>
- Fwupd: no need to pass a pointer reference to read errors. <a href='https://commits.kde.org/discover/e011fc0cf175dce4aa87ef379f1554dfa011c700'>Commit.</a>
- Fwupd: don't ignore fwupd_client_get_devices errors. <a href='https://commits.kde.org/discover/424b8744b98564ffc07d68f513e73b26b9266e94'>Commit.</a>
- Fwupd: don't ignore get releases errors. <a href='https://commits.kde.org/discover/f13f6c9a293afb0e0e20254c99ddde525a14e45c'>Commit.</a>
- Fwupd: silence invalid file errors. <a href='https://commits.kde.org/discover/084df9d90a5f3b2e377aef1aa5c3dec48b7e6dc1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400181'>#400181</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [comicprovider] Restart timer on redirect request. <a href='https://commits.kde.org/kdeplasma-addons/7c7c00798048a036a47a5b888c65a715f3629e24'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15616'>D15616</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Flush kwinrulesrc in RuleBook::save. <a href='https://commits.kde.org/kwin/95ff4a2d49d897739f58e931454edf0272798038'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399778'>#399778</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16580'>D16580</a>
- [effects/desktopgrid] Don't display the close button from Present Windows. <a href='https://commits.kde.org/kwin/558b86e9f6c08fde10933b3a3c7c126ba287c408'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364710'>#364710</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16513'>D16513</a>
- [effects/presentwindows] Avoid potential freeze during fill-gaps. <a href='https://commits.kde.org/kwin/4348cd56834cb17da5aa9d95d16ddc27bf39e0e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364709'>#364709</a>. Fixes bug <a href='https://bugs.kde.org/380865'>#380865</a>. Fixes bug <a href='https://bugs.kde.org/368811'>#368811</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16278'>D16278</a>
- [effects/thumbnailaside] Save shortcuts. <a href='https://commits.kde.org/kwin/00c9b4c1c931c000300bf06bdca5c3a82c559b65'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400351'>#400351</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16456'>D16456</a>
- [effects/desktopgrid] Specify screen projection matrix when drawing moving window. <a href='https://commits.kde.org/kwin/408ed80604bb52870469a4f76704c224e15c02aa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361371'>#361371</a>. Fixes bug <a href='https://bugs.kde.org/364509'>#364509</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16430'>D16430</a>
- [effects/desktopgrid] Specify screen projection matrix when drawing moving window. <a href='https://commits.kde.org/kwin/91b538105ad312ed035fb7a0e3d798e88a2ae6f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361371'>#361371</a>. Fixes bug <a href='https://bugs.kde.org/364509'>#364509</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16430'>D16430</a>
- [effects/invert] Don't block blur and background contrast effect. <a href='https://commits.kde.org/kwin/9fae2f4905d5684f19d79bc2a5f839d9f07d7890'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/279076'>#279076</a>. Fixes bug <a href='https://bugs.kde.org/359583'>#359583</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16358'>D16358</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [Libinput KCMs] Remove tiny dots by using Kirigami.Separator correctly. <a href='https://commits.kde.org/plasma-desktop/f72502ed9c5767790b4739b50ae370b4b69f3abe'>Commit.</a>
- Swap Trash/Delete when Shift is used while the menu is open. <a href='https://commits.kde.org/plasma-desktop/c4285ed1b40714872f002f4fcbb067760c215928'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395155'>#395155</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16368'>D16368</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Use appropriate icon in system tray settings. <a href='https://commits.kde.org/plasma-nm/239687eabbce09886a884180d7decd5f9dd94c4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400587'>#400587</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Set appropriate icon for plasmoid. <a href='https://commits.kde.org/plasma-pa/125c716889b91cdc1abcb384933c747721df969c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400586'>#400586</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16615'>D16615</a>
- Fix connecting to PulseAudio with Qt 5.12 beta 3. <a href='https://commits.kde.org/plasma-pa/c255ac792a0d6018ee19f98dfaccf90448a1a4ee'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16443'>D16443</a>

### <a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a>

- [cuttlefish] Bind StandardKey.Quit (Ctrl+Q) to exit the app. <a href='https://commits.kde.org/plasma-sdk/47e4fa8e5366eb8b2103b979bd4257ac1d51b548'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16521'>D16521</a>
- [cuttlefish] Auto-focus on search textfield when app opens. <a href='https://commits.kde.org/plasma-sdk/bdf7f24499e4b1c09f56febe6980f2aec9425207'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16516'>D16516</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Only use wl-shell for the ksmserver greeters on Qt < 5.12. <a href='https://commits.kde.org/plasma-workspace/790f5bf48f2b6b8c23e2dafc8f0071066215d85d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399918'>#399918</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16381'>D16381</a>