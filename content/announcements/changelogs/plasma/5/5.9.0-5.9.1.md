---
aliases:
- /announcements/plasma-5.9.0-5.9.1-changelog
hidden: true
plasma: true
title: Plasma 5.9.1 Complete Changelog
type: fulllog
version: 5.9.1
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- [KStyle] Don't draw focus indicator on ComboBox entries. <a href='https://commits.kde.org/breeze/4d272dcbb53e666208c91fdda79a2b0de64ba727'>Commit.</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Don't show the version if there's no version to show. <a href='https://commits.kde.org/discover/932131dd822f853668ab95f783e74ea2d700fb51'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376036'>#376036</a>
- Don't sort resources if the delivery is sorted. <a href='https://commits.kde.org/discover/cd38aafa403874f5202c4880a1862964d7bbfc6d'>Commit.</a>
- KNS: Improve how we react to error messages. <a href='https://commits.kde.org/discover/8f1507b21d78b61f94a1b8694e3c90c19ff48c8b'>Commit.</a>
- Use the right name for the method. <a href='https://commits.kde.org/discover/4af47b263d4c3c7995a14b4c67dfeaaa4f6c9899'>Commit.</a>
- Also show passive errors on the console. <a href='https://commits.kde.org/discover/58ef12c97616fd3b314bc6b36aaf9b5be94d7e26'>Commit.</a>
- Simplify code. <a href='https://commits.kde.org/discover/3b1d613fe6c9a348676f8315cea94a05bcfff9a3'>Commit.</a>
- Also expose YaST repository configuration if present. <a href='https://commits.kde.org/discover/4b128419571bb2f45f1dfbdab21ae5419e02d77b'>Commit.</a>
- Fix arguments when running .desktop files with runservice. <a href='https://commits.kde.org/discover/2db9081b379614ecc19331634d31ca827b9cf9e6'>Commit.</a>

### <a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a>

- Backporting the fixes from master. <a href='https://commits.kde.org/kactivitymanagerd/5b42e9e7d57af6d19b1ade9c05beaf97c8f31d17'>Commit.</a>
- Sync the dbus ActivityInfo structure with the framework. <a href='https://commits.kde.org/kactivitymanagerd/750123feed44b5e074069860ef4331cbf062ed82'>Commit.</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Check on current comic to be valid. <a href='https://commits.kde.org/kdeplasma-addons/7195311a1cd8f50686cacbbb5fd5ff6821bfdbb9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373031'>#373031</a>

### <a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a>

- Guard against nullptr-access to the OutputPtr. <a href='https://commits.kde.org/kscreen/d79dc2b8ca4f8c9daf6c6e6f5f3e40580f6d224c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372945'>#372945</a>
- Apply config change after correcting invalid mode. <a href='https://commits.kde.org/kscreen/d5b9d37767adbf2a035281fbeb5f38a309160413'>Commit.</a> See bug <a href='https://bugs.kde.org/356864'>#356864</a>
- No use in setting modes on disabled outputs. <a href='https://commits.kde.org/kscreen/b4b504696b638691ad96c75560e6599c44bea88c'>Commit.</a>
- Correct possibly invalid current mode. <a href='https://commits.kde.org/kscreen/96db1a9a1aedcc3c5816af6fe69e7a9039daeda1'>Commit.</a>
- Disable unify button when only one output is connected. <a href='https://commits.kde.org/kscreen/6dcc9b19be52fdf5b4c8148124758f7392646719'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360700'>#360700</a>
- Increase precision of refresh rate. <a href='https://commits.kde.org/kscreen/7d8ec60c60477d68e91e9bc68f334ff286cffc73'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369422'>#369422</a>
- Move back to runtime connection. <a href='https://commits.kde.org/kscreen/6224df5a8c7e448f097246c32444c7e01a6747cd'>Commit.</a>

### <a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a>

- Workaround for keys stored with an extra space at the end. <a href='https://commits.kde.org/ksshaskpass/bac379cde0e12b2fbe409959fd23f44913f9d3bb'>Commit.</a>
- Fix several problems in algorithm of deriving key filename from a prompt message supplied by ssh-add. <a href='https://commits.kde.org/ksshaskpass/9dbabcb38862c1590503169be3ae7806e6673dba'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127569'>#127569</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Call performMoveResize when we got a new geoemtry during resize. <a href='https://commits.kde.org/kwin/5083adb1b85a7194bd84d170225596ca507b8858'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374869'>#374869</a>
- [autotests] Add test case for resizing window manually. <a href='https://commits.kde.org/kwin/a67f4dfa65ee2c7e6327dd1474fdc3b526e943b2'>Commit.</a>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- Fix type argument for get property call. <a href='https://commits.kde.org/libkscreen/57cd4d12b3c70495935675cb7df6532b98695fb1'>Commit.</a>
- Isable logging to kscreen.log by default, re-enable with export KSCREEN_LOGGING=1. <a href='https://commits.kde.org/libkscreen/04a6483a01caaa17e476ac9e4a47725f0e1ca06f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361688'>#361688</a>
- Allow changing an output's modelist at runtime. <a href='https://commits.kde.org/libkscreen/7367e55b7c172d54d068eb09f308e92368c294e9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356864'>#356864</a>

### <a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a>

- Fix broken kcfgc files. <a href='https://commits.kde.org/oxygen/eb6ba888913283f74de5e2172e872bc9d3f983a7'>Commit.</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [Solid Device Actions KCM] Encode action file name. <a href='https://commits.kde.org/plasma-desktop/acae3816b7ae686542bc4dfeac89ecba7ff5078a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344534'>#344534</a>
- [Task Manager] Enable "Mark applications that play audio" option only if plasma-pa is available. <a href='https://commits.kde.org/plasma-desktop/d798319fee1d29e8da301580301a19be379cab4d'>Commit.</a>
- Fix i18n extraction in Kickoff Buttons. <a href='https://commits.kde.org/plasma-desktop/f8a9c37373bb89543b4c323e55673718da1304db'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375880'>#375880</a>
- Add missing include in sortedactivitiesmodel to fix build with GCC 7. <a href='https://commits.kde.org/plasma-desktop/8f9fc02983b0b1e1a03cf8c673adbd459b182119'>Commit.</a>
- Add missing include in sortedactivitiesmodel to fix build with GCC 7. <a href='https://commits.kde.org/plasma-desktop/f52226d2614d46bc7b3f3477a8d0a24d3d6d9601'>Commit.</a>
- Fix i18n extraction: xgettext doesn't recognize single quotes. <a href='https://commits.kde.org/plasma-desktop/8c174b9c1e0b1b1be141eb9280ca260886f0e2cb'>Commit.</a>
- Use consistent default icon in UserManager and Kicker. <a href='https://commits.kde.org/plasma-desktop/52ba0122ebeb6aa21d23936d0fbef20f7d9f208f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370362'>#370362</a>
- Revamp (Activity) Pager wheel handling. <a href='https://commits.kde.org/plasma-desktop/fff65ad696f4dc34391a0e9eb4b9ef0ed09a9c42'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375769'>#375769</a>
- [Folder View] Open selection in preferred application instead of running them. <a href='https://commits.kde.org/plasma-desktop/bb1d73a99f0cfddae6befb4e14312ebb9c9fe30e'>Commit.</a> See bug <a href='https://bugs.kde.org/375793'>#375793</a>
- [Folder View] show script execution prompt when clicking item. <a href='https://commits.kde.org/plasma-desktop/771e57f3b2c19f4e6f867c01c2457ec87531b4cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375793'>#375793</a>
- And maybe don't undo the fix while fixing the fix ... <a href='https://commits.kde.org/plasma-desktop/84fadc3603f754a326c589fbc86b92d565e4875c'>Commit.</a>
- Fix startup warning introduced in 3568d8e4. <a href='https://commits.kde.org/plasma-desktop/db0723ab7086016367196ad15fced7b4e7edcc9d'>Commit.</a>
- Fix clearing selection when rectangle selection contains no items. <a href='https://commits.kde.org/plasma-desktop/3568d8e4a8a2cc4fac7fccb86132c47be37495f6'>Commit.</a>
- Fix Plasmoid.busy visualization in desktop containment. <a href='https://commits.kde.org/plasma-desktop/37a5d9fb5d0b87b50756555348a17f5d1c523cf5'>Commit.</a>
- Call correct function and fix warning. <a href='https://commits.kde.org/plasma-desktop/f37514e2b551525464cddf08f865a2bbb093cf1f'>Commit.</a>
- Fix crash and loss of favorites model on refresh with pagination on. <a href='https://commits.kde.org/plasma-desktop/9cfcef4fbe45ab96f3710779fd713855fd15921d'>Commit.</a>
- Make parts of KCM Touchpad interface translatable (Patch by Victor <victorr2007@narod.ru>). <a href='https://commits.kde.org/plasma-desktop/733ac20c8df1afdb9784b04633995a2c5b67141a'>Commit.</a>
- Clip by bounding delegate size instead of Item.clip. <a href='https://commits.kde.org/plasma-desktop/6ac5e2b7a05f3d3cc90558ffc1dede78e653d8b2'>Commit.</a>
- Fix autotest. <a href='https://commits.kde.org/plasma-desktop/12697f13b6e4533668ff76e2ab65196435fbac64'>Commit.</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Elide Network Manager KCM tabs. <a href='https://commits.kde.org/plasma-nm/2f950174ccc278a2eb2d5b7bef536ff0f2d6f775'>Commit.</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Always check if context is valid when calling pa functions. <a href='https://commits.kde.org/plasma-pa/1c1a76775cb566b30aa412761494116570e7c49d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375872'>#375872</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [User Switcher] Ungrab keyboard before trying to lock the screen. <a href='https://commits.kde.org/plasma-workspace/8bf479e35834f66cb1ac7b92965526acdf98f555'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375941'>#375941</a>
- React to config change. <a href='https://commits.kde.org/plasma-workspace/a0374d8fffa8649afc8d9c12ba11586daa35b332'>Commit.</a>
- [System Tray Containment] Drop useless Q_INVOKABLE from .cpp file. <a href='https://commits.kde.org/plasma-workspace/6ee6880fb52b7820ee364881637f10d0f9eb6254'>Commit.</a>
- [System Tray Containment] Ungrab mouse before opening context menu. <a href='https://commits.kde.org/plasma-workspace/dfdb7296275c2c4bb9eda77982f8ff5663e58e5f'>Commit.</a>
- [AppMenu Applet] Map from scene after mapping from global. <a href='https://commits.kde.org/plasma-workspace/10246712ae307ae9fb9cbaefbecc06e9a2c60cf5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375535'>#375535</a>
- Fix memory leak in Appmenu when using compact menu. <a href='https://commits.kde.org/plasma-workspace/245e04eb397f854975c07153758312b06f4b9ff6'>Commit.</a>
- [Appmenu Applet] Check whether buttonGrid has a window before calling mapToGlobal. <a href='https://commits.kde.org/plasma-workspace/637fac160057ad7df62ac1688c7c6491037df575'>Commit.</a>
- [Digital Clock] Take into account timezone label width for applet size. <a href='https://commits.kde.org/plasma-workspace/518a55d4fa7e0138eb39c3aa98c709c469c8949a'>Commit.</a>
- [KRunner] In doubt use primary screen for view position. <a href='https://commits.kde.org/plasma-workspace/b0b31dee60defe4d7e9de8abc1dbbadfbced2783'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375574'>#375574</a>
- Drop legacy fixup rule for VirtualBox. <a href='https://commits.kde.org/plasma-workspace/6f99f0dd99efd2d913f18f6911e35c4f4a96d74d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350468'>#350468</a>
- Fix isOutputRedundant logic. <a href='https://commits.kde.org/plasma-workspace/3792ef9e51dc0ba21740e69964f26e241df7aed2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375507'>#375507</a>
- Fix isOutputRedundant logic. <a href='https://commits.kde.org/plasma-workspace/f4faa6f1547c4994893ac5f7a5c251f2846368f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375507'>#375507</a>
- When swapping Wallpapers, save the config to file, not just to propertymap. <a href='https://commits.kde.org/plasma-workspace/a9f5ada73bf86a145e057eff188f45dfdf61917a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375532'>#375532</a>

### <a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a>

- Set wallpaper type in SDDM config. <a href='https://commits.kde.org/sddm-kcm/19e83b28161783d570bde2ced692a8b5f2236693'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370521'>#370521</a>