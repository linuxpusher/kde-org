---
aliases:
- /announcements/plasma-5.18.0-5.18.1-changelog
hidden: true
plasma: true
title: Plasma 5.18.1 Complete Changelog
type: fulllog
version: 5.18.1
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- Add missing cursors for Chromium-based apps. <a href='https://commits.kde.org/breeze/3944c5f74d043c9bc179e50036554e63b5928bb6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27424'>D27424</a>

### <a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a>

- [GTK3] Fix menubar colours. <a href='https://commits.kde.org/breeze-gtk/d3e9aa737502b06e9080a87059f8b4066008c589'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27390'>D27390</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Pk: make sure we get the local package details. <a href='https://commits.kde.org/discover/b5718fdc49b737e5b4e19cb7d4c8cab19d112175'>Commit.</a>
- Simplify UI logic. <a href='https://commits.kde.org/discover/e1960d467dc6bdbd9d81b7a861e193405e82d973'>Commit.</a>
- Fix typo. <a href='https://commits.kde.org/discover/3f4dfaea60628fbd61e4e9a42431a38f0dfff568'>Commit.</a>
- Pk: list codecs as addons. <a href='https://commits.kde.org/discover/04edc61d50f679a3fd7a72a5ea98f0f2d5d948a1'>Commit.</a>
- Don't offer to show more when there's not more to show. <a href='https://commits.kde.org/discover/8a5f3110e9770936fbabe24edbaa2c4395abb0f6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417290'>#417290</a>
- Remove warning when there's no kuserfeedback. <a href='https://commits.kde.org/discover/82256c2fcaf7a3f971a455ee4bc041b80ae7163e'>Commit.</a>
- Snap: mark transactions as cancelled when we cancel them. <a href='https://commits.kde.org/discover/71732f51cd43bf2544b3a9cd7f69bb3a1c4d8878'>Commit.</a>
- Don't escape search text in search page title. <a href='https://commits.kde.org/discover/9bf3f942d329dbff120f98cd12cefb281b21eddc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416781'>#416781</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27079'>D27079</a>
- Use real Kirigami Separators instead of blue rectangles. <a href='https://commits.kde.org/discover/3978c1de9b47984501a55d98e01e0a120a55f100'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27184'>D27184</a>

### <a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a>

- Replace plasma.kde.org with kde.org/plasma-desktop. <a href='https://commits.kde.org/kactivitymanagerd/1076889d06f5ecfbfa401421b2be50e6c6eeb7e5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27308'>D27308</a>

### <a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a>

- Fix compilation with KF 5.67. <a href='https://commits.kde.org/kde-cli-tools/1ec5299339e0c7871629cdff718cde75fc5283ff'>Commit.</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Replace plasma.kde.org with kde.org/plasma-desktop. <a href='https://commits.kde.org/kdeplasma-addons/2b9a7a71b8b359ce0cea99911472db8edf297e64'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27306'>D27306</a>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

- Make sure ifa_addr isn't null. <a href='https://commits.kde.org/kinfocenter/8cb749531a3c3ab139e51bd1f741edddccf69873'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417798'>#417798</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27467'>D27467</a>
- Appdata: fix <lanchable>. <a href='https://commits.kde.org/kinfocenter/1cd6f7c18e21ef91c5f10eb83b9054520dc43dfb'>Commit.</a>
- Import currently used license copies for reuse compliance. <a href='https://commits.kde.org/kinfocenter/4ea9df96ae038f28f3e9f6ecef6a64681bb3e189'>Commit.</a>
- Replace samba module with data that works. <a href='https://commits.kde.org/kinfocenter/f1768f7b3ab972e5717d0291a0407422f253adfd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411433'>#411433</a>. Fixes bug <a href='https://bugs.kde.org/374141'>#374141</a>. Fixes bug <a href='https://bugs.kde.org/325951'>#325951</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27061'>D27061</a>

### <a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a>

- Fix(kcm): use explicit Kirigami.Units over singleton from a theme. <a href='https://commits.kde.org/kscreen/357df7c5c48abd862fcd71e1d611f3f575c74be4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417316'>#417316</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27442'>D27442</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Fix misplaced client windows. <a href='https://commits.kde.org/kwin/cfa5daaad1bfb954e7b2176bba7b985410584e63'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417584'>#417584</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27456'>D27456</a>
- [autotests] Make XWaylandInputTest more robust. <a href='https://commits.kde.org/kwin/4f9cbe4369a95aa29b811bb1c7352a7e7cb8f24d'>Commit.</a>
- Provide input geometry and input transformation matrix for Xwayland clients. <a href='https://commits.kde.org/kwin/1181af2cd16569fb93680de352a4c050533bffff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417444'>#417444</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27375'>D27375</a>
- Return early if close button accepts input event. <a href='https://commits.kde.org/kwin/3dbfa6a32545c9afd27bf8c1e86cba516b71d2a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415155'>#415155</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27333'>D27333</a>
- [libkwineffects] Detect more AMD GPUs with GFX9 (Vega) chips. <a href='https://commits.kde.org/kwin/b94a78c47d944b5e550acf8be7816abd55826bb8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27176'>D27176</a>
- Require kdecoration >= 5.18.0. <a href='https://commits.kde.org/kwin/6d8f3732c1fbd6d19af4159eed92e475aabd650f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27242'>D27242</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [desktoppackage] Fix layout when an applet has no custom config modules. <a href='https://commits.kde.org/plasma-desktop/1123cc4d0b59c87258932b420197ceae230afb50'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410770'>#410770</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27398'>D27398</a>
- Set toolbox to invisble when it's hidden. <a href='https://commits.kde.org/plasma-desktop/7a48de1efa224e616d9f24f1fa1e4cdab0bce196'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416695'>#416695</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27386'>D27386</a>
- [kcm/standardactions] Don't register kcminit hook when we don't have one. <a href='https://commits.kde.org/plasma-desktop/ea7013f17b31a7cd1288474ca686156a955f374b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27007'>D27007</a>
- [KCM Fonts] force need save to false during load to avoid state to be true too early. <a href='https://commits.kde.org/plasma-desktop/0c4da0774e4388b6937b5aa98124b696debcb601'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416358'>#416358</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27384'>D27384</a>
- Baloo KCM: Do not anchor fill ScrollView. <a href='https://commits.kde.org/plasma-desktop/83b920946c52c2f100a21e1e14af693f14f3e3a7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417437'>#417437</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27379'>D27379</a>
- [Styles KCM] Set style display name as window title. <a href='https://commits.kde.org/plasma-desktop/bc5c45e8aa9d0719c9bc09ff2db781c65ca54962'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417563'>#417563</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27368'>D27368</a>
- Unlock widgets. <a href='https://commits.kde.org/plasma-desktop/2bc3c5e92d4789146548e8de4d520cd191994e1c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417424'>#417424</a>
- Replace plasma.kde.org with kde.org/plasma-desktop. <a href='https://commits.kde.org/plasma-desktop/537729cbed02ff7f151f520f5574a3a94fe61d4e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27305'>D27305</a>
- [kcms/cursortheme] Don't skip everything because of lack of XFixes. <a href='https://commits.kde.org/plasma-desktop/0d17a4b4e07c8a3d0ca5ee4b2c4b10c0a0d9dc77'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27055'>D27055</a>
- [Sessions KCM] Fix firmware setup. <a href='https://commits.kde.org/plasma-desktop/b08981bfe444b33588410c61975e97c1903edea6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27206'>D27206</a>
- [KSplash KCM] Fix ghns button label. <a href='https://commits.kde.org/plasma-desktop/1e10987d5af9a890b7b8910928cb713e0d2500cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417224'>#417224</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27191'>D27191</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Fix crash when asking for a modem unlock. <a href='https://commits.kde.org/plasma-nm/24c9505c67d9569cf276341e2297712ecdd95d2c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417545'>#417545</a>

### <a name='plasma-phone-components' href='https://commits.kde.org/plasma-phone-components'>plasma-phone-components</a>

- Replace plasma.kde.org with kde.org/plasma-desktop. <a href='https://commits.kde.org/plasma-phone-components/41c1add0404fe6278e2d96b09e913991c1c5586c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27309'>D27309</a>

### <a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a>

- Extract messages from header file as well. <a href='https://commits.kde.org/plasma-vault/d316e2de3b32b05fee0cba0efb166f29b045a490'>Commit.</a>
- Replace plasma.kde.org with kde.org/plasma-desktop. <a href='https://commits.kde.org/plasma-vault/d6062a6a02737e29b377540f2f8552bfb5550f4a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27307'>D27307</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [plasma-session] Avoid hypothetical race condition starting a service. <a href='https://commits.kde.org/plasma-workspace/854b64fc130b8e1c3a254bf087bb162616cc8041'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27471'>D27471</a>
- [kcms/feedback] Improve default kcmshell window size. <a href='https://commits.kde.org/plasma-workspace/285155fad1f30c35476c074d470cde6f253d3e9b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417747'>#417747</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27448'>D27448</a>
- Extract messages from header files as well (Thanks to Victor Ryzhykh). <a href='https://commits.kde.org/plasma-workspace/203d7b42103bc47ffd1bd81c9cabf87ef9e971a0'>Commit.</a>
- Remove downloadNewWhat usage. <a href='https://commits.kde.org/plasma-workspace/8972b3d8e7bb16816f055f754eae8236fc645319'>Commit.</a>
- Don't delay ksplash until the entire slideshow is loaded. <a href='https://commits.kde.org/plasma-workspace/a58bbc050723583d75a47a3f00d19f41d70d17a4'>Commit.</a> See bug <a href='https://bugs.kde.org/371455'>#371455</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27084'>D27084</a>
- [Notifications] Only let details label grow, never shrink. <a href='https://commits.kde.org/plasma-workspace/0fb8e51b8e1371323cf2e464b57189be42887e7f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417489'>#417489</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27344'>D27344</a>
- [libtaskmanager] Fix shared VirtualDesktopInfo::Private tied to the 1st creator. <a href='https://commits.kde.org/plasma-workspace/8b3788ba3389c85314f261415742b3470100f037'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415200'>#415200</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27266'>D27266</a>
- [Background Finder] Create a valid invocation token. <a href='https://commits.kde.org/plasma-workspace/ae0fafe237006f6d2f9d317cd00dd04f4f7cb74d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27237'>D27237</a>
- Replace plasma.kde.org with kde.org/plasma-desktop. <a href='https://commits.kde.org/plasma-workspace/6f7a878e6d730932ccc3260363abf4cba77a9dab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27304'>D27304</a>

### <a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a>

- Prevent powerdevil from calling DPMS extension calls when DPMS extension isn't present. <a href='https://commits.kde.org/powerdevil/ca614feb833a99d582176b591c04807901ad5837'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27278'>D27278</a>
- Fix FreeBSD compilation, Q_FOREACH -> for-range loop. <a href='https://commits.kde.org/powerdevil/1ea378148a5bb17a4614abba6971edc2472c634a'>Commit.</a>

### <a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a>

- Fix being unable to set the right permissions for kde_settings.conf. <a href='https://commits.kde.org/sddm-kcm/cc33ec575c5e2142dd1a6c3a2563859a59e3fcd5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414396'>#414396</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27388'>D27388</a>
- Don't let the Advanced tab's spacers expand all they want. <a href='https://commits.kde.org/sddm-kcm/26022caf9a2ad5427441d8a22865b0f64b6e4fe9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417657'>#417657</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27418'>D27418</a>
- Sync ("Wayland") appending to match SDDM. <a href='https://commits.kde.org/sddm-kcm/c0fdf0039be47b5d890346d45bbb1d763d65379a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417499'>#417499</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27358'>D27358</a>
- Have authhelper define config file paths on its own. <a href='https://commits.kde.org/sddm-kcm/c4b41ffe66603d3d01a236f6377f5bb1c71b6e58'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27142'>D27142</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- [sidebar] Resolve changes in module before switching. <a href='https://commits.kde.org/systemsettings/979419d7430de2c32a5a6ebc8376039117778baa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416834'>#416834</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27381'>D27381</a>

### <a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a>

- Have user-manager write to SDDM's new config file. <a href='https://commits.kde.org/user-manager/b2d0e463a4901329ce6a0ada4ec720ff9b5cc90f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/342722'>#342722</a>. Phabricator Code review <a href='https://phabricator.kde.org/D26813'>D26813</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- PipeWire: set stream and offset information for the buffer. <a href='https://commits.kde.org/xdg-desktop-portal-kde/58395d456a55cbe8c05dca1b092a674c6be3a315'>Commit.</a>
- Properly combine pipewire and spa includes. <a href='https://commits.kde.org/xdg-desktop-portal-kde/3785348f792efe2f2c53b2ce65e01d25e8707889'>Commit.</a>
- Properly search for PipeWire libs. <a href='https://commits.kde.org/xdg-desktop-portal-kde/05a9caf5e866446d3d1878f58b215ddcea4b6488'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27277'>D27277</a>