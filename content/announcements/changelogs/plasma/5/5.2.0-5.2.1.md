---
aliases:
- /announcements/plasma-5.2.0-5.2.1-changelog
hidden: true
plasma: true
title: Plasma 5.2 complete changelog
type: fulllog
version: 5.1.95
---

### Plasma NM

- Show correct connection name
- Properly set disconnected icon when wireless network/connection is available
- Properly render ampersand and other characters in tooltip
- Improve workaround for older NM versions

### KMenuEdit

- Fix includes
- Port to new connect api
- kdelibs4support--
- Fix add shortcutt
- Fix signal

### KWrited

- Disable session management for kwrited

### KHotkeys

- add include(ECMOptionalAddSubdirectory)
- use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete

### Breeze

- Avoid comparing unintialised variable if BREEZE_HAVE_KSTYLE is not set
- Fix crash when switching to/from breeze
- explicitly delete sizeGrip in destructor (apparently not done automatically) BUG: 343988
- properly adjust menu icon size and positioning, to deal with button's offset in maximized mode. BUG: 343441
- Initialize widget only at first successfull call to registerWidget CCBUG: 341940

### libksysguard

- Clean forward declaration

### kde-cli-tools

- add ECMOptionalAddSubdirectory
- use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete
- kdelibs4support--
- install in KDE_INSTALL_LIBEXECDIR_KF5

### libkscreen

- Fix crash when multiple EDID requests for the same output are enqueued

### kfilemetadata

- Fix version to 5.6.1

### Oxygen

- Improved rendering of checkbox menu item's contrast pixel, especially when selected using Strong highlight. BUG: 343754
- set the right widget style

### Plasma Desktop

- [Application Menu] Always sort.
- [Application Menu] Make subdialog focus explicit.
- [Folder View] Improve rubber band feel and consistency with Dolphin.
- Use smooth transformation for scaling down the user picture
- use QProcess::startDetached instead of KProcess
- fix typo in ecm_optional_add_subdirectory
- add subdirectories optionally for documentation because translations may be incomplete
- When setting colour scheme information for KDE4, don't read from KF5 kdeglobals
- Sort apps by name if configured
- Baloo KCM: Show proper icons (porting bug)
- allow free resize on desktop
- the applet object has own Layout
- more accurate position restore after delete
- the trashcan is always in fullrepresentation
- Reverse dns desktop
- Fix checking the places a radio button not updating pref.
- Add sanity checks to actions operating on selections.
- Migrate settings
- Fix mapping configured URL to places model entry.
- [Folder View] Fix intra-view DND of files into folders generating wrong destination URL.
- adjust size policies
- kdelibs4support--
- Port to qCDebug
- Clean includes
- kdelibs4support--
- Clean up
- leave some space for the decoration
- [Application Menu] Port to QStyleHints and add porting TODOs for Qt 5.5+.
- Make evaluating single vs. double click procedural.
- Sanity checks to avoid ending up with multiple submenus.
- Still save kde5 config into the right place
- Sync back mouse settings into KDE4 configs
- [Task Managers] Don't crash when a window closes while it's context menu is open.
- properly sort the locales
- bring back the flags
- fix building the test
- use KModifierKeyInfo directly to set the numlock status

### polkit-kde-agent-1

### Powerdevil

- Apply OSD silencing on AC plugging also to keyboard brightness controls
- Don't show OSD when brightness changes due to pluggin in/out the AC
- Fix crash when one of the combo boxes is not present
- Fix hardware shutdown button doing nothing on desktops
- Fix PowerDevil brightness calls breaking kded, Volume 3521
- Ensure text in those two combo boxes is never truncated
- Increase grace time for DPMS to 5 seconds
- Unconditionally enable power button handling
- Plugging in or out the AC adapter is always explicit
- Inhibitions posted over the freedesktop interface should always inhibit everything

### KScreen

- KCM: fallback to QComboBox when resolutions list is too long
- KCM: prevent nested scrollbars when opened in System Settings
- KCM: Block save() unti SetConfigOperation finishes
- KLM: correctly initialize output rotation on start
- Fix visual representation of output rotation

### Systemsettings

- Port to new connect api

### KWin

- add include(ECMOptionalAddSubdirectory)
- use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete
- extract UI messages correctly
- Check GL version and/or extension for using texture format GL_R8
- Fix glPixelStore in GLTexture::update
- [effects] Ensure the correct cursor shape is set for mouse interception
- fix e3768b43559fa179f497d5130c0108dd72acf266
- save geom_restore before calling for border update
- correctly handle virtual desktop on packing
- Rules: make WindowRole matching case sensitive
- do not wrap text in deco button drag source
- updateFocusMousePosition() before some actions
- use xcb_time_current_time as setinputfocus default
- [kcmkwin/compositing] Fix ordering of items in keep window thumbnail combo box
- Fix build with clang and gcc 5.0
- [kcmkwin/options] Load/Unload WindowGeometry effect
- [effects] Support building the effect_builtins without need to link the lib
- Disable libinput integration if >= 0.8 is found
- Avoid deleting an engine whilst components from our engine are still in use
- use xembed for the qml view of window decorations modul
- [kcmkwin/deco] Do not runtime depend on QtQuick 2.4 (Qt 5.4)
- Trigger rebuilding of quads after creating a new DecorationShadow

### KSysGuard

- Port to new connect api
- Port to qcommandlineparser

### Muon

- use the static singleshot helper to delay-init the app notifier backend
- Remove unneeded&wrong size specification
- Add a minimum size to the notifier plasmoid
- Fetch updates when network state changes as well.
- Fix updates count on PackageKit backend's notifier
- Add consistency between the List and the Grid
- Add a warning on the console whenever an error happened
- Fix mimetype filtering
- Provide the mimetypes for appstream resources as well.
- Remove jacknjoe featured source
- Only advertise as canExecute if the executables could be found
- Don't fetch updates details if there's no details to find
- Improve error detection
- Disable sources button if no sources available
- Remove unused code
- Fix last updates time report in muon-updater
- Re-populate the updates model if the state of a resource changes
- Make the installed and available versions subject to state changes
- Played around with the Dummy backend
- Nitpicking

### Plasma Workspace

- Set aboutData for plasmashell
- [digital-clock] Invert the JS timezone offset values
- [digital-clock] Improve the scrolling on the applet
- add include(ECMOptionalAddSubdirectory)
- use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete
- Don't show "Unknown" as author in wallpaper tooltip
- Offer editing only for text entries
- properly propagate info and error messages from kcheckpass and PAM
- manage applet destruction
- queue connection to show the alternatives dialog
- sometimes a service from a query is invalid
- delete containments before mass deleting views
- make sure activtyId is migrated
- support 22x22 icons too
- update availableScreenRegion when deleting a panel
- [screenlocker] Use KAuthorized::authorizeKAction for lock_screen
- Treat normal selected timezone as local if it matches the local
- set the right widget theme
- fix total system freeze on some systems like my 10 inch netbook thanks Thomas Lübking for providing the patch BUG: 340294
- Sanitize whitespace in notification popups
- Prevent notifications from accessing the network
- Convert to qDebug
- Remove not necessary kdelibs4 include
- Port to qCDebug
- Don't let notification popup accept focus
- Continuously update notification timestamp
- Handle appdir directory structures in icon paths
- Simplify and fix IconThemePath in StatusNotifierItems
- show busy widget when needed
- plasmashell crash reports go to plasmashell->general
- Fix kscreenlocker not creating sessions
- [ksld/greeter] Disable all IM modules
- Qalculate Engine: Increase the precision
- Slightly taller battery popup dialog
- Fix warnings

### Milou

- Elide category names

### KDE Plasma Addons

- trigger on plasmoid activated

### kio-extras

- add include(ECMOptionalAddSubdirectory)
- use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete