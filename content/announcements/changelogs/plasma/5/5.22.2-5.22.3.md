---
title: Plasma 5.22.3 complete changelog
version: 5.22.3
hidden: true
plasma: true
type: fulllog
---
{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Repair query construction. [Commit.](http://commits.kde.org/drkonqi/d4061d4889d45725d42e86de425dfe8b6ffb7b35) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Remove cell dimension setting. [Commit.](http://commits.kde.org/kinfocenter/8481ec97c26961604c1af9211e0a347bc7815e47) 
+ Unbreak the refresh button. [Commit.](http://commits.kde.org/kinfocenter/d9e0bee1324bb35700722fe53ad666ae9afd0b9a) 
+ Disable layout twinning in NIC kcm. [Commit.](http://commits.kde.org/kinfocenter/622513242df8fc4ad52eb586ff62f8171c314b33) 
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Really fix build. [Commit.](http://commits.kde.org/ksystemstats/42973a80af33dc749a00a8abb5b029a83604190d) 
+ Fix build. [Commit.](http://commits.kde.org/ksystemstats/4dc9c187348aec3ebc8384330c51d75b19ec7d54) 
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Don't move keyboard focus on button press. [Commit.](http://commits.kde.org/kwayland-server/e670832e6657f48812785d3f739ea7001bb2cc03) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Platforms/drm: Port away from gbm_format_get_name(). [Commit.](http://commits.kde.org/kwin/f657826541521bdde95ba39f865078d56d675181) Fixes bug [#439152](https://bugs.kde.org/439152)
+ Xdgshellclient: fix moveresize with touch and CSD. [Commit.](http://commits.kde.org/kwin/d78c89f0ede635d9aa2f6d7f369fe3d38d57b466) Fixes bug [#438283](https://bugs.kde.org/438283). See bug [#431489](https://bugs.kde.org/431489)
+ Input: fix touch input getting borked on quick tile. [Commit.](http://commits.kde.org/kwin/cc3e89972800e13f42120140fc6232f5b0597531) See bug [#430560](https://bugs.kde.org/430560)
+ Fix build. [Commit.](http://commits.kde.org/kwin/1154a6b006c9f303244e6a5a74e083e59ed7e28c) 
+ Platforms/drm: don't crash if drmModeGetConnector fails. [Commit.](http://commits.kde.org/kwin/03dc0681defa3f2f28187c39f1adf935251cd8fe) See bug [#439208](https://bugs.kde.org/439208)
+ Platforms/drm: don't create modeset dumb buffer for import gpu. [Commit.](http://commits.kde.org/kwin/0b41f6aec0f2a13b69c8dbc5c48a4cb74669da12) 
+ Wayland: Implement activities window rule. [Commit.](http://commits.kde.org/kwin/6be5055bdf99f85c1c6eee180451536ee64c79ba) Fixes bug [#439022](https://bugs.kde.org/439022)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Don't hide expanded representation if plasmoid is pinned. [Commit.](http://commits.kde.org/plasma-desktop/0ac0bbc8f297a7f5abe5bc71825b4c7cfcf65271) Fixes bug [#439269](https://bugs.kde.org/439269)
+ [Task Manager] Load album art background asynchronously. [Commit.](http://commits.kde.org/plasma-desktop/7ea448ae8102876d77a12dd155912a2d34d05896) Fixes bug [#439512](https://bugs.kde.org/439512)
+ [kimpanel] Use window flag ToolTip on wayland to avoid grab focus from input method. [Commit.](http://commits.kde.org/plasma-desktop/6bb0bfaaa6baf87473aa8f7dd1724ac989ad193c) 
+ [keyboard layout applet] Fix auto-opening fullRepresentation on start. [Commit.](http://commits.kde.org/plasma-desktop/bc1e9920115b7cf6eae78db3a4ce82f2c4f48d5c) Fixes bug [#438460](https://bugs.kde.org/438460)
{{< /details >}}

{{< details title="plasma-disks" href="https://commits.kde.org/plasma-disks" >}}
+ Don't notify on instabilities. [Commit.](http://commits.kde.org/plasma-disks/e75432da5645bc4f9de96b5c9851ca8332396181) See bug [#438539](https://bugs.kde.org/438539)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Always disconnect streams. [Commit.](http://commits.kde.org/plasma-pa/5644f4e7a4c22432d0c09be00eb56713d06c5c67) Fixes bug [#439347](https://bugs.kde.org/439347)
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Pass parent index when fetching selection data. [Commit.](http://commits.kde.org/plasma-systemmonitor/8cc770f9819f83d0dc60d8eeb923698f82be011d) Fixes bug [#438626](https://bugs.kde.org/438626)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [Media Player] Skip source if it doesn't exist. [Commit.](http://commits.kde.org/plasma-workspace/58fecd9ba1e1a45cd5ab01f0832a3b06ed818c5a) Fixes bug [#439309](https://bugs.kde.org/439309)
+ Guard calls into layer shell. [Commit.](http://commits.kde.org/plasma-workspace/6a5015951f9899e625bf8572f20cffc64d7aea33) Fixes bug [#439356](https://bugs.kde.org/439356)
+ Xembed-sni-proxy: Add Menu property. [Commit.](http://commits.kde.org/plasma-workspace/4ade115a1248cc3c1128a3d10982429ab76cf8f8) Fixes bug [#439229](https://bugs.kde.org/439229)
+ Assing first screen if `screen` is null. [Commit.](http://commits.kde.org/plasma-workspace/71e465210d5491a5805a26a855232bc103ab0215) Fixes bug [#438277](https://bugs.kde.org/438277)
+ Fix copy-paste error. [Commit.](http://commits.kde.org/plasma-workspace/350708a33c6e37b4d55fe01a264287e95c6981a6) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Fix previous commit. [Commit.](http://commits.kde.org/qqc2-breeze-style/eb9b7fc0582f5198e7a31b3fec22c05a85e8369e) 
+ Disable slider ticks. [Commit.](http://commits.kde.org/qqc2-breeze-style/3ddd4d2447e13abc30bcc63cf874fb4be021b60e) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Background portal: avoid crash when allowing app to run in background. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/0260e5962a2f8c3dc07e2233fdcea3576cadfa7a) Fixes bug [#438954](https://bugs.kde.org/438954)
+ Pass the version to the project call. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/921f8a83b4307a2ef82d4bfc75bf957184f62f59) 
{{< /details >}}

