---
title: Press Page
nosubpage: true
---

## Introduction

This page is aimed at journalists and (web) publishers who would like to write about KDE. It offers some guidance on how to write about Free Software and the KDE community in particular and where to find relevant and up-to-date information.

## About KDE

KDE is an international community that creates Free Software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, and comprehensive office productivity and groupware suites. KDE offers hundreds of software titles in many categories including web applications, multimedia, entertainment, educational, graphics and software development.

KDE software is translated into more than 65 languages and is built with ease of use and modern accessibility principles in mind. KDE's full-featured applications run natively on Linux, BSD, Solaris, Windows and macOS.

A list of official <strong>KDE announcements</strong> can be found on the <a href="/announcements/">KDE announcement page</a>.

The KDE technology platform consists of 3 distinct parts::

+ <strong>KDE Frameworks</strong>: A set of high-level libraries for UI development and system integration, including hardware, multimedia, and others.
+ <strong>KDE Plasma</strong>: The primary user interface shown immediately upon logging into the system. KDE Plasma is optimized for desktop computers and laptops, and a second user interface optimized for mobile devices ("Plasma Mobile") such as smartphones is currently under development.
+ <strong>KDE Applications</strong>: A <a href="https://www.kde.org/applications/">wide assortment of desktop applications</a>. This includes basic applications for <a href="https://www.kde.org/applications/system/dolphin/">file management</a>, <a href="https://www.kde.org/applications/graphics/gwenview/">image viewing</a>, <a href="https://www.kde.org/applications/graphics/okular/">PDF and e-book reading</a> and <a href="https://www.falkon.org/">web browsing</a> but also more advanced functionality needed for <a href="https://krita.org/">digital painting</a>, <a href="https://www.digikam.org/">digital photography</a>, <a href="https://kdenlive.org/">video editing</a>, <a href="https://www.kde.org/applications/office/kmymoney/">personal finance</a>. The applications are grouped in categories like the <a href="https://edu.kde.org">KDE educational applications</a>, <a href="https://kontact.kde.org">KDE Personal Information Management applications</a> and the <a href="https://games.kde.org">KDE Games</a>. Some applications are part of the official regular KDE releases, others are part of Extragear and release on their own.

It is important to make the distinction described above. An application written on top of KDE Frameworks  can run anywhere and not just on KDE Plasma, fitting in well on a Windows, Mac or GNOME desktop. Likewise, KDE Plasma works well even with applications not written with KDE Frameworks. It integrates application notifications and other such functionality thanks to extensive efforts to support standardization on the <a href="https://www.freedesktop.org/">freedesktop.org</a> platform.

Another important distinction is that "KDE" is the name of the community, not any particular piece of software. People don't run "KDE" on their computers; they run "KDE Plasma" and "KDE Apps."

KDE is legally represented by the <a href="https://ev.kde.org">KDE e.V.</a>, a German-based but otherwise international non-profit association. The association aids in creating and distributing KDE by securing financial, computer hardware and other donations, using them to aid KDE development and promotion. It also provides legal support to developers when needed. The KDE e.V. consists of over 200 members of the KDE community, is chaired by a board elected by the members and has 1 paid administrative employee. Note that 'the e.V.' has no influence on development whatsoever but only facilitates the community.

<ul>
  <li>General information about the KDE community can be found on <a href="/community/whatiskde/" title="What is KDE">the KDE.org website</a>.</li>
  <li>General information about Linux and Free Software can be found on the <a href=https://www.fsf.org target=_blank title="Free Software Foundation website">Free Software Foundation website</a>.</li>
  <li>An interesting read, though lengthy, would be <a href="https://catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/index.html">The Cathedral and the Bazaar</a> by Eric S Raymond. This article is commonly quoted as one of the best explanations of the difference between the Free Software and Proprietary Software development models.</li>
</ul>


## Additional Information

If you are looking for a <strong>KDE logo or other clipart</strong> have a look at <a href="https://www.kde.org/stuff/clipart">the kde logo and image page</a>. Trading and branding with the KDE Logo is subject to our trademark licence. For more details on their usage please see <a href="https://techbase.kde.org/Development/Guidelines/CIG/KDE_Logo">the KDE CIG Logo page</a>.

<strong>Screenshots</strong> of KDE software can be found on <a href="/screenshots/">the KDE screenshot page</a>. If you need any more images or anything graphics related you can contact the KDE artist team with information found on <a href="https://community.kde.org/Get_Involved/design">this page</a>.

This page offers information on <strong><a href="/presspage/writing_guide" target=_blank title="Writing Guide">writing articles about KDE</a></strong> including pointers to resources and culture of the KDE community. It is highly recommended to read through it if you want to write or talk about the KDE community or KDE Software.<br>

<strong>Further questions</strong>, interview requests, conference and meeting information or other general inquiries can directed to kde-promo@kde.org.

Finally, we offer a few facts &amp; numbers about KDE:

+ <b>KDE was conceived on October 14th, 1996</b> with <a href="http://kde.org/documentation/posting.txt">an email by Matthias Ettrich</a>, who would also later go on to found the <a href="http://en.wikipedia.org/wiki/LyX">LyX</a> project.
+ <b>Over 2500 KDE members have Contributor Accounts</b>, meaning that they contribute source code, art, documentation, etc. on a regular basis. About <b>20 new developers</b> contribute their first code each month, underlining the fast growth of the KDE community.
+ <b>Over 6 million lines of code</b> comprise KDE's codebase. This does not include Qt, which is a major part of our infrastructure and is even larger than the KDE codebase.
+ <b>KDE is translated in over 65 languages.</b>
+ <b>Millions of people around the globe use KDE software</b>. Because KDE software does not spy on users or collect telemetry data, it is hard to provide hard numbers--something that's true for all Free Software projects, and KDE is no exception. We do know that tens of millions of schoolchildren use KDE software in Brazil, but most large deployments are unknown to us. Since KDE software has been made available on Windows, we have gained many users there - tens to hundreds of gigabytes of data are downloaded from the KDE-on-windows servers on a daily basis, but again, we don't have verifiable numbers because we don't spy on our users.
+ <b>KDE is one of the largest Free Software communities in existence.</b> The only one that's consistently larger is the Linux kernel development community itself.
