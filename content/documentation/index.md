---
title: "KDE Documentation"
---

## Application documentation

We provide documentation for many aspects of KDE suitable for users from
varying backgrounds. For more details about where to find information,
please refer to <a href="https://userbase.kde.org/Asking_Questions">"Getting Answers to Your Questions"</a>.


KDE provides application documentation for almost every program. This
documentation is translated into over 20 languages and is available from the
<a href="http://docs.kde.org">KDE Documentation</a> page.



<a href="http://userbase.kde.org">KDE Userbase</a> provides help for end users on how to use KDE and its applications.


## System Administrator Documentation


<a href="http://techbase.kde.org/SysAdmin">KDE for System Administrators</a> provides information for administrators who
are deploying KDE in their organisation.


## How to help with the documentation

If you want to contribute to the documentation project by writing new
documents or translating existing ones, please:


* Contact the <a href="http://l10n.kde.org/docs/index-script.php">Editorial Team</a> if you are interested in writing new documentation.
* Contact one of the existing <a href="http://l10n.kde.org/teams-list.php">translation teams</a> if you want to translate documentation to an existing language.
* Take a look at the practical instructions at the <a href="http://l10n.kde.org/">internationalization and documentation server</a>.

