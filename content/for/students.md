---
title: KDE For Students
description: Study with KDE
images:
- /for/students/thumbnail.png
sassFiles:
- /scss/for/students.scss
layout: students

fun_again: Make computers fun again
fun_again_description: KDE Software helps you study more efficiently, use your devices the way you want and have fun in the process.
plasma: Plasma is an Operating System that adapts to you
distractions: Get rid of distractions
distractions_text: Plasma lets you learn and work without interruption. It doesn't force unwanted apps on you, it doesn't show you ads or promotions, and it lets you update whenever you want (e.g. not before a big deadline).
customize: Customize your workspace
customize_text: You can customize your desktop with widgets, like a calculator; clock and calendar to keep track of time; clipboard manager; dictionary; weather report; application launcher, and more.
get_out: Get more out of your PC
get_out_text: If you have an older device, don't throw it away! Unlike other operating systems, Plasma is lightweight enough to offer a pleasant and snappy experience even on old hardware.
---

{{< section class="text-center text-small apps p-5" >}}

## KDE Apps for Students

### Powerful, multi-platform, and for everyone

Our no-nonsense apps exist to help you get things done more quickly--not to sell you subscriptions and useless products! From note-taking to complex statistical computing, you can rely on KDE's apps and tools for your studies and beyond.

[View all KDE apps](https://apps.kde.org)

{{< /section >}}

{{< section >}}

## Merkuro Calendar

Keep track of deadlines, manage your tasks and always stay on top of your schedule!

{{< for/app-links learn="https://apps.kde.org/merkuro.calendar" >}}

![Merkuro month view showing some events](https://cdn.kde.org/screenshots/merkuro/calendar-month.png)

{{< /section >}}

{{< section class="okular py-5" >}}

## Okular

### The Universal Document Viewer

Okular is a fast and multi-platform document reader that's packed with features! Okular allows you to read PDF documents and comics, fill out forms, sign documents, visualize markdown files, and much more!

{{< for/app-links learn="https://apps.kde.org/okular" >}}

![](https://cdn.kde.org/screenshots/okular/okular-annotation.png)

{{< /section >}}

{{< section >}}

## Francis

Francis uses the [pomodoro technique](https://en.wikipedia.org/wiki/Pomodoro_Technique) to help you better manage your time and be more productive by breaking study sessions into intervals separated by short breaks.

{{< for/app-links learn="https://apps.kde.org/francis" >}}

![](https://cdn.kde.org/screenshots/francis/main.png)

{{< /section >}}

{{< section class="text-center text-small apps py-5" >}}

## Marknote

Marknote is a simple and easy-to-use note-taking application that uses markdown files to capture your ideas.

{{< for/app-links learn="https://apps.kde.org/marknote" centered="true"  >}}

![](https://i.imgur.com/tJba9pK.png.png)

{{< /section >}}

{{< section >}}

## KWordQuiz

KWordQuiz is a word memorization app providing multiple learning modes: flashcards, "questions and answers", and multiple-choice questions.

{{< for/app-links learn="https://apps.kde.org/kwordquiz" >}}

![](https://cdn.kde.org/screenshots/kwordquiz/home.png)

{{< /section >}}

{{< section class="kile py-5" >}}

## Kile

Kile is a user-friendly TeX/LaTeX editor that compiles, converts and shows your document with one click. It's optimized for writing (La)Tex documents and auto-completes (La)TeX commands.

Navigation is also easy, as Kile constructs a list of all the chapters in your document and then lets you use the list to jump to the corresponding section.

You can also click in the viewer and jump to the corresponding LaTeX line in the editor, or jump from the editor to the corresponding page in the viewer.

{{< for/app-links learn="https://apps.kde.org/kile/" dark="true" >}}

![Screenshot of Kile](/for/resources/kile.png)

{{< /section >}}

{{< section class="mentor py-5" >}}

## Mentorship Program

KDE's mentorship programs give you the chance to acquire first-hand experience contributing to a free software project. In the process, you'll gain valuable insights into cutting-edge technologies and build your network of contacts in the IT world. Becoming a valued member of KDE is a great way to jump-start your career in the tech industry!

Since 2013, the KDE Student Program has been running
[Season of KDE](https://season.kde.org), a program similar to, but not quite
the same as Google Summer of Code. Season of KDE offers an opportunity to everyone (not
just students) to participate in both code and non-code projects that contribute to
the KDE ecosystem.

Organized by Google, [Google Summer of Code](https://summerofcode.withgoogle.com/)
is a global, online program focused on bringing new contributors into open
source software development. KDE has participated in Google Summer of Code for every year of its existence.

In the past few years, SoK and GSoC participants have not only contributed new
application features, but have also developed the KDE Continuous Integration
System, statistical reports for developers, a web framework, ported KDE
applications to other platforms, written technical documentation, and done much, much more.

{{< for/app-links learn="https://season.kde.org/" dark="true" >}}

![](konqis.png)

Some other open-source mentorship programs you might want to check out:

- [Outreachy Internship](https://www.outreachy.org/)
- [Linux Kernel Mentorship Program](https://wiki.linuxfoundation.org/lkmp)
- [Youth Hacking 4 Freedom](https://fsfe.org/activities/yh4f/index.en.html) from the FSFE
- [Rails Girls](https://railsgirlssummerofcode.org/)

{{< /section >}}
