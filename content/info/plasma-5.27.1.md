---
version: "5.27.1"
title: "KDE Plasma 5.27.1, feature Release"
type: info/plasma5
signer: Jonathan Esk-Riddell
signing_fingerprint: D7574483BB57B18D
draft: false
---

This is a feature release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.27.1 announcement](/announcements/plasma/5/5.27.1).
