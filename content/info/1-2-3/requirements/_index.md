---
title: KDE Compilation Requirements Listings
---

<ul>
  <li><a href="3.5">KDE 3.5</a></li>
  <li><a href="3.4">KDE 3.4</a></li>
  <li><a href="3.3">KDE 3.3</a></li>
  <li><a href="3.2">KDE 3.2</a></li>
  <li><a href="3.1">KDE 3.1</a></li>
  <li><a href="3.0">KDE 3.0</a></li>
</ul>
