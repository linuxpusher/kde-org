---
title: "KDE 3.5.3 Info Page"
---

<p>
KDE 3.5.3 was released on May 31st, 2006. Read the <a
href="/announcements/announce-3.5.3">official announcement</a>.</p>


<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<p>Patches for the issues mentioned below are available from
<a href="ftp://ftp.kde.org/pub/kde/security_patches">ftp://ftp.kde.org/pub/kde/security_patches</a>
unless stated otherwise.</p>

<ul>
<li>
KDM contains a symlink attack vulnerability that allows a normal
user to read files from other users including root.<br />
Read the <a href="/info/security/advisory-20060614-1.txt">detailed advisory</a>.
All versions of KDE starting with KDE 3.2.0 up to and including KDE 3.5.3
are affected.
</li>
<li>
kpdf contains a denial of service vulnerability in xpdf based code that
can cause the client to crash via a specially crafted pdf file.
<br />Read the <a href="/info/security/advisory-20070115-1.txt">detailed advisory</a>.
All versions of KDE up to and including KDE 3.5.5 are affected.
</li>
<li>Konqueror contains a vulnerability that allows a malicious web
site to spoof the address bar entry to a different one, possibly tricking
the user into believing that they actually visited a different site.
Read the <a href="/info/security/advisory-20070816-1.txt">detailed advisory</a>.
All versions of Konqueror as included with KDE up to including KDE 3.5.7 are
affected.</li>
<li>KDM can be tricked into allowing a passwordless login for logins
with password configured. Read the <a href="/info/security/advisory-20090919-1.txt">detailed advisory</a>.
Versions of KDM as included in KDE 3.3.0 up to including 3.5.7 are affected.</li>




</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release was packaged:</p>

<ul>
<li>None known yet</li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="https://userbase.kde.org/Asking_Questions">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a>.

<h2>Download and Installation</h2>

<p>
<u>Library Requirements</u>.
 <a href="/info/1-2-3/requirements/3.5">KDE 3.5 requires or benefits</a>
 from the given list of libraries, most of which should be already installed
 on your system or available from your OS CD or your vendor's website.
</p>
<p>
  The complete source code for KDE 3.5.3 is available for download:
</p>

{{< readfile "/content/info/1-2-3/source-3.5.3.inc" >}}

<!-- Comment the following if Konstruct is not up-to-date -->
<p>The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset can help you
downloading and installing these tarballs.</p>

<u><a name="binary">Binary packages</a></u>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.5.3 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.3/">http</a> or
  <a href="/mirrors/ftp.php">FTP mirrors</a>.
</p>

<p>
  Currently pre-compiled packages are available for:
</p>

{{< readfile "/content/info/1-2-3/binary-3.5.3.inc" >}}

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.5-api.php/">
programming interface of KDE 3.5</a>.
</p>
