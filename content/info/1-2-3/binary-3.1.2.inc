<ul>

<!-- ASP LINUX -->
<!--
<li><a href="http://www.asp-linux.com/">ASP Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/ASPLinux/README">README</a>)
  : 
  <ul type="disc">
    <li>
      7.3:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/ASPLinux/7.3/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- CONECTIVA LINUX -->
<!--
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Conectiva/README">README</a>
  /
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Conectiva/LEIAME">LEIAME</a>
  <ul type="disc">
    <li>
      8.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Conectiva/conectiva/RPMS.kde/">Intel
      i386</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--  DEBIAN -->
<li><a href="http://www.debian.org/">Debian</a>
  (<a href="http://download.kde.org/stable/3.1.2/Debian/README">README</a>) :
    <ul type="disc">
      <li>
         Debian stable (woody) (Intel i386, IBM PowerPC, DEC Alpha) : <tt>deb http://download.kde.org/stable/3.1.2/Debian stable main</tt>
      </li>
    </ul>
  <p />
</li>

<!--   FREEBSD -->
<li><a href="http://www.freebsd.org/">FreeBSD</a>
  (<a href="http://download.kde.org/stable/3.1.2/FreeBSD/README">README</a>)
  <p />
</li>

<!--   MAC OS X (FINK PROJECT) -->
<!--
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   MANDRAKE LINUX -->
<!--
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Mandrake/README">README</a>): 
  <ul type="disc">
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Mandrake/8.2/">Intel
      i586</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
 (<a href="http://download.kde.org/stable/3.1.2/RedHat/8.0/README">README</a>):
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/RedHat/9//noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/RedHat/9/i386/">Intel i386</a>
    </li>
<!--
     <li>
      8.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/RedHat/8.0/i386/">Intel i386</a>
    </li>
-->
  </ul>
  <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
       9.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/contrib/Slackware/9.0/">Intel i386</a>
     </li>
     <li>
       8.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/contrib/Slackware/8.1/">Intel i386</a>
     </li>
   </ul>
  <p />
</li>

<!-- SOLARIS -->
<!--
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      8.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/contrib/Solaris/8.0/x86/">Intel x86</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
<!--
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/README">README</a>,
   <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/SORRY">SORRY</a>)-->:
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/ix86/8.2/">Intel
      i586</a>
    </li>
    <li>
      8.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/ix86/8.1/">Intel
      i586</a>
    </li>
    <li>
      8.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/ix86/8.0/">Intel
      i386</a>
    </li>
    <li>
      7.3:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/ix86/7.3/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/ppc/7.3/">IBM
      PowerPC</a>
    </li>
    <li>
     7.2:
     <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/SuSE/ix86/7.2/">Intel
      i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- TURBO LINUX -->
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
    (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Turbo/Readme.txt">README</a>):
    <ul type="disc">
      <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       8.0: 
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Turbo/8/i586/">Intel i586</a>
      </li>
      <li>
       7.0:
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Turbo/7/i586/">Intel i586</a>
      </li>
    </ul>
  <p />
</li>

<!--   TRU64 -->
<!--
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/contrib/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/contrib/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   YELLOWDOG LINUX -->
<!--
  <a href="http://www.yellowdoglinux.com/">YellowDog</a>:
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/YellowDog">2.2</a>
-->

<!-- YOPER LINUX -->
<!--
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      1.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/Yoper/">Intel i686 tgz</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- VECTOR LINUX -->
<li>
  <a href="http://www.vectorlinux.com/">VECTORLINUX</a>:
  (<a href="http://download.kde.org/stable/3.1.2/contrib/VectorLinux/3.0/README">README</a>):
  <ul type="disc">
    <li>
      3.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/contrib/VectorLinux/3.2/">Intel i386</a>
    </li>
    <li>
      3.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.2/contrib/VectorLinux/3.0/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

</ul>
