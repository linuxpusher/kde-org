---
version: "5.19.3"
title: "KDE Plasma 5.19.3, Bugfix Release"
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and other essential software for your computer.  Details in the [Plasma 5.19.3 announcement](/announcements/plasma-5.19.3).
