KDE Project Security Advisory
=============================

Title:          kwallet-pam: Access to privileged files
Risk Rating:    High
CVE:            CVE-2018-10380
Versions:       Plasma < 5.12.6
Date:           4 May 2018


Overview
========
kwallet-pam was doing file writing and permission changing
as root that with correct timing and use of carefully
crafted symbolic links could allow a non privileged user
to become the owner of any file on the system.

Workaround
==========
None (other than not using kwallet-pam)

Solution
========
Update to Plasma >= 5.12.6 or Plasma >= 5.13.0

Or apply the following patches:
Plasma 5.12
    https://commits.kde.org/kwallet-pam/2134dec85ce19d6378d03cddfae9e5e464cb24c0
    https://commits.kde.org/kwallet-pam/01d4143fda5bddb6dca37b23304dc239a5fb38b5

Plasma 5.8
    https://commits.kde.org/kwallet-pam/99abc7fde21f40cc6da5feb6ee766cc46fcca1f8
    https://commits.kde.org/kwallet-pam/802f305d81f8771c4f4a8bd7fd0e368ffc6f9b3b


Credits
=======
Thanks to Fabian Vogt for the report and to Albert Astals Cid for the fix.
