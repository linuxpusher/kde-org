KDE Project Security Advisory
=============================

Title:           Ark: maliciously crafted archive can install files outside the extraction directory.
Risk Rating:     Important
CVE:             CVE-2020-16116
Versions:        ark <= 20.04.3
Author:          Elvis Angelaccio <elvis.angelaccio@kde.org>
Date:            30 July 2020

Overview
========

A maliciously crafted archive with "../" in the file paths
would install files anywhere in the user's home directory upon extraction.

Proof of concept
================

For testing, an example of malicious archive can be found at
https://github.com/jwilk/traversal-archives/releases/download/0/relative2.zip

Impact
======

Users can unwillingly install files like a modified .bashrc, or a malicious
script placed in ~/.config/autostart

Workaround
==========

Users should not use the 'Extract' context menu from the Dolphin file manager.
Before extracting a downloaded archive using the Ark GUI, users should inspect it
to make sure it doesn't contain entries with "../" in the file path.

Solution
========

Ark 20.08.0 prevents loading of malicious archives and shows a warning message
to the users.

Alternatively,
https://invent.kde.org/utilities/ark/-/commit/0df592524fed305d6fbe74ddf8a196bc9ffdb92f
can be applied to previous releases.

Credits
=======

Thanks to Dominik Penner for finding and reporting this issue and thanks to
Elvis Angelaccio and Albert Astals Cid for fixing it.
