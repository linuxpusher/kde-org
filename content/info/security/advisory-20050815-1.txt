-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: langen2kvtml tempfile vulnerability
Original Release Date: 2008-08-15
URL: http://www.kde.org/info/security/advisory-20050815-1.txt

0. References

        CAN-2005-2101

1. Systems affected:

        All KDE releases starting from KDE 3.0 up to including
        KDE 3.4.2.


2. Overview:

        Ben Burton notified the KDE security team about several
        tempfile handling related vulnerabilities in langen2kvtml,
        a conversion script for kvoctrain. This vulnerability was
        initially discovered by Javier Fernández-Sanguino Peña.

        The script uses known filenames in /tmp which allow an
        local attacker to overwrite files writeable by the
        user (manually) invoking the conversion script.

3. Impact:

        A local file can overwrite files and possibly elevate
        privileges.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.4.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        0e82c5810df3b04370188ba13cc50203  post-3.4.2-kdeedu.diff


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFC/+ZevsXr+iuy1UoRAh0PAJ9Lun/gca6T+oY5LPmJRDa7vOY41wCeNJY5
D2fO/2ZNBXZzwiCDJLBnIBM=
=uz8a
-----END PGP SIGNATURE-----
