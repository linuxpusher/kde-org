
KDE Security Advisory: KDE ioslave PASV port scanning vulnerability
Original Release Date: 2007-03-26
URL: http://www.kde.org/info/security/advisory-20070326-1.txt

0. References
        CVE-2007-1564


1. Systems affected:

        KDE up to including KDE version 3.5.6.


2. Overview:

        The KDE FTP ioslave parses the host address in the PASV response
        of a FTP server response. mark from bindshell.net pointed
        out that this could be exploited via JavaScript for automated
        port scanning. It was not possible to demonstrate the
        vulnerability via JavaScript with Konqueror from KDE 3.5.x.
        However, other scenarios are possible.


3. Impact:

        Untrusted sites or sites that allow Javascript injection
        could cause Konqueror or other web browsers based on KHTML
        to perform port scanning.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.5.x and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        62872147c2d369feb3d9077e9b32b03d  CVE-2007-1564-kdelibs-3.5.6.diff

        Patch for KDE 3.4.x and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        13535c902a6b3223005adfc1fccdd32f  CVE-2007-1564-kdelibs-3.4.3.diff


