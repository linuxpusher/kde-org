---
title: "KDE SC 4.14.0 Info Page"
---

<p>
For new features, bugfixes and changes read the <a href="/announcements/4.14">KDE SC 4.14.0 announcement</a>.
</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release was packaged:</p>

<ul>
    <li>None</li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>Download and Installation</h2>

<p>
 <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">Build instructions</a>
 are available on Techbase, our developer wiki.
</p>

<h3><a name='desktop'>KDE SC 4.14.0 Release</a></h3>

<p>KDE Software Compilation releases are made up of the KDE Platform,
Plasma Workspaces and many KDE Applications.</p>

<h4><a name="binary">Binary packages</a></h4>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE SC 4.14.0 for some versions of their distribution.
  You can find a list at <a href="http://community.kde.org/KDE_SC/Binary_Packages">http://community.kde.org/KDE_SC/Binary_Packages</a>
</p>

<h4><a name="source">Source Code</a></h4>
<p>
  The complete source code for KDE SC 4.14.0 is available for download:
</p>

{{< readfile "/content/info/4/source-4.14.0.inc" >}}
