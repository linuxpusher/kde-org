<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdeaccessibility-4.3.5.tar.bz2">kdeaccessibility-4.3.5</a></td><td align="right">5,6MB</td><td><tt>64fe8cf8aa3b51bbf01665db13135ba7114c59f1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdeadmin-4.3.5.tar.bz2">kdeadmin-4.3.5</a></td><td align="right">1,8MB</td><td><tt>241c790a193bea14d155f93c5377295dfc74bf2f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdeartwork-4.3.5.tar.bz2">kdeartwork-4.3.5</a></td><td align="right">63MB</td><td><tt>84c7a680a2d2c9cc6250c8d68fec03a34a9aafc1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdebase-4.3.5.tar.bz2">kdebase-4.3.5</a></td><td align="right">4,0MB</td><td><tt>cb003415dbe7f511d869170c7f497f05d9814081</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdebase-runtime-4.3.5.tar.bz2">kdebase-runtime-4.3.5</a></td><td align="right">7,0MB</td><td><tt>4f6c1c3641837bf08b9ba9afc361a363b2da557c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdebase-workspace-4.3.5.tar.bz2">kdebase-workspace-4.3.5</a></td><td align="right">60MB</td><td><tt>703582cb8a3471c5821986dbdfc3c6deedac0ff3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdebindings-4.3.5.tar.bz2">kdebindings-4.3.5</a></td><td align="right">4,7MB</td><td><tt>0273013e4b74a7b1bc4cfc8375c80b5c19cf33e6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdeedu-4.3.5.tar.bz2">kdeedu-4.3.5</a></td><td align="right">56MB</td><td><tt>fc3485dedb501f0befb3fa77d206a3416d094539</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdegames-4.3.5.tar.bz2">kdegames-4.3.5</a></td><td align="right">49MB</td><td><tt>2cff3a7f428798ead8b1fb3ecf2da5cb9296b5ef</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdegraphics-4.3.5.tar.bz2">kdegraphics-4.3.5</a></td><td align="right">3,5MB</td><td><tt>f8cb6afe5df3e548dafc0d0757bdc635675f85e8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdelibs-4.3.5.tar.bz2">kdelibs-4.3.5</a></td><td align="right">11MB</td><td><tt>35dcbc06b468451cb4e839aa5771b4e6bc512188</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdelibs-experimental-4.3.5.tar.bz2">kdelibs-experimental-4.3.5</a></td><td align="right">28KB</td><td><tt>05936c4d3e0df7f135e9a1a4f430c2e9555d9b6e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdemultimedia-4.3.5.tar.bz2">kdemultimedia-4.3.5</a></td><td align="right">1,6MB</td><td><tt>fd215c529d76cf7f998539a9a6a4476f2bd885fc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdenetwork-4.3.5.tar.bz2">kdenetwork-4.3.5</a></td><td align="right">7,1MB</td><td><tt>b94ad996624e170c3a3c3d3a82dd54ca7f2d7910</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdepim-4.3.5.tar.bz2">kdepim-4.3.5</a></td><td align="right">11MB</td><td><tt>0c319929af1828f0ee15c47a957f0a3c95556742</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdepimlibs-4.3.5.tar.bz2">kdepimlibs-4.3.5</a></td><td align="right">1,7MB</td><td><tt>441b10fc0c89a91c447d73a68e3dc877156c5f67</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdepim-runtime-4.3.5.tar.bz2">kdepim-runtime-4.3.5</a></td><td align="right">732KB</td><td><tt>01e794873cbbbf5546b8b71f83786e51c0f7ffa5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdeplasma-addons-4.3.5.tar.bz2">kdeplasma-addons-4.3.5</a></td><td align="right">1,4MB</td><td><tt>b128bbb567bd1bba7be6e29e09869f1fc094ad97</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdesdk-4.3.5.tar.bz2">kdesdk-4.3.5</a></td><td align="right">5,4MB</td><td><tt>663acbdfc7263cd9c6c6098445d8afe043d36514</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdetoys-4.3.5.tar.bz2">kdetoys-4.3.5</a></td><td align="right">1,3MB</td><td><tt>75430ec4937ed20ab6483dce61fc52f00d7e747d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdeutils-4.3.5.tar.bz2">kdeutils-4.3.5</a></td><td align="right">2,5MB</td><td><tt>2ba77e8c81255385e779f10e67bc6e612e50b613</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/kdewebdev-4.3.5.tar.bz2">kdewebdev-4.3.5</a></td><td align="right">2,5MB</td><td><tt>5c1de844cc9a308beee000764b1604bb184eb6dc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.5/src/oxygen-icons-4.3.5.tar.bz2">oxygen-icons-4.3.5</a></td><td align="right">119MB</td><td><tt>9cc70eeb8e6796bab36b6222ada7248a38979e9e</tt></td></tr>
</table>
