<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdeaccessibility-4.6.4.tar.bz2">kdeaccessibility-4.6.4</a></td><td align="right">5,0MB</td><td><tt>5a8ede86e6a483379ff62c1d85445c93c8400857</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdeadmin-4.6.4.tar.bz2">kdeadmin-4.6.4</a></td><td align="right">776KB</td><td><tt>5f683dbdeec75b4baa025a987692a63912b7e4af</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdeartwork-4.6.4.tar.bz2">kdeartwork-4.6.4</a></td><td align="right">112MB</td><td><tt>460b8bff5f6799f0e2553e1b78112e3192ade2b3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdebase-4.6.4.tar.bz2">kdebase-4.6.4</a></td><td align="right">2,6MB</td><td><tt>aef0d3f2ed8cc54cbe1ae6025091c9e0a43235c4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdebase-runtime-4.6.4.tar.bz2">kdebase-runtime-4.6.4</a></td><td align="right">5,6MB</td><td><tt>63f30be47a4b592a0820ed200004543e7cba956d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdebase-workspace-4.6.4.tar.bz2">kdebase-workspace-4.6.4</a></td><td align="right">67MB</td><td><tt>44aef9ad37cf291e9e7528b2c915f8fc129d735e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdebindings-4.6.4.tar.bz2">kdebindings-4.6.4</a></td><td align="right">6,8MB</td><td><tt>ee5d85598e278bda76f7937c0a832723785ffe49</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdeedu-4.6.4.tar.bz2">kdeedu-4.6.4</a></td><td align="right">69MB</td><td><tt>c58c2afbedccfa12d25e47202da995940cc9acb4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdegames-4.6.4.tar.bz2">kdegames-4.6.4</a></td><td align="right">57MB</td><td><tt>a5f6d07d8149fe00dbb61ae667eea741887966f3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdegraphics-4.6.4.tar.bz2">kdegraphics-4.6.4</a></td><td align="right">5,0MB</td><td><tt>a208cef52de6345996a7cc68b1c09944da7253aa</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdelibs-4.6.4.tar.bz2">kdelibs-4.6.4</a></td><td align="right">13MB</td><td><tt>088778bdda8b8c7a7b192abbf0d2a0660a20626d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdemultimedia-4.6.4.tar.bz2">kdemultimedia-4.6.4</a></td><td align="right">1,6MB</td><td><tt>22178350b72e142b9f5e65a4bb85027f82b14988</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdenetwork-4.6.4.tar.bz2">kdenetwork-4.6.4</a></td><td align="right">8,3MB</td><td><tt>63bb58a457aed790e109a9dbeab73abb8c7ea1b2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdepimlibs-4.6.4.tar.bz2">kdepimlibs-4.6.4</a></td><td align="right">3,1MB</td><td><tt>b7e48da0a3fa9c4d435ed2224909a5441450334f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdeplasma-addons-4.6.4.tar.bz2">kdeplasma-addons-4.6.4</a></td><td align="right">1,9MB</td><td><tt>8d107bf8cc6b6b9d66e9178b5f18bdc1751a957f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdesdk-4.6.4.tar.bz2">kdesdk-4.6.4</a></td><td align="right">5,8MB</td><td><tt>6ed75cff6808aabaf3d9c3dc8601960717707c6b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdetoys-4.6.4.tar.bz2">kdetoys-4.6.4</a></td><td align="right">396KB</td><td><tt>12a0228010a1720649807cb8efe71f572bd24af3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdeutils-4.6.4.tar.bz2">kdeutils-4.6.4</a></td><td align="right">3,6MB</td><td><tt>787653b60ce17e0e5900f9ec30f198b8d7bf1edf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/kdewebdev-4.6.4.tar.bz2">kdewebdev-4.6.4</a></td><td align="right">2,2MB</td><td><tt>ef6b5142afd5b97ad8c1bffe0557e25477371344</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.4/src/oxygen-icons-4.6.4.tar.bz2">oxygen-icons-4.6.4</a></td><td align="right">354MB</td><td><tt>a1b2a3a8ca30dc0d5bdba408e852b1a80c11e235</tt></td></tr>
</table>
