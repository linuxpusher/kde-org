---
title: "KDE Applications 14.12.2 Info Page"
announcement: /announcements/announce-applications-14.12.2
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
