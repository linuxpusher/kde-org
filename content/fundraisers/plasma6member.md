---
title: Support Plasma 6
layout: plasma6member
jsFiles:
- /js/plasma6member.js
scssFiles:
- /scss/plasma6member.scss
more: "If you'd prefer a one-time donation, [click here](https://kde.org/community/donations/)."
---

## Spark Innovation with Your Donation

Exciting news on the horizon! In February 2024, **Plasma 6** is set to make its grand debut. But we need your help to ensure its successful launch.

<div id="countdown" class="h1"></div>

**LEFT UNTIL LAUNCH!**<sup>*</sup>

<sup>*</sup> More or less

![](/fundraisers/plasma6member/plasma6.png)

## Why Donate

In short: because generosity is at the heart of KDE and keeps  our projects alive.

It's what motivates our volunteers to share their time and knowledge to keep building new features and keeping KDE software up-to-date and secure.

Generosity also drives our supporters. Their contributions and sponsorships keep KDE's gears turning and ensure that our developers can continue their fantastic work.

### How to Become a Supporter

Fill out the form above and **for less than €10 a month** you too can support the champions who tirelessly improve our software every day. The KDE development ecosystem is a bustling hive of activity, and every supporting member helps keep it buzzing.

If you'd prefer a one-time donation, [please click here](https://kde.org/community/donations/).

### Perks

We know that keeping KDE healthy and running is all the reward you need for your generosity, but, as a token of our appreciation for your support, we are also offering:

- Your name will shine bright on our donation page, acknowledging your contribution.
- Your name will be  displayed in Plasma 6 itself!
- More perks coming soon!

If you don't want any of the above, that is fine too, of course. Remember to mark the **"[✔️] Make donation anonymous"** checkbox in the donation process above.

## How We Use the Money

This where your donation will make a difference:

* **Sprints for Developers**: You will help finance the in-person meetups that keep our developers energized and focused on making KDE even better.
* **Travel Costs to Events**: You will support our team's presence at important gatherings and conferences, like FOSDEM, FOSSAsia and LinuxCons.
* **Akademy Event**: You will ensure the success of KDE's yearly community event for all members, and foster collaboration and growth.
* **Running KDE**: You will keep the lights on at KDE HQ and our digital home running smoothly.
* **Paying Support Staff**: You will ensure KDE has on hand the experts we need to assist our contributors and users.

## Our Goal

Our aim is to sign on **500 new supporters** within the next 4 months **for before Plasma 6 launches in February**. With your support, we can achieve this milestone and ensure the continued success of KDE and the launch of Plasma 6.

Join us in making Plasma 6's launch a spectacular success. Contribute to our fundraiser. Your donation will fuel innovation, support dedicated developers, and ensure that KDE continues to shine brightly in the world of free open-source software. Together, we can achieve great things!
