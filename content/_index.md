---
images:
- /thumbnail.png

highlight_apps:
- name: gcompris
  home: https://gcompris.net
  screen: https://cdn.kde.org/screenshots/gcompris/gcompris.png
  screenShadow: true

- name: labplot2
  home: https://labplot.kde.org/
  screen: https://cdn.kde.org/screenshots/labplot2/labplot.png
  screenShadow: true

- name: krita
  home: https://krita.org
  screen: https://krita.org/wp-content/uploads/2019/08/krita-ui-40.png
  screenShadow: true

- name: kdenlive
  home: https://kdenlive.org
  screen: https://cdn.kde.org/screenshots/kdenlive/k1.png
  screenShadow: true

- name: kontact
  home: https://kontact.kde.org
  screen: https://cdn.kde.org/screenshots/kmail/kmail.png
  screenShadow: false

- name: kdevelop
  home: https://kdevelop.org
  screen: https://cdn.kde.org/screenshots/kdevelop/kdevelop.png
  screenShadow: false
---
